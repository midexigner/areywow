<?php 
require_once 'admin/Common.php'; 
require_once 'functions.php'; 
require_once 'include/head.php'; 
 if(!$_SESSION['LoginCustomer'])
 {
            	redirect(BASE_URL);
}
 ?>
<?php $CatID=99999; ?>
<?php
	$email="";
	$password="";
	$msg1 = "";
	$msg2 = "";
	$msg3 = "";
	if(isset($_POST["action"]) && $_POST["action"] == "submit_form")
	{
		if(isset($_POST["email"]))
			$email=trim($_POST["email"]);
		if(isset($_POST["password"]))
			$password=trim($_POST["password"]);
			
		if ($email=="")
			$msg1 = '<p style="color:red">Please Enter EmailAddress.</p>';
		if ($password=="")
			$msg2 = '<p style="color:red">Please Enter Password.</p>';
			
		if($msg1=='' && $msg2=='')
		{	
			$query="SELECT ID,FirstName,LastName,Password,Country FROM website_users WHERE Status = 1 AND Email='".dbinput($email)."'";
			$result = mysql_query ($query) or die(mysql_error()); 
			$num = mysql_num_rows($result);
			
			
			if($num==0)
			{
				$_SESSION["LoginCustomer"]=false;
				$_SESSION["CustomerID"]='';
				$_SESSION["CustomerFullName"]='';
				$_SESSION["Country"]='';
				$_SESSION["Shipping"]='';
				$msg3 = '<p style="color:red">Invalid Email/Password.</p>';	
			}
			else
			{
				$row = mysql_fetch_array($result,MYSQL_ASSOC);
				if(dboutput($row["Password"]) == $password)
				{
					$_SESSION["LoginCustomer"]=true;
					$_SESSION["CustomerID"]=dboutput($row["ID"]);
					$_SESSION["CustomerFullName"]=dboutput($row["FirstName"]) .' '. dboutput($row["LastName"]);
					$_SESSION["Country"]=dboutput($row["Country"]);
					
					$query2="SELECT Percentage FROM shipping_rates WHERE CountryCode='".$row["Country"]."'";
					$result2 = mysql_query ($query2) or die(mysql_error()); 
					$num2 = mysql_num_rows($result2);
					if($num2 > 0)
					{
						$row2 = mysql_fetch_array($result2,MYSQL_ASSOC);
						$_SESSION["Shipping"]=dboutput($row2["Percentage"]);
					}
					else
					{
						$query3="SELECT Percentage FROM other_countries_shipping WHERE ID=1";
						$result3 = mysql_query ($query3) or die(mysql_error()); 
						$num3 = mysql_num_rows($result3);
						if($num3 == 1)
						{
							$row3 = mysql_fetch_array($result3,MYSQL_ASSOC);
							$_SESSION["Shipping"]=dboutput($row3["Percentage"]);
						}
					}
					
					
					redirect("checkout.php");
				}
				else
				{
					$_SESSION["LoginCustomer"]=false;
					$_SESSION["CustomerID"]='';
					$_SESSION["CustomerFullName"]='';
					$_SESSION["Country"]='';
					$_SESSION["Shipping"]='';
					$msg3 = '<p style="color:red">Invalid Email/Password.</p>';
				}
			}
		}
	}
?>


	<?php require_once 'include/headerpartial.php';  ?>
	<!--//header-->

	

  <div tabindex="-1" class="site-content" id="content">
    <div class="container">

        <nav class="woocommerce-breadcrumb"><a href="home.html">Home</a><span class="delimiter"><i class="fa fa-angle-right"></i></span>Wishlist</nav>
        <div class="content-area" id="primary">
            <main class="site-main" id="main">
                <article class="page type-page status-publish hentry">
                    <div itemprop="mainContentOfPage" class="entry-content">
                        <div id="yith-wcwl-messages"></div>
                        <form class="woocommerce" method="post" id="yith-wcwl-form">

                            <!-- WISHLIST TABLE -->
                            <table data-token="" data-id="" data-page="1" data-per-page="5" data-pagination="no" class="shop_table cart wishlist_table">

                                <thead>
                                    <tr>

                                        <th class="product-remove"></th>

                                        <th class="product-thumbnail"></th>

                                        <th class="product-name">
                                            <span class="nobr">Product Name</span>
                                        </th>

                                        <th class="product-stock-stauts">
                                            <span class="nobr">Product Description</span>
                                        </th>

                                        <th class="product-price">
                                            <span class="nobr">Unit Price</span>
                                        </th>
                                        

                                        <th class="product-add-to-cart"></th>

                                    </tr>
                                </thead>

                                <tbody>

                                	<?php
    $resource = mysql_query("SELECT p.ID, p.ProductName, p.Discount, p.Price, p.URL, p.Overview, p.Image, p.Description, w.DateAdded FROM wishlist w JOIN products p ON p.ID=w.ProductID WHERE w.UserID=".$_SESSION["CustomerID"]) or die(mysql_error());
	if(mysql_num_rows($resource) > 0)
	{
		while($row = mysql_fetch_array($resource))
		{
?>
									<tr>
										<td class="product-remove">
                                            <div>
                                                <a title="Remove this product" class="remove remove_from_wishlist" href="update_wishlist.php?Delete=true&id=<?php echo $row["ID"]; ?>&url=<?php echo $_SERVER["REQUEST_URI"]; ?>">×</a>
                                            </div>
                                        </td>

                                        <td class="product-thumbnail">
                                            <a href="<?php echo BASE_URL.'/prod/'.$row["URL"]; ?>">
									<?php $i=0; foreach(explode(',',$row["Image"]) as $Img)
														{ 
														?>
														<?php echo ($i==0 ? '<img class="wp-post-image" width="180" height="180" src="admin/'.DIR_PRODUCTS_IMAGES.$Img.'" alt="">' : ''); $i++; ?>
														<?php
														}
														?>
                                        </a>
                                        </td>
										
										<td>
											<a href="<?php echo BASE_URL.'/prod/'.$row["URL"]; ?>"><?php echo $row["ProductName"]; ?></a>
										</td>
										<td>
											<span class="more"><?php echo $row["Description"]; ?></span>
										</td>
										<td class="text-center">
											<div class="price-box">
												<span class="special-price"><?php
												if($row["Discount"] != 0)
												{
													$persentage = ($row["Discount"] / $row["Price"]) * 100;
													$persentage = 100 - $persentage;
													$persentage = round($persentage);
												}
												echo CURRENCY_SYMBOL.($row["Discount"] != 0 ? "<small><s>".$row["Price"]."</small></s><br/><h3>".$row["Discount"]."</h3><br/>&emsp;(".$persentage."% Off)" : "<h3>".$row["Price"]."</h3>"); ?></span>
											</div>
											
										</td>
										<td>

<!-- <a class="btn btn-primary" href="<?php echo $row["URL"]; ?>">View Product</a> -->
 <!-- Add to cart button -->
                                            <a href="<?php echo BASE_URL?>/add_to_cart.php?id=<?php echo $row["ID"]; ?>&name=<?php echo add_to_cart_desh($row["ProductName"]); ?>&quantity=1&upsell=2&options=0&url=<?php echo $_SERVER['REQUEST_URI']; ?>" class="button"> Add to Cart</a>
                                            <!-- Change wishlist -->
</td>
									</tr>
<?php
		}
	}
	else
	{
?>
									<tr>
										<td colspan="5"><center><h3>No product in your wishlist</h3></center></td>
									</tr>
<?php
	}
?>
                                    
                                   
                                </tbody>

                                <tfoot>
                                    <tr>
                                        <td colspan="6"></td>
                                    </tr>
                                </tfoot>

                            </table>

                            <input type="hidden" value="85fe311a9d" name="yith_wcwl_edit_wishlist" id="yith_wcwl_edit_wishlist"><input type="hidden" value="/electro/wishlist/" name="_wp_http_referer">

                        </form>

                    </div><!-- .entry-content -->

                </article><!-- #post-## -->

            </main><!-- #main -->
        </div><!-- #primary -->
    </div><!-- .col-full -->
</div>



	<!--footer-->
 <?php require_once 'include/footer.php'; ?>
 <?php require_once 'include/foot.php'; ?>
	