<?php 
require_once 'admin/Common.php'; 
require_once 'functions.php'; 
$Keyword="";
$Description="";
$CatID=99999;
$ID=0;
$CategID=0;

if(isset($_REQUEST["keyword"]))
{
        $Keyword=trim($_REQUEST["keyword"]);
}
if(isset($_REQUEST["product_cat"]))
{
        $CategID=trim($_REQUEST["product_cat"]);
}
if($Keyword == '' && $CategID == '')
{
        redirect(BASE_URL."/404.php");
}


require_once 'include/head.php'; 
require_once 'include/headerpartial.php'; 
?>

    <div id="content" class="site-content" tabindex="-1">
        <div class="container">

            <nav class="woocommerce-breadcrumb" ><a href="<?php echo BASE_URL; ?>">Home</a><span class="delimiter"><i class="fa fa-angle-right"></i></span>Search</nav>

            <div id="primary" class="content-area">
                <main id="main" class="site-main">
 <header class="page-header">
                        <h1 class="page-title">Search</h1>
<!--                        <p class="woocommerce-result-count">Showing 1&ndash;15 of 20 results</p>-->
                    </header>

                    <div class="shop-control-bar">
                        <ul class="shop-view-switcher nav nav-tabs" role="tablist">
                            <li class="nav-item"><a class="nav-link active" data-toggle="tab" title="Grid View" href="#grid"><i class="fa fa-th"></i></a></li>
                            <li class="nav-item"><a class="nav-link " data-toggle="tab" title="Grid Extended View" href="#grid-extended"><i class="fa fa-align-justify"></i></a></li>
                            <li class="nav-item"><a class="nav-link " data-toggle="tab" title="List View" href="#list-view"><i class="fa fa-list"></i></a></li>
                            
                        </ul>
<!--                        <form class="woocommerce-ordering" method="get">-->
<!--                            <select name="orderby" class="orderby">-->
<!--                                <option value="menu_order"  selected='selected'>Default sorting</option>-->
<!--                                <option value="popularity" >Sort by popularity</option>-->
<!--                                <option value="rating" >Sort by average rating</option>-->
<!--                                <option value="date" >Sort by newness</option>-->
<!--                                <option value="price" >Sort by price: low to high</option>-->
<!--                                <option value="price-desc" >Sort by price: high to low</option>-->
<!--                            </select>-->
<!--                        </form>-->

                      
                    </div>

                    <div class="tab-content">

                        <div role="tabpanel" class="tab-pane active" id="grid" aria-expanded="true">



                            <ul class="products columns-3">

<?php
// $per_page = 2;
$query="SELECT ID,ProductName,Image,Price,Discount,URL FROM products WHERE ProductName LIKE '%".$Keyword."%' AND ".($CategID != 0 ? "find_in_set(Categories, '".(int)$CategID."') AND " : '')."Status = 1";

$query2 = "SELECT ID FROM brands WHERE Status = 1 ORDER BY BrandName";
$result2 = mysql_query( $query2 )or die( mysql_error() );
$i2 = 1;
while ( $row2 = mysql_fetch_array( $result2, MYSQL_ASSOC ) ) {

    if ( isset( $_REQUEST[ 'brand' . $i2 . '' ] ) ) {
        $query .= ' AND Manufacture =' . $_REQUEST[ 'brand' . $i2 . '' ];
    };

    $i2++;
}


$query3 = "SELECT ID FROM p_options_values WHERE Status = 1 AND OptionID = 1 ORDER BY Sort";
$result3 = mysql_query( $query3 )or die( mysql_error() );
$i3 = 1;
while ( $row3 = mysql_fetch_array( $result3, MYSQL_ASSOC ) ) {

    if ( isset( $_REQUEST[ 'size' . $i3 . '' ] ) ) {
        $query .= ' AND FIND_IN_SET (' . $_REQUEST[ 'size' . $i3 . '' ] . ',Options)';
    };

    $i3++;
}


$query4 = "SELECT ID FROM p_options_values WHERE Status = 1 AND OptionID = 2 ORDER BY Sort";
$result4 = mysql_query( $query4 )or die( mysql_error() );
$i4 = 1;
$jj= 0;
$ii = mysql_num_rows($result2);;
while ( $row4 = mysql_fetch_array( $result4, MYSQL_ASSOC ) ) {

    if ( isset( $_REQUEST[ 'color' . $i4 . '' ] ) ) {
        $query .= ' AND FIND_IN_SET (' . $_REQUEST[ 'color' . $i4 . '' ] . ',Options)';

    };

    $i4++;
}


if ( isset( $_REQUEST[ 'Price' ] ) ) {
    $PriceFilter = explode( '-', $_REQUEST[ 'Price' ] );
    //$query .= ' AND (Price BETWEEN '.$PriceFilter[0].' AND '.$PriceFilter[1].')';
} else {
    //$query .= ' AND (Price BETWEEN 0 AND '.$maxx['MaxPrice'].')';
}


if ( isset( $_REQUEST[ 'sort' ] ) && $_REQUEST[ 'sort' ] == 0 ) {
    $query .= ' ORDER BY ID DESC';
}
if ( !isset( $_REQUEST[ 'sort' ] ) ) {
    $query .= ' ORDER BY ID DESC';
}
if ( isset( $_REQUEST[ 'sort' ] ) && $_REQUEST[ 'sort' ] == 1 ) {
    $query .= ' ORDER BY ProductName ASC';
}
if ( isset( $_REQUEST[ 'sort' ] ) && $_REQUEST[ 'sort' ] == 2 ) {
    $query .= ' ORDER BY ProductName DESC';
}
if ( isset( $_REQUEST[ 'sort' ] ) && $_REQUEST[ 'sort' ] == 3 )

{
    $query .= ' ORDER BY Price ASC';
}
if ( isset( $_REQUEST[ 'sort' ] ) && $_REQUEST[ 'sort' ] == 4 ) {
    $query .= ' ORDER BY Price DESC';
}
$query .= ' LIMIT 12';
// echo $query;
// exit();
$result = mysql_query( $query )or die( mysql_error() );
$total_results = mysql_num_rows( $result );
$per_page = $total_results;
@$total_pages = ceil( $total_results / $per_page ); //total pages we going to have
//-------------if page is setcheck------------------//
if ( isset( $_REQUEST[ 'page' ] ) ) {
    $show_page = $_REQUEST[ 'page' ]; //it will telles the current page
    if ( $show_page > 0 && $show_page <= $total_pages ) {
        $start = ( $show_page - 1 ) * $per_page;
        $end = $start + $per_page;
    } else {
        // error - show first set of results
        $start = 0;
        $end = $per_page;
    }
} else {
    // if page isn't set, show first set of results
    $show_page = 1;
    $start = 0;
    $end = $per_page;
}

if ( isset( $_REQUEST[ 'page' ] ) ) {
    $currentpage = $_REQUEST[ 'page' ]; //it will telles the current page
} else {
    // if page isn't set, show first set of results
    $currentpage = 1;
}

// display pagination
$page = intval( $currentpage );

$tpages = $total_pages;
if ( $page <= 0 )
    $page = 1;
if ( $total_results == 0 ) {
    echo '<h1>Product Not Found</h1>';
}
for ( $i = $start; $i < $end; $i++ ) {
    if ( $i == $total_results ) {
        break;
    }
    $Image = explode( ',', mysql_result( $result, $i, 'Image' ) );
    $img1 = $Image[ 0 ];
    $img2 = ( isset( $Image[ 1 ] ) ? $Image[ 1 ] : '' );
    if ( mysql_result( $result, $i, 'Discount' ) != 0 ) {
        $persentage = ( mysql_result( $result, $i, 'Discount' ) / mysql_result( $result, $i, 'Price' ) ) * 100;
        $persentage = 100 - $persentage;
        $persentage = round( $persentage );
    }
    $ProductIDTemp = mysql_result( $result, $i, 'ID' );
    $ProductNameTemp = mysql_result( $result, $i, 'ProductName' );
    $URLTemp = mysql_result( $result, $i, 'URL' );
    $PriceTemp = mysql_result( $result, $i, 'Price' );
    $DiscountTemp = mysql_result( $result, $i, 'Discount' );
    ?>
                                <li class="product <?php echo ($ii==0) ? 'first':''; ?> <?php echo ($jj==3) ? 'last':''; ?>">
                                    <div class="product-outer">
                                        <div class="product-inner">
<!--                                            <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>-->
                                            <a href="<?php echo BASE_URL.'/prod/'.$URLTemp; ?>">
                                                <h3><?php echo strlen($ProductNameTemp) > 20 ? substr($ProductNameTemp,0,20)."..." : $ProductNameTemp; ?></h3>
                                                <div class="product-thumbnail">

                                                    <img data-echo="<?php echo BASE_URL.'/admin/'.DIR_PRODUCTS_IMAGES.$img1 ?>" src="<?php echo BASE_URL;?>/assets/images/blank.gif" alt="">

                                                </div>
                                            </a>

                                            <div class="price-add-to-cart">
                                                <?php echo ($DiscountTemp != 0 ? '<span class="price"><span class="electro-price"><ins><span class="amount">'.CURRENCY_SYMBOL.$DiscountTemp.'</span></ins> <del><span class="amount">'.CURRENCY_SYMBOL.$PriceTemp.'</span></del></span></span>' : '<span class="price"><span class="electro-price"><ins><span class="amount">'.CURRENCY_SYMBOL.$PriceTemp.'</span></ins></span></span>
'); ?>
                   
                                                <a rel="nofollow" href="<?=BASE_URL?>/add_to_cart.php?id=<?php echo $ProductIDTemp; ?>&name=<?php echo add_to_cart_desh($ProductNameTemp); ?>&quantity=1&upsell=2&options=0&url=<?php echo $_SERVER['REQUEST_URI']; ?>" class="button add_to_cart_button">Add to cart</a>
                                            </div><!-- /.price-add-to-cart -->

                                            <div class="hover-area">
                                                <div class="action-buttons">

                                                    <a href="<?php echo BASE_URL; ?>/add_to_wishlist.php?id=<?php echo $ProductIDTemp; ?>&name=<?php echo $ProductNameTemp; ?>&quantity=1&upsell=2&options=0&url=<?php echo $_SERVER['REQUEST_URI']; ?>" rel="nofollow" class="add_to_wishlist">
                                                        Wishlist</a>

<!--                                                    <a href="#" class="add-to-compare-link">Compare</a>-->
                                                </div>
                                            </div>
                                        </div><!-- /.product-inner -->
                                    </div><!-- /.product-outer -->
                                </li>
    <?php
    $tutorial_id =  $ProductIDTemp;
    $ii++;$jj++;
}
?>


                            </ul>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="grid-extended" aria-expanded="true">

                            <ul class="products columns-3">
                                <?php
// $per_page = 2;
$query = "SELECT ID,ProductName,Image,Price,Discount,URL,ProductSource,Overview,Description FROM products WHERE FIND_IN_SET (" . $ID . ",Categories) AND Status = 1 ";
$query2 = "SELECT ID FROM brands WHERE Status = 1 ORDER BY BrandName";
$result2 = mysql_query( $query2 )or die( mysql_error() );
$i2 = 1;
while ( $row2 = mysql_fetch_array( $result2, MYSQL_ASSOC ) ) {

    if ( isset( $_REQUEST[ 'brand' . $i2 . '' ] ) ) {
        $query .= ' AND Manufacture =' . $_REQUEST[ 'brand' . $i2 . '' ];
    };

    $i2++;
}


$query3 = "SELECT ID FROM p_options_values WHERE Status = 1 AND OptionID = 1 ORDER BY Sort";
$result3 = mysql_query( $query3 )or die( mysql_error() );
$i3 = 1;
while ( $row3 = mysql_fetch_array( $result3, MYSQL_ASSOC ) ) {

    if ( isset( $_REQUEST[ 'size' . $i3 . '' ] ) ) {
        $query .= ' AND FIND_IN_SET (' . $_REQUEST[ 'size' . $i3 . '' ] . ',Options)';
    };

    $i3++;
}


$query4 = "SELECT ID FROM p_options_values WHERE Status = 1 AND OptionID = 2 ORDER BY Sort";
$result4 = mysql_query( $query4 )or die( mysql_error() );
$i4 = 1;
$jj= 0;
$ii = mysql_num_rows($result2);;
while ( $row4 = mysql_fetch_array( $result4, MYSQL_ASSOC ) ) {

    if ( isset( $_REQUEST[ 'color' . $i4 . '' ] ) ) {
        $query .= ' AND FIND_IN_SET (' . $_REQUEST[ 'color' . $i4 . '' ] . ',Options)';

    };

    $i4++;
}


if ( isset( $_REQUEST[ 'Price' ] ) ) {
    $PriceFilter = explode( '-', $_REQUEST[ 'Price' ] );
    //$query .= ' AND (Price BETWEEN '.$PriceFilter[0].' AND '.$PriceFilter[1].')';
} else {
    //$query .= ' AND (Price BETWEEN 0 AND '.$maxx['MaxPrice'].')';
}


if ( isset( $_REQUEST[ 'sort' ] ) && $_REQUEST[ 'sort' ] == 0 ) {
    $query .= ' ORDER BY ID DESC';
}
if ( !isset( $_REQUEST[ 'sort' ] ) ) {
    $query .= ' ORDER BY ID DESC';
}
if ( isset( $_REQUEST[ 'sort' ] ) && $_REQUEST[ 'sort' ] == 1 ) {
    $query .= ' ORDER BY ProductName ASC';
}
if ( isset( $_REQUEST[ 'sort' ] ) && $_REQUEST[ 'sort' ] == 2 ) {
    $query .= ' ORDER BY ProductName DESC';
}
if ( isset( $_REQUEST[ 'sort' ] ) && $_REQUEST[ 'sort' ] == 3 )

{
    $query .= ' ORDER BY Price ASC';
}
if ( isset( $_REQUEST[ 'sort' ] ) && $_REQUEST[ 'sort' ] == 4 ) {
    $query .= ' ORDER BY Price DESC';
}
$query .= ' LIMIT 12';
// echo $query;
// exit();
$result = mysql_query( $query )or die( mysql_error() );
$total_results = mysql_num_rows( $result );
$per_page = $total_results;
$total_pages = ceil( $total_results / $per_page ); //total pages we going to have
//-------------if page is setcheck------------------//
if ( isset( $_REQUEST[ 'page' ] ) ) {
    $show_page = $_REQUEST[ 'page' ]; //it will telles the current page
    if ( $show_page > 0 && $show_page <= $total_pages ) {
        $start = ( $show_page - 1 ) * $per_page;
        $end = $start + $per_page;
    } else {
        // error - show first set of results
        $start = 0;
        $end = $per_page;
    }
} else {
    // if page isn't set, show first set of results
    $show_page = 1;
    $start = 0;
    $end = $per_page;
}

if ( isset( $_REQUEST[ 'page' ] ) ) {
    $currentpage = $_REQUEST[ 'page' ]; //it will telles the current page
} else {
    // if page isn't set, show first set of results
    $currentpage = 1;
}

// display pagination
$page = intval( $currentpage );

$tpages = $total_pages;
if ( $page <= 0 )
    $page = 1;
if ( $total_results == 0 ) {
    echo '<h1>Product Not Found</h1>';
}
for ( $i = $start; $i < $end; $i++ ) {
    if ( $i == $total_results ) {
        break;
    }
    $Image = explode( ',', mysql_result( $result, $i, 'Image' ) );
    $img1 = $Image[ 0 ];
    $img2 = ( isset( $Image[ 1 ] ) ? $Image[ 1 ] : '' );
    if ( mysql_result( $result, $i, 'Discount' ) != 0 ) {
        $persentage = ( mysql_result( $result, $i, 'Discount' ) / mysql_result( $result, $i, 'Price' ) ) * 100;
        $persentage = 100 - $persentage;
        $persentage = round( $persentage );
    }
    $ProductIDTemp = mysql_result( $result, $i, 'ID' );
    $ProductNameTemp = mysql_result( $result, $i, 'ProductName' );
    $URLTemp = mysql_result( $result, $i, 'URL' );
    $PriceTemp = mysql_result( $result, $i, 'Price' );
    $DiscountTemp = mysql_result( $result, $i, 'Discount' );
   $productSource = mysql_result( $result, $i, 'ProductSource' );
    $Overview = mysql_result( $result, $i, 'Overview' );
    $Description= mysql_result( $result, $i, 'Description' );
    ?>
       
                                <li class="product <?php echo ($ii==0) ? 'first':''; ?> <?php echo ($jj==3) ? 'last':''; ?>">
                                    <div class="product-outer">
                                        <div class="product-inner">

                                            <a href="<?php echo BASE_URL.'/prod/'.$URLTemp; ?>">
                                                <h3><?php echo strlen($ProductNameTemp) > 20 ? substr($ProductNameTemp,0,20)."..." : $ProductNameTemp; ?></h3>
                                                <div class="product-thumbnail">
                                                    <img class="wp-post-image" data-echo="<?php echo BASE_URL.'/admin/'.DIR_PRODUCTS_IMAGES.$img1 ?>" src="<?php echo BASE_URL;?>/assets/images/blank.gif" alt="<?php echo  $ProductNameTemp; ?>">
                                                </div>

                                             

                                                <div class="product-short-description">
                                                   <?php echo $Description; ?>
                                                </div>

                                                <div class="product-sku">SKU: <?php echo $productSource; ?></div>
                                            </a>
                                            <div class="price-add-to-cart">
                                                
                             <?php echo ($DiscountTemp != 0 ? '<span class="price"><span class="electro-price"><ins><span class="amount">'.CURRENCY_SYMBOL.$DiscountTemp.'</span></ins> <del><span class="amount">'.CURRENCY_SYMBOL.$PriceTemp.'</span></del></span></span>' : '<span class="price"><span class="electro-price"><ins><span class="amount">'.CURRENCY_SYMBOL.$PriceTemp.'</span></ins></span></span>
'); ?>
                                                <a rel="nofollow" href="<?=BASE_URL?>/add_to_cart.php?id=<?php echo $ProductIDTemp; ?>&name=<?php echo add_to_cart_desh($ProductNameTemp); ?>&quantity=1&upsell=2&options=0&url=<?php echo $_SERVER['REQUEST_URI']; ?>" class="button add_to_cart_button">Add to cart</a>
                                            </div><!-- /.price-add-to-cart -->
                                            <div class="hover-area">
                                                <div class="action-buttons">

                                                    <a href="<?php echo BASE_URL; ?>/add_to_wishlist.php?id=<?php echo $ProductIDTemp; ?>&name=<?php echo $ProductNameTemp; ?>&quantity=1&upsell=2&options=0&url=<?php echo $_SERVER['REQUEST_URI']; ?>" rel="nofollow" class="add_to_wishlist">
                                                        Wishlist</a>

                                                    
                                                </div>
                                            </div>
                                        </div><!-- /.product-inner -->
                                    </div><!-- /.product-outer -->
                                </li>
                      <?php
    $tutorial_id =  $ProductIDTemp;
    $ii++;$jj++;
}
?>
             
                            </ul>
                        </div>                  <div role="tabpanel" class="tab-pane" id="list-view" aria-expanded="true">

                            <ul class="products columns-3">
                                
                                <?php
// $per_page = 2;
$query = "SELECT p.ID,p.ProductName,p.Image,p.Price,p.Discount,p.URL,p.ProductSource,p.Overview,p.Description,p.Stock,s.StockName FROM products p  LEFT JOIN p_stocks s ON p.Stock = s.ID WHERE FIND_IN_SET (" . $ID . ",Categories) AND p.Status = 1 ";
$query2 = "SELECT ID FROM brands WHERE Status = 1 ORDER BY BrandName";
$result2 = mysql_query( $query2 )or die( mysql_error() );
$i2 = 1;
while ( $row2 = mysql_fetch_array( $result2, MYSQL_ASSOC ) ) {

    if ( isset( $_REQUEST[ 'brand' . $i2 . '' ] ) ) {
        $query .= ' AND Manufacture =' . $_REQUEST[ 'brand' . $i2 . '' ];
    };

    $i2++;
}


$query3 = "SELECT ID FROM p_options_values WHERE Status = 1 AND OptionID = 1 ORDER BY Sort";
$result3 = mysql_query( $query3 )or die( mysql_error() );
$i3 = 1;
while ( $row3 = mysql_fetch_array( $result3, MYSQL_ASSOC ) ) {

    if ( isset( $_REQUEST[ 'size' . $i3 . '' ] ) ) {
        $query .= ' AND FIND_IN_SET (' . $_REQUEST[ 'size' . $i3 . '' ] . ',Options)';
    };

    $i3++;
}


$query4 = "SELECT ID FROM p_options_values WHERE Status = 1 AND OptionID = 2 ORDER BY Sort";
$result4 = mysql_query( $query4 )or die( mysql_error() );
$i4 = 1;
$jj= 0;
$ii = mysql_num_rows($result2);;
while ( $row4 = mysql_fetch_array( $result4, MYSQL_ASSOC ) ) {

    if ( isset( $_REQUEST[ 'color' . $i4 . '' ] ) ) {
        $query .= ' AND FIND_IN_SET (' . $_REQUEST[ 'color' . $i4 . '' ] . ',Options)';

    };

    $i4++;
}


if ( isset( $_REQUEST[ 'Price' ] ) ) {
    $PriceFilter = explode( '-', $_REQUEST[ 'Price' ] );
    //$query .= ' AND (Price BETWEEN '.$PriceFilter[0].' AND '.$PriceFilter[1].')';
} else {
    //$query .= ' AND (Price BETWEEN 0 AND '.$maxx['MaxPrice'].')';
}


if ( isset( $_REQUEST[ 'sort' ] ) && $_REQUEST[ 'sort' ] == 0 ) {
    $query .= ' ORDER BY ID DESC';
}
if ( !isset( $_REQUEST[ 'sort' ] ) ) {
    $query .= ' ORDER BY ID DESC';
}
if ( isset( $_REQUEST[ 'sort' ] ) && $_REQUEST[ 'sort' ] == 1 ) {
    $query .= ' ORDER BY ProductName ASC';
}
if ( isset( $_REQUEST[ 'sort' ] ) && $_REQUEST[ 'sort' ] == 2 ) {
    $query .= ' ORDER BY ProductName DESC';
}
if ( isset( $_REQUEST[ 'sort' ] ) && $_REQUEST[ 'sort' ] == 3 )

{
    $query .= ' ORDER BY Price ASC';
}
if ( isset( $_REQUEST[ 'sort' ] ) && $_REQUEST[ 'sort' ] == 4 ) {
    $query .= ' ORDER BY Price DESC';
}
$query .= ' LIMIT 12';
// echo $query;
// exit();
$result = mysql_query( $query )or die( mysql_error() );
$total_results = mysql_num_rows( $result );
$per_page = $total_results;
$total_pages = ceil( $total_results / $per_page ); //total pages we going to have
//-------------if page is setcheck------------------//
if ( isset( $_REQUEST[ 'page' ] ) ) {
    $show_page = $_REQUEST[ 'page' ]; //it will telles the current page
    if ( $show_page > 0 && $show_page <= $total_pages ) {
        $start = ( $show_page - 1 ) * $per_page;
        $end = $start + $per_page;
    } else {
        // error - show first set of results
        $start = 0;
        $end = $per_page;
    }
} else {
    // if page isn't set, show first set of results
    $show_page = 1;
    $start = 0;
    $end = $per_page;
}

if ( isset( $_REQUEST[ 'page' ] ) ) {
    $currentpage = $_REQUEST[ 'page' ]; //it will telles the current page
} else {
    // if page isn't set, show first set of results
    $currentpage = 1;
}

// display pagination
$page = intval( $currentpage );

$tpages = $total_pages;
if ( $page <= 0 )
    $page = 1;
if ( $total_results == 0 ) {
    echo '<h1>Product Not Found</h1>';
}
for ( $i = $start; $i < $end; $i++ ) {
    if ( $i == $total_results ) {
        break;
    }
    $Image = explode( ',', mysql_result( $result, $i, 'Image' ) );
    $img1 = $Image[ 0 ];
    $img2 = ( isset( $Image[ 1 ] ) ? $Image[ 1 ] : '' );
    if ( mysql_result( $result, $i, 'Discount' ) != 0 ) {
        $persentage = ( mysql_result( $result, $i, 'Discount' ) / mysql_result( $result, $i, 'Price' ) ) * 100;
        $persentage = 100 - $persentage;
        $persentage = round( $persentage );
    }
    $ProductIDTemp = mysql_result( $result, $i, 'p.ID' );
    $ProductNameTemp = mysql_result( $result, $i, 'p.ProductName' );
    $URLTemp = mysql_result( $result, $i, 'p.URL' );
    $PriceTemp = mysql_result( $result, $i, 'p.Price' );
    $DiscountTemp = mysql_result( $result, $i, 'p.Discount' );
   $productSource = mysql_result( $result, $i, 'p.ProductSource' );
    $Overview = mysql_result( $result, $i, 'p.Overview' );
    $Description= mysql_result( $result, $i, 'p.Description' );
    $StockName= mysql_result( $result, $i, 's.StockName' );
    ?>
       
                                
                                
                                <li class="product list-view">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="<?php echo BASE_URL.'/prod/'.$URLTemp; ?>">
                                                <img class="wp-post-image" data-echo="<?php echo BASE_URL.'/admin/'.DIR_PRODUCTS_IMAGES.$img1 ?>" src="<?php echo BASE_URL;?>/assets/images/blank.gif" alt="<?php echo  $ProductNameTemp; ?>">
                                            </a>
                                        </div>
                                        <div class="media-body media-middle">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <a href="<?php echo BASE_URL.'/prod/'.$URLTemp; ?>"><h3><?php echo strlen($ProductNameTemp) > 20 ? substr($ProductNameTemp,0,20)."..." : $ProductNameTemp; ?></h3>
                                                        
                                                        <div class="product-short-description">
                                                            <?php echo $Description; ?>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="col-xs-12">

                                                    <div class="availability in-stock">
                                                        Availablity: <span><?php echo $StockName; ?></span>
                                                    </div>


                                                  <?php echo ($DiscountTemp != 0 ? '<span class="price"><span class="electro-price"><ins><span class="amount">'.CURRENCY_SYMBOL.$DiscountTemp.'</span></ins> <del><span class="amount">'.CURRENCY_SYMBOL.$PriceTemp.'</span></del></span></span>' : '<span class="price"><span class="electro-price"><ins><span class="amount">'.CURRENCY_SYMBOL.$PriceTemp.'</span></ins></span></span>
'); ?>
                                                    <a class="button product_type_simple add_to_cart_button ajax_add_to_cart"  data-product_id="2706" data-quantity="1" href="<?=BASE_URL?>/add_to_cart.php?id=<?php echo $ProductIDTemp; ?>&name=<?php echo add_to_cart_desh($ProductNameTemp); ?>&quantity=1&upsell=2&options=0&url=<?php echo $_SERVER['REQUEST_URI']; ?>" rel="nofollow">Add to cart</a>
                                                    <div class="hover-area">
                                                        <div class="action-buttons">
                                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-2706">
                                                                <a class="add_to_wishlist" data-product-type="simple" data-product-id="2706" rel="nofollow" href="<?php echo BASE_URL; ?>/add_to_wishlist.php?id=<?php echo $ProductIDTemp; ?>&name=<?php echo $ProductNameTemp; ?>&quantity=1&upsell=2&options=0&url=<?php echo $_SERVER['REQUEST_URI']; ?>">Wishlist</a>

                                                               

                                                            </div>
                                                            <div class="clear"></div>
                                                           
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <?php
    $tutorial_id =  $ProductIDTemp;
    $ii++;$jj++;
}
?>                   
                              
                            </ul>
                        </div>
                        
                     
                        
                    </div>
                <!--     <div class="shop-control-bar-bottom">
                        <form class="form-electro-wc-ppp">
                            <select class="electro-wc-wppp-select c-select" onchange="this.form.submit()" name="ppp"><option selected="selected" value="15">Show 15</option><option value="-1">Show All</option></select>
                        </form>
                        <p class="woocommerce-result-count">Showing 1&ndash;15 of 20 results</p>
                        <nav class="woocommerce-pagination">
                            <ul class="page-numbers">
                                <li><span class="page-numbers current">1</span></li>
                                <li><a href="#" class="page-numbers">2</a></li>
                                <li><a href="#" class="next page-numbers">→</a></li>
                            </ul>
                        </nav>
                    </div> -->

                </main><!-- #main -->
            </div><!-- #primary -->
<?php require_once 'include/sidebar_inner.php' ?>
        </div><!-- .container -->
    </div><!-- #content -->

 <?php require_once 'include/footer.php'; ?>
 <?php require_once 'include/foot.php'; ?>