<?php 
require_once 'admin/Common.php'; 
require_once 'functions.php';
$CatID=99999;
if(isset($_SESSION['LoginCustomer']) && $_SESSION['LoginCustomer']==true)
	redirect("index.php");
	$email="";
	$password="";
	$msg1 = "";
	$msg2 = "";
	$msg3 = "";
	if(isset($_POST["action"]) && $_POST["action"] == "submit_form")
	{
		if(isset($_POST["email"]))
			$email=trim($_POST["email"]);
		if(isset($_POST["password"]))
			$password=trim($_POST["password"]);
			
		if ($email=="")
			$msg1 = '<p style="color:red">Please Enter EmailAddress.</p>';
		if ($password=="")
			$msg2 = '<p style="color:red">Please Enter Password.</p>';
			
		if($msg1=='' && $msg2=='')
		{	
			$query="SELECT ID,FirstName,LastName,Password,Email,Country FROM website_users WHERE Status = 1 AND Email='".dbinput($email)."'";
			$result = mysql_query ($query) or die(mysql_error()); 
			$num = mysql_num_rows($result);
			
			
			if($num==0)
			{
				$_SESSION["LoginCustomer"]=false;
				$_SESSION["CustomerID"]='';
				$_SESSION["CustomerFullName"]='';
				$_SESSION["Country"]='';
				$_SESSION["Shipping"]='';
				$_SESSION["Email"]='';
				$msg3 = '<p style="color:red">Invalid Email/Password.</p>';	
			}
			else
			{
				$row = mysql_fetch_array($result,MYSQL_ASSOC);
				if(dboutput($row["Password"]) == $password)
				{
					$_SESSION["LoginCustomer"]=true;
					$_SESSION["CustomerID"]=dboutput($row["ID"]);
					$_SESSION["CustomerFullName"]=dboutput($row["FirstName"]) .' '. dboutput($row["LastName"]);
					$_SESSION["Country"]=dboutput($row["Country"]);
					$_SESSION["Email"]=dboutput($row["Email"]);
					$query2="SELECT Percentage FROM shipping_rates WHERE CountryCode='".$row["Country"]."'";
					$result2 = mysql_query ($query2) or die(mysql_error()); 
					$num2 = mysql_num_rows($result2);
					if($num2 > 0)
					{
						$row2 = mysql_fetch_array($result2,MYSQL_ASSOC);
						$_SESSION["Shipping"]=dboutput($row2["Percentage"]);
					}
					else
					{
						$query3="SELECT Percentage FROM other_countries_shipping WHERE ID=1";
						$result3 = mysql_query ($query3) or die(mysql_error()); 
						$num3 = mysql_num_rows($result3);
						if($num3 == 1)
						{
							$row3 = mysql_fetch_array($result3,MYSQL_ASSOC);
							$_SESSION["Shipping"]=dboutput($row3["Percentage"]);
						}
					}
					
					redirect("index.php");
				}
				else
				{
					$_SESSION["LoginCustomer"]=false;
					$_SESSION["CustomerID"]='';
					$_SESSION["CustomerFullName"]='';
					$_SESSION["Country"]='';
					$_SESSION["Shipping"]='';
					$_SESSION["Email"]='';
					$msg3 = '<p style="color:red">Invalid Email/Password.</p>';
				}
			}
		}
	}
require_once 'include/head.php'; 
require_once 'include/headerpartial.php'; 

?>



            <div id="content" class="site-content" tabindex="-1">
	<div class="container">

		<nav class="woocommerce-breadcrumb"><a href="<?php echo BASE_URL; ?>">Home</a><span class="delimiter"><i class="fa fa-angle-right"></i></span>Login</nav>

		<div id="primary" class="content-area">
			<main id="main" class="site-main">
				<article class="page type-page status-publish hentry">
					<header class="entry-header"><h1 itemprop="name" class="entry-title">Login</h1></header><!-- .entry-header -->
      
          
      <div class="row">
        <div class="col-sm-6">
          <div class="well">
            <h2>New Customer</h2>
            <p><strong>Register Account</strong></p>
            <p>By creating an account you will be able to shop faster, be up to date on an order's status, and keep track of the orders you have previously made.</p>
            <a href="<?php echo BASE_URL;?>/register.php" class="btn btn-primary">Register Now</a></div>
        </div>
        <div class="col-sm-6">
          <div class="well">
            <h2>Returning Customer</h2>
            <p><strong>I am a returning customer</strong></p>
			<?php 
			if(isset($msg3))
			echo $msg3
			?>
            <form action="<?php echo $_SERVER["PHP_SELF"];?>" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <label class="control-label" for="input-email">EmailAddress</label>
				<?php if(isset($msg1)){echo $msg1;}?>
                <input type="text" name="email" value="<?php echo $email; ?>" placeholder="E-Mail Address" id="input-email" class="form-control">
              </div>
              <div class="form-group">
                <label class="control-label" for="input-password">Password</label>
				<?php if(isset($msg2)){echo $msg2;}?>
                <input type="password" name="password" value="" placeholder="Password" id="input-password" class="form-control">
                <!--<a href="">Forgotten Password</a>--></div>
              <input type="submit" value="Login" class="btn btn-primary">
			  <input type="hidden" name="action" value="submit_form" />
            </form>
          </div>
        </div>
      </div>
					
				</article>
			</main><!-- #main -->
		</div><!-- #primary -->
	</div><!-- .container -->
</div><!-- #content -->



 <?php require_once 'include/footer.php'; ?>
 <?php require_once 'include/foot.php'; ?>