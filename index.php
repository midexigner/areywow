<?php 
require_once 'admin/Common.php'; 
require_once 'functions.php'; 
require_once 'include/head.php'; 
require_once 'include/header.php'; 
?>
<!-- http://transvelo.github.io/electro-html/ -->
 <div id="content" class="site-content" tabindex="-1">
    <div class="container">
        <div id="primary" class="content-area">
            <main id="main" class="site-main">
               <?php require_once 'include/slider.php'; ?>
                <div class="home-v1-ads-block animate-in-view fadeIn animated" data-animation="fadeIn">
	<div class="ads-block row">

		<?php 
			$homepageQ = "SELECT ID,Heading,Image,URL,Text,DATE_FORMAT(DateAdded, '%D %b %Y<br>%r') AS Added, 
				DATE_FORMAT(DateModified, '%D %b %Y<br>%r') AS Updated FROM banners WHERE Type = 1 AND Status = 1 LIMIT 3";
	$homepageR = mysql_query($homepageQ);
	while($homepageResult = mysql_fetch_assoc($homepageR)){

		 ?>
		<div class="ad col-xs-12 col-sm-4">
			<div class="media">
				<div class="media-left media-middle">
					<img data-echo="<?php echo BASE_URL.'/admin/'.DIR_WEBSITE_BANNERS.$homepageResult['Image'] ?>" src="assets/images/blank.gif" alt="">
				</div>
				<div class="media-body media-middle">
					<div class="ad-text">

						<?php 

						$homepageName =explode(' ', $homepageResult['Heading']);
								$lastcount  = count($homepageName)-1;
								echo $lastcount[0];
					 for($st=0;$st<count($homepageName);$st++){
                  if($st>=$lastcount){
                    echo '<br><strong>'.$homepageName[$st].'</strong>';
                  }else{
                    echo $homepageName[$st].' ';
                  }
            }
				
								 ?>
							
						<!-- Catch Big <br><strong>Deals</strong> on the <br>Cameras -->
					</div>
					<div class="ad-action">
						<a href="<?php echo $homepageResult['URL']; ?>"><?php echo $homepageResult['Text']; ?></a>
					</div>
				</div>
			</div>
		</div>
<?php } ?>

	</div>
</div>

                <div class="home-v1-deals-and-tabs deals-and-tabs row animate-in-view fadeIn animated" data-animation="fadeIn">
	<div class="deals-block col-lg-4">
		<section class="section-onsale-product">
			<header>
				<h2 class="h1">Special Offer</h2>
				<div class="savings">
					<span class="savings-text">Save <span class="amount"><?php echo  $specialOffer['save'] ?> %</span></span>
				</div>
			</header><!-- /header -->

			<div class="onsale-products">
				<div class="onsale-product">
					<a href="<?php echo BASE_URL.'/'.$specialOffer['slug'];?>">
						<div class="product-thumbnail">
							<img class="wp-post-image" data-echo="<?php echo BASE_URL.'/admin/'.DIR_PRODUCTS_IMAGES.$specialOffer['Image'] ?>" src="assets/images/blank.gif" alt=""></div>
<h3>
<?php echo $specialOffer['title'];?>
								 </h3>
							<!-- <h3>Game Console Controller <br>+ USB 3.0 Cable</h3> -->
					</a>

					<span class="price">
						<span class="electro-price">
							<ins><span class="amount"><?php echo CURRENCY_SYMBOL.' '.$specialOffer['Price'];?></span></ins>
							<del><span class="amount"><?php echo CURRENCY_SYMBOL.' '.$specialOffer['Discount'];?></span></del>
						</span>
					</span><!-- /.price -->

					<div class="deal-progress">
						<!-- <div class="deal-stock">
							<span class="stock-sold">Already Sold: <strong>2</strong></span>
							<span class="stock-available">Available: <strong>26</strong></span>
						</div> -->

						<!-- <div class="progress">
							<span class="progress-bar" style="width:8%">8</span>
						</div> -->
					</div><!-- /.deal-progress -->

					<div class="deal-countdown-timer">
						<div class="marketing-text text-xs-center">Hurry Up! Offer ends in:	</div>


						<div id="deal-countdown" class="countdown">
							<span data-value="0" class="days"><span class="value">0</span><b>Days</b></span>
							<span class="hours"><span class="value">7</span><b>Hours</b></span>
							<span class="minutes"><span class="value">29</span><b>Mins</b></span>
							<span class="seconds"><span class="value">13</span><b>Secs</b></span>
						</div>
						<span class="deal-end-date" style="display:none;"><?php if($specialOffer['OfferDate'] != ''){echo $specialOffer['OfferDate']; }  ?></span>
						<script>
						// set the date we're counting down to
						var deal_end_date = document.querySelector(".deal-end-date").textContent;
						var target_date = new Date( deal_end_date ).getTime();

						// variables for time units
						var days, hours, minutes, seconds;

						// get tag element
						var countdown = document.getElementById( 'deal-countdown' );

						// update the tag with id "countdown" every 1 second
						setInterval( function () {

						// find the amount of "seconds" between now and target
						var current_date = new Date().getTime();
						var seconds_left = (target_date - current_date) / 1000;

						// do some time calculations
						days = parseInt(seconds_left / 86400);
						seconds_left = seconds_left % 86400;

						hours = parseInt(seconds_left / 3600);
						seconds_left = seconds_left % 3600;

						minutes = parseInt(seconds_left / 60);
						seconds = parseInt(seconds_left % 60);

						// format countdown string + set tag value
						countdown.innerHTML = '<span data-value="' + days + '" class="days"><span class="value">' + days +  '</span><b>Days</b></span><span class="hours"><span class="value">' + hours + '</span><b>Hours</b></span><span class="minutes"><span class="value">'
						+ minutes + '</span><b>Mins</b></span><span class="seconds"><span class="value">' + seconds + '</span><b>Secs</b></span>';

						}, 1000 );
						</script>
					</div><!-- /.deal-countdown-timer -->
				</div><!-- /.onsale-product -->
			</div><!-- /.onsale-products -->
		</section><!-- /.section-onsale-product -->
	</div><!-- /.col -->


	<div class="tabs-block col-lg-8">
		<div class="products-carousel-tabs">
			<ul class="nav nav-inline">
				<li class="nav-item"><a class="nav-link active" href="#tab-products-1" data-toggle="tab">Featured</a></li>
				<li class="nav-item"><a class="nav-link" href="#tab-products-2" data-toggle="tab">On Sale</a></li>
				<li class="nav-item"><a class="nav-link" href="#tab-products-3" data-toggle="tab">Top Rated</a></li>
			</ul>

			<div class="tab-content">
				<div class="tab-pane active" id="tab-products-1" role="tabpanel">
					<div class="woocommerce columns-3">




		<ul class="products columns-3">

<?php 
$i = 0;
$j = mysql_num_rows($featured);
while($featuredRow= mysql_fetch_assoc($featured)) {  $i++; $j++;
    $featuredImage=explode(',', $featuredRow["Image"]);
    $featuredimg1 = $featuredImage[0];
    $featuredimg2 = (isset($featuredImage[1]) ? $featuredImage[1] : '');
 ?>
			<li class="product <?php echo ($i==0) ? 'first':''; ?> <?php echo ($i==3) ? 'last':''; ?> ">
				<div class="product-outer">
    <div class="product-inner">
    <!-- 	<?php 
    	 $catID=explode(',',$featuredRow["Categories"]);
    	 foreach ($catID as $Cat) {
          $sqlss = mysql_query("SELECT CategoryName, URL FROM categories WHERE ID = " . (int)$Cat) or die(mysql_error());
          
      echo '<span class="loop-product-categories"><a href="'.BASE_URL.'/'.mysql_result($sqlss, 0, 1).'">'.mysql_result($sqlss, 0, 0).'</a></span>';
    }

    	 ?> -->
        
        <a href="<?php echo BASE_URL."/prod/".$featuredRow['URL']; ?>">
            <h3><?php echo $featuredRow['ProductName']; ?></h3>
            <div class="product-thumbnail">
                <img src="assets/images/blank.gif" data-echo="<?php echo BASE_URL.'/admin/'.DIR_PRODUCTS_IMAGES.$featuredimg1; ?>" class="img-responsive" alt="">
            </div>
        </a>

        <div class="price-add-to-cart">

        	
            <span class="price">

                <?php if($featuredRow['Discount'] !=0){ ?>

                 <span class="electro-price">
                    <ins>
                    	<span class="amount">

                     <?php echo CURRENCY_SYMBOL.' '.$featuredRow['Price'];?></span></ins>
                                            <del><span class="amount"><?php echo CURRENCY_SYMBOL.' '.$featuredRow['Discount'];?></span></del>
                                        <span class="amount"> </span>
                </span>
               
                <?php }else{ ?>

                <span class="electro-price">
                    <ins>
                    	<span class="amount">

                     <?php echo CURRENCY_SYMBOL.' '.$featuredRow['Price'];?></span></ins>
                                        
                </span>

                
 
                <?php } ?>
            </span>

            <a rel="nofollow" href="<?php echo BASE_URL?>/add_to_cart.php?id=<?php echo $featuredRow['ID']; ?>&name=<?php echo add_to_cart_desh($featuredRow['ProductName']); ?>&quantity=1&upsell=2&options=0&url=<?php echo $_SERVER['REQUEST_URI']; ?>" class="button add_to_cart_button">Add to cart</a>
        </div><!-- /.price-add-to-cart -->

        <div class="hover-area">
            <div class="action-buttons">

                <a href="<?php echo BASE_URL; ?>/add_to_wishlist.php?id=<?php echo $featuredRow['ID']; ?>&name=<?php echo add_to_cart_desh($featuredRow['ProductName']); ?>&quantity=1&upsell=2&options=0&url=<?php echo $_SERVER['REQUEST_URI']; ?>" rel="nofollow" class="add_to_wishlist"> Wishlist</a>

<!--                <a href="#" class="add-to-compare-link"> Compare</a>-->
            </div>
        </div>
    </div><!-- /.product-inner -->
</div><!-- /.product-outer -->
			</li><!-- /.products -->

			<?php } ?>
			
					</ul>
					</div>
				</div>

				<div class="tab-pane" id="tab-products-2" role="tabpanel">
					<div class="woocommerce columns-3">
												<ul class="products columns-3">

							<?php 
$onsalei = 0;
$onsalej = mysql_num_rows($sale);
while ($onsaleRow = mysql_fetch_assoc($sale)) { $onsalei++; $onsalej++;
    $onsaleImage=explode(',', $onsaleRow["Image"]);
    $onsaleimg1 = $onsaleImage[0];
    $onsaleimg2 = (isset($onsaleImage[1]) ? $onsaleImage[1] : '');
 ?>
			<li class="product <?php echo ($onsalei==0) ? 'first':''; ?> <?php echo ($onsalei==3) ? 'last':''; ?> ">
				<div class="product-outer">
    <div class="product-inner">

    	<!-- <?php 
    	 $catID=explode(',',$onsaleRow["Categories"]);
    	 foreach ($catID as $Cat) {
          $sqlss = mysql_query("SELECT CategoryName, URL FROM categories WHERE ID = " . (int)$Cat) or die(mysql_error());
          
      echo '<span class="loop-product-categories"><a href="'.BASE_URL.'/'.mysql_result($sqlss, 0, 1).'">'.mysql_result($sqlss, 0, 0).'</a></span>';
    }

    	 ?> -->
        <!-- <span class="loop-product-categories"><a href="product-category.html" rel="tag"><?php echo $featuredRow['']; ?></a></span> -->
        <a href="<?php echo BASE_URL."/prod/".$onsaleRow['URL']; ?>">


            <h3><?php echo strlen($onsaleRow['ProductName']) > 20 ? substr($onsaleRow['ProductName'],0,20)."..." : $onsaleRow['ProductName']; ?></h3>
            <div class="product-thumbnail">

                <img src="<?php echo BASE_URL; ?>/assets/images/blank.gif" data-echo="<?php echo BASE_URL.'/admin/'.DIR_PRODUCTS_IMAGES.$onsaleimg1 ?>" class="img-responsive" alt="">
            </div>
        </a>

        <div class="price-add-to-cart">

            <span class="price">

                <?php if($onsaleRow['Discount'] !=0){ ?>

                 <span class="electro-price">
                    <ins>
                    	<span class="amount">

                     <?php echo CURRENCY_SYMBOL.' '.$onsaleRow['Price'];?></span></ins>
                                            <del><span class="amount"><?php echo CURRENCY_SYMBOL.' '.$onsaleRow['Discount'];?></span></del>
                                        <span class="amount"> </span>
                </span>
              
                <?php }else{ ?>

                 <span class="electro-price">
                    <ins>
                    	<span class="amount">

                     <?php echo CURRENCY_SYMBOL.' '.$onsaleRow['Price'];?></span></ins>
                                        
                </span>

                
 
                <?php } ?>
            </span>
            <a rel="nofollow" href="<?php echo BASE_URL?>/add_to_cart.php?id=<?php echo $onsaleRow['ID']; ?>&name=<?php echo add_to_cart_desh($onsaleRow['ProductName']); ?>&quantity=1&upsell=2&options=0&url=<?php echo $_SERVER['REQUEST_URI']; ?>" class="button add_to_cart_button">Add to cart</a>
        </div><!-- /.price-add-to-cart -->

        <div class="hover-area">
            <div class="action-buttons">

                <a href="<?php echo BASE_URL; ?>/add_to_wishlist.php?id=<?php echo $onsaleRow['ID']; ?>&name=<?php echo add_to_cart_desh($onsaleRow['ProductName']); ?>&quantity=1&upsell=2&options=0&url=<?php echo $_SERVER['REQUEST_URI']; ?>" rel="nofollow" class="add_to_wishlist"> Wishlist</a>

<!--                <a href="#" class="add-to-compare-link"> Compare</a>-->
            </div>
        </div>
    </div><!-- /.product-inner -->
</div><!-- /.product-outer -->
			</li><!-- /.products -->

			<?php } ?>


														</ul>
					</div>
				</div>

				<div class="tab-pane" id="tab-products-3" role="tabpanel">
					<div class="woocommerce columns-3">




		<ul class="products columns-3">


            <?php
            $UPSQ = mysql_query("SELECT ProductID FROM upsaleproducts LIMIT 1") or die(mysql_error());
            $UPSR = mysql_fetch_assoc($UPSQ);
            $UPSRID = $UPSR['ProductID'];
            $topRelatedQ = "SELECT b.BrandName AS BrandName, p.ID,p.ProductName,p.Categories,p.ProductNameArabic,p.Categories,p.RelatedProducts,p.MetaDescription,p.Quantity,p.Image,p.Shipping,p.Overview,p.OverviewArabic,s.StockName,s.Text,p.Discount,p.Manufacture,p.CustomDesign,p.Ratings,b.Image as bimg,p.Price,p.MetaKeywords,p.Description,p.DescriptionArabic,p.URL FROM products p LEFT JOIN brands b ON p.Manufacture = b.ID LEFT JOIN p_stocks s ON p.Stock = s.ID WHERE p.onsale = 1 AND p.ID <> '".$UPSRID."' LIMIT 6";
            $topRelatedResult = mysql_query($topRelatedQ) or die(mysql_error());
            $topRelatedi = 0;
            $topRelatedj = mysql_num_rows($topRelatedResult);
            while ($topRelatedRow = mysql_fetch_assoc($topRelatedResult)) { $onsalei++; $onsalej++;
                $topRelatedRowImage=explode(',', $topRelatedRow["Image"]);
                $topRelatedimg1 = $topRelatedRowImage[0];
                $topRelatedimg2 = (isset($topRelatedRowImage[1]) ? $topRelatedRowImage[1] : '');
                ?>
                <li class="product <?php echo ($onsalei==0) ? 'first':''; ?> <?php echo ($onsalei==3) ? 'last':''; ?> ">
                    <div class="product-outer">
                        <div class="product-inner">

                            <!-- <?php
                            $catID=explode(',',$topRelatedRow["Categories"]);
                            foreach ($catID as $Cat) {
                                $sqlss = mysql_query("SELECT CategoryName, URL FROM categories WHERE ID = " . (int)$Cat) or die(mysql_error());

                                echo '<span class="loop-product-categories"><a href="'.BASE_URL.'/'.mysql_result($sqlss, 0, 1).'">'.mysql_result($sqlss, 0, 0).'</a></span>';
                            }

                            ?> -->
                            <!-- <span class="loop-product-categories"><a href="product-category.html" rel="tag"><?php echo $featuredRow['']; ?></a></span> -->
                            <a href="<?php echo BASE_URL."/prod/".$topRelatedRow['URL']; ?>">
                                <h3><?php echo $topRelatedRow['ProductName']; ?></h3>
                                <div class="product-thumbnail">
                                    <img src="<?php echo BASE_URL; ?>/assets/images/blank.gif" data-echo="<?php echo BASE_URL.'/admin/'.DIR_PRODUCTS_IMAGES.$topRelatedimg1 ?>" class="img-responsive" alt="">
                                </div>
                            </a>

                            <div class="price-add-to-cart">

                                <?php
                                //echo ($Discountt != 0 ? '<small><del>'.CURRENCY_SYMBOL.$Pricee.'</del></small><br/>'.CURRENCY_SYMBOL.$Discountt : CURRENCY_SYMBOL.$Pricee); ?>
                                <span class="price">

                <?php if($topRelatedRow['Discount'] !=0){ ?>
                    <span class="electro-price">
                    <ins>
                    	<span class="amount">

                     <?php echo CURRENCY_SYMBOL.' '.$topRelatedRow['Price'];?></span></ins>

                </span>
                <?php }else{ ?>

                    <span class="electro-price">
                    <ins>
                    	<span class="amount">

                     <?php echo CURRENCY_SYMBOL.' '.$topRelatedRow['Price'];?></span></ins>
                                            <del><span class="amount"><?php echo CURRENCY_SYMBOL.' '.$topRelatedRow['Discount'];?></span></del>
                                        <span class="amount"> </span>
                </span>

                <?php } ?>
            </span>
                                <a rel="nofollow" href="<?php echo BASE_URL?>/add_to_cart.php?id=<?php echo $topRelatedRow['ID']; ?>&name=<?php echo add_to_cart_desh($topRelatedRow['ProductName']); ?>&quantity=1&upsell=2&options=0&url=<?php echo $_SERVER['REQUEST_URI']; ?>" class="button add_to_cart_button">Add to cart</a>
                            </div><!-- /.price-add-to-cart -->

                            <div class="hover-area">
                                <div class="action-buttons">

                                    <a href="<?php echo BASE_URL; ?>/add_to_wishlist.php?id=<?php echo $topRelatedRow['ID']; ?>&name=<?php echo $topRelatedRow["ProductName"]; ?>&quantity=1&upsell=2&options=0&url=<?php echo $_SERVER['REQUEST_URI']; ?>" rel="nofollow" class="add_to_wishlist"> Wishlist</a>


                                </div>
                            </div>
                        </div><!-- /.product-inner -->
                    </div><!-- /.product-outer -->
                </li><!-- /.products -->

            <?php } ?>
					</ul>

					</div>
				</div>
			</div>
		</div>
	</div><!-- /.tabs-block -->
</div><!-- /.deals-and-tabs -->
                <!-- ============================================================= 2-1-2 Product Grid ============================================================= -->
<section class="products-2-1-2 animate-in-view fadeIn animated" style="display: block" data-animation="fadeIn">
<h2 class="sr-only">Products Grid</h2>
<div class="container">


	<ul class="nav nav-inline nav-justified">

		<li class="nav-item"><a href="javascript:avoid(0)" class="active nav-link">Best Deals</a></li>
<!--		<li class="nav-item"><a class="nav-link" href="shop.html">TV &amp; Audio</a></li>-->
<!--		<li class="nav-item"><a class="nav-link" href="shop.html">Cameras</a></li>-->
<!--		<li class="nav-item"><a class="nav-link" href="shop.html">Audio</a></li>-->
<!--		<li class="nav-item"><a class="nav-link" href="shop.html">Smartphones</a></li>-->
<!--		<li class="nav-item"><a class="nav-link" href="shop.html">GPS &amp; Navi</a></li>-->
<!--		<li class="nav-item"><a class="nav-link" href="shop.html">Computers</a></li>-->
<!--		<li class="nav-item"><a class="nav-link" href="shop.html">Portable Audio</a></li>-->
<!--		<li class="nav-item"><a class="nav-link" href="shop.html">Accessories</a></li>-->

	</ul>



	<div class="columns-2-1-2">
				<ul class="products exclude-auto-height">
							<li class="product">
                    <div class="product-outer">
                        <div class="product-inner">
                            
                            <a href="<?php echo BASE_URL."/prod/".$bestdeal1['URL']; ?>">
                                <h3><?php echo $bestdeal1['ProductName'];?></h3>
                                <div class="product-thumbnail">

                                    <img class="wp-post-image" data-echo="<?php echo BASE_URL.'/admin/'.DIR_PRODUCTS_IMAGES.$bestdeal1img1 ?>" src="assets/images/blank.gif" alt="">


                                </div>
                            </a>

                            <div class="price-add-to-cart">
                                <span class="price">
                                    <span class="electro-price">
                                        <ins><span class="amount"><?php echo CURRENCY_SYMBOL.' '.$bestdeal1['Price'];?></span></ins>
                            <del><span class="amount"><?php echo CURRENCY_SYMBOL.' '.$bestdeal1['Discount'];?></span></del>
                                    </span>
                                </span>
                                <a rel="nofollow" href="<?php echo BASE_URL?>/add_to_cart.php?id=<?php echo $bestdeal1['ID']; ?>&name=<?php echo add_to_cart_desh($bestdeal1['ProductName']); ?>&quantity=1&upsell=2&options=0&url=<?php echo $_SERVER['REQUEST_URI']; ?>" class="button add_to_cart_button">Add to cart</a>
                            </div><!-- /.price-add-to-cart -->

                            <div class="hover-area">
                                <div class="action-buttons">

                                    <a href="<?php echo BASE_URL; ?>/add_to_wishlist.php?id=<?php echo $bestdeal1['ID']; ?>&name=<?php echo add_to_cart_desh($bestdeal1['ProductName']); ?>&quantity=1&upsell=2&options=0&url=<?php echo $_SERVER['REQUEST_URI']; ?>" rel="nofollow" class="add_to_wishlist">
                                            Wishlist</a>

                                </div>
                            </div>
                        </div><!-- /.product-inner -->
                    </div><!-- /.product-outer -->
                </li>
							<li class="product">
                    <div class="product-outer">
                        <div class="product-inner">
                            
                            <a href="<?php echo BASE_URL."/prod/".$bestdeal2['URL']; ?>">
                                <h3><?php echo $bestdeal2['ProductName'];?></h3>
                                <div class="product-thumbnail">

                                    <img class="wp-post-image" data-echo="<?php echo BASE_URL.'/admin/'.DIR_PRODUCTS_IMAGES.$bestdeal2img1 ?>" src="assets/images/blank.gif" alt="">


                                </div>
                            </a>

                            <div class="price-add-to-cart">
                                <span class="price">
                                    <span class="electro-price">
                                        <ins><span class="amount"><?php echo CURRENCY_SYMBOL.' '.$bestdeal2['Price'];?></span></ins>
                            <del><span class="amount"><?php echo CURRENCY_SYMBOL.' '.$bestdeal2['Discount'];?></span></del>
                                    </span>
                                </span>
                                <a rel="nofollow" href="<?php echo BASE_URL?>/add_to_cart.php?id=<?php echo $bestdeal2['ID']; ?>&name=<?php echo add_to_cart_desh($bestdeal2['ProductName']); ?>&quantity=1&upsell=2&options=0&url=<?php echo $_SERVER['REQUEST_URI']; ?>" class="button add_to_cart_button">Add to cart</a>
                            </div><!-- /.price-add-to-cart -->

                            <div class="hover-area">
                                <div class="action-buttons">

                                    <a href="<?php echo BASE_URL; ?>/add_to_wishlist.php?id=<?php echo $bestdeal2['ID']; ?>&name=<?php echo add_to_cart_desh($bestdeal2['ProductName']); ?>&quantity=1&upsell=2&options=0&url=<?php echo $_SERVER['REQUEST_URI']; ?>" rel="nofollow" class="add_to_wishlist">
                                            Wishlist</a>

                                </div>
                            </div>
                        </div><!-- /.product-inner -->
                    </div><!-- /.product-outer -->
                </li>
					</ul>

		<ul class="products exclude-auto-height product-main-2-1-2">
			<li class="last product">
				<div class="product-outer">
					<div class="product-inner">
						
						<a href="<?php echo BASE_URL."/prod/".$bestdeal3['URL']; ?>">
                                <h3><?php echo $bestdeal3['ProductName'];?></h3>
                                <div class="product-thumbnail">

                                    <img class="wp-post-image" data-echo="<?php echo BASE_URL.'/admin/'.DIR_PRODUCTS_IMAGES.$bestdeal3img1 ?>" src="assets/images/blank.gif" alt="">


                                </div>
                            </a>

						<div class="price-add-to-cart">
                                <span class="price">
                                    <span class="electro-price">
                                        <ins><span class="amount"><?php echo CURRENCY_SYMBOL.' '.$bestdeal3['Price'];?></span></ins>
                            <del><span class="amount"><?php echo CURRENCY_SYMBOL.' '.$bestdeal3['Discount'];?></span></del>
                                    </span>
                                </span>
                                <a rel="nofollow" href="<?php echo BASE_URL?>/add_to_cart.php?id=<?php echo $bestdeal3['ID']; ?>&name=<?php echo add_to_cart_desh($bestdeal3['ProductName']); ?>&quantity=1&upsell=2&options=0&url=<?php echo $_SERVER['REQUEST_URI']; ?>" class="button add_to_cart_button">Add to cart</a>
                            </div><!-- /.price-add-to-cart -->

						<div class="hover-area">
							<div class="action-buttons">

								  <a href="<?php echo BASE_URL; ?>/add_to_wishlist.php?id=<?php echo $bestdeal3['ID']; ?>&name=<?php echo add_to_cart_desh($bestdeal3['ProductName']); ?>&quantity=1&upsell=2&options=0&url=<?php echo $_SERVER['REQUEST_URI']; ?>" rel="nofollow" class="add_to_wishlist">
                                            Wishlist</a>

								
							</div>
						</div>
					</div><!-- /.product-inner -->
				</div><!-- /.product-outer -->
			</li>

		</ul>

		<ul class="products exclude-auto-height">
							<li class="product">
                    <div class="product-outer">
                        <div class="product-inner">
                            
                            <a href="<?php echo BASE_URL."/prod/".$bestdeal4['URL']; ?>">
                                <h3><?php echo $bestdeal4['ProductName'];?></h3>
                                <div class="product-thumbnail">

                                    <img class="wp-post-image" data-echo="<?php echo BASE_URL.'/admin/'.DIR_PRODUCTS_IMAGES.$bestdeal4img1 ?>" src="assets/images/blank.gif" alt="">


                                </div>
                            </a>

                            <div class="price-add-to-cart">
                                <span class="price">
                                    <span class="electro-price">
                                        <ins><span class="amount"><?php echo CURRENCY_SYMBOL.' '.$bestdeal4['Price'];?></span></ins>
                            <del><span class="amount"><?php echo CURRENCY_SYMBOL.' '.$bestdeal4['Discount'];?></span></del>
                                    </span>
                                </span>
                                <a rel="nofollow" href="<?php echo BASE_URL?>/add_to_cart.php?id=<?php echo $bestdeal4['ID']; ?>&name=<?php echo add_to_cart_desh($bestdeal4['ProductName']); ?>&quantity=1&upsell=2&options=0&url=<?php echo $_SERVER['REQUEST_URI']; ?>" class="button add_to_cart_button">Add to cart</a>
                            </div><!-- /.price-add-to-cart -->

                            <div class="hover-area">
                                <div class="action-buttons">

                                    <a href="<?php echo BASE_URL; ?>/add_to_wishlist.php?id=<?php echo $bestdeal4['ID']; ?>&name=<?php echo add_to_cart_desh($bestdeal4['ProductName']); ?>&quantity=1&upsell=2&options=0&url=<?php echo $_SERVER['REQUEST_URI']; ?>" rel="nofollow" class="add_to_wishlist">
                                            Wishlist</a>

                                </div>
                            </div>
                        </div><!-- /.product-inner -->
                    </div><!-- /.product-outer -->
                </li>
							<li class="product">
					<div class="product-outer">
						<div class="product-inner">
							
							<a href="<?php echo BASE_URL."/prod/".$bestdeal5['URL']; ?>">
								<h3><?php echo $bestdeal5['ProductName'];?></h3>
								<div class="product-thumbnail">

									<img class="wp-post-image" data-echo="<?php echo BASE_URL.'/admin/'.DIR_PRODUCTS_IMAGES.$bestdeal5img1 ?>" src="assets/images/blank.gif" alt="">


								</div>
							</a>

							<div class="price-add-to-cart">
								<span class="price">
									<span class="electro-price">
										<ins><span class="amount"><?php echo CURRENCY_SYMBOL.' '.$bestdeal5['Price'];?></span></ins>
                            <del><span class="amount"><?php echo CURRENCY_SYMBOL.' '.$bestdeal5['Discount'];?></span></del>
									</span>
								</span>
								<a rel="nofollow" href="<?php echo BASE_URL?>/add_to_cart.php?id=<?php echo $bestdeal5['ID']; ?>&name=<?php echo add_to_cart_desh($bestdeal5['ProductName']); ?>&quantity=1&upsell=2&options=0&url=<?php echo $_SERVER['REQUEST_URI']; ?>" class="button add_to_cart_button">Add to cart</a>
							</div><!-- /.price-add-to-cart -->

							<div class="hover-area">
								<div class="action-buttons">

									<a href="<?php echo BASE_URL; ?>/add_to_wishlist.php?id=<?php echo $bestdeal5['ID']; ?>&name=<?php echo add_to_cart_desh($bestdeal5['ProductName']); ?>&quantity=1&upsell=2&options=0&url=<?php echo $_SERVER['REQUEST_URI']; ?>" rel="nofollow" class="add_to_wishlist">
											Wishlist</a>

								</div>
							</div>
						</div><!-- /.product-inner -->
					</div><!-- /.product-outer -->
				</li>
					</ul>

		</div>
	</div>
</section>

<!-- ============================================================= 2-1-2 Product Grid : End============================================================= -->                <section class="section-product-cards-carousel animate-in-view fadeIn animated" data-animation="fadeIn">

	<header>

		<h2 class="h1">Best Sellers</h2>

<!--		<ul class="nav nav-inline">-->
<!---->
<!--			<li class="nav-item active"><span class="nav-link">Top 20</span></li>-->
<!---->
<!--			<li class="nav-item"><a class="nav-link" href="product-category.html">Smart Phones &amp; Tablets</a></li>-->
<!---->
<!--			<li class="nav-item"><a class="nav-link" href="product-category.html">Laptops &amp; Computers</a></li>-->
<!---->
<!--			<li class="nav-item"><a class="nav-link" href="product-category.html">Video Cameras</a></li>-->
<!--		</ul>-->
	</header>

	<div id="home-v1-product-cards-careousel">
		<div class="woocommerce columns-3 home-v1-product-cards-carousel product-cards-carousel owl-carousel">

				<ul class="products columns-3">

<?php 
$BestSelleri = 0;
$BestSellerj = mysql_num_rows($featured); 

while($BestSellerSet= mysql_fetch_assoc($BestSeller)) { $BestSelleri++;$BestSellerj++;
									$Image=explode(',', $BestSellerSet["Image"]);
    								$img1 = $Image[0];
    								$img2 = (isset($Image[1]) ? $Image[1] : '');
									?>
														<li class="product product-card <?php echo ($BestSelleri==0) ? 'first':''; ?> <?php echo ($BestSelleri==3) ? 'last':''; ?>">

    <div class="product-outer">
		<div class="media product-inner">

			<a class="media-left" href="<?php echo BASE_URL."/prod/".$BestSellerSet['URL']; ?>" title="Pendrive USB 3.0 Flash 64 GB">
				<img class="media-object wp-post-image img-responsive" src="assets/images/blank.gif" data-echo="<?php echo BASE_URL.'/admin/'.DIR_PRODUCTS_IMAGES.$img1 ?>" alt="<?php echo $BestSellerSet['ProductName'];?>">

			</a>

			<div class="media-body">
				<!-- <span class="loop-product-categories">
					<a href="product-category.html" rel="tag">Headphone Cases</a>
				</span> -->

				<a href="single-product.html">
					<h3><?php echo strlen($BestSellerSet['ProductName']) > 40 ? substr($BestSellerSet['ProductName'],0,40)."..." : $BestSellerSet['ProductName']; ?></h3>
				</a>

				<div class="price-add-to-cart">
					<span class="price">
	                    <?php if($BestSellerSet['Discount'] !=0){ ?>

                 <span class="electro-price">
                    <ins>
                    	<span class="amount">

                     <?php echo CURRENCY_SYMBOL.' '.$BestSellerSet['Price'];?></span></ins>
                                            <del><span class="amount"><?php echo CURRENCY_SYMBOL.' '.$BestSellerSet['Discount'];?></span></del>
                                        <span class="amount"> </span>
                </span>
              
                <?php }else{ ?>

                 <span class="electro-price">
                    <ins>
                    	<span class="amount">

                     <?php echo CURRENCY_SYMBOL.' '.$BestSellerSet['Price'];?></span></ins>
                                        
                </span>

                
 
                <?php } ?>
	                </span>

					<a href="<?php echo BASE_URL?>/add_to_cart.php?id=<?php echo $BestSellerSet['ID']; ?>&name=<?php echo add_to_cart_desh($BestSellerSet['ProductName']); ?>&quantity=1&upsell=2&options=0&url=<?php echo $_SERVER['REQUEST_URI']; ?>" class="button add_to_cart_button">Add to cart</a>
				</div><!-- /.price-add-to-cart -->

				<div class="hover-area">
	                <div class="action-buttons">

	                    <a href="<?php echo BASE_URL; ?>/add_to_wishlist.php?id=<?php echo $BestSellerSet['ID']; ?>&name=<?php echo add_to_cart_desh($BestSellerSet['ProductName']); ?>&quantity=1&upsell=2&options=0&url=<?php echo $_SERVER['REQUEST_URI']; ?>" class="add_to_wishlist">
	                            Wishlist</a>

	                </div>
	            </div>

			</div>
		</div><!-- /.product-inner -->
	</div><!-- /.product-outer -->

					</li><!-- /.products -->


    <?php }?>

							</ul>
				</div>
	</div><!-- #home-v1-product-cards-careousel -->

</section>

<div class="home-v1-banner-block animate-in-view fadeIn animated" data-animation="fadeIn">

	<div class="home-v1-fullbanner-ad fullbanner-ad" style="margin-bottom: 70px">
		<a href="#">
			<img src="<?php echo BASE_URL; ?>assets/images/blank.gif" data-echo="<?php echo BASE_URL.'/admin/'.DIR_WEBSITE_BANNERS.$singleBanner['Image'] ?>" class="img-responsive" alt="">

		</a>
	</div>
</div><!-- /.home-v1-banner-block -->



<section class="home-v1-recently-viewed-products-carousel section-products-carousel animate-in-view fadeIn animated" data-animation="fadeIn">

	<header>
		<h2 class="h1">Recently Added</h2>
		<div class="owl-nav">
			<a href="#products-carousel-prev" data-target="#recently-added-products-carousel" class="slider-prev"><i class="fa fa-angle-left"></i></a>
			<a href="#products-carousel-next" data-target="#recently-added-products-carousel" class="slider-next"><i class="fa fa-angle-right"></i></a>
		</div>
	</header>

	<div id="recently-added-products-carousel">
		<div class="woocommerce columns-6">
			<div class="products owl-carousel recently-added-products products-carousel columns-6">

<?php while($recentlySet = mysql_fetch_assoc($recentProduct)){
$Image=explode(',', $recentlySet["Image"]);
                                    $img1 = $Image[0];
                                    $img2 = (isset($Image[1]) ? $Image[1] : ''); ?>
			<div class="product">
				<div class="product-outer">
    <div class="product-inner">
        <a href="<?php echo BASE_URL."/prod/".$recentlySet['URL']; ?>">
            <h3><?php echo strlen($recentlySet['ProductName']) > 40 ? substr($recentlySet['ProductName'],0,40)."..." : $recentlySet['ProductName']; ?></h3>
            <div class="product-thumbnail">
                <img src="<?php echo BASE_URL; ?>/assets/images/blank.gif" data-echo="<?php echo BASE_URL.'/admin/'.DIR_PRODUCTS_IMAGES.$img1 ?>" class="img-responsive" alt="<?php echo $recentlySet['ProductName'];?>">
            </div>
        </a>

        <div class="price-add-to-cart">
            <span class="price">
                <?php if($recentlySet['Discount'] !=0){ ?>

                 <span class="electro-price">
                    <ins>
                        <span class="amount">

                     <?php echo CURRENCY_SYMBOL.' '.$recentlySet['Price'];?></span></ins>
                                            <del><span class="amount"><?php echo CURRENCY_SYMBOL.' '.$recentlySet['Discount'];?></span></del>
                                        <span class="amount"> </span>
                </span>
              
                <?php }else{ ?>

                 <span class="electro-price">
                    <ins>
                        <span class="amount">

                     <?php echo CURRENCY_SYMBOL.' '.$recentlySet['Price'];?></span></ins>
                                        
                </span>

                
 
                <?php } ?>
            </span>
            <a rel="nofollow" href="<?php echo BASE_URL?>/add_to_cart.php?id=<?php echo $recentlySet['ID']; ?>&name=<?php echo add_to_cart_desh($recentlySet['ProductName']); ?>&quantity=1&upsell=2&options=0&url=<?php echo $_SERVER['REQUEST_URI']; ?>" class="button add_to_cart_button">Add to cart</a>
        </div><!-- /.price-add-to-cart -->

        <div class="hover-area">
            <div class="action-buttons">
                <a href="<?php echo BASE_URL; ?>/add_to_wishlist.php?id=<?php echo $recentlySet['ID']; ?>&name=<?php echo add_to_cart_desh($recentlySet['ProductName']); ?>&quantity=1&upsell=2&options=0&url=<?php echo $_SERVER['REQUEST_URI']; ?>" rel="nofollow" class="add_to_wishlist"> Wishlist</a>
            </div>
        </div>
    </div><!-- /.product-inner -->
</div><!-- /.product-outer -->
			</div><!-- /.products -->
<?php } ?>


				</div>
		</div>
	</div>
</section>
            </main><!-- #main -->
        </div><!-- #primary -->

    </div><!-- .container -->
</div><!-- #content -->


<?php require_once 'include/footer.php'; ?>
 <?php require_once 'include/foot.php'; ?>