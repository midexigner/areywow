  <section class="brands-carousel">
	<h2 class="sr-only">Brands Carousel</h2>
	<div class="container">
		<div id="owl-brands" class="owl-brands owl-carousel unicase-owl-carousel owl-outer-nav">


			<?php 
				
				while($brandRow = mysql_fetch_assoc($brand)){
			 ?>

			<div class="item">

				<a href="#">

					<figure>
						<figcaption class="text-overlay">
							<div class="info">
								<h4><?php echo $brandRow['BrandName'] ?></h4>
							</div><!-- /.info -->
						</figcaption>

						 <img src="assets/images/blank.gif" data-echo="<?php echo BASE_URL.'/admin/'.DIR_BRANDS_IMAGES.$brandRow['Image'] ?>" class="img-responsive" alt="">

					</figure>
				</a>
			</div><!-- /.item -->

<?php } ?>
			


		</div><!-- /.owl-carousel -->

	</div>
</section>

            <footer id="colophon" class="site-footer">
	<div class="footer-widgets">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-4 col-xs-12">
					<aside class="widget clearfix">
						<div class="body">
							<h4 class="widget-title">Featured Products</h4>
							<ul class="product_list_widget">
								<?php 
									
								 while($FootfeaturedSet= mysql_fetch_assoc($Footfeatured)) { 
								 	$Image=explode(',', $FootfeaturedSet["Image"]);
    								$img1 = $Image[0];
    								$img2 = (isset($Image[1]) ? $Image[1] : '');
								 	?>
								<li>
									<a href="<?php echo BASE_URL."/prod/".$FootfeaturedSet['URL']; ?>" title="<?php echo $FootsaleSet['ProductName'];?>">
										<img class="wp-post-image" data-echo="<?php echo BASE_URL.'/admin/'.DIR_PRODUCTS_IMAGES.$img1 ?>" src="<?php echo BASE_URL; ?>/assets/images/blank.gif" alt="<?php echo $FootfeaturedSet['ProductName'];?>">
										<span class="product-title"><?php echo strlen($FootfeaturedSet['ProductName']) > 40 ? substr($FootfeaturedSet['ProductName'],0,40)."..." : $FootfeaturedSet['ProductName']; ?></span>
									</a>
									 <?php if($FootfeaturedSet['Discount'] !=0){ ?>

                 <span class="electro-price">
                    <ins>
                    	<span class="amount">

                     <?php echo CURRENCY_SYMBOL.' '.$FootfeaturedSet['Price'];?></span></ins>
                                            <del><span class="amount"><?php echo CURRENCY_SYMBOL.' '.$FootfeaturedSet['Discount'];?></span></del>
                                        <span class="amount"> </span>
                </span>
              
                <?php }else{ ?>

                 <span class="electro-price">
                    <ins>
                    	<span class="amount">

                     <?php echo CURRENCY_SYMBOL.' '.$FootfeaturedSet['Price'];?></span></ins>
                                        
                </span>

                
 
                <?php } ?>
								</li>
<?php } ?>
								
							</ul>
						</div>
					</aside>
				</div>
				<div class="col-lg-4 col-md-4 col-xs-12">
					<aside class="widget clearfix">
						<div class="body"><h4 class="widget-title">Onsale Products</h4>
							<ul class="product_list_widget">
								<?php  while($FootsaleSet= mysql_fetch_assoc($Footsale)) { 
									$Image=explode(',', $FootsaleSet["Image"]);
    								$img1 = $Image[0];
    								$img2 = (isset($Image[1]) ? $Image[1] : '');
									?>
								<li>
									<a href="<?php echo BASE_URL."/prod/".$FootsaleSet['URL']; ?>" title="<?php echo $FootsaleSet['ProductName'];?>">
										<img class="wp-post-image" data-echo="<?php echo BASE_URL.'/admin/'.DIR_PRODUCTS_IMAGES.$img1 ?>" src="<?php echo BASE_URL; ?>/assets/images/blank.gif" alt="<?php echo $FootsaleSet['ProductName'];?>">
										<span class="product-title"><?php echo strlen($FootsaleSet['ProductName']) > 40 ? substr($FootsaleSet['ProductName'],0,40)."..." : $FootsaleSet['ProductName']; ?></span>
									</a>
									 <?php if($FootsaleSet['Discount'] !=0){ ?>

                 <span class="electro-price">
                    <ins>
                    	<span class="amount">

                     <?php echo CURRENCY_SYMBOL.' '.$FootsaleSet['Price'];?></span></ins>
                                            <del><span class="amount"><?php echo CURRENCY_SYMBOL.' '.$FootsaleSet['Discount'];?></span></del>
                                        <span class="amount"> </span>
                </span>
              
                <?php }else{ ?>

                 <span class="electro-price">
                    <ins>
                    	<span class="amount">

                     <?php echo CURRENCY_SYMBOL.' '.$FootsaleSet['Price'];?></span></ins>
                                        
                </span>

                
 
                <?php } ?>
								</li>
<?php } ?>
								
							</ul>
						</div>
					</aside>
				</div>
				<div class="col-lg-4 col-md-4 col-xs-12">
					<aside class="widget clearfix">
						<div class="body">
							<h4 class="widget-title">Top Rated Products</h4>
							<ul class="product_list_widget">
								<?php  while($FootRatedSet= mysql_fetch_assoc($FootRated)) { 
									$Image=explode(',', $FootRatedSet["Image"]);
    								$img1 = $Image[0];
    								$img2 = (isset($Image[1]) ? $Image[1] : '');
									?>
								<li>
									<a href="<?php echo BASE_URL."/prod/".$FootRatedSet['URL']; ?>" title="<?php echo $FootRatedSet['ProductName'];?>">
										<img class="wp-post-image" data-echo="<?php echo BASE_URL.'/admin/'.DIR_PRODUCTS_IMAGES.$img1 ?>" src="<?php echo BASE_URL; ?>/assets/images/blank.gif" alt="<?php echo $FootRatedSet['ProductName'];?>">
										<span class="product-title"><?php echo strlen($FootRatedSet['ProductName']) > 40 ? substr($FootRatedSet['ProductName'],0,40)."..." : $FootRatedSet['ProductName']; ?></span>
									</a>
									 <?php if($FootRatedSet['Discount'] !=0){ ?>

                 <span class="electro-price">
                    <ins>
                    	<span class="amount">

                     <?php echo CURRENCY_SYMBOL.' '.$FootRatedSet['Price'];?></span></ins>
                                            <del><span class="amount"><?php echo CURRENCY_SYMBOL.' '.$FootRatedSet['Discount'];?></span></del>
                                        <span class="amount"> </span>
                </span>
              
                <?php }else{ ?>

                 <span class="electro-price">
                    <ins>
                    	<span class="amount">

                     <?php echo CURRENCY_SYMBOL.' '.$FootRatedSet['Price'];?></span></ins>
                                        
                </span>

                
 
                <?php } ?>
								</li>
<?php } ?>
							</ul>
						</div>
					</aside>
				</div>
			</div>
		</div>
	</div>

	<div class="footer-newsletter">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-7">
					<h5 class="newsletter-title">Sign up to Newsletter</h5>
					<span class="newsletter-marketing-text">...and receive <strong>$20 coupon for first shopping</strong></span>
				</div>
				<div class="col-xs-12 col-sm-5">
					<form method="post" action="#newsletterdiv">
						<div class="input-group" >
							<input type="text" name="Email"  class="form-control" placeholder="Enter your email address">
							<span class="input-group-btn">
								<button class="btn btn-secondary" name="s" type="submit">Subscribe</button>
							</span>
						</div>
					</form>
					<?php echo $nlmsg; ?>
				</div>
			</div>
		</div>
	</div>

	<div class="footer-bottom-widgets">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-7 col-md-push-5">
					
					<div class="columns">
						<aside id="nav_menu-2" class="widget clearfix widget_nav_menu">
							<div class="body">
								<h4 class="widget-title">Find It Fast</h4>
								<div class="menu-footer-menu-1-container">
									<ul id="menu-footer-menu-1" class="menu">
										<?php 
										$i=1;
				while($footerRow = mysql_fetch_assoc($FootsLink)){
$i++;
				 if ($i >= 7) :
			
				 ?>
			
				<li class="menu-item"><a href="<?php echo BASE_URL.'/'.$footerRow['URL']; ?>"><?php echo $footerRow['Name']; ?></a> 
         </li>  
    					<?php endif; ?>
<?php } ?>
									
										
									</ul>
								</div>
							</div>
						</aside>
					</div><!-- /.columns -->

					<div class="columns">
						<aside id="nav_menu-3" class="widget clearfix widget_nav_menu">
							<div class="body">
								<h4 class="widget-title">&nbsp;</h4>
								<div class="menu-footer-menu-2-container">
									<ul id="menu-footer-menu-2" class="menu">
										<?php 
										$ii=1;
				while($footerRows = mysql_fetch_assoc($FootsLink)){
$ii++;
				// if ($ii >= 2) :
			
				 ?>
					
										<li class="menu-item"><a href="javascript:avoid(0)">Printers &#038; Ink</a></li>

			<?php //endif; ?>
<?php } ?>
														
										
									</ul>
								</div>
							</div>
						</aside>
					</div>
					<!--/.columns -->

					<div class="columns">
						<aside id="nav_menu-4" class="widget clearfix widget_nav_menu">
							<div class="body">
								<h4 class="widget-title">Customer Care</h4>
								<div class="menu-footer-menu-3-container">
									<ul id="menu-footer-menu-3" class="menu">
										<li class="menu-item"><a href="<?php echo BASE_URL; ?>/customerProfile.php">My Account</a></li>
										<li class="menu-item"><a href="<?php echo BASE_URL; ?>/pages/about-us">About Us</a></li>
										<li class="menu-item"><a href="<?php echo BASE_URL; ?>/pages/terms-and-conditions">Terms and Conditions</a></li>
										<li class="menu-item"><a href="<?php echo BASE_URL; ?>/wishlist.php">Wishlist</a></li>
										<li class="menu-item"><a href="<?php echo BASE_URL; ?>/pages/customer-service">Customer Service</a></li>
										<li class="menu-item"><a href="<?php echo BASE_URL; ?>/pages/returns-exchange">Returns/Exchange</a></li>
										<li class="menu-item"><a href="<?php echo BASE_URL; ?>/pages/faq">FAQs</a></li>
										<li class="menu-item"><a href="<?php echo BASE_URL; ?>/pages/contact-us">Contact Us</a></li>
									<!-- 	<li class="menu-item"><a href="hjavascript:avoid(0)">Product Support</a></li> -->
									</ul>
								</div>
							</div>
						</aside>
					</div><!-- /.columns -->

				</div><!-- /.col -->

				<div class="footer-contact col-xs-12 col-sm-12 col-md-5 col-md-pull-7">
					<div class="footer-logo">
						<a href="<?php echo BASE_URL; ?>" class="header-logo-link">
		
<img src="<?php echo BASE_URL.'/admin/'.DIR_LOGOS.$titleRow['Logo']; ?>" alt="<?php echo $titleRow['Name']; ?>" >
	</a>
					</div><!-- /.footer-contact -->

					<div class="footer-call-us">
						<div class="media">
							<span class="media-left call-us-icon media-middle"><i class="ec ec-support"></i></span>
							<div class="media-body">
								<span class="call-us-text">Got Questions ? Call us 24/7!</span>
								<span class="call-us-number"><?php echo $telephone['PhoneNumber']; ?></span>
							</div>
						</div>
					</div><!-- /.footer-call-us -->


					<div class="footer-address">
						<strong class="footer-address-title">Contact Info</strong>
						<address><?php echo $contactaddress['Address']; ?></address>
					</div><!-- /.footer-address -->

					<div class="footer-social-icons">
						<ul class="social-icons list-unstyled">
							<?php while($FootsocialSet= mysql_fetch_assoc($FooterSocial)){ ?>

							<li><a class="fa fa-<?php echo strtolower($FootsocialSet['Name']); ?>" href="#" target="_blank"></a></li>

							<?php } ?>
							
							</ul>
					</div>
				</div>

			</div>
		</div>
	</div>

	<div class="copyright-bar">
		<div class="container">
			<div class="pull-left flip copyright">&copy; <a href="<?php echo BASE_URL; ?>"><?php echo $titleRow['Name']; ?></a> - All Rights Reserved</div>
			<div class="pull-right flip payment">
				<div class="footer-payment-logo">
					<ul class="cash-card card-inline">
						<li class="card-item"><img src="<?php echo BASE_URL; ?>/assets/images/footer/payment-icon/1.png" alt="" width="52"></li>
						<li class="card-item"><img src="<?php echo BASE_URL; ?>/assets/images/footer/payment-icon/2.png" alt="" width="52"></li>
						<li class="card-item"><img src="<?php echo BASE_URL; ?>/assets/images/footer/payment-icon/3.png" alt="" width="52"></li>
						<li class="card-item"><img src="<?php echo BASE_URL; ?>/assets/images/footer/payment-icon/4.png" alt="" width="52"></li>
						<li class="card-item"><img src="<?php echo BASE_URL; ?>/assets/images/footer/payment-icon/5.png" alt="" width="52"></li>
					</ul>
				</div><!-- /.payment-methods -->
			</div>
		</div><!-- /.container -->
	</div><!-- /.copyright-bar -->
</footer><!-- #colophon -->

        </div><!-- #page -->

