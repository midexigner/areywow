 <header id="masthead" class="site-header header-v1">
    <div class="container">
        <div class="row">
<div class="header-logo">
	<a href="<?php echo BASE_URL; ?>" class="header-logo-link">
		
<img src="<?php echo BASE_URL.'/admin/'.DIR_LOGOS.$titleRow['Logo']; ?>" alt="<?php echo $titleRow['Name']; ?>" >
	</a>
</div>

            <form class="navbar-search" method="get" action="<?php echo BASE_URL; ?>/searchproducts.php?search=yes">
	<label class="sr-only screen-reader-text" for="search">Search for:</label>
	<div class="input-group">
		<input type="text" id="search" class="form-control search-field" dir="ltr" spellcheck="false" name="keyword" placeholder="Search for products" />
		<div class="input-group-addon search-categories">
			<select name='product_cat' id='product_cat' class='postform resizeselect' >
				<option value='0' selected='selected'>All Categories</option>
				 <?php
				$query="SELECT ID,Banner,Sort,Status,CategoryName,Parent,DATE_FORMAT(DateAdded, '%D %b %Y<br>%r') AS Added,
				DATE_FORMAT(DateModified, '%D %b %Y<br>%r') AS Updated
				FROM categories WHERE Parent = 0";

				$query .= " ORDER BY Sort";

				$result = mysql_query ($query) or die("Could not select because: ".mysql_error());
				while($row = mysql_fetch_array($result,MYSQL_ASSOC))
				{
?>

                        <option class="level-0" value="<?php echo $row["ID"]; ?>"> <?php echo $row["CategoryName"]; ?></option>


                  <?php } ?>
			</select>
		</div>
		<div class="input-group-btn">
			<input type="hidden" id="search-param" name="post_type" value="product" />
			<button type="submit" class="btn btn-secondary"><i class="ec ec-search"></i></button>
		</div>
	</div>
</form>
            <ul class="navbar-mini-cart navbar-nav animate-dropdown nav pull-right flip">
	<li class="nav-item dropdown">
		<a href="#" class="nav-link" data-toggle="dropdown">
			<i class="ec ec-shopping-bag"></i>
			<span class="cart-items-count count" id="cartCount"><?php if(isset($_SESSION['cart_items'])){
                    echo  count($_SESSION['cart_items']);
			} ?></span>
			<span class="cart-items-total-price total-price"><?php echo CURRENCY_SYMBOL; ?><span class="amount" id="cartAmount">0</span></span>
		</a>
		<ul class="dropdown-menu dropdown-menu-mini-cart">
			<li>
				<div class="widget_shopping_cart_content">

					<ul class="cart_list product_list_widget ">
                        <?php
                        $shippingAmount = 0;
                        $subtotal=0;
                        if(isset($_SESSION['cart_items']) && !empty($_SESSION['cart_items']))
                        {
                            foreach($_SESSION['cart_items'] as $cart_items)
                            {
                                $items = explode('-',$cart_items);
                                if($items[3] == 2)
                                {
                                    $query="SELECT Price,Discount,Shipping FROM products WHERE Status = 1 AND ID=".$items[0];
                                    $res = mysql_query($query) or die(mysql_error());
                                    $row=mysql_fetch_array($res);


                                    $Discount = $row["Discount"];
                                    $Price = $row["Price"];
                                    $Shipping = $row["Shipping"];

                                    ($Discount != 0 ? $subtotal = $subtotal + ($Discount * $items[2]) : $subtotal = $subtotal + ($Price * $items[2]));

                                    $shippingAmount = $shippingAmount + ($Shipping * $items[2]);

                                }
                                else
                                {
                                    $query="SELECT p.Price,p.Discount,p.Shipping,up.OfferPrice FROM upsaleproducts up LEFT JOIN products p ON up.ProductID = p.ID  WHERE p.Status = 1 AND p.ID=".$items[0];
                                    $res = mysql_query($query) or die(mysql_error());
                                    $row=mysql_fetch_array($res);


                                    $Discount = $row["Discount"];
                                    $Price = $row["Price"];
                                    $OfferPrice = $row["OfferPrice"];
                                    $Shipping = $row["Shipping"];

                                    if($OfferPrice == 0)
                                    {
                                        ($Discount != 0 ? $subtotal = $subtotal + ($Discount * $items[2]) : $subtotal = $subtotal + ($Price * $items[2]));
                                    }
                                    else if($OfferPrice != 0)
                                    {
                                        $subtotal = $subtotal + ($OfferPrice * $items[2]);
                                    }

                                    $shippingAmount = $shippingAmount + ($Shipping * $items[2]);
                                }
                            }
                        }

                        $total=0;
                        if(isset($_SESSION['cart_items']) && !empty($_SESSION['cart_items']))
                        {
                            foreach($_SESSION['cart_items'] as $cart_items)
                            {
                                $items = explode('-',$cart_items);
                                if(!empty($items[4]))
                                {
                                    $optionarray = explode(',',$items[4]);
                                    foreach($optionarray as $op)
                                    {
                                        $query="SELECT Increment FROM product_options WHERE ID=".$op;
                                        $res = mysql_query($query) or die(mysql_error());
                                        $number = mysql_num_rows($res);
                                        if($number != 0)
                                        {
                                            $row=mysql_fetch_array($res);

                                            $IncTotal = $row["Increment"];

                                            $total = $total + ($IncTotal * $items[2]);
                                        }
                                    }
                                }
                            }
                        }
                        ?>
                        <?php
                        $shippingAmount=0;
                        $taa = 0;
                        $grandtotal = $total + $subtotal + $shippingAmount;
                        if(isset($_SESSION['cart_items']) && !empty($_SESSION['cart_items']))
                        {

                            $_SESSION['Payment_Amount'] = round($grandtotal);

                        }
                        else
                        {
                            $_SESSION['Payment_Amount'] = 0;
                        }
                        if(isset($_SESSION['cart_items']) && !empty($_SESSION['cart_items']))
                        {
                            foreach($_SESSION['cart_items'] as $cart_items)
                            {
                                $items = explode('-',$cart_items);
                                if($items[3] == 2)
                                {
                                    $query="SELECT ID,ProductName,Image,Price,Discount,URL FROM products WHERE Status = 1 AND ID=".$items[0];
                                    $res = mysql_query($query) or die(mysql_error());
                                    $row=mysql_fetch_array($res);
                                    $Image=explode(',', $row["Image"]);
                                    $img1 = $Image[0];
                                    ?>
                                    <li class="mini_cart_item">
                                        <a title=<?php echo $row['ProductName']; ?>" class="remove" href="<?php echo BASE_URL; ?>/remove_from_cart.php?id=<?php echo $row['ID']; ?>&name=<?php echo $row['ProductName']; ?>&url=<?php echo $_SERVER['REQUEST_URI']; ?>">×</a>
                                        <a href="<?php echo BASE_URL.'/prod/'.$row['URL']; ?>">
                                            <img class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" src="<?php echo ($items[6] == 0 ? BASE_URL.'/admin/'.DIR_PRODUCTS_IMAGES.$img1 : $items[5]) ?>" alt="<?php echo $row['ProductName']; ?>"><?php echo $row['ProductName']; ?>&nbsp;
                                        </a>

                                        <span class="quantity"><?php echo $items[2]; ?> × <span class="amount"><?php

                                                $Discount = $row["Discount"];
                                                $Price = $row["Price"];
                                                $taa += $Discount;


                                                echo ($Discount != 0 ? CURRENCY_SYMBOL.$Discount : CURRENCY_SYMBOL.$Price);

                                                ?></span></span>
                                    </li>
                                <?php }else{
                                    $query="SELECT p.ID,p.ProductName,p.ProductNameArabic,p.Image,p.Price,p.Discount,p.URL,up.OfferPrice FROM upsaleproducts up LEFT JOIN products p ON up.ProductID = p.ID  WHERE p.Status = 1 AND p.ID=".$items[0];
                                    $res = mysql_query($query) or die(mysql_error());
                                    $row=mysql_fetch_array($res);
                                    $Image=explode(',', $row["Image"]);
                                    $img1 = $Image[0];
                                    ?>
                                    ?>

                                    <li class="mini_cart_item">
                                        <a title=<?php echo $row['ProductName']; ?>" class="remove" href="<?php echo BASE_URL; ?>/remove_from_cart.php?id=<?php echo $row['ID']; ?>&name=<?php echo $row['ProductName']; ?>&url=<?php echo $_SERVER['REQUEST_URI']; ?>">×</a>
                                        <a href="<?php echo BASE_URL.'/'.$row['URL']; ?>">
                                            <img class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" src="<?php echo ($items[6] == 0 ? BASE_URL.'/admin/'.DIR_PRODUCTS_IMAGES.$img1 : $items[5]) ?>" alt="<?php echo $row['ProductName']; ?>"><?php echo $row['ProductName']; ?>&nbsp;
                                        </a>

                                        <span class="quantity"><?php echo $items[2]; ?> × <span class="amount"><?php

                                                $Discount = $row["Discount"];
                                                $Price = $row["Price"];
                                                $taa += $Discount;


                                                echo ($Discount != 0 ? CURRENCY_SYMBOL.$Discount : CURRENCY_SYMBOL.$Price);

                                                ?></span></span>
                                    </li>


                                <?php }}}else
                        {
                            ?>
                            <li class="mini_cart_item">
                                Your Cart is Empty!
                            </li>

                        <?php } ?>




					</ul><!-- end product list -->

<?php if(isset($_SESSION['cart_items']) && !empty($_SESSION['cart_items']))
				{ ?>
					<p class="total"><strong>Subtotal:</strong> <span class="amount"><?php echo $taa; ?></span></p>


					<p class="buttons">
						<a class="button wc-forward" href="<?php echo BASE_URL; ?>/cart.php">View Cart</a>
						<a class="button checkout wc-forward" href="<?php echo BASE_URL; ?>/checkout.php">Checkout</a>
					</p>
<?php } ?>

				</div>
			</li>
		</ul>
	</li>
</ul>

<!-- <ul class="navbar-wishlist nav navbar-nav pull-right flip">
	<li class="nav-item">
		<a href="wishlist.html" class="nav-link"><i class="ec ec-favorites"></i></a>
	</li>
</ul>
<ul class="navbar-compare nav navbar-nav pull-right flip">
	<li class="nav-item">
		<a href="compare.html" class="nav-link"><i class="ec ec-compare"></i></a>
	</li>
</ul> -->
        </div><!-- /.row -->

        <div class="row">
	        <div class="col-xs-12 col-lg-3">
	        	<nav>
	<ul class="list-group vertical-menu yamm make-absolute">
		<li class="list-group-item"><span><i class="fa fa-list-ul"></i> All Categories</span></li>

<?php 
$catQ= "SELECT ID,CategoryName,URL,Banner FROM categories WHERE Status = 1 AND Parent = 0 ORDER BY Sort  LIMIT 12";
$catR = mysql_query($catQ);

 while($catRow = mysql_fetch_assoc($catR)){
$id = $catRow['ID'];	
$subcatQ = "SELECT ID,CategoryName,URL,Banner FROM categories WHERE Status = 1 AND Parent = '$id'   ORDER BY Sort" ;
$subcatR = mysql_query($subcatQ);
$subcatCount = mysql_num_rows($subcatR);

 	?>
		<li id="menu-item-2695" class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdow">
			<a title="<?php echo $catRow['CategoryName']; ?>"<?php if( $subcatCount > 0){ ?> data-hover="dropdown"  data-toggle="dropdown"   class="dropdown-toggle" aria-haspopup="true" <?php } ?> href="<?php echo BASE_URL.'/'.$catRow['URL'] ?>"><?php echo $catRow['CategoryName']; ?></a>
			<?php if( $subcatCount > 0){ ?>
            <ul role="menu" class=" dropdown-menu">
    <li class="menu-item animate-dropdown menu-item-object-static_block">
        <div class="yamm-content">
            <div class="row">
                 <?php while($rowCat = mysql_fetch_assoc($subcatR )  ){ ?>
                <div class="col-sm-4">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="wpb_text_column wpb_content_element ">
                                <div class="wpb_wrapper">
                                   
                                    <ul>
                                         
                                        <li class="nav-title"><a title="<?php echo $rowCat['CategoryName']; ?>" href="<?php echo BASE_URL.'/'.$catRow['URL'].'/'.$rowCat['URL'] ?>"><?php echo $rowCat['CategoryName']; ?></a></li>
                                      
<?php
$childId = $rowCat['ID'];
$subChildcatQ = "SELECT ID,CategoryName,URL,Banner FROM categories WHERE Status = 1 AND Parent = '$childId'   ORDER BY Sort" ;
$subChildcatR = mysql_query($subChildcatQ);
$subChildcatCount = mysql_num_rows($subChildcatR);
?>
 <?php while($rowChildCat = mysql_fetch_assoc($subChildcatR )  ){ ?>
                                        <li><a href="<?php echo BASE_URL.'/subcat/'.$rowChildCat['URL'] ?>"><?php echo $rowChildCat['CategoryName']; ?></a></li>
                                    
                             <?php } ?>          
                                        
                                    </ul>
                                     

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 <?php } ?>
                <!--  <div class="col-sm-4">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="wpb_text_column wpb_content_element ">
                                <div class="wpb_wrapper">
                                    <ul>
                                        <li class="nav-title">Computers &amp; Accessories</li>
                                        <li><a href="#">All Computers &amp; Accessories</a></li>
                                        <li><a href="#">Laptops, Desktops &amp; Monitors</a></li>
                                        <li><a href="#">Pen Drives, Hard Drives &amp; Memory Cards</a></li>
                                        <li><a href="#">Printers &amp; Ink</a></li>
                                        <li><a href="#">Networking &amp; Internet Devices</a></li>
                                        <li><a href="#">Computer Accessories</a></li>
                                        <li><a href="#">Software</a></li>
                                        <li class="nav-divider"></li>
                                        <li><a href="#"><span class="nav-text">All Electronics</span><span class="nav-subtext">Discover more products</span></a></li>
                                    </ul>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="wpb_text_column wpb_content_element ">
                                <div class="wpb_wrapper">
                                    <ul>
                                        <li class="nav-title">Office &amp; Stationery</li>
                                        <li><a href="#">All Office &amp; Stationery</a></li>
                                        <li><a href="#">Pens &amp; Writing</a></li>
                                    </ul>

                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </li>
</ul>
			<!-- <ul role="menu" class=" dropdown-menu">
                <?php while($rowCat = mysql_fetch_assoc($subcatR )  ){ ?>
                <li class="menu-item animate-dropdown"><a title="<?php echo $rowCat['CategoryName']; ?>" href="<?php echo BASE_URL.'/'.$catRow['URL'].'/'.$rowCat['URL'] ?>"><?php echo $rowCat['CategoryName']; ?></a></li>
                <?php } ?>
                
            </ul>  -->
			<?php } ?>
		</li>
		<?php } ?>
	</ul>
</nav>
	        </div>

	<!--         <div class="col-xs-12 col-lg-9">
            	<nav>
	<ul id="menu-secondary-nav" class="secondary-nav">
		<li class="highlight menu-item"><a href="home-v2.html">Super Deals</a></li>
		<li class="menu-item"><a href="home-v3.html">Featured Brands</a></li>
		<li class="menu-item"><a href="home-v3-full-color.html">Trending Styles</a></li>
		<li class="menu-item"><a href="blog-v1.html">Gift Cards</a></li>
		<li class="pull-right menu-item"><a href="blog-v2.html">Free Shipping on Orders $50+</a></li>
	</ul>
</nav>
            </div>
        </div> -->
    </div>
</header><!-- #masthead -->

<?php 
if(isset($_SESSION['CartMessage']))
{
	echo $_SESSION['CartMessage'];
	unset($_SESSION['CartMessage']);
}
?>