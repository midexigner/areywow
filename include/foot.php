
        <script type="text/javascript" src="<?php echo BASE_URL; ?>/assets/js/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_URL; ?>/assets/js/tether.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_URL; ?>/assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_URL; ?>/assets/js/bootstrap-hover-dropdown.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_URL; ?>/assets/js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_URL; ?>/assets/js/echo.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_URL; ?>/assets/js/wow.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_URL; ?>/assets/js/jquery.easing.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_URL; ?>/assets/js/jquery.waypoints.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_URL; ?>/assets/js/electro.js"></script>

        <!-- For demo purposes – can be removed on production -->

       <script type="text/javascript">
            jQuery('#cartAmount').html(<?php echo $taa; ?>);

            jQuery(document).ready(function(){
                jQuery("#msgcart").slideUp(3000);
});
        </script>

        <?php if(isset($validate) == 1){  ?>

            <script type="text/javascript">

                function optchange(){

                    var abc = document.getElementById('opt_req').value;

                    if(abc != 0){

                        jQuery('#add_to').prop("disabled", false);
                        jQuery("#reqlab").removeClass('display').addClass('hide');

                    }else{
                        jQuery('#add_to').prop("disabled", true);
                        jQuery("#reqlab").addClass('display').removeClass('hide');
                    }

                }
            </script>

        <?php } ?>

    </body>
</html>
