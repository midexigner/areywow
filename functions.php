<?php
// Time Zone Setting
date_default_timezone_set("Asia/Karachi");
// front-end BASE_URL Setting
define('BASE_URL', "http://localhost/king-chemical");
#define('BASE_URL','http://localhost:8000/');
#define('BASE_URL', "http://192.168.10.54/king-chemical");
define('BASE_URL_STYLES_PLUGIN',BASE_URL ."/assets/");
define('BASE_URL_SCRIPTS_PLUGIN',BASE_URL ."/assets/js/vendor/");
define('BASE_URL_STYLES',BASE_URL ."/assets/css/");
define('BASE_URL_IMAGES', BASE_URL."/assets/img/");
define('IMAGES', BASE_URL."/assets/images/");
define('BASE_URL_SCRIPTS', BASE_URL."/assets/js/");
// general function
function redirect_to($new_location){header("Location:" . $new_location);exit;}
function pretty_date($date){return date("M d, Y h:i A",strtotime($date));}

// HTML
if (!function_exists('html')){
function html($arg){
   $doctypes = array(
					'xhtml11'=> '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">',
					'xhtml1-strict'	=> '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">',
					'xhtml1-trans'	=> '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">',
					'xhtml1-frame'	=> '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">',
					'html5'			=> '<!DOCTYPE html>',
					'html4-strict'	=> '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">',
					'html4-trans'	=> '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">',
					'html4-frame'	=> '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">'
					);

    return $doctypes[$arg];
    
}
}

// Special Character 
if ( ! function_exists('esc_html')){
function esc_html($text){
	return	htmlspecialchars($text,ENT_QUOTES,'UTF-8');
}
}

// Limit String
if ( ! function_exists('except')){
function except($text, $min_length, $max_length){

echo substr($text, $min_length,$max_length);

}
}

/* front-end*/
if ( ! function_exists('style_plugin')){

function style_plugin($styles){
    
    return "<link rel='stylesheet' href=".BASE_URL_STYLES_PLUGIN.$styles.'.css'." media=\"all\">";
}
}
if ( ! function_exists('styles')){

function styles($styles){
    
    return "<link rel='stylesheet' href=".BASE_URL.'/'.$styles.'.css'." media=\"all\">";
}
}

if ( ! function_exists('style')){

function style($styles){
    
    return "<link rel='stylesheet' href=".BASE_URL_STYLES.$styles.'.css'." media=\"all\">";
}
}

if ( ! function_exists('script')){
function script($scripts){
    return "<script src=".BASE_URL_SCRIPTS.$scripts.'.js'."></script>";
}
}

if ( ! function_exists('script_plugin')){
function script_plugin($scripts){
    return "<script src=".BASE_URL_SCRIPTS_PLUGIN.$scripts.'.js'."></script>";
}
}

// Session MSG
if(!function_exists('message')){
function message(){
if(isset($_SESSION["message"])){
	$output = "<div class=\"form-message correct\" id=\"msg\">";
$output .= htmlentities($_SESSION["message"]);
$output .= "</div>";

// clear message after use
$_SESSION["message"] = null;
return $output;
}
}
}


// Print_r Debug
if(!function_exists('pr')){
	function pr($debug){
		echo "<pre>";
		print_r($debug);
		echo "</pre>";
		
	}
}
// var_dump Debug
if(!function_exists('vd')){
	function vd($debug){
		echo "<pre>";
		var_dump($debug);
		echo "</pre>";
		
	}
}

// Active class function
if(!function_exists('selected')){
function selected($value1, $value2,$return){
	if($value1 == $value2){
		echo $return;
	}
}
}



?>