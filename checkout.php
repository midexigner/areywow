<?php
require_once 'admin/Common.php';
require_once 'functions.php';
require_once 'include/head.php';
$email="";
$password="";
$msg1 = "";
$msg2 = "";
$msg3 = "";
if(isset($_POST["action"]) && $_POST["action"] == "submit_form")
{
    if(isset($_POST["email"]))
        $email=trim($_POST["email"]);
    if(isset($_POST["password"]))
        $password=trim($_POST["password"]);

    if ($email=="")
        $msg1 = '<p style="color:red">Please Enter EmailAddress.</p>';
    if ($password=="")
        $msg2 = '<p style="color:red">Please Enter Password.</p>';

    if($msg1=='' && $msg2=='')
    {
        $query="SELECT ID,FirstName,LastName,Password,Country FROM website_users WHERE Status = 1 AND Email='".dbinput($email)."'";
        $result = mysql_query ($query) or die(mysql_error());
        $num = mysql_num_rows($result);


        if($num==0)
        {
            $_SESSION["LoginCustomer"]=false;
            $_SESSION["CustomerID"]='';
            $_SESSION["CustomerFullName"]='';
            $_SESSION["Country"]='';
            $_SESSION["Shipping"]='';
            $msg3 = '<p style="color:red">Invalid Email/Password.</p>';
        }
        else
        {
            $row = mysql_fetch_array($result,MYSQL_ASSOC);
            if(dboutput($row["Password"]) == $password)
            {
                $_SESSION["LoginCustomer"]=true;
                $_SESSION["CustomerID"]=dboutput($row["ID"]);
                $_SESSION["CustomerFullName"]=dboutput($row["FirstName"]) .' '. dboutput($row["LastName"]);
                $_SESSION["Country"]=dboutput($row["Country"]);

                $query2="SELECT Percentage FROM shipping_rates WHERE CountryCode='".$row["Country"]."'";
                $result2 = mysql_query ($query2) or die(mysql_error());
                $num2 = mysql_num_rows($result2);
                if($num2 > 0)
                {
                    $row2 = mysql_fetch_array($result2,MYSQL_ASSOC);
                    $_SESSION["Shipping"]=dboutput($row2["Percentage"]);
                }
                else
                {
                    $query3="SELECT Percentage FROM other_countries_shipping WHERE ID=1";
                    $result3 = mysql_query ($query3) or die(mysql_error());
                    $num3 = mysql_num_rows($result3);
                    if($num3 == 1)
                    {
                        $row3 = mysql_fetch_array($result3,MYSQL_ASSOC);
                        $_SESSION["Shipping"]=dboutput($row3["Percentage"]);
                    }
                }


                redirect(BASE_URL."/checkout.php");
            }
            else
            {
                $_SESSION["LoginCustomer"]=false;
                $_SESSION["CustomerID"]='';
                $_SESSION["CustomerFullName"]='';
                $_SESSION["Country"]='';
                $_SESSION["Shipping"]='';
                $msg3 = '<p style="color:red">Invalid Email/Password.</p>';
            }
        }
    }
}

require_once 'include/headerpartial.php';
?>

    <div id="content" class="site-content" tabindex="-1">
    <div class="container">

    <nav class="woocommerce-breadcrumb"><a href="<?php echo BASE_URL; ?>">Home</a><span class="delimiter"><i class="fa fa-angle-right"></i></span>Checkout</nav>

    <div id="primary" class="content-area">
    <main id="main" class="site-main">
    <article class="page type-page status-publish hentry">
    <header class="entry-header"><h1 itemprop="name" class="entry-title">Checkout</h1></header><!-- .entry-header -->

        <div id="MainMenu" class="accordians">
        <div class="list-group panel">
            <h4 class="panel-title">
                <a href="#checkoutmethod" class="list-group-item list-group-item-info" data-toggle="collapse" data-parent="#MainMenu">
                    <span class="number">STEP 1:</span>Login</a>
            </h4>
            <?php if(!isset($_SESSION['LoginCustomer']) || $_SESSION['LoginCustomer']==false){ ?>
                <div class="collapse common <?php if(!isset($_SESSION['LoginCustomer']) || $_SESSION['LoginCustomer']==false){ echo 'in';} ?>" id="checkoutmethod">

                    <div class="row">
                        <br class="clear" />

                        <div class="col-sm-6">
                            <div class="well">
                                <h2>New Customer</h2>
                                <p><strong>Register Account</strong></p>
                                <p>By creating an account you will be able to shop faster, be up to date on an order's status, and keep track of the orders you have previously made.</p>
                                <a href="<?php echo BASE_URL; ?>/register.php" class="btn btn-primary" style="color:#fff;">Continue</a>

                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="well">
                                <h2>Returning Customer</h2>
                                <p><strong>I am a returning customer</strong></p>
                                <?php
                                if(isset($msg3))
                                    echo $msg3
                                ?>
                                <form action="<?php echo $_SERVER["PHP_SELF"];?>" method="post" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label class="control-label" for="input-email">EmailAddress</label>
                                        <?php if(isset($msg1)){echo $msg1;}?>
                                        <input type="text" name="email" value="<?php echo $email; ?>" placeholder="E-Mail Address" id="input-email" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="input-password">Password</label>
                                        <?php if(isset($msg2)){echo $msg2;}?>
                                        <input type="password" name="password" value="" placeholder="Password" id="input-password" class="form-control">
                                        <!--<a href="">Forgotten Password</a>--></div>
                                    <input type="submit" value="Login" class="btn btn-primary">
                                    <input type="hidden" name="action" value="submit_form" />
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            <?php } ?>

            <div class="clearfix"></div>

            <h4 class="panel-title"><a href="#shippinginfo" class="list-group-item list-group-item-info" data-toggle="collapse" data-parent="#MainMenu">
                    <span class="number">STEP 2:</span>Shipping Information</a></h4>
            <?php if(isset($_SESSION['LoginCustomer']) && $_SESSION['LoginCustomer']==true){ ?>
                <div class="collapse common <?php if(isset($_SESSION['LoginCustomer']) && $_SESSION['LoginCustomer']==true){ echo 'in';} ?>" id="shippinginfo">
                    <div class="well">

                        <?php
                        $query="SELECT Phone,PostCode,Country,Region,City,Address
		FROM website_users WHERE ID <>0 AND Status = 1 AND ID=".$_SESSION['CustomerID'];
                        $r = mysql_query($query) or die(mysql_error());
                        $n = mysql_num_rows($r);
                        if($n == 1)
                        {
                            $row =  mysql_fetch_array($r);
                            ?>
                            <p><b>Name:</b> <?php echo $_SESSION['CustomerFullName']; ?></p>
                            <p><b>Address:</b> <?php echo $row['Address'].', '.$row['City'].', '.$row['Region'].', '; getCountryByCode($row['Country']); echo ', '.$row['PostCode'];?></p>
                            <p><b>Phone:</b> <?php echo $row['Phone']; ?></p>

                            <?php
                        }
                        ?>
                    </div>





                </div>
            <?php } ?>
            <h4 class="panel-title"><a href="#paymentinfo" class="list-group-item list-group-item-info" data-toggle="collapse" data-parent="#MainMenu"><span class="number">STEP 3:</span>Payment</a></h4>
            <?php if(isset($_SESSION['LoginCustomer']) && $_SESSION['LoginCustomer']==true){ ?>
                <div class="collapse" id="paymentinfo">
                    <!--<form action="PayNow.php" method="POST">-->
                    <div class="row">
                        <div class="col-md-4">
                            <ul class="no-list-icon">
                                <h5><li><label for="COD"><input checked type="radio" id="COD" name="COD" value="COC"> Cash On Delivery</label></li></h5>
                                <!--					<h3><li><label for="Paypal"><input checked type="radio" id="Paypal" name="paymentType" value="Paypal"> Paypal</label></li></h3> -->
                            </ul>
                        </div>

                        <div class="col-md-8" style="margin-top:15px;">
                            <div class="buttons">
                                <div class="col">
                                    <div class="col">
                                        <div class="pull-right">
                                            <form action='<?php echo BASE_URL; ?>/OrderSuccess.php' METHOD='POST'>
                                                <div class="buttons-cart">
                                                    <input type='submit' name='PaymentNow' value="Confirm Order"/>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--			<div class="col-md-8" style="margin-top:15px;">
                                            <div class="buttons">
                                              <div class="col">
                                                  <div class="col">
                                                      <div class="pull-right">
                                                        <form action='expresscheckout.php' METHOD='POST'>
                                                        <input type='image' name='submit' src='https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif' border='0' align='top' alt='Check out with PayPal'/>
                                                        </form>
                                                      </div>
                                                   </div>
                                              </div>
                                            </div>
                                    </div> -->
                        <!--</form> -->

                    </div>
                </div>
            <?php } ?>



        </div>
    </div>


</article>
    </main>
    </div>
    </div>
    </div>

<?php require_once 'include/footer.php'; ?>
<?php require_once 'include/foot.php'; ?>