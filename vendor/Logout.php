<?php
	session_start();
	$_SESSION["LoginVendor"]=false;
	$_SESSION["VendorID"]='';
	$_SESSION["VendorFullName"]='';
	$_SESSION["Country"]='';
	$_SESSION["Shipping"]='';
	$_SESSION["Email"]='';
	unset($_SESSION["LoginVendor"]);
	unset($_SESSION["VendorID"]);
	unset($_SESSION["VendorFullName"]);
	unset($_SESSION["Country"]);
	unset($_SESSION["Shipping"]);
	unset($_SESSION["Email"]);
	header("Location: login.php");
?>