<?php 
require_once '../admin/Common.php'; 
require_once '../functions.php';
include("CheckVendorLogin.php");?>
<?php
	$action = "";
	$DeleteID = 0;
	$CopyID = 0;
	$msg = "";
	if(isset($_POST["action"]))
		$action = $_POST["action"];
	if(isset($_POST["DeleteID"]))
		$DeleteID = $_POST["DeleteID"];
	if(isset($_POST["CopyID"]))
		$CopyID = $_POST["CopyID"];
if($action == "delete")
	{
		if(isset($_REQUEST["ids"]) && is_array($_REQUEST["ids"]))
		{
			foreach($_REQUEST["ids"] as $BID)
			{
				
			mysql_query("DELETE FROM orders WHERE ID IN (" . $BID . ")") or die (mysql_error());
			
			}
						
			$_SESSION["msg"]='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Delete All selected Orders.</b>
			</div>';
			redirect("Orders.php");
		
		}
		else
		{
			$msg='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Please select order to delete.</b>
			</div>';
		}
	}
	
if($action == "SingleDelete")
	{
		if(isset($_REQUEST["DeleteID"]))
		{
				
			mysql_query("DELETE FROM orders WHERE ID IN (" . $DeleteID . ")") or die (mysql_error());
			
						
			$_SESSION["msg"]='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Delete All selected Orders.</b>
			</div>';
			redirect("Orders.php");
		
		}
		else
		{
			$msg='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Please select order to delete.</b>
			</div>';
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Orders</title>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<!-- bootstrap 3.0.2 -->
<!-- jQuery 2.0.2 -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="../admin/js/ui.datepicker.js" type="text/javascript"></script>
<script language="javascript" src="../admin/js/local_clock.js" type="text/javascript"></script>
<script language="javascript" src="../admin/js/functions.js" type="text/javascript"></script>
<link href="../admin/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- font Awesome -->
<link href="../admin/../admin/../admin/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Ionicons -->
<link href="../admin/../admin/css/ionicons.min.css" rel="stylesheet" type="text/css" />
<link href="../admin/css/datepicker.css" rel="stylesheet" type="text/css" />
<!-- DATA TABLES -->
<link href="../admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- Theme style -->
<link href="../admin/css/AdminLTE.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

</head>
<body class="skin-blue">
<!-- header logo: style can be found in header.less -->
<?php
	include_once("Header.php");
?>
<div class="wrapper row-offcanvas row-offcanvas-left">
  <!-- Left side column. contains the logo and sidebar -->
  <?php
		include_once("Sidebar.php");

				$prev="";
				$next="";
				$nav="";
				$rowsPerPage=100;
				$pageNum=1;
				
				$DFrom="";
				$DTo="";
				$DateFrom="";
				$DateTo="";
				
				$Keywords="";
				
				$SortField=2;
				$SortType="ASC";
					
				$_SORT_FIELDS = array("DateAdded");
				
				if(isset($_REQUEST["Keywords"]))
					$Keywords = trim($_REQUEST["Keywords"]);

				if(isset($_REQUEST["PageIndex"]) && ctype_digit(trim($_REQUEST["PageIndex"])))
					$pageNum=trim($_REQUEST["PageIndex"]);

				$offset = ($pageNum - 1) * $rowsPerPage;
				$limit=" Limit ".$offset.", ".$rowsPerPage;
				
				/*$query="SELECT o.ID,o.Verified,u.FirstName,u.LastName,w.FirstName AS wf,w.LastName AS wl,w.Phone,o.PaymentMethod,o.ProcedureID,o.ReShipping,o.PickUp,DATE_FORMAT(o.DateAdded, '%D %b %Y<br>%r') AS Added, 
				DATE_FORMAT(o.DateModified, '%D %b %Y<br>%r') AS Updated
				FROM orders o LEFT JOIN users u ON o.PerformedBy = u.ID LEFT JOIN website_users w ON o.CustomerID = w.ID WHERE o.ID <>0 ";*/
				$vendorID = $_SESSION['VendorID'];
				$query = "SELECT o.ID,o.Verified,od.ID as OrderDetailsId,od.ProductID,p.vendorID,v.FirstName,v.LastName,w.FirstName AS wf,w.LastName AS wl,w.Phone,o.PaymentMethod,o.ProcedureID,o.ReShipping,o.PickUp,DATE_FORMAT(o.DateAdded, '%D %b %Y<br>%r') AS Added, 
				DATE_FORMAT(o.DateModified, '%D %b %Y<br>%r') AS Updated
				FROM orders o LEFT JOIN users u ON o.PerformedBy = u.ID LEFT JOIN orders_details od ON o.ID = od.OrderID  LEFT JOIN products p ON od.ProductID = p.ID LEFT JOIN website_users w ON o.CustomerID = w.ID Left join vendor v On p.vendorID = v.ID WHERE p.vendorID = $vendorID";
				if($Keywords != "")
					$query .= " AND (o.ID LIKE '%" . dbinput($Keywords) . "%')";
				$query .= " ORDER BY o.ID DESC";
				
				
				
				$result = mysql_query ($query.$limit) or die("Could not select because: ".mysql_error()); 
				$num = mysql_num_rows($result);
				
				$r = mysql_query ($query) or die("Could not select because: ".mysql_error());
				$self = $_SERVER['PHP_SELF'];

				$maxRow = mysql_num_rows($r);
				if($maxRow > 0)
				{ 
					$maxPage = ceil($maxRow/$rowsPerPage);
					$nav  = '';
					if($maxPage>1)
					for($page = 1; $page <= $maxPage; $page++)
					{
						
					   if ($page == $pageNum)
						  $nav .= "&nbsp;<li class=\"active\"><a href='#'>$page</a></li >"; 
					   else
						  $nav .= "&nbsp;<li ><a href=\"$self?PageIndex=$page&rpp=$rowsPerPage&Keywords=$Keywords&SortField=$SortField&SortType=$SortType&DFrom=$DFrom&DTo=$DTo\" class=\"lnk\">$page</a> </li>";
					}
					
					if ($pageNum > 1)
					{
						$page  = $pageNum - 1;
						$prev  = "&nbsp;<a href=\"$self?PageIndex=$page&rpp=$rowsPerPage&Keywords=$Keywords&SortField=$SortField&SortType=$SortType&DFrom=$DFrom&DTo=$DTo\" class=\"lnk\">Previous</a> ";
						$first = "&nbsp;<a href=\"$self?PageIndex=1&rpp=$rowsPerPage&Keywords=$Keywords&SortField=$SortField&SortType=$SortType&DFrom=$DFrom&DTo=$DTo\" class=\"lnk\">First</a> ";
					} 
					else
					{
					   $prev  = ''; // we're on page one, don't print previous link
					   $first = '&nbsp;First'; // nor the first page link
					}
					
					if ($pageNum < $maxPage)
					{
						$page = $pageNum + 1;
						$next = "&nbsp;<a href=\"$self?PageIndex=$page&rpp=$rowsPerPage&Keywords=$Keywords&SortField=$SortField&SortType=$SortType&DFrom=$DFrom&DTo=$DTo\" class=\"lnk\">Next</a> ";
						$last = "&nbsp;<a href=\"$self?PageIndex=$maxPage&rpp=$rowsPerPage&Keywords=$Keywords&SortField=$SortField&SortType=$SortType&DFrom=$DFrom&DTo=$DTo\" class=\"lnk\">Last Page</a> ";
					} 
					else
					{
					   $next = "";
					   $last = '&nbsp;Last'; // nor the last page link
					}
				}
		?>
  <!-- Right side column. Contains the navbar and content of the page -->
  <aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> Orders <small></small> </h1>
      <ol class="breadcrumb">
        <li><a href="Dashboard.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Orders</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Orders</h3>
			  <form action="<?php echo $self;?>" method="post" style="clear:both; margin:0 5px">
                    <div class="input-group input-group-sm">
                        <input value="<?php if(isset($_REQUEST["Keywords"])){echo trim($_REQUEST["Keywords"]);}?>" type="text" name="Keywords" class="form-control span2">
                        <span class="input-group-btn">
                        <button type="submit" class="btn btn-info btn-flat">Go!</button>
                        </span>
                    </div>
                </form>
            </div>
           <!--  <a onClick="javascript:doDelete()" class="btn btn-danger active" style="float: right; margin-bottom: 10px;"><i class="fa fa-trash"></i> Bulk Delete</a> -->
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <?php
			  	echo $msg;
				if(isset($_SESSION["msg"]) && $_SESSION["msg"]!="")
				{
					echo $_SESSION["msg"];
					$_SESSION["msg"]="";
				}
			?>
              <form id="frmPages"  method="post" action="<?php echo $self;?>">
                <input type="hidden" name="rpp" value="<?php echo $rowsPerPage; ?>" />
                <input type="hidden" name="PageIndex" value="<?php echo $pageNum; ?>" />
                <input type="hidden" id="action" name="action" value="" />
                <table id="dataTable" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th style="text-align:center"><input type="checkbox" name="chkAll" class="checkUncheckAll"></th>
                      <th>Order ID</th>
					  <th>Customer Info</th>
					  <th>Payment Method</th>
					  <th>Order Time</th>
					  <th>Pickup From Store</th>
					  <th>Is Verified</th>
					  <th>Is ReShipping</th>
                      <th>Current Status</th>
                      <th>Updated</th>
					  <th>Perform By</th>
                      <th class="sorting"></th>
                    </tr>
                  </thead>
				  
                  <tbody>
                    <?php
			if($maxRow==0)
			{
		?>
                    <tr class="noPrint">
                      <td colspan="12" align="center" class="error"><b>No Order listed.</b></td>
                    </tr>
                    <?php 
			}
			else
			{
				?>
				
				<?php
				while($row = mysql_fetch_array($result,MYSQL_ASSOC))
				{
				?>
                    <tr>
                      <td align="center" class="noPrint"><input class="chkIds" type="checkbox" name="ids[]" value="<?php echo $row["ID"]; ?>" />
                        <input type="hidden" name="ids1[]" value="<?php echo $row["ID"]; ?>"></td>
                      <td><a href='EditOrder.php?ID=<?php echo $row["ID"]; ?>'' state="1" class="main-page<?php echo $row["ID"]; ?>">Order# :<?php echo dboutput($row["ID"]); ?></a></td>
					  <td><?php echo $row["wf"].' '.$row["wl"].' ('.$row["Phone"].')'; ?></td>
					   <td><?php echo dboutput($row["PaymentMethod"]); ?></td>
					   <td><?php echo $row["Added"]; ?></td>
						<td><?php if(dboutput($row["PickUp"])=='1'){echo '<i class="fa fa-fw fa-check-circle"></i>';}else{echo '<i class="fa fa-fw fa-times-circle"></i>';} ?></td>
						<td><?php if(dboutput($row["Verified"])=='1'){echo '<i class="fa fa-fw fa-check-circle"></i>';}else{echo '<i class="fa fa-fw fa-times-circle"></i>';} ?></td>
						<td><?php if(dboutput($row["ReShipping"])=='1'){echo '<i class="fa fa-fw fa-check-circle"></i>';}else{echo '<i class="fa fa-fw fa-times-circle"></i>';} ?></td>
                      <td><?php if(dboutput($row["ProcedureID"])=='0'){echo 'Order Pending';}else if(dboutput($row["ProcedureID"])=='1'){echo 'Order Cancled';}else if(dboutput($row["ProcedureID"])=='2'){echo 'Ready To Dispatch';}else if(dboutput($row["ProcedureID"])=='3'){echo 'Delivered to Courier';}else if(dboutput($row["ProcedureID"])=='4'){echo 'Successfully Delivered';}else if(dboutput($row["ProcedureID"])=='5'){echo 'Order Return';} ?></td>
                      <td><?php echo $row["Updated"]; ?></td>
					  <td><?php echo $row["FirstName"]. ' ' .$row["LastName"]; ?></td>
					  <td align="center" class="noPrint"><button class="btn btn-info margin" type="button" onClick="location.href='EditOrder.php?ID=<?php echo $row["ID"]; ?>'">Edit</button></td>
                    </tr>
                    <?php				
				}
			} 
			mysql_close($dbh);
		?>
                  </tbody>
                </table>
              </form>
            </div>
            <br>
            <div class="row">
              <div class="col-xs-6">
                <div class="dataTables_info" id="example2_info"> Total <?php echo $maxRow;?> entries </div>
              </div>
              <div class="col-xs-6">
                <div class="dataTables_paginate paging_bootstrap">
                  <ul class="pagination">
                    <li class="prev "> <?php echo $prev;?> </li>
                    <?php
					echo $nav;
					?>
                    <li class="next"> <?php echo $next;?> </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
    </section>
    <!-- /.content -->
  </aside>
  <!-- /.right-side -->
</div>
<?php include_once("Footer.php"); ?>
<!-- ./wrapper -->
<!-- Bootstrap -->
<script src="../admin/js/bootstrap.min.js" type="text/javascript"></script>
<!-- DATA TABES SCRIPT -->
<!--<script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>-->
<script src="../admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="../admin/js/AdminLTE/app.js" type="text/javascript"></script>
<!-- page script -->
<script type="text/javascript">
$(document).ready(function () {				
		$(".checkUncheckAll").click(function () {
			$(".chkIds").prop("checked", $(this).prop("checked"));			
		});
	});
	var counter = 0;
	function doDelete()
	{
		if($(".chkIds").is(":checked"))
		{
			if(confirm("Are you sure to delete."))
			{
				$("#action").val("delete");
				$("#frmPages").submit();
			}
		}
		else
			alert("Please select Package to delete");
	}
	
	function SingleDelete(x)
	{
		if(confirm("Are you sure to delete."))
		{
			$("#action").val("SingleDelete");
			$("#DeleteID").val(x);
			$("#frmPages").submit();
		}
	}
</script>

</body>
</html>
