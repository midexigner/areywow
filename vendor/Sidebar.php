<aside class="left-side sidebar-offcanvas sidebar-navigation">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->

    <div class="user-panel">
      <div class="pull-left image"> </div>
      <div class="pull-left info"> </div>
    </div>
    <!-- search form -->
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li> <a href="dashboard.php"> <i class="fa fa-dashboard"></i> <span>Dashboard</span> </a> </li>



	  <li class="treeview">
		<a href="#">
			<i class="fa fa-book"></i>
			<span>Catalog</span>
			<i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="Products.php"><i class="fa fa-angle-double-right"></i> Products</a></li>
		<!-- 	<li><a href="P_Options.php"><i class="fa fa-angle-double-right"></i> Product Options</a></li>
		<li><a href="P_Stocks.php"><i class="fa fa-angle-double-right"></i> Product Stocks</a></li>
			<li><a href="Categories.php"><i class="fa fa-angle-double-right"></i> Categories</a></li>
			<li><a href="Brands.php"><i class="fa fa-angle-double-right"></i> Brands</a></li> -->
		</ul>
	 </li>



	 <li class="treeview">
		<a href="#">
			<i class="fa fa-shopping-cart"></i>
			<span>Orders</span>
			<i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="Orders.php"><i class="fa fa-angle-double-right"></i> Orders</a></li>
			<li><a href="Cancelled.php"><i class="fa fa-angle-double-right"></i> Canceled</a></li>
			<li><a href="pending.php"><i class="fa fa-angle-double-right"></i> Pending</a></li>
			<li><a href="Confirmed.php"><i class="fa fa-angle-double-right"></i> Ready To Dispatch</a></li>
			<li><a href="Courier.php"><i class="fa fa-angle-double-right"></i> Delivered to Courier</a></li>
			<li><a href="Delivered.php"><i class="fa fa-angle-double-right"></i> Successfully Delivered</a></li>
			<li><a href="Returns.php"><i class="fa fa-angle-double-right"></i> Returns</a></li>
		<!-- 	<li><a href="Shippingoffers.php"><i class="fa fa-angle-double-right"></i> Shipping Offers</a></li>
			<li><a href="promotions.php"><i class="fa fa-angle-double-right"></i> Promotions</a></li> -->
		</ul>
	 </li>


	 
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
