<?php 
require_once '../admin/Common.php'; 
require_once '../functions.php';
$CatID=99999;
if(isset($_SESSION['LoginVendor']) && $_SESSION['LoginVendor']==true){
	redirect("dashboard.php");
}
	$email="";
	$password="";
	$msg1 = "";
	$msg2 = "";
	$msg3 = "";
	if(isset($_POST["action"]) && $_POST["action"] == "submit_form")
	{
		if(isset($_POST["email"])){
			$email=trim($_POST["email"]);
		}
		if(isset($_POST["password"])){
			$password=trim($_POST["password"]);
		}
			
		if ($email==""){
			$msg1 = '<p style="color:red">Please Enter EmailAddress.</p>';
		}
		if ($password==""){
			$msg2 = '<p style="color:red">Please Enter Password.</p>';
		}
			
		if($msg1=='' && $msg2=='')
		{	
			$query="SELECT ID,FirstName,LastName,Password,Email,Country,DateAdded FROM vendor WHERE Status = 1 AND Email='".dbinput($email)."'";
			$result = mysql_query ($query) or die(mysql_error()); 
			$num = mysql_num_rows($result);
			
			
			if($num==0)
			{
				$_SESSION["LoginVendor"]=false;
				$_SESSION["LockScreen"]=true;
				$_SESSION["VendorID"]='';
				$_SESSION["VendorFullName"]='';
				$_SESSION["Country"]='';
				$_SESSION["Shipping"]='';
				$_SESSION["Email"]='';
				$_SESSION['DateAdded']='';
				$msg3 = '<p style="color:red">Invalid Email/Password.</p>';	
			}
			else
			{
				$row = mysql_fetch_array($result,MYSQL_ASSOC);
				if(dboutput($row["Password"]) == $password)
				{
					$_SESSION["LoginVendor"]=true;
					$_SESSION["VendorID"]=dboutput($row["ID"]);
					$_SESSION["VendorFullName"]=dboutput($row["FirstName"]) .' '. dboutput($row["LastName"]);
					$_SESSION["Country"]=dboutput($row["Country"]);
					$_SESSION["Email"]=dboutput($row["Email"]);
					$_SESSION["DateAdded"]=dboutput($row["DateAdded"]);
					$_SESSION["LockScreen"]=false;
					$query2="SELECT Percentage FROM shipping_rates WHERE CountryCode='".$row["Country"]."'";
					$result2 = mysql_query ($query2) or die(mysql_error()); 
					$num2 = mysql_num_rows($result2);
					if($num2 > 0)
					{
						$row2 = mysql_fetch_array($result2,MYSQL_ASSOC);
						$_SESSION["Shipping"]=dboutput($row2["Percentage"]);
					}
					else
					{
						$query3="SELECT Percentage FROM other_countries_shipping WHERE ID=1";
						$result3 = mysql_query ($query3) or die(mysql_error()); 
						$num3 = mysql_num_rows($result3);
						if($num3 == 1)
						{
							$row3 = mysql_fetch_array($result3,MYSQL_ASSOC);
							$_SESSION["Shipping"]=dboutput($row3["Percentage"]);
						}
					}
					
					redirect("dashboard.php");
				}
				else
				{
					$_SESSION["LoginVendor"]=false;
					$_SESSION["LockScreen"]=true;
					$_SESSION["VendorID"]='';
					$_SESSION["VendorFullName"]='';
					$_SESSION["Country"]='';
					$_SESSION["Shipping"]='';
					$_SESSION["Email"]='';
					$_SESSION['DateAdded']='';
					$msg3 = '<p style="color:red">Invalid Email/Password.</p>';
				}
			}
		}
	}
?>

<!DOCTYPE html>
<html class="bg-black">
<head>
<meta charset="UTF-8">
<title>Vendor Login</title>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<!-- bootstrap 3.0.2 -->
<link href="../admin/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- font Awesome -->
<script language="javascript" src="../admin/js/local_clock.js" type="text/javascript"></script>
<link href="../admin/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Theme style -->
<link href="../admin/css/AdminLTE.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
</head>
<body class="bg-black">
<div class="form-box" id="login-box">
  <div class="header">Vendor Login</div>

  <form action="<?php echo $_SERVER["PHP_SELF"];?>" method="post">
    <div class="body bg-gray"> <span style="color:red;font-size:12px;">
      <?php 
			if(isset($msg3))
			echo $msg3
			?>
      <div class="form-group"> <span style="color:red;font-size:12px;">
        <?php if(isset($msg1)){echo $msg1;}?>
        </span>
        <input type="text" name="email" value="<?php echo $email; ?>" placeholder="E-Mail Address" id="input-email" class="form-control">
      </div>
      <div class="form-group"> <span style="color:red;font-size:12px;">
        <?php if(isset($msg2)){echo $msg2;}?>
        </span>
        <input type="password" name="password" value="" placeholder="Password" id="input-password" class="form-control">
      </div>
     <!--  <div class="form-group">
        <label><input type="checkbox" name="remember_me" 
						<?php if(isset($_COOKIE['remember_me_u'])) {
						echo 'checked="checked"';
						}
						else {
							echo '';
						}
						?> />
        Remember me</label></div> -->
    </div>
    <div class="footer">
      <button type="submit" class="btn bg-olive btn-block">Login</button>
      <!--<p><a href="#">I forgot my password</a></p>-->
    </div>
    <input type="hidden" name="action" value="submit_form" />
  </form>
  <!--<div class="margin text-center">
                <span>Sign in using social networks</span>
                <br/>
                <button class="btn bg-light-blue btn-circle"><i class="fa fa-facebook"></i></button>
                <button class="btn bg-aqua btn-circle"><i class="fa fa-twitter"></i></button>
                <button class="btn bg-red btn-circle"><i class="fa fa-google-plus"></i></button>

            </div>-->
</div>
<!-- jQuery 2.0.2 -->
<script src="../admin/ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../admin/js/bootstrap.min.js" type="text/javascript"></script>
</body>
</html>
