<?php
require_once '../admin/Common.php'; 
require_once '../functions.php';
include("CheckVendorLogin.php");
	
	$msg="";
	$Status=1;
	$BestSeller=0;
	$SpecialProduct=0;
	$DealProduct=0;
	$FeaturedProduct=0;
	$ThisWeekProduct=0;
	$Name="";
	$NameArabic="";
	$MetaDes="";
	$MetaKey="";
	$URL="";
	$Price=0;
	$Source="";
	$Overview="";
	$OverviewArabic="";
	$Description="";
	$DescriptionArabic="";
	$Stock=0;
	$Manufacture=0;
	$CustomDesign=0;
	$Category="";
	$Cat=array();
	$RelatedProducts="";
	$RelProd=array();
	$Discount=0;
	$Quantity=0;
	$Ratings=60;
	$Shipping=0;
	$Image="";
	$Option=array();
	$Value=array();
	$Increment=array();
	$numb=0;
	$UploadedflPage="";
	
	$ID=0;
	if(isset($_REQUEST["ProID"]) && ctype_digit(trim($_REQUEST["ProID"])))
		$ID=trim($_REQUEST["ProID"]);
		
	
if(isset($_POST["action"]) && $_POST["action"] == "submit_form")
{	
	if(isset($_REQUEST["ProID"]) && ctype_digit(trim($_REQUEST["ProID"])))
		$ID=trim($_REQUEST["ProID"]);
	if(isset($_POST["Status"]) && ((int)$_POST["Status"] == 0 || (int)$_POST["Status"] == 1))
		$Status=trim($_POST["Status"]);	
	if(isset($_POST["BestSeller"]) && ((int)$_POST["BestSeller"] == 0 || (int)$_POST["BestSeller"] == 1))
		$BestSeller=trim($_POST["BestSeller"]);	
	if(isset($_POST["SpecialProduct"]) && ((int)$_POST["SpecialProduct"] == 0 || (int)$_POST["SpecialProduct"] == 1))
		$SpecialProduct=trim($_POST["SpecialProduct"]);
	if(isset($_POST["DealProduct"]) && ((int)$_POST["DealProduct"] == 0 || (int)$_POST["DealProduct"] == 1))
		$DealProduct=trim($_POST["DealProduct"]);
	if(isset($_POST["FeaturedProduct"]) && ((int)$_POST["FeaturedProduct"] == 0 || (int)$_POST["FeaturedProduct"] == 1))
		$FeaturedProduct=trim($_POST["FeaturedProduct"]);
	if(isset($_POST["ThisWeekProduct"]) && ((int)$_POST["ThisWeekProduct"] == 0 || (int)$_POST["ThisWeekProduct"] == 1))
		$ThisWeekProduct=trim($_POST["ThisWeekProduct"]);
	if(isset($_POST["Name"]))
		$Name=trim($_POST["Name"]);
	if(isset($_POST["NameArabic"]))
		$NameArabic=trim($_POST["NameArabic"]);
	if(isset($_POST["MetaDes"]))
		$MetaDes=trim($_POST["MetaDes"]);
	if(isset($_POST["MetaKey"]))
		$MetaKey=trim($_POST["MetaKey"]);
	if(isset($_POST["URL"]))
		$URL=trim($_POST["URL"]);
	if(isset($_POST["Price"]))
		$Price=trim($_POST["Price"]);
	if(isset($_POST["Source"]))
		$Source=trim($_POST["Source"]);
	if(isset($_POST["Overview"]))
		$Overview=trim($_POST["Overview"]);
	if(isset($_POST["OverviewArabic"]))
		$OverviewArabic=trim($_POST["OverviewArabic"]);
	if(isset($_POST["Description"]))
		$Description=trim($_POST["Description"]);
	if(isset($_POST["DescriptionArabic"]))
		$DescriptionArabic=trim($_POST["DescriptionArabic"]);
	if(isset($_POST["Stock"]) && ctype_digit($_POST['Stock']))
		$Stock=trim($_POST["Stock"]);
	if(isset($_POST["Manufacture"]) && ctype_digit($_POST['Manufacture']))
		$Manufacture=trim($_POST["Manufacture"]);
	if(isset($_POST["CustomDesign"]) && ctype_digit($_POST['CustomDesign']))
		$CustomDesign=trim($_POST["CustomDesign"]);
	if(isset($_POST["Discount"]))
		$Discount=trim($_POST["Discount"]);
	if(isset($_POST["Quantity"]) && ctype_digit($_POST['Quantity']))
		$Quantity=trim($_POST["Quantity"]);
	if(isset($_POST["Ratings"]) && ctype_digit($_POST['Ratings']))
		$Ratings=trim($_POST["Ratings"]);
	if(isset($_POST["Category"]))
	{
		$Category=implode(',', $_POST['Category']);
		$Cat=$_POST['Category'];
	}
	if(isset($_POST["RelatedProducts"]))
	{
		$RelatedProducts=implode(',', $_POST['RelatedProducts']);
		$RelProd=$_POST['RelatedProducts'];
	}
	if(isset($_POST["Shipping"]) && ctype_digit($_POST['Shipping']))
		$Shipping=trim($_POST["Shipping"]);
	if(isset($_POST["Option"]))
		$Option=$_POST['Option'];
	if(isset($_POST["Value"]))
		$Value=$_POST['Value'];
	if(isset($_POST["Increment"]))
		$Increment=$_POST['Increment'];
	if(isset($_FILES["flPage"]))
	{
		if(isset($_POST["UploadedflPage"]))
		{
			$UploadedflPage=implode(',', $_POST["UploadedflPage"]);			
		}
		$totaluploaded = count(explode(',',$UploadedflPage));
	}

	
		if($Name == "")
		{
			$msg='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Please enter Name.</b>
			</div>';
		}
		else if($MetaDes == "")
		{
			$msg='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Please enter Meta Description.</b>
			</div>';
		}
		else if($MetaKey == "")
		{
			$msg='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Please enter Meta Keywords.</b>
			</div>';
		}
		else if($URL == "")
		{
			$msg='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Please enter URL.</b>
			</div>';
		}
		else if($Price == 0)
		{
			$msg='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Please enter Price.</b>
			</div>';
		}
		else if($Price < 0)
		{
			$msg='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Please enter Positive Price.</b>
			</div>';
		}
		else if($Stock == 0)
		{
			$msg='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Please Select Stock.</b>
			</div>';
		}
		else if($Manufacture == 0)
		{
			$msg='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Please Select Manufacture.</b>
			</div>';
		}
		else if($Category == "")
		{
			$msg='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Please Select Category.</b>
			</div>';
		}
		else if($Ratings > 100)
		{
			$msg='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Your Rating is Out of 100%.</b>
			</div>';
		}
		else if($Discount > $Price)
		{
			$msg='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Your Discounted Prize is Out of 100%.</b>
			</div>';
		}
		else if($Shipping == 0)
		{
			$msg='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Please Select Shipping Method.</b>
			</div>';
		}
		else if($URL != "")
		{
			$sql = "SELECT ID from products WHERE URL = '".$URL."'";
			$res = mysql_query($sql) or die(mysql_error());
			$unum = mysql_num_rows($res);
			if($unum > 1)
			{
				$msg='<div class="alert alert-danger alert-dismissable">
				<i class="fa fa-ban"></i>
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<b>URL is already exists.</b>
				</div>';
			}
		}
		
	
	if($msg=="")
	{
		$arrlength1 = count($Value);
		for($x = 0; $x < $arrlength1; $x++) {
		$query1="INSERT INTO product_options SET DateAdded=NOW(),
				ProductID='".(int)$ID . "',
				OptionID='".(int)$Option[$x] . "',
				ValueID='".(int)$Value[$x] . "',
				Increment='".$Increment[$x] . "'";
		mysql_query($query1) or die("Option not inserted bacause".mysql_error());
		
		$query2="SELECT Options FROM products where ID = ".$ID;
		$res = mysql_query($query2) or die(mysql_error());
		$val = mysql_fetch_array($res);
		$str = $val['Options'];
		$arr = array(0);
		$arr = explode(',',$str);
		if (($key = array_search($Value[$x], $arr)) !== false) {
			unset($arr[$key]);
		}
		array_push($arr, $Value[$x]);
		$str = implode(',',$arr);

		$query3="UPDATE products SET Options='".$str. "' where ID = ".$ID;
		mysql_query($query3) or die(mysql_error());
		}
	
		$r = mysql_query("SELECT ProductName FROM products WHERE 
		ProductName='".dbinput($Name)."' AND ID <> '".(int)$ID."'") or die(mysql_error());
		if(mysql_num_rows($r) == 0)
		{
			$cats = array();
			$cats = explode(',', $Category);
			for ($i=0; $i < count($cats); $i++) { 
				$sqlcat = mysql_query("SELECT Parent FROM categories WHERE ID = '".$cats[$i]."' AND Parent <> 0 ") or die(mysql_error());
				$rowcat = mysql_fetch_assoc($sqlcat);
				if ($rowcat['Parent'] != "") {
					$Category .= ','.$rowcat['Parent'];
				}
				$sqlcat1 = mysql_query("SELECT Parent FROM categories WHERE ID = '".$rowcat['Parent']."' AND Parent <> 0 ") or die(mysql_error());
				$rowcat1 = mysql_fetch_assoc($sqlcat1);
				if ($rowcat1['Parent'] != "") {
					$Category .= ','.$rowcat1['Parent'];
				}
			}
			/*echo $Category;
			exit();*/
			$query="UPDATE products SET  
				DateModified=NOW(),
				ProductName = '" . dbinput($Name) . "',
				Status='".(int)$Status . "',
				BestSeller='".(int)$BestSeller . "',
				SpecialProduct='".(int)$SpecialProduct . "',
				DealProduct='".(int)$DealProduct . "',
				featured='".(int)$FeaturedProduct . "',
				ThisWeekProduct='".(int)$ThisWeekProduct . "',
				MetaDescription = '" . dbinput($MetaDes) . "',
				MetaKeywords = '" . dbinput($MetaKey) . "',
				Description = '" . dbhtmlinput($Description) . "',
				ProductSource = '" . dbinput($Source) . "',
				Overview = '" . dbinput($Overview) . "',
				Categories = '" . dbinput($Category) . "',
				RelatedProducts = '" . dbinput($RelatedProducts) . "',
				URL = '" . dbinput($URL) . "',
				Stock = '" . (int)$Stock . "',
				Shipping = '" . (int)$Shipping . "',
				Discount = '" . (int)$Discount . "',
				Quantity = '" . (int)$Quantity . "',
				Ratings = '" . (int)$Ratings . "',
				Manufacture = '" . (int)$Manufacture . "',
				CustomDesign = '" . (int)$CustomDesign . "',
				PerformedBy = '" . $_SESSION["UserID"] . "',
				Price = '" . (int)$Price . "'
				WHERE ID='".(int)$ID."'";
			mysql_query($query) or die ('Could not update product because: ' . mysql_error());

			//echo $query;
			if ($Discount > 0) {
				$sale = "UPDATE products SET onsale = 1 WHERE ID = '".(int)$ID."'";
				mysql_query($sale);
			}
			else{
				$sale = "UPDATE products SET onsale = 0 WHERE ID = '".(int)$ID."'";
				mysql_query($sale);
			}
			if ($Quantity > 0) {
				$sqll = "SELECT u.Email,u.Phone FROM `notification` n LEFT JOIN website_users u ON n.UserID = u.ID WHERE ProductID = '".$ID."'";
				$qu = mysql_query($sqll) or die(mysql_error());
				while ($r = mysql_fetch_assoc($qu)) {
					$m = $Name." is now available!";
					mail($r['Email'], 'AndyBirds', $m);
						$type = "xml";
						$id = SMS_USERNAME;
						$pass = SMS_PASSWORD;
						$lang = "English";
						$mask = "AndyBirds";
						// Data for text message
						$to = $r["Phone"];
						$message = urlencode($m);
						// Prepare data for POST request
						$data = "id=".$id."&pass=".$pass."&msg=".$message."&to=".$to."&lang=".$lang."&mask=".$mask."&type=".$type;
						// Send the POST request with cURL
						$ch = curl_init('http://www.outreach.pk/api/sendsms.php/sendsms/url');
						curl_setopt($ch, CURLOPT_POST, true);
						curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						$result = curl_exec($ch); //This is the result from Outreach
						curl_close($ch);
				}
				$sqlll = "DELETE FROM `notification` WHERE ProductID = '".$ID."'";
				mysql_query($sqlll) or die(mysql_error());
			}
			
			$_SESSION["msg"]='<div class="alert alert-success alert-dismissable">
				<i class="fa fa-ban"></i>
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<b>Product has been Updated.</b>
			</div>';
			
			if(isset($_FILES["flPage"]) && $_FILES["flPage"]['name'] != "")
			{
				//if(is_file(DIR_PAYROLL_IMAGES . $StoreImage))
				//	unlink(DIR_PAYROLL_IMAGES . $StoreImage);
			
				//ini_set('memory_limit', '-1');
				$num=count($_FILES["flPage"]['name']);
				// echo $num;
				$Dbinputarray = "";
				$success = 0;
				
				$totaluploaded = explode(',',$UploadedflPage);
				$totaluploaded = explode('.',end($totaluploaded));
				$totaluploaded = explode('-',$totaluploaded[0]);
				$totaluploaded = end($totaluploaded);
				$totaluploaded++;	
				
				for($i = 0 ; $i < $num ; $i++)
				{
					
					$filenamearray=explode(".", $_FILES["flPage"]['name'][$i]);
					
					$ext=strtolower($filenamearray[sizeof($filenamearray)-1]);
					
					$tempName = $_FILES["flPage"]['tmp_name'][$i];
					
					$realName = "P".$ID . "-".$totaluploaded++."." . $ext;
					$StoreImage = $realName; 
					$target = DIR_PRODUCTS_IMAGES . $realName;
					
					$Dbinputarray .= $realName.",";
					$moved = move_uploaded_file($tempName, $target);
					
					if($moved)
						$success++;
					
				}
				
				
				// print_r($_FILES["flPage"]['name']);
				$Dbinputarray = substr($Dbinputarray,0,strlen($Dbinputarray)-1);
				// echo $UploadedflPage.','.$Dbinputarray;
				
				// exit();
				if($success==$num)
				{			
					$query="UPDATE products SET Image='" . dbinput($UploadedflPage.','.$Dbinputarray) . "' WHERE  ID=" . (int)$ID;
					mysql_query($query) or die(mysql_error());
				}
				else
				{
					$_SESSION["msg"]='<div class="alert alert-success alert-dismissable">
						<i class="fa fa-ban"></i>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<b>Product has been Updated.</b>
					</div>';
						
				}
			}
			
		
		}
		else
		{
			$msg = '<div class="alert alert-danger alert-dismissable">
				<i class="fa fa-ban"></i>
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<b>Name already exists.</b>
			</div>';
		}
			
		
	redirect($_SERVER["PHP_SELF"].'?ProID='.$ID);		
	}
	
}
else
{
	$query="SELECT ID, Status,ProductName,BestSeller,SpecialProduct,DealProduct,featured,ThisWeekProduct,ProductNameArabic,MetaDescription,Image,Shipping,ProductSource,Overview,OverviewArabic,Categories,RelatedProducts,Stock,Discount,Quantity,Ratings,Manufacture,CustomDesign,Price,MetaKeywords,Description,DescriptionArabic,URL,DATE_FORMAT(DateAdded, '%D %b %Y %r') AS Added, DATE_FORMAT(DateModified,
	 '%D %b %Y %r') AS Updated FROM products WHERE  ID='" . (int)$ID . "'";
	$result = mysql_query ($query) or die("Could not select product because: ".mysql_error()); 
	$num = mysql_num_rows($result);
	
	if($num==0)
	{
		$_SESSION["msg"]='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Invalid Product ID.</b>
		</div>';
		redirect("Products.php");
	}
	else
	{
		$row = mysql_fetch_array($result,MYSQL_ASSOC);
		
		$ID=$row["ID"];
		$Status=$row["Status"];
		$BestSeller=$row["BestSeller"];
		$SpecialProduct=$row["SpecialProduct"];
		$DealProduct=$row["DealProduct"];
		$FeaturedProduct=$row["featured"];
		$ThisWeekProduct=$row["ThisWeekProduct"];
		$Name=$row["ProductName"];
		$NameArabic=$row["ProductNameArabic"];
		$MetaDes=$row["MetaDescription"];
		$MetaKey=$row["MetaKeywords"];
		$URL=$row["URL"];
		$Price=$row["Price"];
		$Source=$row["ProductSource"];
		$Overview=$row["Overview"];
		$OverviewArabic=$row["OverviewArabic"];
		$Description=$row["Description"];
		$DescriptionArabic=$row["DescriptionArabic"];
		$Stock=$row["Stock"];
		$Manufacture=$row["Manufacture"];
		$CustomDesign=$row["CustomDesign"];
		$Discount=$row["Discount"];
		$Quantity=$row["Quantity"];
		$Ratings=$row["Ratings"];
		$Category=$row["Categories"];
		$Cat = explode(',',$Category);
		if($row["RelatedProducts"] == '')
		{
		$RelatedProducts=0;
		}
		else
		{
		$RelatedProducts=$row["RelatedProducts"];
		$RelProd = explode(',',$RelatedProducts);
		}
		$Shipping=$row["Shipping"];
		$Image=$row["Image"];
		$Img = explode(',',$Image);
		$numb=count($Img);
	}
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Edit Product</title>
<style>
    .multiselect {
        width: auto;
    }
    .selectBox {
        position: relative;
    }
    .selectBox select {
        width: 100%;
    }
    .overSelect {
        position: absolute;
        left: 0; right: 0; top: 0; bottom: 0;
    }
    #checkboxes {
        display: none;
        border: 1px #dadada solid;
    }
    #checkboxes label {
        display: block;
    }
    #checkboxes label:hover {
        background-color: #1e90ff;
    }
	#checkboxes2 {
        display: none;
        border: 1px #dadada solid;
    }
    #checkboxes2 label {
        display: block;
    }
    #checkboxes2 label:hover {
        background-color: #1e90ff;
    }
	</style>
	<script>
    var expanded = false;
    function showCheckboxes() {
        var checkboxes = document.getElementById("checkboxes");
        if (!expanded) {
            checkboxes.style.display = "block";
            expanded = true;
        } else {
            checkboxes.style.display = "none";
            expanded = false;
        }
    }
	</script>
	<script>
    var expanded = false;
    function showCheckboxes2() {
        var checkboxes = document.getElementById("checkboxes2");
        if (!expanded) {
            checkboxes.style.display = "block";
            expanded = true;
        } else {
            checkboxes.style.display = "none";
            expanded = false;
        }
    }
	</script>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<!-- bootstrap 3.0.2 -->
<script language="javascript" src="../admin/scripts/innovaeditor.js"></script>
<link href="../admin/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../admin/js/local_clock.js" type="text/javascript"></script>
<link href="../admin/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Ionicons -->
<link href="../admin/css/ionicons.min.css" rel="stylesheet" type="text/css" />
<!-- Theme style -->
<link href="../admin/css/AdminLTE.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
<style>
#labelimp {
	background-color: rgba(60, 141, 188, 0.19);
	padding: 4px;
	font-size: 20px;
	width: 100%;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	padding-left: 5px;
}
</style>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>

$(document).ready(function(){ 

		
		$("#Name").keyup(function () {
			
			$.ajax({			
					url: 'get_p_meta_des.php?name='+$("#Name").val()+'&price='+$("#Price").val(),
					success: function(data) {
						$("#MetaDes").html(data);
					},
					error: function (xhr, textStatus, errorThrown) {
						alert(xhr.responseText);
						$("#MetaDes").removeAttr("disabled");
					}
			});

		});		
		
		
	});   

</script>
<script>
$(document).ready(function(){ 
		
		$("#Price").keyup(function () {
			
			$.ajax({			
					url: 'get_p_meta_des.php?name='+$("#Name").val()+'&price='+$("#Price").val(),
					success: function(data) {
						$("#MetaDes").html(data);
					},
					error: function (xhr, textStatus, errorThrown) {
						alert(xhr.responseText);

					}
			});

		});		
		
		
	});   

</script>
<script>
$(document).ready(function(){ 
		
		$("#Name").keyup(function () {
			
			$.ajax({			
					url: 'get_p_meta_key.php?name='+$("#Name").val(),
					success: function(data) {
						$("#MetaKey").html(data);
					},
					error: function (xhr, textStatus, errorThrown) {
						alert(xhr.responseText);
						$("#MetaKey").removeAttr("disabled");
					}
			});

		});		
		
		
	});   

</script>
<script>
$(document).ready(function(){ 
		
		$("#Name").keyup(function () {
			
			$.ajax({			
					url: 'get_url.php?name='+$("#Name").val(),
					success: function(data) {
						$("#URL").html(data);
					},
					error: function (xhr, textStatus, errorThrown) {
						alert(xhr.responseText);
						$("#URL").removeAttr("disabled");
					}
			});

		});		
		
		
	});   

</script>
<script>
	$(document).ready(function(){
		$(".Imgbtn2").click(function(){
			$("#images2").append('<br><input type="file" name="flPage[]" />');
		});
	});
</script>
<script>
$(document).ready(function(){ 
		
		$("#Option0").change(function () {
			
			$.ajax({			
					url: 'get_value.php?opt='+$("#Option0").val(),
					success: function(data) {
						$("#Value0").html(data);
					},
					error: function (xhr, textStatus, errorThrown) {
						alert(xhr.responseText);
						$("#Value0").removeAttr("disabled");
					}
			});

		});		
		
		
	});   

</script>
<script>
	$(document).ready(function(){
		$(".Optbtn").click(function(){
		
			valuee = $("#noofoptions" ).attr("text");
			var newValue = parseInt(valuee) + 1;
			
			html = '';
			html  = '<br><div class="row"><div class="col-md-4"><select name="Option[]" id="Option'+ newValue +'" class="form-control"><option value="0">Option</option>';		
			<?php
			$query = "SELECT OptionName,ID FROM p_options where Status = 1";
			$res = mysql_query($query);
			while($row = mysql_fetch_array($res))
			{
			echo ' html = html + \'<option value="'.$row['ID'].'">'.$row['OptionName'].'</option>\';';
			}
			?>
			htmlr = "<script> $(\"#Option"+ newValue +"\").change(function () {\n";
			htmlr = htmlr + "\n$.ajax({			";
			htmlr = htmlr + "\n		url: 'get_value.php?opt='+$(\"#Option"+ newValue +"\").val(),";
			htmlr = htmlr + "\n		success: function(data) {";
			htmlr = htmlr + "\n			$(\"#Value"+ newValue +"\").html(data);";
			htmlr = htmlr + "\n		},";
			htmlr = htmlr + "\n		error: function (xhr, textStatus, errorThrown) {";
			htmlr = htmlr + "\n			alert(xhr.responseText);";
			htmlr = htmlr + "\n			$(\"#Value"+ newValue +"\").removeAttr(\"disabled\");";
			htmlr = htmlr + "\n		}";
			htmlr = htmlr + "\n });";
			htmlr = htmlr + "\n ";
			htmlr = htmlr + "\n });		";
			htmlr = htmlr + "\n <\/script> ";
			
			html = html  + '</select></div><div class="col-md-4"><select name="Value[]" id="Value'+ newValue +'" class="form-control"></select> </div> <div class="col-md-4"> <input type="number" maxlength="50" class="Increment" name="Increment[]" class="form-control"  value="0" /> </div><br></div>'+htmlr;
			$("#options").append(html);
			$("#noofoptions" ).attr( "text", newValue);		
			
			
	
		});
	});
</script>
</head>
<body class="skin-blue">
<!-- header logo: style can be found in header.less -->
<?php
	include_once("Header.php");
?>
<div class="wrapper row-offcanvas row-offcanvas-left">
  <!-- Left side column. contains the logo and sidebar -->
  <?php
	include_once("Sidebar.php");
?>
  <!-- Right side column. Contains the navbar and content of the page -->
  <aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Edit Product</h1>
      <ol class="breadcrumb">
        <li><a href="Products.php"><i class="fa fa-dashboard"></i>Products</a></li>
        <li class="active">Edit Product</li>
      </ol>
    </section>
    <!-- Main content -->
    <form role="form" action="<?php echo $_SERVER["PHP_SELF"];?>?ProID=<?php echo $ID; ?>"" method="post" enctype="multipart/form-data" name="frmPage">
      <input type="hidden" name="action" value="submit_form" />
      <input type="hidden" name="ProID" value="<?php echo $ID; ?>" />
      <section class="content">
        <div class="box-footer" style="text-align:right;">
          <button type="submit" class="btn btn-success margin">Save</button>
          <button class="btn btn-primary margin" type="button" onClick="location.href='Products.php'">Cancel</button>
        </div>
        <?php
			  	echo $msg;
				if(isset($_SESSION["msg"]))
				{
					echo $_SESSION["msg"];
					$_SESSION["msg"]="";
				}
				?>
        <div class="col-md-6">
          <div class="box">
            <!-- general form elements -->
            <div style="padding:15px;" class="box-primary">
              <!-- form start -->
              <div class="box-body">
               
                <div class="form-group">
                  <label id="labelimp" class="labelimp" for="Name">Product Name: </label>
                  <?php 
				echo '<input type="text" maxlength="200" id="Name" name="Name" class="form-control"  value="'.$Name.'" />';
				?>
                </div>
				
		
				
				<div class="form-group">
                  <label id="labelimp" for="MetaDes" >Meta Description: </label>
                  <?php 
				echo '<textarea id="MetaDes" name="MetaDes" class="form-control">'.$MetaDes.'</textarea>';
				?>
                </div>
				
				<div class="form-group">
                  <label id="labelimp" for="MetaKey" >Meta Keywords: </label>
                  <?php 
				echo '<textarea id="MetaKey" name="MetaKey" class="form-control">'.$MetaKey.'</textarea>';
				?>
                </div>
				
				<div class="form-group">
                  <label id="labelimp" for="URL">URL: </label>
                  <?php 
				echo '<textarea id="URL" name="URL" class="form-control">'.$URL.'</textarea>';
				?>
                </div>
				
				<div class="form-group">
                  <label id="labelimp" for="Price">Price: </label>
                  <?php 
				echo '<input type="text" maxlength="50" id="Price" name="Price" class="form-control"  value="'.$Price.'" />';
				?>
                </div>
				
				<div class="form-group">
                  <label id="labelimp" for="CustomDesign" >Custom Design: </label>
                  
				  <select name="CustomDesign" id="CustomDesign" class="form-control">
					<option value="0" >Select Custom Design</option>
					<?php
					 $query = "SELECT TemplateName,ID FROM custom_templates where Status = 1";
					$res = mysql_query($query);
					while($row = mysql_fetch_array($res))
					{
					echo '<option '.($CustomDesign == $row['ID'] ? 'selected' : '').' value="'.$row['ID'].'">'.$row['TemplateName'].'</option>';
					} 
					?>
					</select>
                </div>
				
				 <div class="form-group">
                  <label id="labelimp" for="Source">Product Source: </label>
                  <?php 
				echo '<input type="text" maxlength="100" id="Source" name="Source" class="form-control"  value="'.$Source.'" />';
				?>
                </div>
				
				<div class="form-group">
                  <label id="labelimp" for="Overview" >Overview: </label>
                  <?php 
				echo '<textarea id="Overview" name="Overview" class="form-control">'.$Overview.'</textarea>';
				?>
                </div>
				
				
				
				<div class="form-group">

				  <label id="labelimp">Description: </label>

					<?php

						$v = '<textarea name="Description" id="Description">';

							$v .= $Description;

						$v .= '</textarea><script>

							var NSD = new InnovaEditor("NSD");

							NSD.width="100%";

							NSD.height=300;

							NSD.btnPrint=true;

							NSD.btnPasteText=true;

							NSD.btnFlash=true;

							NSD.btnForm=false;

							NSD.btnMedia=true;

							NSD.btnLTR=true;

							NSD.btnRTL=true;

							NSD.btnStrikethrough=true;

							NSD.btnSuperscript=true;

							NSD.btnSubscript=true;

							NSD.btnClearAll=true;

							NSD.btnSave=true;

							NSD.btnStyles=true;

							if(navigator.appName == \'Microsoft Internet Explorer\')

								NSD.cmdAssetManager = "modalDialogShow(\'../assetmanager/assetmanager.php\',640,465)";

							else						

								NSD.cmdAssetManager = "modalDialogShow(\'../../assetmanager/assetmanager.php\',640,465)";						

							NSD.css="../css/style.css";

							NSD.mode="XHTMLBody";

							NSD.REPLACE("Description");

							</script>';

							

							echo $v;

							?>

				</div>
				
                <input type="hidden" name="action" value="submit_form" />
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
            <!-- Form Element sizes -->
          </div>
        </div>
        <div class="col-md-6">
          <div class="box">
            <!-- general form elements -->
            <div style="padding:15px;" class="box-primary">
              <!-- form start -->
              <div class="box-body">
			  
				<div class="form-group" style="display: none;">
                  <label id="labelimp" >Status: </label>
                  <label>
                  <input type="radio" name="Status" value="1"<?php echo ($Status == 1 ? ' checked="checked"' : ''); ?>>
                  Enable</label>
                  <label>
                  <input type="radio" name="Status" value="0"<?php echo ($Status == 0 ? ' checked="checked"' : ''); ?>>
                  Disable</label>
                </div>
				
				<div class="form-group">
                  <label id="labelimp" >Best Seller: </label>
                  <label>
                  <input type="radio" name="BestSeller" value="1"<?php echo ($BestSeller == 1 ? ' checked="checked"' : ''); ?>>
                  Yes</label>
                  <label>
                  <input type="radio" name="BestSeller" value="0"<?php echo ($BestSeller == 0 ? ' checked="checked"' : ''); ?>>
                  No</label>
                </div>
				
				<div class="form-group" style="display: none;">
                  <label id="labelimp" >Special Offer: </label>
                  <label>
                  <input type="radio" name="SpecialProduct" value="1"<?php echo ($SpecialProduct == 1 ? ' checked="checked"' : ''); ?>>
                  Yes</label>
                  <label>
                  <input type="radio" name="SpecialProduct" value="0"<?php echo ($SpecialProduct == 0 ? ' checked="checked"' : ''); ?>>
                  No</label>
                </div>
				
				<div class="form-group" style="display: none;">
                  <label id="labelimp" >AndyBird Deal: </label>
                  <label>
                  <input type="radio" name="DealProduct" value="1"<?php echo ($DealProduct == 1 ? ' checked="checked"' : ''); ?>>
                  Yes</label>
                  <label>
                  <input type="radio" name="DealProduct" value="0"<?php echo ($DealProduct == 0 ? ' checked="checked"' : ''); ?>>
                  No</label>
                </div>
				
				<div class="form-group">
                  <label id="labelimp" >Featured Product: </label>
                  <label>
                  <input type="radio" name="FeaturedProduct" value="1"<?php echo ($FeaturedProduct == 1 ? ' checked="checked"' : ''); ?>>
                  Yes</label>
                  <label>
                  <input type="radio" name="FeaturedProduct" value="0"<?php echo ($FeaturedProduct == 0 ? ' checked="checked"' : ''); ?>>
                  No</label>
                </div>
				
				<div class="form-group">
                  <label id="labelimp" >Top Rated: </label>
                  <label>
                  <input type="radio" name="ThisWeekProduct" value="1"<?php echo ($ThisWeekProduct == 1 ? ' checked="checked"' : ''); ?>>
                  Yes</label>
                  <label>
                  <input type="radio" name="ThisWeekProduct" value="0"<?php echo ($ThisWeekProduct == 0 ? ' checked="checked"' : ''); ?>>
                  No</label>
                </div>
				
				<div class="form-group">
                  <label id="labelimp" for="Stock" >Stock: </label>
                  
				  <select name="Stock" id="Stock" class="form-control">
					<option value="0" >Select Stock</option>
					<?php
					 $query = "SELECT StockName,ID FROM p_stocks where Status = 1";
					$res = mysql_query($query);
					while($row = mysql_fetch_array($res))
					{
					echo '<option '.($Stock == $row['ID'] ? 'selected' : '').' value="'.$row['ID'].'">'.$row['StockName'].'</option>';
					}
					?>
					</select>
                </div>
				
				<div class="form-group">
                  <label id="labelimp" for="Slider">Image:<a class="Imgbtn2" style="float:right; cursor:pointer">More Images</a></label>
                  <div id="images2">
				  <?php
				  for($i = 0 ; $i < $numb ; $i++)
				  {
				  //$Img=explode(",", $Image[$i]);
				  echo '<img src="../admin/'. DIR_PRODUCTS_IMAGES . $Img[$i] . '" width="110" height="100">
						
						<input type="hidden" name="UploadedflPage[]" value="'.$Img[$i].'" />
						';
					if($numb > 1)
					{
					?>
					<button class="btn btn-danger margin" type="button" onClick="location.href='DeleteImage.php?Name=<?php echo $Img[$i]; ?>&ProdID=<?php echo $ID; ?>'">Delete</button><?php } ?><br><br>
					<?php
				  }
				  ?>
				  <input type="file" name="flPage[]" />
				  </div>
                </div>
				
				<div class="form-group">
                  <label id="labelimp" for="Manufacture" >Manufacture: </label>
                  
				  <select name="Manufacture" id="Manufacture" class="form-control">
					<option value="0" >Select Manufacture</option>
					<?php
					 $query = "SELECT BrandName,ID FROM brands where Status = 1";
					$res = mysql_query($query);
					while($row = mysql_fetch_array($res))
					{
					echo '<option '.($Manufacture == $row['ID'] ? 'selected' : '').' value="'.$row['ID'].'">'.$row['BrandName'].'</option>';
					} 
					?>
					</select>
                </div>
				
				<div class="form-group">
                  <label id="labelimp" for="Categories" >Categories: </label>
                 <div class="selectBox" onclick="showCheckboxes()">
						<select class="form-control">
							<option>Select Category</option>
						</select>
						<div class="overSelect"></div>
					</div>
					<div id="checkboxes" style="height:250px; overflow:scroll;">						
						<?php
						$query = "SELECT CategoryName,ID FROM categories where Parent = 0 ORDER BY Sort";
						$res = mysql_query($query);
						while($row = mysql_fetch_array($res))
						{
						echo '<label><input '.(in_array($row['ID'], $Cat) ? "checked = checked" : "").' type="checkbox" name="Category[]" value="'.$row['ID'].'"/>'.$row['CategoryName'].'</label>';
						
							$query1 = "SELECT CategoryName,ID FROM categories where Parent = ".$row['ID']." ORDER BY Sort";
							$res1 = mysql_query($query1);
							while($row1 = mysql_fetch_array($res1))
							{
							echo '<label><input '.(in_array($row1['ID'], $Cat) ? "checked = checked" : "").' type="checkbox" name="Category[]" value="'.$row1['ID'].'"/>--------'.$row1['CategoryName'].'</label>';

									$query2 = "SELECT CategoryName,ID FROM categories where Parent = ".$row1['ID']." ORDER BY Sort";
									$res2 = mysql_query($query2);
									while($row2 = mysql_fetch_array($res2))
									{
									echo '<label><input '.(in_array($row2['ID'], $Cat) ? "checked = checked" : "").' type="checkbox" name="Category[]" value="'.$row2['ID'].'"/>----------------'.$row2['CategoryName'].'</label>';						
									}
							}
						}
						?>
				  </div>
                </div>
				
				<div class="form-group">
                  <label id="labelimp" for="RelatedProducts" >Related Products: </label>
                 <div class="selectBox" onclick="showCheckboxes2()">
						<select class="form-control">
							<option>Select Related Products</option>
						</select>
						<div class="overSelect"></div>
					</div>
					<div id="checkboxes2" style="height:250px; overflow:scroll;">
						
						<?php
						$query = "SELECT ProductName,ID FROM products WHERE ID <> 0 AND ID <> ".$ID." AND Status = 1 AND (ID IN (".$RelatedProducts.") OR Categories IN (".$Category.")) ORDER BY ProductName ASC ";
						$res = mysql_query($query);
						while($row = mysql_fetch_array($res))
						{
						echo '<label><input '.(in_array($row['ID'], $RelProd) ? "checked = checked" : "").' type="checkbox" name="RelatedProducts[]" value="'.$row['ID'].'"/> '.$row['ProductName'].'</label>';
						}
						?>
				  </div>
                </div>
				
				<div class="form-group">
                  <label id="labelimp" for="Ratings">Ratings: </label>
                  <?php 
				echo '<input type="number" min="0" max="100" id="Ratings" name="Ratings" class="form-control"  value="'.$Ratings.'" />';
				?>
                </div>
				
				<div class="form-group">
                  <label id="labelimp" for="Discount">Discounted Price: </label>
                  <?php 
				echo '<input type="text" maxlength="50" id="Discount" name="Discount" class="form-control"  value="'.$Discount.'" />';
				?>
                </div>
				
				<div class="form-group">
                  <label id="labelimp" for="Quantity">Quantity: </label>
                  <?php 
				echo '<input type="text" maxlength="50" id="Quantity" name="Quantity" class="form-control"  value="'.$Quantity.'" />';
				?>
                </div>
				
				<div class="form-group" style="display: none;">
                  <label id="labelimp" for="Shipping" >Shipping: </label>
                  
				  <select name="Shipping" id="Shipping" class="form-control">
					<option value="0" >Select Shipping Method</option>
					<?php
					 $query = "SELECT ShippingName,ID FROM shippings where Status = 1";
					$res = mysql_query($query);
					while($row = mysql_fetch_array($res))
					{
					echo '<option '.($Shipping == $row['ID'] ? 'selected' : '').' value="'.$row['ID'].'">'.$row['ShippingName'].'</option>';
					}
					?>
					</select>
                </div>
				
				<div class="form-group">
                  <label id="labelimp" for="Slider">Options:<a class="Optbtn" style="float:right; cursor:pointer">Add More</a></label>
     
					<?php
					$query = "SELECT po.ID,o.OptionName,v.ValueName,po.Increment,po.ValueID FROM product_options po LEFT JOIN p_options o ON po.OptionID = o.ID LEFT JOIN p_options_values v ON po.ValueID = v.ID where ProductID = ".$ID." AND po.ValueID <> 0";
					$res = mysql_query($query); $i = 0;
					while($row = mysql_fetch_array($res))
					{
					?>
					<div class="row">
					<div class="col-md-3">
					<input type="text" maxlength="100" disabled class="form-control"  value="<?php echo $row['OptionName']; ?>" />
					</div>
					
					<div class="col-md-3">
					<input type="text" maxlength="100" disabled class="form-control"  value="<?php echo $row['ValueName']; ?>" />
					</div>
					
					<div class="col-md-2">
					<input type="number" name="Inc" class="form-control" id="inc<?php echo $i; ?>"  value="<?php echo $row['Increment']; ?>" />
					</div>
					
					<div class="col-md-2">
					<input type="hidden" name="ProdID" value="<?php echo $ID; ?>">
					<input type="hidden" name="OptID" value="<?php echo $row['ID']; ?>">
					<a class="btn btn-success" onclick="window.location='UpdateOption.php?ProdID=<?php echo $ID; ?>&OptID=<?php echo $row['ID']; ?>&Inc='+document.getElementById('inc<?php echo $i; ?>').value">Update</a>
					</div>
					<div class="col-md-2">
					<button class="btn btn-danger" type="button" onClick="location.href='DeleteOption.php?OptID=<?php echo $row['ID']; ?>&ProdID=<?php echo $ID; ?>&ValID=<?php echo $row['ValueID']; ?>'">Delete</button>
					</div>
					<br>
					</div>
					<br>
					<?php
					$i++; }
					?>
				
				  
				  <span text="1" id="noofoptions"></span>
                  <div id="options">
					<div class="row">
					<div class="col-md-4">
					<select name="Option[]" id="Option0" class="form-control">
					<option value="0" >Option</option>
					<?php
					$query = "SELECT OptionName,ID FROM p_options where Status = 1";
					$res = mysql_query($query);
					while($row = mysql_fetch_array($res))
					{
					echo '<option  value="'.$row['ID'].'">'.$row['OptionName'].'</option>';
					}
					?>
					</select>
					</div>
					
					<div class="col-md-4">
					<select name="Value[]" id="Value0" class="form-control">

					</select>
					</div>
					
					<div class="col-md-4">
					<?php 
					echo '<input type="number" maxlength="50" class="Increment" name="Increment[]" class="form-control"  value="0" />';
					?>
					</div><br>
					
				  </div>
				  </div>
                </div>
				
                <input type="hidden" name="action" value="submit_form" />
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
            <!-- Form Element sizes -->
          </div>
        </div>
      </section>
    </form>
    <!-- /.content -->
  </aside>
  <!-- /.right-side -->
</div>
<!-- ./wrapper -->
<?php include_once("Footer.php"); ?>
<!-- add new calendar event modal -->
<!-- jQuery 2.0.2 -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<!-- jQuery UI 1.10.3 -->
<script src="../admin/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="../admin/js/bootstrap.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="../admin/js/AdminLTE/app.js" type="text/javascript"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="../admin/js/AdminLTE/dashboard.js" type="text/javascript"></script>
</body>
</html>