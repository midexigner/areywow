<?php 
require_once 'admin/Common.php'; 
require_once 'functions.php'; 
require_once 'include/head.php'; 
 $CatID=99999;

if(isset($_SESSION['LoginCustomer']) && $_SESSION['LoginCustomer']==true)
	redirect("index.php");
$msg="";
$Status=1;
$Gender=0;
$Subscribe=1;
$ID=0;
$FirstName="";
$LastName="";
$Address1="";
$Address2="";
$Region="";
$PostCode="";
$Fax="";
$Phone="";
$Company="";
$Country="XXX";
$City="";
$Email="";
$Password="";
$Password2="";
$IP = '';
function get_client_ip() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}
$IP = get_client_ip();
if(isset($_POST["action"]) && $_POST["action"] == "submit_form")
{
	if(isset($_POST["Subscribe"]) && ((int)$_POST["Subscribe"] == 0 || (int)$_POST["Subscribe"] == 1))
		$Subscribe=trim($_POST["Subscribe"]);
	if(isset($_POST["Gender"]) && ((int)$_POST["Gender"] == 1 || (int)$_POST["Gender"] == 2))
		$Gender=trim($_POST["Gender"]);
	if(isset($_POST["FirstName"]))
		$FirstName=trim($_POST["FirstName"]);
	if(isset($_POST["LastName"]))
		$LastName=trim($_POST["LastName"]);
	if(isset($_POST["Password"]))
		$Password=trim($_POST["Password"]);
	if(isset($_POST["Password2"]))
		$Password2=trim($_POST["Password2"]);
	/*if(isset($_POST["Fax"]))
		$Fax=trim($_POST["Fax"]);*/
	if(isset($_POST["City"]))
		$City=trim($_POST["City"]);
/*	if(isset($_POST["Company"]))
		$Company=trim($_POST["Company"]);*/
	if(isset($_POST["Phone"]))
		$Phone=trim($_POST["Phone"]);
	if(isset($_POST["PostCode"]))
		$PostCode=trim($_POST["PostCode"]);
	if(isset($_POST["Region"]))
		$Region=trim($_POST["Region"]);
	if(isset($_POST["Address1"]))
		$Address1=trim($_POST["Address1"]);
/*	if(isset($_POST["Address2"]))
		$Address2=trim($_POST["Address2"]);*/
	if(isset($_POST["Email"]))
		$Email=trim($_POST["Email"]);
	if(isset($_POST["Country"]))
		$Country=trim($_POST["Country"]);

		if($Gender == 0)
		{
			$msg='<p  class="alert alert-danger">Please Select Gender</p>';
		}
		else if($FirstName == "")
		{
			$msg='<p  class="alert alert-danger">Please Enter First Name</p>';
		}
		else if($LastName == "")
		{
			$msg='<p  class="alert alert-danger">Please Enter Last Name</p>';
		}
		else if($Email == "")
		{
			$msg='<p  class="alert alert-danger">Please Enter EmailAddresss</p>';
		}
		else if(!validEmailAddress($Email))
		{
			$msg='<p  class="alert alert-danger">EmailAddress is not Valid!</p>';
		}else if($Phone == "")
		{
			$msg='<p  class="alert alert-danger">Please Enter Phone Number</p>';
		}
		
		else if($Address1 == "")
		{
			$msg='<p  class="alert alert-danger">Please Enter Address</p>';
		}
		else if($City == "")
		{
			$msg='<p  class="alert alert-danger">Please Enter City</p>';
		}
		else if($Country == "XXX")
		{
			$msg='<p  class="alert alert-danger">Please Select Country</p>';
		}
		else if($Region == "")
		{
			$msg='<p  class="alert alert-danger"Please Enter Region / State</p>';
		}
		else if($Password == "")
		{
			$msg='<p  class="alert alert-danger">Please Enter Password</p>';
		}
		else if($Password2 == "")
		{
			$msg='<p  class="alert alert-danger">Please Confirm Password</p>';
		}
		else if($Password != $Password2)
		{
			$msg='<p class="alert alert-danger">Password not Matching</p>';
		}else if($Email != "")
		{
			if(!validEmailAddress($Email))
		        {
			$msg='<p class="alert alert-danger">EmailAddress is not Valid!</p>';
		        }

                        $sql3 = "SELECT ID from website_users WHERE Email= '".$Email."'";
			$res3 = mysql_query($sql3) or die(mysql_error());
			$unum3 = mysql_num_rows($res3);
			if($unum3 > 0)
			{
				$msg='<p class="alert alert-danger">Email Address is already Registered</p>';
			}
		}




	if($msg=="")
	{

		$query="INSERT INTO website_users SET DateAdded=NOW(),
				Status='".(int)$Status . "',
				Gender='".(int)$Gender . "',
				Subscribe='".(int)$Subscribe . "',
				FirstName = '" . dbinput($FirstName) . "',
				LastName = '" . dbinput($LastName) . "',
				IP = '" . dbinput($IP) . "',
				Region = '" . dbinput($Region) . "',
				Country = '" . dbinput($Country) . "',
				PostCode = '" . dbinput($PostCode) . "',
				Company = '" . dbinput($Company) . "',
				Fax = '" . dbinput($Fax) . "',
				City = '" . dbinput($City) . "',
				Phone = '" . dbinput($Phone) . "',
				Email = '" . dbinput($Email) . "',
				Password = '" . dbinput($Password) . "',
				Address = '" . dbinput($Address1) . " ".dbinput($Address2)."'";
		mysql_query($query) or die ('Could not add user because: ' . mysql_error());
		// echo $query;
		//$ID = mysql_insert_id();

		
		if($Subscribe == 1)
		{
				$Email = strtolower($Email);
				$query="SELECT ID FROM newsletter_subscribers WHERE Email='".dbinput($Email) . "'";
				$r=mysql_query($query) or die (mysql_error());
				$n=mysql_num_rows($r);
				if($n==0)
				{
					$query="INSERT INTO newsletter_subscribers SET Email='".dbinput($Email) . "', DateAdded=NOW()";
					mysql_query($query) or die (mysql_error());
				}
		}
		$_SESSION["msg"]='<p class="alert alert-success">User has been Registred Successfully | Please Login Now</p>';

		redirect("register.php");
	}


}

require_once 'include/headerpartial.php'; 
?>



            <div id="content" class="site-content" tabindex="-1">
	<div class="container">

		<nav class="woocommerce-breadcrumb"><a href="<?php echo BASE_URL; ?>">Home</a><span class="delimiter"><i class="fa fa-angle-right"></i></span>Register</nav>

		<div id="primary" class="content-area">
			<main id="main" class="site-main">
				<article class="page type-page status-publish hentry">
					<header class="entry-header"><h1 itemprop="name" class="entry-title">Register</h1></header><!-- .entry-header -->

					<?php
				echo $msg;
				if(isset($_SESSION["msg"]))
				{
				echo $_SESSION["msg"];
				$_SESSION["msg"]="";
				}
				?>
<div class="row">
	<div class="panel-body">
	            <form action="<?php echo $_SERVER["PHP_SELF"];?>" method="post" enctype="multipart/form-data" class="login reg" >
	              <div class="col-md-6">
	              <h2>Your Personal Details</h2>

	  <div class="form-group col-md-12">
	    <label for="fname">First Name <span class="red-text">*</span></label>
	    <input type="text" class="form-control" name="FirstName" value="<?php echo $FirstName; ?>" id="input-firstname"  required>
	  </div>
	 <div class="form-group col-md-12">
	    <label for="lname">Last Name <span class="red-text">*</span></label>
	    <input type="text" class="form-control" name="LastName" value="<?php echo $LastName; ?>" id="input-lastname" required>
	  </div>
	  <div class="form-group col-md-12">
	    <label for="fname">Email <span class="red-text">*</span></label>
	    <input type="email" class="form-control" name="Email" value="<?php echo $Email; ?>" id="input-email"  required>
	  </div>

	   <div class="form-group col-md-12">
	    <label for="telephone">Telephone </label>
	    <input type="tel" class="form-control" name="Phone" value="<?php echo $Phone; ?>" id="input-telephone">
	  </div>
<!-- 
	  <div class="form-group col-md-12">
	    <label for="fax">Fax </label>
	    <input type="tel" class="form-control" name="Fax" value="<?php echo $Fax; ?>" id="input-fax" >
	  </div> -->
	   <div class="col-md-12">
	 <div class="row">
	 
	 <legend>Your Password</legend>
	 
	   <div class="form-group col-md-12">
	    <label for="password">Password <span class="red-text">*</span></label>
	    <input type="password" class="form-control" name="Password" value="<?php echo $Password; ?>" id="input-password" required>
	  </div>
</div>
	  </div>
	   <div class="form-group col-md-12">
	    <label for="passwordConfirm">Password Confirm <span class="red-text">*</span></label>
	    <input type="password" class="form-control" name="Password2" value="<?php echo $Password2; ?>" id="input-confirm" required>
	  </div>
 <div class="form-group col-md-12">
		<fieldset>
			<legend>Newsletter</legend>
			<div class="form-group">
				<label class="col-sm-3 control-label">Subscribe</label>
				<div class="col-sm-9">
					<label class="radio-inline"><input type="radio" <?php echo ($Subscribe == 1 ? 'checked' : ''); ?> name="Subscribe" value="1">Yes</label>
					<label class="radio-inline"><input type="radio" name="Subscribe" value="0" <?php echo ($Subscribe == 0 ? 'checked' : ''); ?>>No</label>
				</div>
			</div>
		</fieldset>
</div>


	              </div>


	                       <div class="col-md-6">

	         <h2>Your Address</h2>
	  <!-- <div class="form-group col-md-12">
	    <label for="comapny">Company</label>
	    <input type="text" class="form-control" name="Company" value="<?php echo $Company; ?>"  id="input-company" >
	  </div> -->
	  <div class="form-group col-md-12">
	    <label for="address1">Address <span class="red-text">*</span></label>
	    <input type="text" class="form-control" name="Address1" value="<?php echo $Address1; ?>" id="input-address-1"  required>
	  </div>
	  <!--  <div class="form-group col-md-12">
	    <label for="address2">Address 2 <span class="red-text">*</span></label>
	    <input type="text" class="form-control" name="Address2" value="<?php echo $Address2; ?>" id="input-address-2"  required>
	  </div> -->

	  <div class="form-group col-md-12">
	    <label for="city">City<span class="red-text">*</span></label>
	    <input type="text" class="form-control" name="City" value="<?php echo $City; ?>" id="input-city" required>
	  </div>

	   <div class="form-group col-md-12">
	    <label for="postalcode">Postal Code</label>
	    <input type="text" class="form-control" name="PostCode" value="<?php echo $PostCode; ?>" id="input-postcode">
	  </div>

	   <div class="form-group col-md-12">
	    <label for="input-country">Country<span class="red-text">*</span></label>
	 <select class="form-control"  name="Country" id="input-country" required>
		 <option <?php echo ($Country == 'XXX' ? 'selected' : ''); ?> value="XXX"> --- Please Select --- </option>
		 <option <?php echo ($Country == 'AFG' ? 'selected' : ''); ?> value="AFG">Afghanistan</option>
		 <option <?php echo ($Country == 'ALA' ? 'selected' : ''); ?> value="ALA">Aland Islands</option>
		 <option <?php echo ($Country == 'ALB' ? 'selected' : ''); ?> value="ALB">Albania</option>
		 <option <?php echo ($Country == 'DZA' ? 'selected' : ''); ?> value="DZA">Algeria</option>
		 <option <?php echo ($Country == 'ASM' ? 'selected' : ''); ?> value="ASM">American Samoa</option>
		 <option <?php echo ($Country == 'AND' ? 'selected' : ''); ?> value="AND">Andorra</option>
		 <option <?php echo ($Country == 'AGO' ? 'selected' : ''); ?> value="AGO">Angola</option>
		 <option <?php echo ($Country == 'AIA' ? 'selected' : ''); ?> value="AIA">Anguilla</option>
		 <option <?php echo ($Country == 'ATA' ? 'selected' : ''); ?> value="ATA">Antarctica</option>
		 <option <?php echo ($Country == 'ATG' ? 'selected' : ''); ?> value="ATG">Antigua and Barbuda</option>
		 <option <?php echo ($Country == 'ARG' ? 'selected' : ''); ?> value="ARG">Argentina</option>
		 <option <?php echo ($Country == 'ARM' ? 'selected' : ''); ?> value="ARM">Armenia</option>
		 <option <?php echo ($Country == 'ABW' ? 'selected' : ''); ?> value="ABW">Aruba</option>
		 <option <?php echo ($Country == 'AUS' ? 'selected' : ''); ?> value="AUS">Australia</option>
		 <option <?php echo ($Country == 'AUT' ? 'selected' : ''); ?> value="AUT">Austria</option>
		 <option <?php echo ($Country == 'AZE' ? 'selected' : ''); ?> value="AZE">Azerbaijan</option>
		 <option <?php echo ($Country == 'BHS' ? 'selected' : ''); ?> value="BHS">Bahamas</option>
		 <option <?php echo ($Country == 'BHR' ? 'selected' : ''); ?> value="BHR">Bahrain</option>
		 <option <?php echo ($Country == 'BGD' ? 'selected' : ''); ?> value="BGD">Bangladesh</option>
		 <option <?php echo ($Country == 'BRB' ? 'selected' : ''); ?> value="BRB">Barbados</option>
		 <option <?php echo ($Country == 'BLR' ? 'selected' : ''); ?> value="BLR">Belarus</option>
		 <option <?php echo ($Country == 'BEL' ? 'selected' : ''); ?> value="BEL">Belgium</option>
		 <option <?php echo ($Country == 'BLZ' ? 'selected' : ''); ?> value="BLZ">Belize</option>
		 <option <?php echo ($Country == 'BEN' ? 'selected' : ''); ?> value="BEN">Benin</option>
		 <option <?php echo ($Country == 'BMU' ? 'selected' : ''); ?> value="BMU">Bermuda</option>
		 <option <?php echo ($Country == 'BTN' ? 'selected' : ''); ?> value="BTN">Bhutan</option>
		 <option <?php echo ($Country == 'BOL' ? 'selected' : ''); ?> value="BOL">Bolivia, Plurinational State of</option>
		 <option <?php echo ($Country == 'BES' ? 'selected' : ''); ?> value="BES">Bonaire, Sint Eustatius and Saba</option>
		 <option <?php echo ($Country == 'BIH' ? 'selected' : ''); ?> value="BIH">Bosnia and Herzegovina</option>
		 <option <?php echo ($Country == 'BWA' ? 'selected' : ''); ?> value="BWA">Botswana</option>
		 <option <?php echo ($Country == 'BVT' ? 'selected' : ''); ?> value="BVT">Bouvet Island</option>
		 <option <?php echo ($Country == 'BRA' ? 'selected' : ''); ?> value="BRA">Brazil</option>
		 <option <?php echo ($Country == 'IOT' ? 'selected' : ''); ?> value="IOT">British Indian Ocean Territory</option>
		 <option <?php echo ($Country == 'BRN' ? 'selected' : ''); ?> value="BRN">Brunei Darussalam</option>
		 <option <?php echo ($Country == 'BGR' ? 'selected' : ''); ?> value="BGR">Bulgaria</option>
		 <option <?php echo ($Country == 'BFA' ? 'selected' : ''); ?> value="BFA">Burkina Faso</option>
		 <option <?php echo ($Country == 'BDI' ? 'selected' : ''); ?> value="BDI">Burundi</option>
		 <option <?php echo ($Country == 'KHM' ? 'selected' : ''); ?> value="KHM">Cambodia</option>
		 <option <?php echo ($Country == 'CMR' ? 'selected' : ''); ?> value="CMR">Cameroon</option>
		 <option <?php echo ($Country == 'CAN' ? 'selected' : ''); ?> value="CAN">Canada</option>
		 <option <?php echo ($Country == 'CPV' ? 'selected' : ''); ?> value="CPV">Cape Verde</option>
		 <option <?php echo ($Country == 'CYM' ? 'selected' : ''); ?> value="CYM">Cayman Islands</option>
		 <option <?php echo ($Country == 'CAF' ? 'selected' : ''); ?> value="CAF">Central African Republic</option>
		 <option <?php echo ($Country == 'TCD' ? 'selected' : ''); ?> value="TCD">Chad</option>
		 <option <?php echo ($Country == 'CHL' ? 'selected' : ''); ?> value="CHL">Chile</option>
		 <option <?php echo ($Country == 'CHN' ? 'selected' : ''); ?> value="CHN">China</option>
		 <option <?php echo ($Country == 'CXR' ? 'selected' : ''); ?> value="CXR">Christmas Island</option>
		 <option <?php echo ($Country == 'CCK' ? 'selected' : ''); ?> value="CCK">Cocos (Keeling) Islands</option>
		 <option <?php echo ($Country == 'COL' ? 'selected' : ''); ?> value="COL">Colombia</option>
		 <option <?php echo ($Country == 'COM' ? 'selected' : ''); ?> value="COM">Comoros</option>
		 <option <?php echo ($Country == 'COG' ? 'selected' : ''); ?> value="COG">Congo</option>
		 <option <?php echo ($Country == 'COD' ? 'selected' : ''); ?> value="COD">Congo, the Democratic Republic of the</option>
		 <option <?php echo ($Country == 'COK' ? 'selected' : ''); ?> value="COK">Cook Islands</option>
		 <option <?php echo ($Country == 'CRI' ? 'selected' : ''); ?> value="CRI">Costa Rica</option>
		 <option <?php echo ($Country == 'CIV' ? 'selected' : ''); ?> value="CIV">Côte d'Ivoire</option>
		 <option <?php echo ($Country == 'HRV' ? 'selected' : ''); ?> value="HRV">Croatia</option>
		 <option <?php echo ($Country == 'CUB' ? 'selected' : ''); ?> value="CUB">Cuba</option>
		 <option <?php echo ($Country == 'CUW' ? 'selected' : ''); ?> value="CUW">Curaçao</option>
		 <option <?php echo ($Country == 'CYP' ? 'selected' : ''); ?> value="CYP">Cyprus</option>
		 <option <?php echo ($Country == 'CZE' ? 'selected' : ''); ?> value="CZE">Czech Republic</option>
		 <option <?php echo ($Country == 'DNK' ? 'selected' : ''); ?> value="DNK">Denmark</option>
		 <option <?php echo ($Country == 'DJI' ? 'selected' : ''); ?> value="DJI">Djibouti</option>
		 <option <?php echo ($Country == 'DMA' ? 'selected' : ''); ?> value="DMA">Dominica</option>
		 <option <?php echo ($Country == 'DOM' ? 'selected' : ''); ?> value="DOM">Dominican Republic</option>
		 <option <?php echo ($Country == 'ECU' ? 'selected' : ''); ?> value="ECU">Ecuador</option>
		 <option <?php echo ($Country == 'EGY' ? 'selected' : ''); ?> value="EGY">Egypt</option>
		 <option <?php echo ($Country == 'SLV' ? 'selected' : ''); ?> value="SLV">El Salvador</option>
		 <option <?php echo ($Country == 'GNQ' ? 'selected' : ''); ?> value="GNQ">Equatorial Guinea</option>
		 <option <?php echo ($Country == 'ERI' ? 'selected' : ''); ?> value="ERI">Eritrea</option>
		 <option <?php echo ($Country == 'EST' ? 'selected' : ''); ?> value="EST">Estonia</option>
		 <option <?php echo ($Country == 'ETH' ? 'selected' : ''); ?> value="ETH">Ethiopia</option>
		 <option <?php echo ($Country == 'FLK' ? 'selected' : ''); ?> value="FLK">Falkland Islands (Malvinas)</option>
		 <option <?php echo ($Country == 'FRO' ? 'selected' : ''); ?> value="FRO">Faroe Islands</option>
		 <option <?php echo ($Country == 'FJI' ? 'selected' : ''); ?> value="FJI">Fiji</option>
		 <option <?php echo ($Country == 'FIN' ? 'selected' : ''); ?> value="FIN">Finland</option>
		 <option <?php echo ($Country == 'FRA' ? 'selected' : ''); ?> value="FRA">France</option>
		 <option <?php echo ($Country == 'GUF' ? 'selected' : ''); ?> value="GUF">French Guiana</option>
		 <option <?php echo ($Country == 'PYF' ? 'selected' : ''); ?> value="PYF">French Polynesia</option>
		 <option <?php echo ($Country == 'ATF' ? 'selected' : ''); ?> value="ATF">French Southern Territories</option>
		 <option <?php echo ($Country == 'GAB' ? 'selected' : ''); ?> value="GAB">Gabon</option>
		 <option <?php echo ($Country == 'GMB' ? 'selected' : ''); ?> value="GMB">Gambia</option>
		 <option <?php echo ($Country == 'GEO' ? 'selected' : ''); ?> value="GEO">Georgia</option>
		 <option <?php echo ($Country == 'DEU' ? 'selected' : ''); ?> value="DEU">Germany</option>
		 <option <?php echo ($Country == 'GHA' ? 'selected' : ''); ?> value="GHA">Ghana</option>
		 <option <?php echo ($Country == 'GIB' ? 'selected' : ''); ?> value="GIB">Gibraltar</option>
		 <option <?php echo ($Country == 'GRC' ? 'selected' : ''); ?> value="GRC">Greece</option>
		 <option <?php echo ($Country == 'GRL' ? 'selected' : ''); ?> value="GRL">Greenland</option>
		 <option <?php echo ($Country == 'GRD' ? 'selected' : ''); ?> value="GRD">Grenada</option>
		 <option <?php echo ($Country == 'GLP' ? 'selected' : ''); ?> value="GLP">Guadeloupe</option>
		 <option <?php echo ($Country == 'GUM' ? 'selected' : ''); ?> value="GUM">Guam</option>
		 <option <?php echo ($Country == 'GTM' ? 'selected' : ''); ?> value="GTM">Guatemala</option>
		 <option <?php echo ($Country == 'GGY' ? 'selected' : ''); ?> value="GGY">Guernsey</option>
		 <option <?php echo ($Country == 'GIN' ? 'selected' : ''); ?> value="GIN">Guinea</option>
		 <option <?php echo ($Country == 'GNB' ? 'selected' : ''); ?> value="GNB">Guinea-Bissau</option>
		 <option <?php echo ($Country == 'GUY' ? 'selected' : ''); ?> value="GUY">Guyana</option>
		 <option <?php echo ($Country == 'HTI' ? 'selected' : ''); ?> value="HTI">Haiti</option>
		 <option <?php echo ($Country == 'HMD' ? 'selected' : ''); ?> value="HMD">Heard Island and McDonald Islands</option>
		 <option <?php echo ($Country == 'VAT' ? 'selected' : ''); ?> value="VAT">Holy See (Vatican City State)</option>
		 <option <?php echo ($Country == 'HND' ? 'selected' : ''); ?> value="HND">Honduras</option>
		 <option <?php echo ($Country == 'HKG' ? 'selected' : ''); ?> value="HKG">Hong Kong</option>
		 <option <?php echo ($Country == 'HUN' ? 'selected' : ''); ?> value="HUN">Hungary</option>
		 <option <?php echo ($Country == 'ISL' ? 'selected' : ''); ?> value="ISL">Iceland</option>
		 <option <?php echo ($Country == 'IND' ? 'selected' : ''); ?> value="IND">India</option>
		 <option <?php echo ($Country == 'IDN' ? 'selected' : ''); ?> value="IDN">Indonesia</option>
		 <option <?php echo ($Country == 'IRN' ? 'selected' : ''); ?> value="IRN">Iran, Islamic Republic of</option>
		 <option <?php echo ($Country == 'IRQ' ? 'selected' : ''); ?> value="IRQ">Iraq</option>
		 <option <?php echo ($Country == 'IRL' ? 'selected' : ''); ?> value="IRL">Ireland</option>
		 <option <?php echo ($Country == 'IMN' ? 'selected' : ''); ?> value="IMN">Isle of Man</option>
		 <option <?php echo ($Country == 'ISR' ? 'selected' : ''); ?> value="ISR">Israel</option>
		 <option <?php echo ($Country == 'ITA' ? 'selected' : ''); ?> value="ITA">Italy</option>
		 <option <?php echo ($Country == 'JAM' ? 'selected' : ''); ?> value="JAM">Jamaica</option>
		 <option <?php echo ($Country == 'JPN' ? 'selected' : ''); ?> value="JPN">Japan</option>
		 <option <?php echo ($Country == 'JEY' ? 'selected' : ''); ?> value="JEY">Jersey</option>
		 <option <?php echo ($Country == 'JOR' ? 'selected' : ''); ?> value="JOR">Jordan</option>
		 <option <?php echo ($Country == 'KAZ' ? 'selected' : ''); ?> value="KAZ">Kazakhstan</option>
		 <option <?php echo ($Country == 'KEN' ? 'selected' : ''); ?> value="KEN">Kenya</option>
		 <option <?php echo ($Country == 'KIR' ? 'selected' : ''); ?> value="KIR">Kiribati</option>
		 <option <?php echo ($Country == 'PRK' ? 'selected' : ''); ?> value="PRK">Korea, Democratic People's Republic of</option>
		 <option <?php echo ($Country == 'KOR' ? 'selected' : ''); ?> value="KOR">Korea, Republic of</option>
		 <option <?php echo ($Country == 'KWT' ? 'selected' : ''); ?> value="KWT">Kuwait</option>
		 <option <?php echo ($Country == 'KGZ' ? 'selected' : ''); ?> value="KGZ">Kyrgyzstan</option>
		 <option <?php echo ($Country == 'LAO' ? 'selected' : ''); ?> value="LAO">Lao People's Democratic Republic</option>
		 <option <?php echo ($Country == 'LVA' ? 'selected' : ''); ?> value="LVA">Latvia</option>
		 <option <?php echo ($Country == 'LBN' ? 'selected' : ''); ?> value="LBN">Lebanon</option>
		 <option <?php echo ($Country == 'LSO' ? 'selected' : ''); ?> value="LSO">Lesotho</option>
		 <option <?php echo ($Country == 'LBR' ? 'selected' : ''); ?> value="LBR">Liberia</option>
		 <option <?php echo ($Country == 'LBY' ? 'selected' : ''); ?> value="LBY">Libya</option>
		 <option <?php echo ($Country == 'LIE' ? 'selected' : ''); ?> value="LIE">Liechtenstein</option>
		 <option <?php echo ($Country == 'LTU' ? 'selected' : ''); ?> value="LTU">Lithuania</option>
		 <option <?php echo ($Country == 'LUX' ? 'selected' : ''); ?> value="LUX">Luxembourg</option>
		 <option <?php echo ($Country == 'MAC' ? 'selected' : ''); ?> value="MAC">Macao</option>
		 <option <?php echo ($Country == 'MKD' ? 'selected' : ''); ?> value="MKD">Macedonia, the former Yugoslav Republic of</option>
		 <option <?php echo ($Country == 'MDG' ? 'selected' : ''); ?> value="MDG">Madagascar</option>
		 <option <?php echo ($Country == 'MWI' ? 'selected' : ''); ?> value="MWI">Malawi</option>
		 <option <?php echo ($Country == 'MYS' ? 'selected' : ''); ?> value="MYS">Malaysia</option>
		 <option <?php echo ($Country == 'MDV' ? 'selected' : ''); ?> value="MDV">Maldives</option>
		 <option <?php echo ($Country == 'MLI' ? 'selected' : ''); ?> value="MLI">Mali</option>
		 <option <?php echo ($Country == 'MLT' ? 'selected' : ''); ?> value="MLT">Malta</option>
		 <option <?php echo ($Country == 'MHL' ? 'selected' : ''); ?> value="MHL">Marshall Islands</option>
		 <option <?php echo ($Country == 'MTQ' ? 'selected' : ''); ?> value="MTQ">Martinique</option>
		 <option <?php echo ($Country == 'MRT' ? 'selected' : ''); ?> value="MRT">Mauritania</option>
		 <option <?php echo ($Country == 'MUS' ? 'selected' : ''); ?> value="MUS">Mauritius</option>
		 <option <?php echo ($Country == 'MYT' ? 'selected' : ''); ?> value="MYT">Mayotte</option>
		 <option <?php echo ($Country == 'MEX' ? 'selected' : ''); ?> value="MEX">Mexico</option>
		 <option <?php echo ($Country == 'FSM' ? 'selected' : ''); ?> value="FSM">Micronesia, Federated States of</option>
		 <option <?php echo ($Country == 'MDA' ? 'selected' : ''); ?> value="MDA">Moldova, Republic of</option>
		 <option <?php echo ($Country == 'MCO' ? 'selected' : ''); ?> value="MCO">Monaco</option>
		 <option <?php echo ($Country == 'MNG' ? 'selected' : ''); ?> value="MNG">Mongolia</option>
		 <option <?php echo ($Country == 'MNE' ? 'selected' : ''); ?> value="MNE">Montenegro</option>
		 <option <?php echo ($Country == 'MSR' ? 'selected' : ''); ?> value="MSR">Montserrat</option>
		 <option <?php echo ($Country == 'MAR' ? 'selected' : ''); ?> value="MAR">Morocco</option>
		 <option <?php echo ($Country == 'MOZ' ? 'selected' : ''); ?> value="MOZ">Mozambique</option>
		 <option <?php echo ($Country == 'MMR' ? 'selected' : ''); ?> value="MMR">Myanmar</option>
		 <option <?php echo ($Country == 'NAM' ? 'selected' : ''); ?> value="NAM">Namibia</option>
		 <option <?php echo ($Country == 'NRU' ? 'selected' : ''); ?> value="NRU">Nauru</option>
		 <option <?php echo ($Country == 'NPL' ? 'selected' : ''); ?> value="NPL">Nepal</option>
		 <option <?php echo ($Country == 'NLD' ? 'selected' : ''); ?> value="NLD">Netherlands</option>
		 <option <?php echo ($Country == 'NCL' ? 'selected' : ''); ?> value="NCL">New Caledonia</option>
		 <option <?php echo ($Country == 'NZL' ? 'selected' : ''); ?> value="NZL">New Zealand</option>
		 <option <?php echo ($Country == 'NIC' ? 'selected' : ''); ?> value="NIC">Nicaragua</option>
		 <option <?php echo ($Country == 'NER' ? 'selected' : ''); ?> value="NER">Niger</option>
		 <option <?php echo ($Country == 'NGA' ? 'selected' : ''); ?> value="NGA">Nigeria</option>
		 <option <?php echo ($Country == 'NIU' ? 'selected' : ''); ?> value="NIU">Niue</option>
		 <option <?php echo ($Country == 'NFK' ? 'selected' : ''); ?> value="NFK">Norfolk Island</option>
		 <option <?php echo ($Country == 'MNP' ? 'selected' : ''); ?> value="MNP">Northern Mariana Islands</option>
		 <option <?php echo ($Country == 'NOR' ? 'selected' : ''); ?> value="NOR">Norway</option>
		 <option <?php echo ($Country == 'OMN' ? 'selected' : ''); ?> value="OMN">Oman</option>
		 <option <?php echo ($Country == 'PAK' ? 'selected' : ''); ?> value="PAK">Pakistan</option>
		 <option <?php echo ($Country == 'PLW' ? 'selected' : ''); ?> value="PLW">Palau</option>
		 <option <?php echo ($Country == 'PSE' ? 'selected' : ''); ?> value="PSE">Palestinian Territory, Occupied</option>
		 <option <?php echo ($Country == 'PAN' ? 'selected' : ''); ?> value="PAN">Panama</option>
		 <option <?php echo ($Country == 'PNG' ? 'selected' : ''); ?> value="PNG">Papua New Guinea</option>
		 <option <?php echo ($Country == 'PRY' ? 'selected' : ''); ?> value="PRY">Paraguay</option>
		 <option <?php echo ($Country == 'PER' ? 'selected' : ''); ?> value="PER">Peru</option>
		 <option <?php echo ($Country == 'PHL' ? 'selected' : ''); ?> value="PHL">Philippines</option>
		 <option <?php echo ($Country == 'PCN' ? 'selected' : ''); ?> value="PCN">Pitcairn</option>
		 <option <?php echo ($Country == 'POL' ? 'selected' : ''); ?> value="POL">Poland</option>
		 <option <?php echo ($Country == 'PRT' ? 'selected' : ''); ?> value="PRT">Portugal</option>
		 <option <?php echo ($Country == 'PRI' ? 'selected' : ''); ?> value="PRI">Puerto Rico</option>
		 <option <?php echo ($Country == 'QAT' ? 'selected' : ''); ?> value="QAT">Qatar</option>
		 <option <?php echo ($Country == 'REU' ? 'selected' : ''); ?> value="REU">Réunion</option>
		 <option <?php echo ($Country == 'ROU' ? 'selected' : ''); ?> value="ROU">Romania</option>
		 <option <?php echo ($Country == 'RUS' ? 'selected' : ''); ?> value="RUS">Russian Federation</option>
		 <option <?php echo ($Country == 'RWA' ? 'selected' : ''); ?> value="RWA">Rwanda</option>
		 <option <?php echo ($Country == 'BLM' ? 'selected' : ''); ?> value="BLM">Saint Barthélemy</option>
		 <option <?php echo ($Country == 'SHN' ? 'selected' : ''); ?> value="SHN">Saint Helena, Ascension and Tristan da Cunha</option>
		 <option <?php echo ($Country == 'KNA' ? 'selected' : ''); ?> value="KNA">Saint Kitts and Nevis</option>
		 <option <?php echo ($Country == 'LCA' ? 'selected' : ''); ?> value="LCA">Saint Lucia</option>
		 <option <?php echo ($Country == 'MAF' ? 'selected' : ''); ?> value="MAF">Saint Martin (French part)</option>
		 <option <?php echo ($Country == 'SPM' ? 'selected' : ''); ?> value="SPM">Saint Pierre and Miquelon</option>
		 <option <?php echo ($Country == 'VCT' ? 'selected' : ''); ?> value="VCT">Saint Vincent and the Grenadines</option>
		 <option <?php echo ($Country == 'WSM' ? 'selected' : ''); ?> value="WSM">Samoa</option>
		 <option <?php echo ($Country == 'SMR' ? 'selected' : ''); ?> value="SMR">San Marino</option>
		 <option <?php echo ($Country == 'STP' ? 'selected' : ''); ?> value="STP">Sao Tome and Principe</option>
		 <option <?php echo ($Country == 'SAU' ? 'selected' : ''); ?> value="SAU">Saudi Arabia</option>
		 <option <?php echo ($Country == 'SEN' ? 'selected' : ''); ?> value="SEN">Senegal</option>
		 <option <?php echo ($Country == 'SRB' ? 'selected' : ''); ?> value="SRB">Serbia</option>
		 <option <?php echo ($Country == 'SYC' ? 'selected' : ''); ?> value="SYC">Seychelles</option>
		 <option <?php echo ($Country == 'SLE' ? 'selected' : ''); ?> value="SLE">Sierra Leone</option>
		 <option <?php echo ($Country == 'SGP' ? 'selected' : ''); ?> value="SGP">Singapore</option>
		 <option <?php echo ($Country == 'SXM' ? 'selected' : ''); ?> value="SXM">Sint Maarten (Dutch part)</option>
		 <option <?php echo ($Country == 'SVK' ? 'selected' : ''); ?> value="SVK">Slovakia</option>
		 <option <?php echo ($Country == 'SVN' ? 'selected' : ''); ?> value="SVN">Slovenia</option>
		 <option <?php echo ($Country == 'SLB' ? 'selected' : ''); ?> value="SLB">Solomon Islands</option>
		 <option <?php echo ($Country == 'SOM' ? 'selected' : ''); ?> value="SOM">Somalia</option>
		 <option <?php echo ($Country == 'ZAF' ? 'selected' : ''); ?> value="ZAF">South Africa</option>
		 <option <?php echo ($Country == 'SGS' ? 'selected' : ''); ?> value="SGS">South Georgia and the South Sandwich Islands</option>
		 <option <?php echo ($Country == 'SSD' ? 'selected' : ''); ?> value="SSD">South Sudan</option>
		 <option <?php echo ($Country == 'ESP' ? 'selected' : ''); ?> value="ESP">Spain</option>
		 <option <?php echo ($Country == 'LKA' ? 'selected' : ''); ?> value="LKA">Sri Lanka</option>
		 <option <?php echo ($Country == 'SDN' ? 'selected' : ''); ?> value="SDN">Sudan</option>
		 <option <?php echo ($Country == 'SUR' ? 'selected' : ''); ?> value="SUR">Suriname</option>
		 <option <?php echo ($Country == 'SJM' ? 'selected' : ''); ?> value="SJM">Svalbard and Jan Mayen</option>
		 <option <?php echo ($Country == 'SWZ' ? 'selected' : ''); ?> value="SWZ">Swaziland</option>
		 <option <?php echo ($Country == 'SWE' ? 'selected' : ''); ?> value="SWE">Sweden</option>
		 <option <?php echo ($Country == 'CHE' ? 'selected' : ''); ?> value="CHE">Switzerland</option>
		 <option <?php echo ($Country == 'SYR' ? 'selected' : ''); ?> value="SYR">Syrian Arab Republic</option>
		 <option <?php echo ($Country == 'TWN' ? 'selected' : ''); ?> value="TWN">Taiwan, Province of China</option>
		 <option <?php echo ($Country == 'TJK' ? 'selected' : ''); ?> value="TJK">Tajikistan</option>
		 <option <?php echo ($Country == 'TZA' ? 'selected' : ''); ?> value="TZA">Tanzania, United Republic of</option>
		 <option <?php echo ($Country == 'THA' ? 'selected' : ''); ?> value="THA">Thailand</option>
		 <option <?php echo ($Country == 'TLS' ? 'selected' : ''); ?> value="TLS">Timor-Leste</option>
		 <option <?php echo ($Country == 'TGO' ? 'selected' : ''); ?> value="TGO">Togo</option>
		 <option <?php echo ($Country == 'TKL' ? 'selected' : ''); ?> value="TKL">Tokelau</option>
		 <option <?php echo ($Country == 'TON' ? 'selected' : ''); ?> value="TON">Tonga</option>
		 <option <?php echo ($Country == 'TTO' ? 'selected' : ''); ?> value="TTO">Trinidad and Tobago</option>
		 <option <?php echo ($Country == 'TUN' ? 'selected' : ''); ?> value="TUN">Tunisia</option>
		 <option <?php echo ($Country == 'TUR' ? 'selected' : ''); ?> value="TUR">Turkey</option>
		 <option <?php echo ($Country == 'TKM' ? 'selected' : ''); ?> value="TKM">Turkmenistan</option>
		 <option <?php echo ($Country == 'TCA' ? 'selected' : ''); ?> value="TCA">Turks and Caicos Islands</option>
		 <option <?php echo ($Country == 'TUV' ? 'selected' : ''); ?> value="TUV">Tuvalu</option>
		 <option <?php echo ($Country == 'UGA' ? 'selected' : ''); ?> value="UGA">Uganda</option>
		 <option <?php echo ($Country == 'UKR' ? 'selected' : ''); ?> value="UKR">Ukraine</option>
		 <option <?php echo ($Country == 'ARE' ? 'selected' : ''); ?> value="ARE">United Arab Emirates</option>
		 <option <?php echo ($Country == 'GBR' ? 'selected' : ''); ?> value="GBR">United Kingdom</option>
		 <option <?php echo ($Country == 'USA' ? 'selected' : ''); ?> value="USA">United States</option>
		 <option <?php echo ($Country == 'UMI' ? 'selected' : ''); ?> value="UMI">United States Minor Outlying Islands</option>
		 <option <?php echo ($Country == 'URY' ? 'selected' : ''); ?> value="URY">Uruguay</option>
		 <option <?php echo ($Country == 'UZB' ? 'selected' : ''); ?> value="UZB">Uzbekistan</option>
		 <option <?php echo ($Country == 'VUT' ? 'selected' : ''); ?> value="VUT">Vanuatu</option>
		 <option <?php echo ($Country == 'VEN' ? 'selected' : ''); ?> value="VEN">Venezuela, Bolivarian Republic of</option>
		 <option <?php echo ($Country == 'VNM' ? 'selected' : ''); ?> value="VNM">Viet Nam</option>
		 <option <?php echo ($Country == 'VGB' ? 'selected' : ''); ?> value="VGB">Virgin Islands, British</option>
		 <option <?php echo ($Country == 'VIR' ? 'selected' : ''); ?> value="VIR">Virgin Islands, U.S.</option>
		 <option <?php echo ($Country == 'WLF' ? 'selected' : ''); ?> value="WLF">Wallis and Futuna</option>
		 <option <?php echo ($Country == 'ESH' ? 'selected' : ''); ?> value="ESH">Western Sahara</option>
		 <option <?php echo ($Country == 'YEM' ? 'selected' : ''); ?> value="YEM">Yemen</option>
		 <option <?php echo ($Country == 'ZMB' ? 'selected' : ''); ?> value="ZMB">Zambia</option>
		 <option <?php echo ($Country == 'ZWE' ? 'selected' : ''); ?> value="ZWE">Zimbabwe</option>
	</select>
	</div>

	   <div class="form-group col-md-12">
	    <label for="region">Region / State</label>
			 <input type="text" class="form-control" name="Region" value="<?php echo $Region; ?>"  id="input-region">

	</div>

	<div class="form-group col-md-12">
	 <label for="gender">Gender</label>
	 <select name="Gender" id="gender" class="form-control">
	 	<option <?php echo ($Gender == 0 ? 'selected' : ''); ?> value="0"> --- Please Select --- </option>
	 	<option <?php echo ($Gender == 1 ? 'selected' : ''); ?> value="1">Male</option>
	 	<option <?php echo ($Gender == 2 ? 'selected' : ''); ?> value="2">Female</option>
	 	</select>
</div>

	  <div class="col-md-12">
			<div class="buttons-cart pull-right">
	<input type="submit" class="btn continue" value="Register">
<input type="hidden" name="action" value="submit_form" />
</div>
	  </div>
	              </div>


	             </form>
	            </div>

						

</div>
					
				</article>
			</main><!-- #main -->
		</div><!-- #primary -->
	</div><!-- .container -->
</div><!-- #content -->



 <?php require_once 'include/footer.php'; ?>
 <?php require_once 'include/foot.php'; ?>