<?php 
require_once 'admin/Common.php'; 
require_once 'functions.php'; 
require_once 'include/head.php'; 
require_once 'include/headerpartial.php';
 $CatID=99999;
if(!isset($_SESSION['Country']))
{
    $_SESSION['Country'] = '';
}
?>

 <div id="content" class="site-content" tabindex="-1">
	<div class="container">

		<nav class="woocommerce-breadcrumb"><a href="home.html">Home</a><span class="delimiter"><i class="fa fa-angle-right"></i></span>Cart</nav>

		<div id="primary" class="content-area">
			<main id="main" class="site-main">
				<article class="page type-page status-publish hentry">
					<header class="entry-header"><h1 itemprop="name" class="entry-title">Cart</h1></header><!-- .entry-header -->

		
                    
    <table class="shop_table shop_table_responsive cart">
									<thead>
										<tr>
                                            <th class="product-remove">&nbsp;</th>
											<th class="product-thumbnail">&nbsp;</th>
											<th class="product-name">Product</th>
											<th class="product-options">Options</th>
											<th class="product-shipping">Shipping</th>
											<th class="product-quantity">Quantity</th>
											<th class="product-charge">Option Charges</th>
											<th class="product-unit-price">Unit Price</th>
											<th class="product-subtotal">Total</th>
											
										</tr>
									</thead>
									<tbody>
										<?php
					if(isset($_SESSION['cart_items']) && !empty($_SESSION['cart_items']))
					{
						foreach($_SESSION['cart_items'] as $cart_items)
						{
						$items = explode('-',$cart_items);
						if($items[3] == 2)
						{
						$query="SELECT ID,ProductName,Image,Price,Discount,URL,Shipping FROM products WHERE Status = 1 AND ID=".$items[0];
						$res = mysql_query($query) or die(mysql_error());
						$row=mysql_fetch_array($res);
						$Image=explode(',', $row["Image"]);
						$img1 = $Image[0];
					?>
										<tr class="cart_item">
											 <td class="product-remove"><a href="javascript:void;"onclick="location.href='remove_from_cart.php?id=<?php echo $row['ID']; ?>&name=<?php echo $row['ProductName']; ?>&url=<?php echo $_SERVER['REQUEST_URI']; ?>'">x</a></td>
                                            
					<td class="product-thumbnail">
                        
                        <a href="<?php echo BASE_URL.'/'.$row['URL']; ?>"><img width="180" height="180" src="<?php echo ($items[6] == 0 ? 'admin/'.DIR_PRODUCTS_IMAGES.$img1 : $items[5]) ?>" alt="<?php echo $row['ProductName']; ?>" /></a></td>
                                            
                                            
				<td class="product-name"><a href="<?php echo BASE_URL.'/'.$row['URL']; ?>"><?php echo dboutput($row['ProductName']); ?></a>
<?php echo ($items[6] == 0 ? '' : '<br><a target="_blank" href="'.$items[5].'">Customized Link</a>'); ?>
				</td>
											<td class="product-price"><span class="amount"><?php
									$items = explode('-',$cart_items);
										if(!empty($items[4]) && $items[4] != 0)
										{
										$optionnamearray = explode(',',$items[4]);
											foreach($optionnamearray as $opt)
											{
											$query="SELECT v.ValueName,o.OptionName FROM product_options po LEFT JOIN p_options_values v ON po.ValueID = v.ID LEFT JOIN p_options o ON po.OptionID = o.ID WHERE po.ID=".$opt;
											$res = mysql_query($query) or die(mysql_error());
											$number = mysql_num_rows($res);
												if($number != 0)
												{
													while($rowoptnam=mysql_fetch_array($res))
													{
														echo $rowoptnam['OptionName'].' : ('.$rowoptnam['ValueName'].')<br>';
													}
												}

											}
										}
										else
										{
											echo 'Not Selected any Option';
										}
								?></span></td>
								<td class="product-shipping"><?php echo ($row['Shipping'] == 0 ? 'Free Shipping' : 'Shipping Charges will apply according to your City'); ?></td>
			<td class="product-quantity">
                <div class="quantity buttons_added">
<form action="update_quantity.php" method="GET">
				<input type="number" size="4"  name="quantity" class="input-text qty text" value="<?php echo $items[2]; ?>" />

				<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI']; ?>" />
								<input type="hidden" name="name" value="<?php echo add_to_cart_desh($row['ProductName']); ?>" />
								<input type="hidden" name="id" value="<?php echo $items[0]; ?>" />
								<input type="hidden" name="upsell" value="<?php echo $items[3]; ?>" />
								<input type="hidden" name="options" value="<?php echo $items[4]; ?>" />
								<input type="hidden" name="image" value="<?php echo $items[5]; ?>" />
								<input type="hidden" name="checkcustom" value="<?php echo $items[6]; ?>" />
				<button type="submit" data-toggle="tooltip" title="" class="plus" data-original-title="Update"><i class="fa fa-refresh" style="font-size:12px;"></i></button>

</form>
                </div>
			</td>
											<td class="product-charge"><?php
											$optiontotal=0;
												$items = explode('-',$cart_items);
													if(!empty($items[4]))
													{
													$optiontotalarray = explode(',',$items[4]);
														foreach($optiontotalarray as $opt)
														{
														$query="SELECT Increment FROM product_options WHERE ID=".$opt;
														$res = mysql_query($query) or die(mysql_error());
														$number = mysql_num_rows($res);
															if($number != 0)
															{
																$rowopt=mysql_fetch_array($res);

																	$IncTotal = $rowopt["Increment"];

																$optiontotal = $optiontotal + ($IncTotal * $items[2]);
															}
														}
													}

												echo CURRENCY_SYMBOL.$optiontotal;

											?></td>
											<td class="product-unit-price">
												<?php

												$Discount = $row["Discount"];
												$Price = $row["Price"];


											echo ($Discount != 0 ? CURRENCY_SYMBOL.$Discount : CURRENCY_SYMBOL.$Price);
											?></td>
										<td class="product-subtotal">
											<?php
											$productTotal = 0;
											if($Discount != 0)
											{
											$productTotal = $optiontotal + ($Discount * $items[2]);
											}
											else
											{
											$productTotal = $optiontotal + ($Price * $items[2]);
											}


											echo CURRENCY_SYMBOL.round($productTotal);

											?>
										</td>
											
										</tr>
										<?php
						}
						else
						{
						$query="SELECT p.ID,p.ProductName,p.Image,p.Price,p.Discount,p.URL,p.Shipping,up.OfferPrice FROM upsaleproducts up LEFT JOIN products p ON up.ProductID = p.ID  WHERE p.Status = 1 AND p.ID=".$items[0];
						$res = mysql_query($query) or die(mysql_error());
						$row=mysql_fetch_array($res);
						$Image=explode(',', $row["Image"]);
						$img1 = $Image[0];?>
						<tr>
						 <td class="text-center">
							 <a href="<?php echo BASE_URL.'/'.$row['URL']; ?>"><img src="<?php echo ($items[6] == 0 ? BASE_URL.'/'.'admin/'.DIR_PRODUCTS_IMAGES.$img1 : $items[5]) ?>" alt="<?php echo $row['ProductName']; ?>" title="<?php echo $row['ProductName']; ?>" class="img-thumbnail" style="height:100px;"></a>
						 </td>
						 <td>
							 <a href="<?php echo BASE_URL.'/'.$row['URL']; ?>"><?php echo dboutput($row['ProductName']); ?></a>
							 <?php echo ($items[6] == 0 ? '' : '<br><a target="_blank" href="'.$items[5].'">Customized Link</a>'); ?>
						 </td>
						 <td>
						 <?php
							 $items = explode('-',$cart_items);
								 if(!empty($items[4]))
								 {
								 $optionnamearray = explode(',',$items[4]);
									 foreach($optionnamearray as $opt)
									 {
									 $query="SELECT v.ValueName,o.OptionName FROM product_options po LEFT JOIN p_options_values v ON po.ValueID = v.ID LEFT JOIN p_options o ON po.OptionID = o.ID WHERE po.ID=".$opt;
									 $res = mysql_query($query) or die(mysql_error());
									 $number = mysql_num_rows($res);
										 if($number != 0)
										 {
											 while($rowoptnam=mysql_fetch_array($res))
											 {
												 echo $rowoptnam['OptionName'].' : ('.$rowoptnam['ValueName'].')<br>';
											 }
										 }

									 }
								 }
								 else
								 {
									 echo 'Not Selected any Option';
								 }
						 ?>
						 </td>
						 <td><?php echo ($row['Shipping'] == 1 ? 'Free Shipping' : 'Shipping Charges will apply according to your Country'); ?></td>
						 <td class="text-left">
							 <form action="update_quantity.php" method="GET">
							 <div class="input-group btn-block" style="max-width: 200px;">
							 <input type="text" name="quantity" value="<?php echo $items[2]; ?>" size="1" class="form-control custom-padding">
							 <span class="input-group-btn">
							 <input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI']; ?>" />
							 <input type="hidden" name="name" value="<?php echo add_to_cart_desh($row['ProductName']); ?>" />
							 <input type="hidden" name="id" value="<?php echo $items[0]; ?>" />
							 <input type="hidden" name="upsell" value="<?php echo $items[3]; ?>" />
							 <input type="hidden" name="options" value="<?php echo $items[4]; ?>" />
							 <input type="hidden" name="image" value="<?php echo $items[5]; ?>" />
							 <input type="hidden" name="checkcustom" value="<?php echo $items[6]; ?>" />
							 <button type="submit" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Update"><i class="fa fa-refresh"></i></button>
							 </span></div>
							 </form>
						 </td>
						 <td class="text-right">
						 <?php
						 $optiontotal=0;
							 $items = explode('-',$cart_items);
								 if(!empty($items[4]))
								 {
								 $optiontotalarray = explode(',',$items[4]);
									 foreach($optiontotalarray as $opt)
									 {
									 $query="SELECT Increment FROM product_options WHERE ID=".$opt;
									 $res = mysql_query($query) or die(mysql_error());
									 $number = mysql_num_rows($res);
										 if($number != 0)
										 {
											 $row=mysql_fetch_array($res);

												 $IncTotal = $row["Increment"];

											 $optiontotal = $optiontotal + ($IncTotal * $items[2]);
										 }
									 }
								 }


							 echo CURRENCY_SYMBOL.round($optiontotal);

						 ?>
						 </td>
						 <td class="text-right">
						 <?php

							 $Discount = $row["Discount"];
							 $Price = $row["Price"];
							 $OfferPrice = $row["OfferPrice"];


						 if($OfferPrice == 0)
						 {
							echo ($Discount != 0 ? CURRENCY_SYMBOL.$Discount : CURRENCY_SYMBOL.$Price);
						 }
						 else if($OfferPrice != 0)
						 {
							echo CURRENCY_SYMBOL.$OfferPrice;
						 }
						 ?>
						 </td>
						 <td class="text-right">
						 <?php
						 $productTotal = 0;
						 if($OfferPrice != 0)
						 {
						 $productTotal = $optiontotal + ($OfferPrice * $items[2]);
						 }
						 else if($Discount != 0)
						 {
						 $productTotal = $optiontotal + ($Discount * $items[2]);
						 }
						 else
						 {
						 $productTotal = $optiontotal + ($Price * $items[2]);
						 }


						 echo CURRENCY_SYMBOL.$productTotal;

						 ?>
						 </td>
						
					 </tr>

					 <?php
					 }
					 }
					 }
					 else
					 {
					 ?>
					 <tr>
					 	<td colspan="9" class="text-center">
					 		Your Cart is Empty!
					 	</td>
					 </tr>
					 <?php
					 }
					 ?>
                                        
                                        
                 <tr>
                <td class="actions" colspan="9">

                    
<a href="<?php echo BASE_URL;?>/index.php" class="button">Continue Shopping</a>
                   

                    <div class="wc-proceed-to-checkout">

                        <a class="checkout-button button alt wc-forward" href="<?php echo BASE_URL;?>/checkout.php">Proceed to Checkout</a>
                    </div>

                    
                </td>
            </tr>                       
                                        
                                        
									</tbody>
								</table>
                    
                    
           <div class="cart-collaterals">         
            <div class="cart_totals ">
                 <h2>Cart Totals</h2>
                        
										<table class="shop_table shop_table_responsive">
											<tbody>
												<tr class="cart-subtotal">
                                                    <th>Subtotal</th>
													<td><span class="amount"><?php echo CURRENCY_SYMBOL; ?><?php echo round($subtotal); ?></span></td>
												</tr>
												<tr class="option-increment">
                                                    <th>Option Increment</th>
													<td>
														<strong><?php echo CURRENCY_SYMBOL; ?><?php echo round($total); ?></strong>
</td>
												</tr>
												<tr class="shipping">
                                                    <th>Shipping Charges</th>
													<td>
                                                        <span class="amount"><?php echo CURRENCY_SYMBOL; ?><?php echo round($shippingAmount); ?></span>
</td>
												</tr>
												<tr class="order-total">
                                                    <th>Total</th>
													<td>
														<strong><span class="amount"><?php echo CURRENCY_SYMBOL; ?><?php echo round($grandtotal); ?></span></strong>
													</td>
												</tr>
											</tbody>
										</table>

                    </div>
                    </div>
							
                    
                    
					
				</article>
			</main><!-- #main -->
		</div><!-- #primary -->
	</div><!-- .container -->
</div><!-- #content -->



<?php require_once 'include/footer.php'; ?>
 <?php require_once 'include/foot.php'; ?>