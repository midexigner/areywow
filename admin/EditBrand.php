<?php
include_once("Common.php");
include("CheckAdminLogin.php");
	
	$msg="";
	$ID=0;
	$Status=1;
	$BrandName="";
	$MetaDes="";
	$MetaKey="";
	$Description="";
	$URL="";
	$Image="";
	
	$ID=0;
	if(isset($_REQUEST["BrandID"]) && ctype_digit(trim($_REQUEST["BrandID"])))
		$ID=trim($_REQUEST["BrandID"]);
	
		
	
if(isset($_POST["action"]) && $_POST["action"] == "submit_form")
{	
	if(isset($_REQUEST["BrandID"]) && ctype_digit(trim($_REQUEST["BrandID"])))
		$ID=trim($_REQUEST["BrandID"]);
	if(isset($_POST["Status"]) && ((int)$_POST["Status"] == 0 || (int)$_POST["Status"] == 1))
		$Status=trim($_POST["Status"]);		
	if(isset($_POST["BrandName"]))
		$BrandName=trim($_POST["BrandName"]);
	if(isset($_POST["MetaDes"]))
		$MetaDes=trim($_POST["MetaDes"]);
	if(isset($_POST["MetaKey"]))
		$MetaKey=trim($_POST["MetaKey"]);
	if(isset($_POST["Description"]))
		$Description=trim($_POST["Description"]);
	if(isset($_POST["URL"]))
		$URL=trim($_POST["URL"]);
	if(isset($_FILES["flPage"]) && $_FILES["flPage"]['name'] != "")
	{
		$filenamearray=explode(".", $_FILES["flPage"]['name']);
		$ext=strtolower($filenamearray[sizeof($filenamearray)-1]);
	
		if(!in_array($ext, $_IMAGE_ALLOWED_TYPES))
		{
			$msg='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Only '.implode(", ", $_IMAGE_ALLOWED_TYPES) . ' files can be uploaded.
			</div>';
		}			
		else if($_FILES["flPage"]['size'] > (MAX_IMAGE_SIZE*1024))
		{
			$msg='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Image size must be ' . MAX_IMAGE_SIZE . ' KB or less.
			</div>';
		}
	}

	
		if($BrandName == "")
		{
			$msg='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Please enter Brand Name.</b>
			</div>';
		}
		else if($MetaDes == "")
		{
			$msg='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Please enter Meta Description.</b>
			</div>';
		}
		else if($MetaKey == "")
		{
			$msg='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Please enter Meta Keywords.</b>
			</div>';
		}
		else if($URL == "")
		{
			$msg='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Please enter URL.</b>
			</div>';
		}
		
		
	
	if($msg=="")
	{
		$r = mysql_query("SELECT BrandName FROM brands WHERE 
		BrandName='".dbinput($BrandName)."' AND ID <> '".(int)$ID."'") or die(mysql_error());
		if(mysql_num_rows($r) == 0)
		{
			$query="UPDATE brands SET  
				Status='".(int)$Status . "', DateModified=NOW(),
				BrandName = '" . dbinput($BrandName) . "',
				MetaDescription = '" . dbinput($MetaDes) . "',
				MetaKeywords = '" . dbinput($MetaKey) . "',
				Description = '" . dbinput($Description) . "',
				URL = '" . dbinput($URL) . "'
			WHERE ID='".(int)$ID."'";
			mysql_query($query) or die ('Could not update brand because: ' . mysql_error());
			
			$result=mysql_query ("SELECT BrandName FROM brands WHERE ID = ".$ID."") or die("Query error: ". mysql_error());
		$row = mysql_fetch_array($result);

		 $query="INSERT INTO log_report SET DateAdded=NOW(),
				Type = 2,
				Form = 'Brand',
				Name = '".$row["BrandName"]."',
				UserID = '" . (int)$_SESSION["UserID"] . "'";
			
	mysql_query($query) or die ('Could not add user because: ' . mysql_error());
			
			$msg='<div class="alert alert-success alert-dismissable">
				<i class="fa fa-ban"></i>
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<b>Brand has been Updated.</b>
			</div>';
			
			if(isset($_FILES["flPage"]) && $_FILES["flPage"]['name'] != "")
			{
				if(is_file(DIR_BRANDS_IMAGES . $Image))
					unlink(DIR_BRANDS_IMAGES . $Image);
			
				flush();
				ini_set('memory_limit', '-1');
				
				$tempName = $_FILES["flPage"]['tmp_name'];
				$realName = "B".$ID . "." . $ext;
				$Image = $realName; 
				$target = DIR_BRANDS_IMAGES . $realName;

				flush();
			
				$moved=move_uploaded_file($tempName, $target);
			
				if($moved)
				{			
				
					$query="UPDATE brands SET Image='" . dbinput($realName) . "' WHERE  ID=" . (int)$ID;
					mysql_query($query) or die(mysql_error());
				}
				else
				{
					$msg='<div class="alert alert-warning alert-dismissable">
						<i class="fa fa-ban"></i>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<b>Brand has been saved but Image can not be uploaded.</b>
						</div>';
				}
			}
		
		}
		else
		{
			$msg = '<div class="alert alert-danger alert-dismissable">
				<i class="fa fa-ban"></i>
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<b>Name already exists.</b>
			</div>';
		}
				
			
	}

}
else
{
	$query="SELECT ID, Status,BrandName,MetaDescription,Image,MetaKeywords,Description,URL,DATE_FORMAT(DateAdded, '%D %b %Y %r') AS Added, DATE_FORMAT(DateModified,
	 '%D %b %Y %r') AS Updated FROM brands WHERE  ID='" . (int)$ID . "'";
	
	$result = mysql_query ($query) or die("Could not select brand because: ".mysql_error()); 
	$num = mysql_num_rows($result);
	
	if($num==0)
	{
		$_SESSION["msg"]='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Invalid Brand ID.</b>
		</div>';
		redirect("Brands.php");
	}
	else
	{
		$row = mysql_fetch_array($result,MYSQL_ASSOC);
		
		$ID=$row["ID"];
		$BrandName=$row["BrandName"];
		$MetaDes=$row["MetaDescription"];
		$MetaKey=$row["MetaKeywords"];
		$Status=$row["Status"];
		$Description=$row["Description"];
		$URL=$row["URL"];
		$Image=$row["Image"];
	}
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Edit Brand</title>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<!-- bootstrap 3.0.2 -->
<script language="javascript" src="scripts/innovaeditor.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="js/local_clock.js" type="text/javascript"></script>
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Ionicons -->
<link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
<!-- Theme style -->
<link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
<style>
#footer {
	width:100%;
	height:50px;
	background-color:#3c8dbc;
	text-align:center;
	vertical-align:center;
	padding-top:15px;
}
#labelimp {
	background-color: rgba(60, 141, 188, 0.19);
	padding: 4px;
	font-size: 20px;
	width: 100%;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	padding-left: 5px;
}
</style>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
$(document).ready(function(){ 
		
		$("#Name").keyup(function () {
			
			$.ajax({			
					url: 'get_b_meta_des.php?name='+$("#Name").val(),
					success: function(data) {
						$("#MetaDes").html(data);
					},
					error: function (xhr, textStatus, errorThrown) {
						alert(xhr.responseText);
						$("#MetaDes").removeAttr("disabled");
					}
			});

		});		
		
		
	});   

</script>
<script>
$(document).ready(function(){ 
		
		$("#Name").keyup(function () {
			
			$.ajax({			
					url: 'get_b_meta_key.php?name='+$("#Name").val(),
					success: function(data) {
						$("#MetaKey").html(data);
					},
					error: function (xhr, textStatus, errorThrown) {
						alert(xhr.responseText);
						$("#MetaKey").removeAttr("disabled");
					}
			});

		});		
		
		
	});   

</script>
<script>
$(document).ready(function(){ 
		
		$("#Name").keyup(function () {
			
			$.ajax({			
					url: 'get_url.php?name='+$("#Name").val(),
					success: function(data) {
						$("#URL").html(data);
					},
					error: function (xhr, textStatus, errorThrown) {
						alert(xhr.responseText);
						$("#URL").removeAttr("disabled");
					}
			});

		});		
		
		
	});   

</script>
</head>
<body class="skin-blue">
<!-- header logo: style can be found in header.less -->
<?php
	include_once("Header.php");
?>
<div class="wrapper row-offcanvas row-offcanvas-left">
  <!-- Left side column. contains the logo and sidebar -->
  <?php
	include_once("Sidebar.php");
?>
  <!-- Right side column. Contains the navbar and content of the page -->
  <aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Edit Brand</h1>
      <ol class="breadcrumb">
        <li><a href="Brands.php"><i class="fa fa-dashboard"></i>Brands</a></li>
        <li class="active">Edit Brand</li>
      </ol>
    </section>
    <!-- Main content -->
    <form role="form" action="<?php echo $_SERVER["PHP_SELF"];?>?BrandID=<?php echo $ID; ?>"" method="post" enctype="multipart/form-data" name="frmPage">
      <input type="hidden" name="action" value="submit_form" />
      <input type="hidden" name="BrandID" value="<?php echo $ID; ?>" />
      <section class="content">
        <div class="box-footer" style="text-align:right;">
          <button type="submit" class="btn btn-success margin">Save</button>
          <button class="btn btn-primary margin" type="button" onClick="location.href='Brands.php'">Cancel</button>
        </div>
        <?php
			  	echo $msg;
				if(isset($_SESSION["msg"]))
				{
					echo $_SESSION["msg"];
					$_SESSION["msg"]="";
				}
				?>
        <div class="col-md-6">
          <div class="box">
            <!-- general form elements -->
            <div style="padding:15px;" class="box-primary">
              <!-- form start -->
              <div class="box-body">
                <div class="form-group">
                  <label id="labelimp" class="labelimp" for="BrandName">Brand Name: </label>
                  <?php 
				echo '<input type="text" id="Name" name="BrandName" class="form-control"  value="'.$BrandName.'" />';
				?>
                </div>
				
				<div class="form-group">
                  <label id="labelimp" for="MetaDes" >Meta Description: </label>
                  <?php 
				echo '<textarea id="MetaDes" name="MetaDes" class="form-control">'.$MetaDes.'</textarea>';
				?>
                </div>
				
				<div class="form-group">
                  <label id="labelimp" for="MetaKey" >Meta Keywords: </label>
                  <?php 
				echo '<textarea id="MetaKey" name="MetaKey" class="form-control">'.$MetaKey.'</textarea>';
				?>
                </div>
				
				<div class="form-group">
                  <label id="labelimp" for="URL">URL: </label>
                  <?php 
				echo '<textarea id="URL" name="URL" class="form-control">'.$URL.'</textarea>';
				?>
                </div>
				
				<div class="form-group">
				  <label id="labelimp">Description: </label>
					<?php
						$v = '<textarea name="Description" id="Description">';
						$v .= $Description;
						$v .= '</textarea><script>
							var NSD = new InnovaEditor("NSD");
							NSD.width="100%";
							NSD.height=300;
							NSD.btnPrint=true;
							NSD.btnPasteText=true;
							NSD.btnFlash=true;
							NSD.btnForm=false;
							NSD.btnMedia=true;
							NSD.btnLTR=true;
							NSD.btnRTL=true;
							NSD.btnStrikethrough=true;
							NSD.btnSuperscript=true;
							NSD.btnSubscript=true;
							NSD.btnClearAll=true;
							NSD.btnSave=true;
							NSD.btnStyles=true;
							if(navigator.appName == \'Microsoft Internet Explorer\')
								NSD.cmdAssetManager = "modalDialogShow(\'../assetmanager/assetmanager.php\',640,465)";
							else						
								NSD.cmdAssetManager = "modalDialogShow(\'../../assetmanager/assetmanager.php\',640,465)";						
							NSD.css="../css/style.css";
							NSD.mode="XHTMLBody";
							NSD.REPLACE("Description");
							</script>';
							
							echo $v;
							?>
				</div> 
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
            <!-- Form Element sizes -->
          </div>
        </div>
        <div class="col-md-6">
          <div class="box">
            <!-- general form elements -->
            <div style="padding:15px;" class="box-primary">
              <!-- form start -->
              <div class="box-body">
                <div class="form-group">
                  <label id="labelimp" for="exampleInputEmail1">Status: </label>
                  <label>
                  <input type="radio" name="Status" value="1"<?php echo ($Status == 1 ? ' checked="checked"' : ''); ?>>
                  Enable</label>
                  <label>
                  <input type="radio" name="Status" value="0"<?php echo ($Status == 0 ? ' checked="checked"' : ''); ?>>
                  Disable</label>
                </div>
				
			 
			<div class="form-group">
                  <label id="labelimp" for="Header">Image: </label>
                  <input type="file" id="Header" class="form-control" name="flPage" placeholder="Enter ...">
                  <input type="hidden" name="Image" value="<?php echo $Image; ?>" />
                  <p class="help-block">Image types allowed: jpg, jpeg, gif, png.</p>
                  <p style="padding:15px;">
                    <?php 
					if(isset($Image) && $Image !="")
					{
					echo '<img src="'.DIR_BRANDS_IMAGES.$Image.'" width="100" height="100">';
					}
					?>
                  </p>
                </div>
	  </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
            <!-- Form Element sizes -->
          </div>
        </div>
      </section>
    </form>
    <!-- /.content -->
  </aside>
  <!-- /.right-side -->
</div>
<!-- ./wrapper -->
<?php include_once("Footer.php"); ?>
<!-- add new calendar event modal -->
<!-- jQuery 2.0.2 -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<!-- jQuery UI 1.10.3 -->
<script src="js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="js/AdminLTE/app.js" type="text/javascript"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="js/AdminLTE/dashboard.js" type="text/javascript"></script>
</body>
</html>