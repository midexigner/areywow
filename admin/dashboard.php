<?php include("Common.php"); ?>
<?php include("CheckAdminLogin.php");
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Dashboard</title>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<!-- bootstrap 3.0.2 -->
<!-- jQuery 2.0.2 -->
<link rel="stylesheet" href="css/style-2.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="js/ui.datepicker.js" type="text/javascript"></script>
<script language="javascript" src="js/local_clock.js" type="text/javascript"></script>
<!-- <script language="javascript" src="../js/functions.js" type="text/javascript"></script>
 --><link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- font Awesome -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- font Awesome -->
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Ionicons -->
<script language="javascript" src="scripts/innovaeditor.js"></script>
<link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
<!-- Morris chart -->
<link href="css/morris/morris.css" rel="stylesheet" type="text/css" />
<!-- jvectormap -->
<link href="css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
<!-- fullCalendar -->
<link href="css/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
<!-- Daterange picker -->
<link href="css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
<!-- bootstrap wysihtml5 - text editor -->
<link href="css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />

<!-- Theme style -->
<link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
<style>
#labelimp {
	background-color: rgba(60, 141, 188, 0.19);
	padding: 4px;
	font-size: 20px;
	width: 100%;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	padding-left: 5px;
}
</style>


</head>
<body class="skin-blue">
<!-- header logo: style can be found in header.less -->
<?php
	include_once("Header.php");
?>
<div class="wrapper row-offcanvas row-offcanvas-left">
  <!-- Left side column. contains the logo and sidebar -->
  <?php
			include_once("Sidebar.php");
?>
  <!-- Right side column. Contains the navbar and content of the page -->
  <aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> Dashboard <small></small> </h1>
      <ol class="breadcrumb">
        <li><a href="Dashboard.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
                    <!-- <div class="row">
                        <div class="col-lg-4 col-xs-12">
                           
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h3>
                                        <?php //echo total_products(); ?>
                                    </h3>
                                    <p>
                                        Products 
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-bag"></i>
                                </div>
                                <a href="#" class="small-box-footer">
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-xs-12">
                            
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3>
                                         <?php //echo total_categories(); ?>
                                    </h3>
                                    <p>
                                        Categories
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-pie-graph"></i>
                                </div>
                                <a href="#" class="small-box-footer">
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-xs-12">
                            
                            <div class="small-box bg-red">
                                <div class="inner">
                                    <h3>
                                        <?php //echo total_brands(); ?>
                                    </h3>
                                    <p>
                                        Manufactures
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-ios7-pricetag-outline"></i>
                                </div>
                                <a href="#" class="small-box-footer">
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
                    </div> -->

                    <div class="row-one">
                    <div class="col-md-4 widget">
                        <div class="stats-left ">
                            <!-- <h5>Today</h5> -->
                            <h4>Products</h4>
                        </div>
                        <div class="stats-right">
                            <label> <?php echo total_products(); ?></label>
                        </div>
                        <div class="clearfix"> </div>   
                    </div>
                    <div class="col-md-4 widget states-mdl">
                        <div class="stats-left">
                            <!-- <h5>Today</h5> -->
                            <h4>Categories</h4>
                        </div>
                        <div class="stats-right">
                            <label> <?php echo total_categories(); ?></label>
                        </div>
                        <div class="clearfix"> </div>   
                    </div>
                    <div class="col-md-4 widget states-last">
                        <div class="stats-left">
                            <h4>Manufactures</h4>
                        </div>
                        <div class="stats-right">
                            <label><?php echo total_brands(); ?></label>
                        </div>
                        <div class="clearfix"> </div>   
                    </div>
                    <div class="clearfix"> </div>   
                </div>

                <div class="row-one">
                    <div class="col-md-4 widget">
                        <div class="stats-left tv">
                            <!-- <h5>Today</h5> -->
                            <h4>Total Visits</h4>
                        </div>
                        <div class="stats-right tvn">
                            <label> <?php 
                            $sql = "SELECT COUNT(ID) AS 'Num' From visits";
                            $q = mysql_query($sql);
                            $r = mysql_fetch_assoc($q);
                            echo $r['Num'];
                            $totalv = $r['Num'];

                             ?></label>
                        </div>
                        <div class="clearfix"> </div>   
                    </div>
                    <div class="col-md-4 widget states-mdl">
                        <div class="stats-left rv">
                            <h4>Total Re-visitors</h4>
                        </div>
                        <div class="stats-right rvn">
                            <label> <?php 
                            $sql = "SELECT COUNT(DISTINCT ip,page_name) AS 'Num' FROM visits WHERE revisit = true";
                            $q = mysql_query($sql);
                            $r = mysql_fetch_assoc($q);
                            echo $r['Num'];
                            $revisitt = $r['Num'];

                             ?></label>
                        </div>
                        <div class="clearfix"> </div>   
                    </div>
                    <div class="col-md-4 widget states-last">
                        <div class="stats-left to">
                            <h4>Total Orders</h4>
                        </div>
                        <div class="stats-right ton">
                            <label><?php
                            $sql = "SELECT COUNT(ID) AS 'Num' FROM orders";
                            $q = mysql_query($sql);
                            $r = mysql_fetch_assoc($q);
                            echo $r['Num'];
                            $or = $r['Num'];
                             ?></label>
                        </div>
                        <div class="clearfix"> </div>   
                    </div>
                    <div class="clearfix"> </div>   
                </div>

                        
                    <div class="row">
                        <div class="col-md-4 ">
                            <h4 class="title">Visitors</h4>
                            <canvas id="bar" height="300" width="400"> </canvas>
                        </div>
                        <div class="col-md-4 ">
                            <h4 class="title">Total Orders and Re-Orderers</h4>
                            <canvas id="line" height="300" width="400"> </canvas>
                        </div>
                        <div class="col-md-4 ">
                            <h4 class="title">Most Sold Products</h4>
                            <canvas id="pie" height="300" width="400"> </canvas>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 ">
                            <h4 class="title">Top Searches</h4>
                            <canvas id="doughnut" height="300" width="400"> </canvas>
                        </div>
                        <div class="col-md-4 ">
                            <h4 class="title">Number of orders by cities</h4>
                            <canvas id="polar" height="300" width="400"> </canvas>
                        </div>
                        <div class="col-md-4 ">
                           <!--  <h4 class="title">radar Chart Example</h4>
                            <canvas id="radar" height="300" width="400"> </canvas> -->
                        </div>
                        
                    </div>

              
   
                   <!-- <div class="row">
                     
                        <section class="col-lg-12 connectedSortable">                            
                            
							
                            <div class="nav-tabs-custom">

                                <ul class="nav nav-tabs pull-right">
                                    <li class="pull-left header"><i class="fa fa-inbox"></i> Sales</li>
                                </ul>
                                <div class="tab-content no-padding">
                                    
                                    <div class="chart tab-pane active" id="revenue-chart" style="position: relative; height: 300px;"></div>
                                </div>
                            </div>
							
							
                            <div class="box box-info">
                                <div class="box-header">
                                    <i class="fa fa-envelope"></i>
                                    <h3 class="box-title">Quick Email</h3>

                                    <div class="pull-right box-tools">
										<button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button> 
                                    </div>
                                </div>
                                <div class="box-body">
                                    <form action="#" method="post">
                                        <div class="form-group">
                                            <input type="email" class="form-control" name="emailto" placeholder="Email to:"/>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="subject" placeholder="Subject"/>
                                        </div>
                                        <div>
                                            <textarea class="textarea" placeholder="Message" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                        </div>
                                    </form>
                                </div>
                                <div class="box-footer clearfix">
                                    <button class="pull-right btn btn-default" id="sendEmail">Send <i class="fa fa-arrow-circle-right"></i></button>
                                </div>
                            </div>
							
							
                       
                            <div class="box box-primary">
                                <div class="box-header">
                                    <i class="ion ion-clipboard"></i>
                                    <h3 class="box-title">To Do List</h3>
                                </div>
                                <div class="box-body">
                                    <ul class="todo-list">
                                        <li>
                                       
                                            <span class="handle">
                                                <i class="fa fa-ellipsis-v"></i>
                                                <i class="fa fa-ellipsis-v"></i>
                                            </span>
                                            
                                            <input type="checkbox" value="" name=""/>

                                            <span class="text">Design a nice theme</span>
                                           
                                            <small class="label label-danger"><i class="fa fa-clock-o"></i> 2 mins</small>

                                            <div class="tools">
                                                <i class="fa fa-edit"></i>
                                                <i class="fa fa-trash-o"></i>
                                            </div>
                                        </li>
                                        <li>
                                            <span class="handle">
                                                <i class="fa fa-ellipsis-v"></i>
                                                <i class="fa fa-ellipsis-v"></i>
                                            </span>
                                            <input type="checkbox" value="" name=""/>
                                            <span class="text">Make the theme responsive</span>
                                            <small class="label label-info"><i class="fa fa-clock-o"></i> 4 hours</small>
                                            <div class="tools">
                                                <i class="fa fa-edit"></i>
                                                <i class="fa fa-trash-o"></i>
                                            </div>
                                        </li>
                                        <li>
                                            <span class="handle">
                                                <i class="fa fa-ellipsis-v"></i>
                                                <i class="fa fa-ellipsis-v"></i>
                                            </span>
                                            <input type="checkbox" value="" name=""/>
                                            <span class="text">Let theme shine like a star</span>
                                            <small class="label label-warning"><i class="fa fa-clock-o"></i> 1 day</small>
                                            <div class="tools">
                                                <i class="fa fa-edit"></i>
                                                <i class="fa fa-trash-o"></i>
                                            </div>
                                        </li>
                                        <li>
                                            <span class="handle">
                                                <i class="fa fa-ellipsis-v"></i>
                                                <i class="fa fa-ellipsis-v"></i>
                                            </span>
                                            <input type="checkbox" value="" name=""/>
                                            <span class="text">Let theme shine like a star</span>
                                            <small class="label label-success"><i class="fa fa-clock-o"></i> 3 days</small>
                                            <div class="tools">
                                                <i class="fa fa-edit"></i>
                                                <i class="fa fa-trash-o"></i>
                                            </div>
                                        </li>
                                        <li>
                                            <span class="handle">
                                                <i class="fa fa-ellipsis-v"></i>
                                                <i class="fa fa-ellipsis-v"></i>
                                            </span>
                                            <input type="checkbox" value="" name=""/>
                                            <span class="text">Check your messages and notifications</span>
                                            <small class="label label-primary"><i class="fa fa-clock-o"></i> 1 week</small>
                                            <div class="tools">
                                                <i class="fa fa-edit"></i>
                                                <i class="fa fa-trash-o"></i>
                                            </div>
                                        </li>
                                        <li>
                                            <span class="handle">
                                                <i class="fa fa-ellipsis-v"></i>
                                                <i class="fa fa-ellipsis-v"></i>
                                            </span>
                                            <input type="checkbox" value="" name=""/>
                                            <span class="text">Let theme shine like a star</span>
                                            <small class="label label-default"><i class="fa fa-clock-o"></i> 1 month</small>
                                            <div class="tools">
                                                <i class="fa fa-edit"></i>
                                                <i class="fa fa-trash-o"></i>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="box-footer clearfix no-border">
                                    <button class="btn btn-default pull-right"><i class="fa fa-plus"></i> Add item</button>
                                </div>
                            </div>

                           
							
							                          

                        </section>
                    </div>-->
    </section>
    <!-- /.content -->
  </aside>
  <!-- /.right-side -->
</div>
<?php include_once("Footer.php"); ?>
        <!-- jQuery 2.0.2 -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- jQuery UI 1.10.3 -->
        <script src="js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <!-- Morris.js charts -->
        <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <!-- <script src="js/plugins/morris/morris.min.js" type="text/javascript"></script> -->
        <!-- Sparkline -->
        <script src="js/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
        <!-- jvectormap -->
        <script src="js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
        <script src="js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
        <!-- fullCalendar -->
        <script src="js/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
        <!-- jQuery Knob Chart -->
        <script src="js/plugins/jqueryKnob/jquery.knob.js" type="text/javascript"></script>
        <!-- daterangepicker -->
        <script src="js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
        <!-- iCheck -->
        <script src="js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>

        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app2.js" type="text/javascript"></script>
        
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <!-- <script src="js/AdminLTE/dashboard.js" type="text/javascript"></script> -->

        <script src="js/Chart.js"></script>      
        
        <script>
                var randomScalingFactor = function(){ return Math.round(Math.random()*100)};
    var barChartData = {
        labels : [
        
        // "January","February","March","April","May","June","July"
        "Total Visits / Re Visitors"

        ],
        datasets : [
            {
                fillColor : "rgba(233, 78, 2, 0.83)",
                strokeColor : "#ef553a",
                highlightFill: "#ef553a",
                highlightStroke: "rgba(220,220,220,1)",
                data : [

<?php echo $totalv; ?>

                ]
            },
            {
                fillColor : "rgba(79, 82, 186, 0.83)",
                strokeColor : "#ed1c24",
                highlightFill: "#ed1c24",
                highlightStroke : "rgba(151,187,205,1)",
                data : [
                <?php echo $revisitt; ?>
                ]
            }
        ]
    }

    var doughnutData = [
    <?php 
    $k=0;
$sql = "SELECT keywords, numberOfTimes FROM searches ORDER BY numberOfTimes DESC LIMIT 5";
$query = mysql_query($sql);
while($r = mysql_fetch_array($query)){ ?>

                {
                    value: <?php echo $r['numberOfTimes']; ?>,
                    <?php switch ($k) {
                        case 0: ?> 
                            color:"#F7464A",
                            highlight: "#FF5A5E",  <?php
                            break;
                        
                        case 1: ?>
                            color: "#FDB45C",
                    highlight: "#FFC870",  <?php
                            break;
                        case 2: ?>
                        color: "#46BFBD",
                        highlight: "#5AD3D1",  <?php
                        break;
                        case 3:  ?>
                            color: "#949FB1",
                            highlight: "#A8B3C5",  <?php
                            break;
                        case 4: ?>
                            color: "#4D5360",
                            highlight: "#616774",  <?php
                            break;
                     } ?>
                    
                    label: "<?php echo $r['keywords']; ?>"
                },

                <?php $k++; } ?>

               
            ];
            
        
        var lineChartData = {
            labels : ["Total Orders / Reorderers", "Total Orders / Reorderers"],
            datasets : [
                {
                    label: "Total Orders / Reorderers",
                    fillColor : "rgba(233, 78, 2, 0.2)",
                    strokeColor : "rgba(233, 78, 2, 0.83)",
                    pointColor : "rgba(233, 78, 2, 0.83)",
                    pointStrokeColor : "#fff",
                    pointHighlightFill : "#fff",
                    pointHighlightStroke : "rgba(220,220,220,1)",
                    data : [0,
                    <?php echo $or;
         ?>
                    ]
                },
                <?php 
        $sql = "SELECT COUNT(DISTINCT CustomerID) AS 're' FROM orders GROUP BY CustomerID";
$query = mysql_query($sql);
$r = mysql_fetch_array($query);
         
                 ?>
                {
                    label: "Total Orders / Reorderers",
                    fillColor : "rgba(151,187,205,0.2)",
                    strokeColor : "rgba(151,187,205,1)",
                    pointColor : "#4D5360",
                    pointStrokeColor : "#fff",
                    pointHighlightFill : "#fff",
                    pointHighlightStroke : "rgba(151,187,205,1)",
                    data : [ 0, <?php echo $r['re']; ?>]
                }
            ]
        }
    <?php 
    $sql = "SELECT o.OrderID, o.ProductID, p.ProductName, COUNT(o.Quantity) AS 'Number of sales'
FROM orders_details AS o
INNER JOIN products AS p
WHERE o.ProductID = p.ID
GROUP BY p.ProductName
ORDER BY COUNT(o.Quantity) DESC
LIMIT 5";
$query = mysql_query($sql);
    $i =0;

    ?>
        var pieData = [
        <?php 
while ($r = mysql_fetch_assoc($query)) {
         ?>
                {
                    value: <?php echo $r['Number of sales']; ?>,
                    <?php switch ($i) {
                        case 0: ?> 
                            color:"#F7464A",
                            highlight: "#FF5A5E",  <?php
                            break;
                        
                        case 1: ?>
                            color: "#FDB45C",
                    highlight: "#FFC870",  <?php
                            break;
                        case 2: ?>
                        color: "#46BFBD",
                        highlight: "#5AD3D1",  <?php
                        break;
                        case 3:  ?>
                            color: "#949FB1",
                            highlight: "#A8B3C5",  <?php
                            break;
                        case 4: ?>
                            color: "#4D5360",
                            highlight: "#616774",  <?php
                            break;
                     } ?>
                    
                    label: "<?php echo $r['ProductName']; ?>"
                },
                <?php  $i++; } ?>


            ];
            
            
            
            var radarChartData = {
        labels: ["Eating", "Drinking", "Sleeping", "Designing", "Coding", "Cycling", "Running"],
        datasets: [
            {
                label: "My First dataset",
                fillColor: "rgba(220,220,220,0.2)",
                strokeColor: "rgba(220,220,220,1)",
                pointColor: "rgba(220,220,220,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: [65,59,90,81,56,55,40]
            },
            {
                label: "My Second dataset",
                fillColor: "rgba(151,187,205,0.2)",
                strokeColor: "rgba(151,187,205,1)",
                pointColor: "rgba(151,187,205,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(151,187,205,1)",
                data: [28,48,40,19,96,27,100]
            }
        ]
    };
    
        
    
    var polarData = [
<?php 
    $k=0;
$sql = "SELECT City,COUNT(w.ID) AS 'Number of Orders' 
FROM orders as o 
INNER JOIN website_users as w 
ON o.ID=w.ID 
GROUP BY City
ORDER BY COUNT(w.ID) DESC
LIMIT 5";
$query = mysql_query($sql);
while($r = mysql_fetch_array($query)){ ?>
{
                value: <?php echo $r['Number of Orders']; ?>,
                    <?php switch ($k) {
                        case 0: ?> 
                            color:"#F7464A",
                            highlight: "#FF5A5E",  <?php
                            break;
                        
                        case 1: ?>
                            color: "#FDB45C",
                    highlight: "#FFC870",  <?php
                            break;
                        case 2: ?>
                        color: "#46BFBD",
                        highlight: "#5AD3D1",  <?php
                        break;
                        case 3:  ?>
                            color: "#949FB1",
                            highlight: "#A8B3C5",  <?php
                            break;
                        case 4: ?>
                            color: "#4D5360",
                            highlight: "#616774",  <?php
                            break;
                     } ?>
                    
                    label: "<?php echo $r['City']; ?>"
                },

                <?php $k++; } ?>
                
            ];
            
            
        

            

    window.onload = function(){
        
        var ctx = document.getElementById("bar").getContext("2d");
        window.myBar = new Chart(ctx).Bar(barChartData, {
            responsive : true
        });

        var ctx = document.getElementById("doughnut").getContext("2d");
        window.myDoughnut = new Chart(ctx).Doughnut(doughnutData, {responsive : true});
        
        var ctx = document.getElementById("line").getContext("2d");
        window.myLine = new Chart(ctx).Line(lineChartData, {
            responsive: true
        });

        var ctx = document.getElementById("pie").getContext("2d");
        window.myPie = new Chart(ctx).Pie(pieData,{
            responsive: true
        });
    
        /*window.myRadar = new Chart(document.getElementById("radar").getContext("2d")).Radar(radarChartData, {
            responsive: true
        });*/

        var ctx = document.getElementById("polar").getContext("2d");
            window.myPolarArea = new Chart(ctx).PolarArea(polarData, {
                responsive:true
        });

    }

        </script>


    </body>
</html>                         
                                
                                
