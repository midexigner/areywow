<?php include("Common.php"); ?>
<?php include("CheckAdminLogin.php");?>
<?php 
$msg = "";
  if (isset($_REQUEST['dID'])) {
    $s = "DELETE FROM locator WHERE id = '".$_REQUEST['dID']."'";
    mysql_query($s) or die(mysql_error());
    $msg = '<div class="alert alert-danger alert-dismissable">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Location Deleted.</b>
      </div>';
  }
  if (isset($_REQUEST['addloc'])) {
    $chksql = "SELECT * FROM locator WHERE address = '".$_REQUEST['address']."'";
    $chkQue = mysql_query($chksql);
    $n = mysql_num_rows($chkQue);
    if ($n >= 1) {
      $msg = '<div class="alert alert-danger alert-dismissable">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Address already exists.</b>
      </div>';
    } else {
      $s = "INSERT INTO locator SET
      city = '".$_REQUEST['city']."',
      name = '".$_REQUEST['name']."',
      address = '".$_REQUEST['address']."',
      iframe = '".$_REQUEST['iframe']."'";
      mysql_query($s) or die(mysql_error());
      $msg = '<div class="alert alert-success alert-dismissable">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Location Added.</b>
      </div>';
    }
    }
?>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Store Locator</title>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<!-- bootstrap 3.0.2 -->
<!-- jQuery 2.0.2 -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="js/ui.datepicker.js" type="text/javascript"></script>
<script language="javascript" src="js/local_clock.js" type="text/javascript"></script>
<script language="javascript" src="../js/functions.js" type="text/javascript"></script>
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- font Awesome -->
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Ionicons -->
<link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
<link href="css/datepicker.css" rel="stylesheet" type="text/css" />
<!-- DATA TABLES -->
<link href="css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- Theme style -->
<link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
<script language="javascript">
	$(document).ready(function () {				
		$(".checkUncheckAll").click(function () {
			$(".chkIds").prop("checked", $(this).prop("checked"));			
		});
	});
	var counter = 0;
	
	
	function doDelete()
	{
		if($(".chkIds").is(":checked"))
		{
			if(confirm("Are you sure to delete."))
			{
				$("#action").val("delete");
				$("#frmPages").submit();
			}
		}
		else
			alert("Please select category to delete");
	}
	function doUpdate()
	{
		$("#action").val('update');
		$("#frmPages").submit();
	}
	
</script>
</head>
<body class="skin-blue">
<!-- header logo: style can be found in header.less -->
<?php
	include_once("Header.php");
?>
<div class="wrapper row-offcanvas row-offcanvas-left">
  <!-- Left side column. contains the logo and sidebar -->
  <?php
		include_once("Sidebar.php");
		?>
  <!-- Right side column. Contains the navbar and content of the page -->
  <aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> Store locators <small></small> </h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
       <?php
          echo $msg;
        if(isset($_SESSION["msg"]))
        {
          echo $_SESSION["msg"];
          $_SESSION["msg"]="";
        }
        ?>
        <div class="col-md-10">
          <div class="box">
            <!-- general form elements -->
            <div style="padding:15px;" class="box-primary">
              <!-- form start -->
              <div class="box-body" style="min-width: 80% !important;">
                <div class="table-responsive">
            <table class="table table-bordered">
            <thead>
                <tr>
                  <th>City</th>
                  <th>Name</th>
                  <th>Address</th>
                  <th>IFrame</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              <?php $query="SELECT id,city,name,address, iframe FROM locator";
  $result = mysql_query($query) or die("Could not select image because: ".mysql_error()); 
  while ($row = mysql_fetch_assoc($result)) { ?>
                <tr>
                  <td><?php echo $row['city']; ?></td>
                  <td><?php echo $row['name']; ?></td>
                  <td><?php echo $row['address']; ?></td>
                  <td><iframe src="<?php echo $row['iframe']; ?>" width="100%" height="250" frameborder="0" style="border: 2px solid black" allowfullscreen></iframe></td>
                  <td><a class="btn btn-danger" href="<?php echo $_SERVER['PHP_SELF']; ?>?dID=<?php echo $row['id']; ?>">Delete</a></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
                </div>
              
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
            <!-- Form Element sizes -->
          </div>
      </div>
      <div class="row">
      <div style="margin-left: 20px;" class="col-md-10">
        <h3>Add new location</h3>
      
      <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
      <div class="box-body">
      <div class="form-group">
        <label for="city">Enter City</label>
        <input type="text" required="required" class="form-control" name="city" id="city">
      </div>
      <div class="form-group">
        <label for="name">Enter Name</label>
        <input type="text" required="required" class="form-control" name="name" id="name">
      </div>
      <div class="form-group">
        <label for="address">Enter Address</label>
        <textarea required="required" name="address" id="address" class="form-control"></textarea> 
      </div>
      <div class="form-group">
        <label for="iframe">Enter Google Map IFrame's src</label>
        <textarea required="required" name="iframe" id="iframe" class="form-control"></textarea>
      </div>
      
      <input class="btn-success btn-lg" type="submit" name="addloc" value="Add">
      </div>
      </form>
      </div>
      </div>
    </section>

<?php include_once("Footer.php"); ?>
<!-- ./wrapper -->
<!-- Bootstrap -->
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<!-- DATA TABES SCRIPT -->
<!--<script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>-->
<script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="js/AdminLTE/app.js" type="text/javascript"></script>
<!-- page script -->

</body>
</html>
