<?php
include_once("Common.php");
include("CheckAdminLogin.php");
	
	$msg="";
	$Status=1;
	$ProductID=0;
	$OfferPrice=0;
	$Categories="";
	$Cat=array();
	$Products="";
	$Prod=array();
	$OfferDate = "0000-00-00";
	
	$ID=0;
	if(isset($_REQUEST["ID"]) && ctype_digit(trim($_REQUEST["ID"])))
		$ID=trim($_REQUEST["ID"]);
		
	
if(isset($_POST["action"]) && $_POST["action"] == "submit_form")
{	
	if(isset($_REQUEST["ID"]) && ctype_digit(trim($_REQUEST["ID"])))
		$ID=trim($_REQUEST["ID"]);
	if(isset($_POST["Status"]) && ((int)$_POST["Status"] == 0 || (int)$_POST["Status"] == 1))
		$Status=trim($_POST["Status"]);	
	if(isset($_POST["ProductID"]) && ctype_digit($_POST['ProductID']))
		$ProductID=trim($_POST["ProductID"]);
	if(isset($_POST["Categories"]))
	{
		$Categories=implode(',', $_POST['Categories']);
		$Cat=$_POST['Categories'];
	}
	if(isset($_POST["Products"]))
	{
		$Products=implode(',', $_POST['Products']);
		$Prod=$_POST['Products'];
	}
	if(isset($_POST["OfferPrice"]))
		$OfferPrice=trim($_POST["OfferPrice"]);
	if(isset($_POST["OfferDate"]))
		$OfferDate=trim($_POST["OfferDate"]);
		

		if($ProductID == 0)
		{
			$msg='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Please Select Product.</b>
			</div>';
		}
		
	
	if($msg=="")
	{	
		$r = mysql_query("SELECT ProductID FROM upsaleproducts WHERE 
		ProductID='".dbinput($ProductID)."' AND ID <> '".(int)$ID."'") or die(mysql_error());
		if(mysql_num_rows($r) == 0)
		{
			$query="UPDATE upsaleproducts SET  
				DateModified=NOW(),
				ProductID='".(int)$ProductID . "',
				Status='".(int)$Status."',
				Categories = '" . dbinput($Categories) . "',
				Products = '" . dbinput($Products) . "',
				PerformedBy = '" . $_SESSION["UserID"] . "',
				OfferDate = '" . $OfferDate . "',
				OfferPrice = '" . (int)$OfferPrice . "'
			WHERE ID='".(int)$ID."'";
			mysql_query($query) or die (mysql_error());
			//echo $query;
			
			$_SESSION["msg"]='<div class="alert alert-success alert-dismissable">
				<i class="fa fa-ban"></i>
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<b>Up Sale Product has been Updated.</b>
			</div>';
			
			
		
		}
		else
		{
			$_SESSION["msg"] = '<div class="alert alert-danger alert-dismissable">
				<i class="fa fa-ban"></i>
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<b>Name already exists.</b>
			</div>';
		}
			
		
	redirect($_SERVER["PHP_SELF"].'?ID='.$ID);		
	}
	
}
else
{
	$query="SELECT ID, Status,ProductID,Categories,Products,OfferPrice,OfferDate FROM upsaleproducts WHERE  ID='" . (int)$ID . "'";
	$result = mysql_query ($query) or die("Could not select UpSaleProduct because: ".mysql_error()); 
	$num = mysql_num_rows($result);
	
	if($num==0)
	{
		$_SESSION["msg"]='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Invalid Up Sale Product ID.</b>
		</div>';
		redirect("UpSaleProducts.php");
	}
	else
	{
		$row = mysql_fetch_array($result,MYSQL_ASSOC);
		
		$ID=$row["ID"];
		$Status=$row["Status"];
		$ProductID=$row["ProductID"];
		$OfferPrice=$row["OfferPrice"];
		$OfferDate=$row["OfferDate"];
		if($row["Categories"] == '')
		{
		$Categories=0;
		}
		else
		{
		$Categories=$row["Categories"];
		$Cat = explode(',',$Categories);
		}
		if($row["Products"] == '')
		{
		$Products=0;
		}
		else
		{
		$Products=$row["Products"];
		$Prod = explode(',',$Products);
		}
	}
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Edit UpSaleProduct</title>
<style>
    .multiselect {
        width: auto;
    }
    .selectBox {
        position: relative;
    }
    .selectBox select {
        width: 100%;
    }
    .overSelect {
        position: absolute;
        left: 0; right: 0; top: 0; bottom: 0;
    }
    #checkboxes {
        display: none;
        border: 1px #dadada solid;
    }
    #checkboxes label {
        display: block;
    }
    #checkboxes label:hover {
        background-color: #1e90ff;
    }
	#checkboxes2 {
        display: none;
        border: 1px #dadada solid;
    }
    #checkboxes2 label {
        display: block;
    }
    #checkboxes2 label:hover {
        background-color: #1e90ff;
    }
	</style>
	<script>
    var expanded = false;
    function showCheckboxes() {
        var checkboxes = document.getElementById("checkboxes");
        if (!expanded) {
            checkboxes.style.display = "block";
            expanded = true;
        } else {
            checkboxes.style.display = "none";
            expanded = false;
        }
    }
	</script>
	<script>
    var expanded = false;
    function showCheckboxes2() {
        var checkboxes = document.getElementById("checkboxes2");
        if (!expanded) {
            checkboxes.style.display = "block";
            expanded = true;
        } else {
            checkboxes.style.display = "none";
            expanded = false;
        }
    }
	</script>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<!-- bootstrap 3.0.2 -->
<script language="javascript" src="scripts/innovaeditor.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="js/local_clock.js" type="text/javascript"></script>
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Ionicons -->
<link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
<!-- Theme style -->
<link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
<style>
#labelimp {
	background-color: rgba(60, 141, 188, 0.19);
	padding: 4px;
	font-size: 20px;
	width: 100%;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	padding-left: 5px;
}
</style>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
$(document).ready(function(){ 	
		
		$("#ProductID").change(function () {
				if($(this).val() == 0)
				{
					$("#imagediv").slideUp();
				}
				else
				{
					$.ajax({			
							url: 'get_product_image.php?ID='+$("#ProductID").val(),
							success: function(data) {
								$("#image").html(data);
								$("#imagediv").slideDown(); 
							},
							error: function (xhr, textStatus, errorThrown) {
								alert(xhr.responseText);
								$("#image").removeAttr("disabled");
							}
					});
				}

			});
		$("#ProductID").change(function () {
				if($(this).val() == 0)
				{
					$("#pricediv").slideUp();

				}
				else
				{
					$.ajax({			
							url: 'get_product_price.php?ID='+$("#ProductID").val(),
							success: function(data) {
								$("#price").html(data);
								$("#pricediv").slideDown(); 
							},
							error: function (xhr, textStatus, errorThrown) {
								alert(xhr.responseText);
								$("#price").removeAttr("disabled");
							}
					});
				}

			});
		$("#ProductID").change(function () {
				if($(this).val() == 0)
				{
					$("#quantitydiv").slideUp();

				}
				else
				{
					$.ajax({			
							url: 'get_product_quantity.php?ID='+$("#ProductID").val(),
							success: function(data) {
								$("#quantity").html(data);
								$("#quantitydiv").slideDown(); 
							},
							error: function (xhr, textStatus, errorThrown) {
								alert(xhr.responseText);
								$("#quantity").removeAttr("disabled");
							}
					});
				}

			});
		$("#ProductID").change(function () {
				if($(this).val() == 0)
				{
					$("#statusdiv").slideUp();

				}
				else
				{
					$.ajax({			
							url: 'get_product_status.php?ID='+$("#ProductID").val(),
							success: function(data) {
								$("#status").html(data);
								$("#statusdiv").slideDown(); 
							},
							error: function (xhr, textStatus, errorThrown) {
								alert(xhr.responseText);
								$("#status").removeAttr("disabled");
							}
					});
				}

			});
		
		
	});   

</script>
</head>
<body class="skin-blue">
<!-- header logo: style can be found in header.less -->
<?php
	include_once("Header.php");
?>
<div class="wrapper row-offcanvas row-offcanvas-left">
  <!-- Left side column. contains the logo and sidebar -->
  <?php
	include_once("Sidebar.php");
?>
  <!-- Right side column. Contains the navbar and content of the page -->
  <aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Edit Up Sale Product</h1>
      <ol class="breadcrumb">
        <li><a href="UpSaleProducts.php"><i class="fa fa-dashboard"></i>Up Sale Products</a></li>
        <li class="active">Edit Up Sale Product</li>
      </ol>
    </section>
    <!-- Main content -->
    <form role="form" action="<?php echo $_SERVER["PHP_SELF"];?>?ID=<?php echo $ID; ?>" method="post" enctype="multipart/form-data" name="frmPage">
      <input type="hidden" name="action" value="submit_form" />
      <input type="hidden" name="ID" value="<?php echo $ID; ?>" />
      <section class="content">
        <div class="box-footer" style="text-align:right;">
          <button type="submit" class="btn btn-success margin">Save</button>
          <button class="btn btn-primary margin" type="button" onClick="location.href='UpSaleProducts.php'">Cancel</button>
        </div>
        <?php
			  	echo $msg;
				if(isset($_SESSION["msg"]))
				{
					echo $_SESSION["msg"];
					$_SESSION["msg"]="";
				}
		?>
        <div class="col-md-6">
          <div class="box">
            <!-- general form elements -->
            <div style="padding:15px;" class="box-primary">
              <!-- form start -->
              <div class="box-body">
               
                <div class="form-group">
                  <label id="labelimp" for="ProductID" >Product: </label>
                  
				  <select name="ProductID" id="ProductID" class="form-control">
					<option value="0" >Select Product</option>
					<?php
					 $query = "SELECT ProductName,ID FROM products where Status = 1 ORDER BY ProductName ASC";
					$res = mysql_query($query);
					while($row = mysql_fetch_array($res))
					{
					echo '<option '.($ProductID == $row['ID'] ? 'selected' : '').' value="'.$row['ID'].'">'.$row['ProductName'].'</option>';
					}
					?>
					</select>
                </div>
				
				<div class="form-group"  id="imagediv" <?php echo ($ProductID == 0 ? ' style="display:none;" ' : '');?> >
					<label id="labelimp" for="image" >Image: </label>
						<div id="image" disabled>
						<?php
							$r = mysql_query("SELECT Image FROM products WHERE ID = '".dbinput($ProductID)."'") or die(mysql_error());
							$n = mysql_num_rows($r);
							
							if($n != 0)
							{
								$row = mysql_fetch_array($r);
								$Image=$row["Image"];
								$Img = explode(',',$Image);
								echo '<img src="'. DIR_PRODUCTS_IMAGES . $Img[0] . '" height="100">';
							}
							
						?>
						</div>
				</div>
				
				<div class="form-group"  id="pricediv" <?php echo ($ProductID == 0 ? ' style="display:none;" ' : '');?> >
					<label id="labelimp" for="price" >Price: </label>
						<div id="price" disabled>
						<?php
							$r = mysql_query("SELECT Discount,Price FROM products WHERE ID = '".dbinput($ProductID)."'") or die(mysql_error());
							$n = mysql_num_rows($r);
							
							if($n != 0)
							{
								$row = mysql_fetch_array($r);
								if($row["Discount"] != 0)
								{
									$persentage = ($row["Discount"] / $row["Price"]) * 100;
									$persentage = 100 - $persentage;
									$persentage = round($persentage);
									$dis = "Orignal: ".$row["Price"]." | With Discount: ".$row["Discount"]."&emsp;(".$persentage."% Off)";
								}

								echo '<input type="text" disabled maxlength="100" name="price" class="form-control"  value="'.($row["Discount"] != 0 ? $dis : "Price: ".$row["Price"]).'" />';
							}
							
						?>
						</div>
				</div>
				
				<div class="form-group"  id="quantitydiv" <?php echo ($ProductID == 0 ? ' style="display:none;" ' : '');?> >
					<label id="labelimp" for="quantity" >Quantity: </label>
						<div id="quantity" disabled>
						<?php
							$r = mysql_query("SELECT Quantity FROM products WHERE ID = '".dbinput($ProductID)."'") or die(mysql_error());
							$n = mysql_num_rows($r);
							
							if($n != 0)
							{
								$row = mysql_fetch_array($r);
								echo '<input type="text" disabled maxlength="100" name="quantity" class="form-control"  value="'.$row["Quantity"].'  Pieces" />';
							}
							
						?>
						</div>
				</div>
				
				<div class="form-group"  id="statusdiv" <?php echo ($ProductID == 0 ? ' style="display:none;" ' : '');?> >
					<label id="labelimp" for="status" >Status: </label>
						<div id="status" disabled>
						<?php
							$r = mysql_query("SELECT Status FROM products WHERE ID = '".dbinput($ProductID)."'") or die(mysql_error());
							$n = mysql_num_rows($r);
							
							if($n != 0)
							{
								$row = mysql_fetch_array($r);
								echo '<input type="text" disabled maxlength="100" name="status" class="form-control"  value="'.($row["Status"] == 0 ? "Disabled" : "Enable").'" />';
							}
							
						?>
						</div>
				</div>
				
                <input type="hidden" name="action" value="submit_form" />
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
            <!-- Form Element sizes -->
          </div>
        </div>
        <div class="col-md-6">
          <div class="box">
            <!-- general form elements -->
            <div style="padding:15px;" class="box-primary">
              <!-- form start -->
              <div class="box-body">
			  
				<div class="form-group">
                  <label id="labelimp" >Status: </label>
                  <label>
                  <input type="radio" name="Status" value="1"<?php echo ($Status == 1 ? ' checked="checked"' : ''); ?>>
                  Enable</label>
                  <label>
                  <input type="radio" name="Status" value="0"<?php echo ($Status == 0 ? ' checked="checked"' : ''); ?>>
                  Disable</label>
                </div>
<?php if($_REQUEST["ID"] == 1){ ?>
				 <div class="form-group">
                  <label id="labelimp" >Offer Date: </label>
                  <label>
                  	<input type="date" name="OfferDate" value="<?php echo $OfferDate; ?>" >
                  </label>
                 
                </div>
				
				
				<!-- <div class="form-group">
                  <label id="labelimp" for="RelatedUpSaleProducts" >Show In These Products: </label>
                 <div class="selectBox" onclick="showCheckboxes2()">
						<select class="form-control">
							<option>Select Products</option>
						</select>
						<div class="overSelect"></div>
					</div>
					<div id="checkboxes2" style="height:250px; overflow:scroll;">						
						<?php
						$query = "SELECT ProductName,ID FROM products WHERE ID <> 0 AND Status = 1 ORDER BY ProductName ASC";
						$res = mysql_query($query);
						while($row = mysql_fetch_array($res))
						{
						echo '<label><input '.(in_array($row['ID'], $Prod) ? "checked = checked" : "").' type="checkbox" name="Products[]" value="'.$row['ID'].'"/> '.$row['ProductName'].'</label>';
						}
						?>
				  </div>
                </div> -->
				
				<div class="form-group">
                  <label id="labelimp" for="OfferPrice">Offer Price: </label>
                <?php 
				echo '<input type="number" maxlength="50" id="OfferPrice" name="OfferPrice" class="form-control"  value="'.$OfferPrice.'" />';
				?>
                </div>
				<?php } ?>
                <input type="hidden" name="action" value="submit_form" />
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
            <!-- Form Element sizes -->
          </div>
        </div>
      </section>
    </form>
    <!-- /.content -->
  </aside>
  <!-- /.right-side -->
</div>
<!-- ./wrapper -->
<?php include_once("Footer.php"); ?>
<!-- add new calendar event modal -->
<!-- jQuery 2.0.2 -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<!-- jQuery UI 1.10.3 -->
<script src="js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="js/AdminLTE/app.js" type="text/javascript"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="js/AdminLTE/dashboard.js" type="text/javascript"></script>
</body>
</html>