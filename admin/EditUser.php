<?php
include_once("Common.php");
include("CheckAdminLogin.php");
	
	$msg="";
	$Status=1;
	$Admin=0;
	$ID=0;
	$FName="";
	$LName="";
	$UName="";
	$Email="";
	$Pswd="";
	$Role=0;
	$UserGroup=0;
	
	$ID=0;
	if(isset($_REQUEST["ID"]) && ctype_digit(trim($_REQUEST["ID"])))
		$ID=trim($_REQUEST["ID"]);
		
	
if(isset($_POST["action"]) && $_POST["action"] == "submit_form")
{	
	if(isset($_REQUEST["ID"]) && ctype_digit(trim($_REQUEST["ID"])))
		$ID=trim($_REQUEST["ID"]);
	if(isset($_POST["Status"]) && ((int)$_POST["Status"] == 0 || (int)$_POST["Status"] == 1))
		$Status=trim($_POST["Status"]);	
	if(isset($_POST["Admin"]) && ((int)$_POST["Admin"] == 0 || (int)$_POST["Admin"] == 1))
		$Admin=trim($_POST["Admin"]);	
	if(isset($_POST["UName"]))
		$UName=trim($_POST["UName"]);
	if(isset($_POST["FName"]))
		$FName=trim($_POST["FName"]);
	if(isset($_POST["LName"]))
		$LName=trim($_POST["LName"]);
	if(isset($_POST["Pswd"]))
		$Pswd=trim($_POST["Pswd"]);
	if(isset($_POST["Email"]))
		$Email=trim($_POST["Email"]);
	if(isset($_POST["Role"]) && ctype_digit($_POST['Role']))
		$Role=trim($_POST["Role"]);
	if(isset($_POST["UserGroup"]) && ctype_digit($_POST['UserGroup']))
		$UserGroup=trim($_POST["UserGroup"]);

	
		if($FName == "")
		{
			$msg='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Please enter First Name.</b>
			</div>';
		}
		else if($LName == "")
		{
			$msg='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Please enter Last Name.</b>
			</div>';
		}		
		else if($UName == "")
		{
			$msg='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Please enter User Name.</b>
			</div>';
		}	
		else if($Role == 0 && $Admin == 0)
		{
			$msg='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Please Select Role.</b>
			</div>';
		}
		else if($Admin == 1 && $Role != 0)
		{
			$msg='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Role and Admin(Manager) both Selected</b>
			</div>';
		}
		else if($Email != "")
		{
			if(!validEmailAddress($Email))
			{
			
			$msg='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Please Enter valid EmailAddress.</b>
			</div>';
			}
		}
		
		
	
	if($msg=="")
	{
		$r = mysql_query("SELECT UserName FROM users WHERE 
		UserName='".dbinput($UName)."' AND ID <> '".(int)$ID."'") or die(mysql_error());
		if(mysql_num_rows($r) == 0)
		{
			$query="UPDATE users SET  
				DateModified=NOW(),
				FirstName = '" . dbinput($FName) . "',
				LastName = '" . dbinput($LName) . "',
				UserName = '" . dbinput($UName) . "',
				Status='".(int)$Status . "',
				Admin='".(int)$Admin . "',
				EmailAddress = '" . dbinput($Email) . "',
				".($Pswd=="" ? '' : "Password = '" . dbinput($Pswd) . "',")."
				Role = '" . (int)$Role . "',
				UserGroup = '" . (int)$UserGroup . "' 
			WHERE ID='".(int)$ID."'";
			mysql_query($query) or die ('Could not update user because: ' . mysql_error());


	$result=mysql_query ("SELECT FirstName,LastName FROM users WHERE ID = ".$ID."") or die("Query error: ". mysql_error());
		$row = mysql_fetch_array($result);

		 $query="INSERT INTO log_report SET DateAdded=NOW(),
				Type = 2,
				Form = 'Users',
				Name = '".$row["FirstName"].' '.$row["LastName"]."',
				UserID = '" . (int)$_SESSION["UserID"] . "'";
			
		mysql_query($query) or die ('Could not add user because: ' . mysql_error());
			//echo $query;
			//exit();
			
			$_SESSION["msg"] ='<div class="alert alert-success alert-dismissable">
				<i class="fa fa-ban"></i>
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<b>User has been Updated.</b>
			</div>';		
		}
		else
		{
			$_SESSION["msg"] = '<div class="alert alert-danger alert-dismissable">
				<i class="fa fa-ban"></i>
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<b>Name already exists.</b>
			</div>';
		}
			
		
	redirect($_SERVER["PHP_SELF"].'?ID='.$ID);		
	}
	
}
else
{
	$query="SELECT ID, Status,UserName,FirstName,LastName,EmailAddress,Role,UserGroup,Admin FROM users WHERE  ID='" . (int)$ID . "'";
	$result = mysql_query ($query) or die("Could not select user because: ".mysql_error()); 
	$num = mysql_num_rows($result);
	
	if($num==0)
	{
		$_SESSION["msg"]='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Invalid User ID.</b>
		</div>';
		redirect("Users.php");
	}
	else
	{
		$row = mysql_fetch_array($result,MYSQL_ASSOC);
		
		$ID=$row["ID"];
		$Status=$row["Status"];
		$Admin=$row["Admin"];
		$FName=$row["FirstName"];
		$LName=$row["LastName"];
		$UName=$row["UserName"];
		$Email=$row["EmailAddress"];
		$Role=$row["Role"];
		$UserGroup=$row["UserGroup"];
	}
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Edit User</title>
<style>
    .multiselect {
        width: auto;
    }
    .selectBox {
        position: relative;
    }
    .selectBox select {
        width: 100%;
    }
    .overSelect {
        position: absolute;
        left: 0; right: 0; top: 0; bottom: 0;
    }
    #checkboxes {
        display: none;
        border: 1px #dadada solid;
    }
    #checkboxes label {
        display: block;
    }
    #checkboxes label:hover {
        background-color: #1e90ff;
    }
	</style>
	<script>
    var expanded = false;
    function showCheckboxes() {
        var checkboxes = document.getElementById("checkboxes");
        if (!expanded) {
            checkboxes.style.display = "block";
            expanded = true;
        } else {
            checkboxes.style.display = "none";
            expanded = false;
        }
    }
	</script>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<!-- bootstrap 3.0.2 -->
<script language="javascript" src="scripts/innovaeditor.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="js/local_clock.js" type="text/javascript"></script>
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Ionicons -->
<link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
<!-- Theme style -->
<link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
<style>
#labelimp {
	background-color: rgba(60, 141, 188, 0.19);
	padding: 4px;
	font-size: 20px;
	width: 100%;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	padding-left: 5px;
}
</style>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
</head>
<body class="skin-blue">
<!-- header logo: style can be found in header.less -->
<?php
	include_once("Header.php");
?>
<div class="wrapper row-offcanvas row-offcanvas-left">
  <!-- Left side column. contains the logo and sidebar -->
  <?php
	include_once("Sidebar.php");
?>
  <!-- Right side column. Contains the navbar and content of the page -->
  <aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Edit User</h1>
      <ol class="breadcrumb">
        <li><a href="Users.php"><i class="fa fa-dashboard"></i>Users</a></li>
        <li class="active">Edit User</li>
      </ol>
    </section>
    <!-- Main content -->
    <form role="form" action="<?php echo $_SERVER["PHP_SELF"];?>?ID=<?php echo $ID; ?>"" method="post" enctype="multipart/form-data" name="frmPage">
      <input type="hidden" name="action" value="submit_form" />
      <input type="hidden" name="ID" value="<?php echo $ID; ?>" />
      <section class="content">
        <div class="box-footer" style="text-align:right;">
          <button type="submit" class="btn btn-success margin">Save</button>
          <button class="btn btn-primary margin" type="button" onClick="location.href='Users.php'">Cancel</button>
        </div>
        <?php
			  	echo $msg;
				if(isset($_SESSION["msg"]))
				{
					echo $_SESSION["msg"];
					$_SESSION["msg"]="";
				}
				?>
        <div class="col-md-6">
          <div class="box">
            <!-- general form elements -->
            <div style="padding:15px;" class="box-primary">
              <!-- form start -->
              <div class="box-body">
               
               <div class="form-group">
                  <label id="labelimp" class="labelimp" for="FName">First Name: </label>
                  <?php 
				echo '<input type="text" maxlength="100" id="FName" name="FName" class="form-control"  value="'.$FName.'" />';
				?>
                </div>
				
				<div class="form-group">
                  <label id="labelimp" class="labelimp" for="LName">Last Name: </label>
                  <?php 
				echo '<input type="text" maxlength="100" id="LName" name="LName" class="form-control"  value="'.$LName.'" />';
				?>
                </div>
				
				<div class="form-group">
                  <label id="labelimp" class="labelimp" for="UName">User Name: </label>
                  <?php 
				echo '<input type="text" maxlength="100" id="UName" name="UName" class="form-control"  value="'.$UName.'" />';
				?>
                </div>
				
				<div class="form-group">
                  <label id="labelimp" class="labelimp" for="Pswd">Password: </label>
                  <?php 
				echo '<input type="password" maxlength="100" id="Pswd" name="Pswd" class="form-control"  value="" />';
				?>
                </div>
				
                <input type="hidden" name="action" value="submit_form" />
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
            <!-- Form Element sizes -->
          </div>
        </div>
        <div class="col-md-6">
          <div class="box">
            <!-- general form elements -->
            <div style="padding:15px;" class="box-primary">
              <!-- form start -->
              <div class="box-body">
			  
				<div class="form-group">
                  <label id="labelimp" >Status: </label>
                  <label>
                  <input type="radio" name="Status" value="1"<?php echo ($Status == 1 ? ' checked="checked"' : ''); ?>>
                  Enable</label>
                  <label>
                  <input type="radio" name="Status" value="0"<?php echo ($Status == 0 ? ' checked="checked"' : ''); ?>>
                  Disable</label>
                </div>
				
				<div class="form-group">
                  <label id="labelimp" class="labelimp" for="Email">Email Address: </label>
                  <?php 
				echo '<input type="text" maxlength="100" id="Email" name="Email" class="form-control"  value="'.$Email.'" />';
				?>
                </div>
				
				<div class="form-group">
                  <label id="labelimp" for="UserGroup" >User Group: </label>
                  
				  <select name="UserGroup" id="UserGroup" class="form-control">
					<option value="0" >Select User Group</option>
					<?php
					 $query = "SELECT UserGroup,ID FROM user_groups where Status = 1";
					$res = mysql_query($query);
					while($row = mysql_fetch_array($res))
					{
					echo '<option '.($UserGroup == $row['ID'] ? 'selected' : '').' value="'.$row['ID'].'">'.$row['UserGroup'].'</option>';
					} 
					?>
					</select>
                </div>
				
				<div class="form-group">
                  <label id="labelimp" for="Role" >Role: </label>
                  
				  <select name="Role" id="Role" class="form-control">
					<option value="0" >Select Role</option>
					<?php
					 $query = "SELECT Role,ID FROM roles where Status = 1";
					$res = mysql_query($query);
					while($row = mysql_fetch_array($res))
					{
					echo '<option '.($Role == $row['ID'] ? 'selected' : '').' value="'.$row['ID'].'">'.$row['Role'].'</option>';
					} 
					?>
					</select>
                </div>
				
				<div class="form-group">
                  <label id="labelimp" >Admin(Manager): </label>
                  <label>
                  <input type="radio" name="Admin" value="1"<?php echo ($Admin == 1 ? ' checked="checked"' : ''); ?>>
                  Yes</label>
                  <label>
                  <input type="radio" name="Admin" value="0"<?php echo ($Admin == 0 ? ' checked="checked"' : ''); ?>>
                  No</label>
                </div>
				
                <input type="hidden" name="action" value="submit_form" />
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
            <!-- Form Element sizes -->
          </div>
        </div>
      </section>
    </form>
    <!-- /.content -->
  </aside>
  <!-- /.right-side -->
</div>
<!-- ./wrapper -->
<?php include_once("Footer.php"); ?>
<!-- add new calendar event modal -->
<!-- jQuery 2.0.2 -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<!-- jQuery UI 1.10.3 -->
<script src="js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="js/AdminLTE/app.js" type="text/javascript"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="js/AdminLTE/dashboard.js" type="text/javascript"></script>
</body>
</html>