<?php
include_once("Common.php");
include("CheckAdminLogin.php");
	
	$msg = "";
	if (isset($_REQUEST['dID'])) {
		$ss = "SELECT Image FROM lookbook WHERE ID = '".$_REQUEST['dID']."'";
		$r = mysql_query($ss);
		$t = mysql_fetch_assoc($r);
		unlink($t['Image']);

		$s = "DELETE FROM lookbook WHERE ID = '".$_REQUEST['dID']."'";
		mysql_query($s) or die(mysql_error());
	}
	
if(isset($_POST["action"]) && $_POST["action"] == "submit_form")
{	
	if (isset($_POST["Name"])) {
		$Name = $_POST["Name"];
	}
	if (isset($_POST["sort"])) {
		$sort = $_POST["sort"];
	}
	$target_dir = "../modal/pages/";
	$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
	$uploadOk = 1;
	$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        $msg = '<div class="alert alert-danger alert-dismissable">
				<i class="fa fa-ban"></i>
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<b>File is an image - " . $check["mime"] . ".</b>
			</div>';
        $uploadOk = 1;
    } else {
        $msg = '<div class="alert alert-danger alert-dismissable">
				<i class="fa fa-ban"></i>
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<b>File is not an image.</b>
			</div>';
        $uploadOk = 0;
    }
// Check if file already exists
if (file_exists($target_file)) {
    $msg = '<div class="alert alert-danger alert-dismissable">
				<i class="fa fa-ban"></i>
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<b>File already exists.</b>
			</div>';
    $uploadOk = 0;
}

// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    $msg = '<div class="alert alert-danger alert-dismissable">
				<i class="fa fa-ban"></i>
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<b>Sorry, only JPG, JPEG, PNG & GIF files are allowed.</b>
			</div>';
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    $msg = '<div class="alert alert-danger alert-dismissable">
				<i class="fa fa-ban"></i>
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<b>Sorry, your file was not uploaded.</b>
			</div>';
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        $msg = '<div class="alert alert-success alert-dismissable">
						<i class="fa fa-ban"></i>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<b>The Image has been uploaded.</b>
					</div>';
					$sql = "INSERT INTO lookbook SET
					Name = '".$Name."',
					Image = '".$target_file."',
					sort = '".$sort."'";
					mysql_query($sql) or die(mysql_error());
    } else {
        $msg = '<div class="alert alert-danger alert-dismissable">
				<i class="fa fa-ban"></i>
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<b>Sorry, there was an error uploading your file.</b>
			</div>';
    }
}
	
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Look Book's Images</title>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<!-- bootstrap 3.0.2 -->
<script language="javascript" src="scripts/innovaeditor.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="js/local_clock.js" type="text/javascript"></script>
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Ionicons -->
<link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
<!-- Theme style -->
<link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
</head>
<body class="skin-blue">
<!-- header logo: style can be found in header.less -->
<?php
	include_once("Header.php");
?>
<div class="wrapper row-offcanvas row-offcanvas-left">
  <!-- Left side column. contains the logo and sidebar -->
  <?php
	include_once("Sidebar.php");
?>
  <!-- Right side column. Contains the navbar and content of the page -->
  <aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Look Book's Images</h1>
      <ol class="breadcrumb">
        <li><a href="lookbook.php"><i class="fa fa-dashboard"></i>Look Book's Images</a></li>
        <li class="active">Look Book's Images</li>
      </ol>
    </section>
    <!-- Main content -->
    <form role="form" action="<?php echo $_SERVER["PHP_SELF"];?>" method="post" enctype="multipart/form-data" name="frmPage">
      <input type="hidden" name="action" value="submit_form" />
      <section class="content">
        <div class="box-footer" style="text-align:right;">
          <button type="submit" class="btn btn-success margin">Upload</button>
        </div>
        <?php
			  	echo $msg;
				if(isset($_SESSION["msg"]))
				{
					echo $_SESSION["msg"];
					$_SESSION["msg"]="";
				}
				?>
        <div class="col-md-6">
          <div class="box">
            <!-- general form elements -->
            <div style="padding:15px;" class="box-primary">
              <!-- form start -->
              <div class="box-body" style="min-width: 80% !important;">
	              <div class="table-responsive">
					  <table class="table-bordered">
					  <thead>
					      <tr>
					        <th>Name</th>
					        <th>Image</th>
					        <th>Sort</th>
					        <th>Actions</th>
					      </tr>
					    </thead>
					    <tbody>
					    <?php $query="SELECT ID,Name,Image,sort FROM lookbook ORDER BY sort ASC";
	$result = mysql_query($query) or die("Could not select image because: ".mysql_error()); 
	while ($row = mysql_fetch_assoc($result)) { ?>
					      <tr>
					        <td><?php echo $row['Name']; ?></td>
					        <td><img src="<?php echo $row['Image']; ?>" alt="<?php echo $row['Name']; ?>" class="img-thumbnail" style="width: 150px; height:100px;"></td>
					        <td><input type="number" readonly="readonly" value="<?php echo $row['sort']; ?>"></td>
					        <td><a class="btn btn-danger" href="<?php echo $_SERVER['PHP_SELF']; ?>?dID=<?php echo $row['ID']; ?>">Delete</a></td>
					      </tr>
					      <?php } ?>
					    </tbody>
					  </table>
					</div>
	              </div>
              <div class="box-body">
              <h3>Upload Image</h3>
               
                <div class="form-group">
                	<label for="Name">Enter Name:</label> 
                	<input id="Name" type="text" name="Name">
                </div>
                <div class="form-group">
                	<label for="sort">Sort:</label> 
                	<input id="sort" type="number" min="1" name="sort" required="required">
                </div>
				<div class="form-group">
                    <label for="fu">Choose Image:</label>
				    <input id="fu" type="file" name="fileToUpload" id="fileToUpload">
                </div>
				
				  </div>
				  </div>
                </div>
				
                <input type="hidden" name="action" value="submit_form" />
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
            <!-- Form Element sizes -->
          </div>
        </div>
      </section>
    </form>
    <!-- /.content -->
  </aside>
  <!-- /.right-side -->
</div>
<!-- ./wrapper -->
<?php include_once("Footer.php"); ?>
<!-- add new calendar event modal -->
<!-- jQuery 2.0.2 -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<!-- jQuery UI 1.10.3 -->
<script src="js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="js/AdminLTE/app.js" type="text/javascript"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="js/AdminLTE/dashboard.js" type="text/javascript"></script>
</body>
</html>