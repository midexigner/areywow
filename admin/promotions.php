<?php
include_once("Common.php");
include("CheckAdminLogin.php");
	
	$msg="";
	if (isset($_GET['dID'])) {
		$ID = $_GET['dID'];
		$dQuery = mysql_query("DELETE FROM coupons WHERE ID = '".$ID."'") or die("Could not delete because ".mysql_error());
		if ($dQuery) {
			$_SESSION["msg"]='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Coupon has been deleted.</b>
		</div>';
			redirect('Promotions.php');
		}
	}
	

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Promotion</title>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<!-- bootstrap 3.0.2 -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="js/local_clock.js" type="text/javascript"></script>
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Ionicons -->
<link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
<!-- Theme style -->
<link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
<style>
#footer {
	width:100%;
	height:50px;
	background-color:#3c8dbc;
	text-align:center;
	vertical-align:center;
	padding-top:15px;
}
#labelimp {
	background-color: rgba(60, 141, 188, 0.19);
	padding: 4px;
	font-size: 20px;
	width: 100%;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	padding-left: 5px;
}
</style>
</head>
<body class="skin-blue">
<!-- header logo: style can be found in header.less -->
<?php
	include_once("Header.php");
?>
<div class="wrapper row-offcanvas row-offcanvas-left">
  <!-- Left side column. contains the logo and sidebar -->
  <?php
	include_once("Sidebar.php");
?>
  <!-- Right side column. Contains the navbar and content of the page -->
  <aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Promotions</h1>
      
    </section>
    <!-- Main content -->
    <!-- <form role="form" action="<?php// echo $_SERVER["PHP_SELF"];?>?ID=<?php// echo $ID; ?>"" method="post" enctype="multipart/form-data" name="frmPage"> -->
      <input type="hidden" name="action" value="submit_form" />
      <section class="content">
        <div class="box-footer" style="text-align:right;">
          <a href="AddCoupon.php" class="btn btn-success margin">Add New</a>
          <button class="btn btn-primary margin" type="button" onClick="location.href='dashboard.php'">Cancel</button>
        </div>
        <?php
			  	echo $msg;
				if(isset($_SESSION["msg"]))
				{
					echo $_SESSION["msg"];
					$_SESSION["msg"]="";
				}
				?>
				<table class="table table-responsive table-bordered">
				<thead>
					<tr>
					<th>S.No</th>
						<th>Name</th>
						<th>Code</th>
						<th>Categories</th>
						<th>Discount</th>
						<th>Expires On</th>
						<th>Date Added</th>
						<th>Date Modified</th>
						<th>Actions</th>
					</tr>
</thead>
<tbody>
					<?php
					$query="SELECT * FROM coupons WHERE ID <> 0 ORDER BY DateAdded DESC";
	
	$result = mysql_query ($query) or die("Could not select promotion because: ".mysql_error()); 
	$n = mysql_num_rows($result);
					if ($n == 0) {
						echo "<h2>No Promotions Listed.</h2>";
					} else {
					 $i=1;
					 while ($row = mysql_fetch_assoc($result)) { ?>
						<tr>
						<td><?php echo $i; ?></td>
							<td><?php echo $row['name']; ?></td>
							<td><?php echo $row['code']; ?></td>
							<td><?php $Categories = array();
							$Categories = explode(',', $row['categories']);
							foreach ($Categories as $Category) {
								$sql = mysql_query("SELECT CategoryName FROM categories WHERE ID = '".$Category."'") or die(mysql_error());
								$r = mysql_fetch_assoc($sql);
								echo $r['CategoryName']."<br>";
							}
							 ?></td>
							<td><?php echo $row['amount']; ?><?php echo ($row['type'] == 1 ? ' %' : ' rupees'); ?></td>
							<td><?php echo $row['expire']; ?></td>
							<td><?php echo $row['DateAdded']; ?></td>
							<td><?php echo $row['DateModified']; ?></td>
							<td><a class="btn btn-md btn-success" href="editPromotion.php?ID=<?php echo $row['ID']; ?>">Edit</a><a class="btn btn-md btn-danger" href="<?php echo $_SERVER['PHP_SELF']."?dID=".$row['ID']; ?>">Delete</a></td>
						</tr>
					<?php $i++; }
					} ?>
					</tbody>
				</table>
          </div>
        </div>
		
      </section>
    <!-- </form> -->
    <!-- /.content -->
  </aside>
  <!-- /.right-side -->
</div>
<!-- ./wrapper -->
<?php include_once("Footer.php"); ?>
<!-- add new calendar event modal -->
<!-- jQuery 2.0.2 -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<!-- jQuery UI 1.10.3 -->
<script src="js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="js/AdminLTE/app.js" type="text/javascript"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="js/AdminLTE/dashboard.js" type="text/javascript"></script>
</body>
</html>
