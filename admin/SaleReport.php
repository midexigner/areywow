<?php include("Common.php"); ?>
<?php include("CheckAdminLogin.php");?>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Sale Report</title>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<!-- bootstrap 3.0.2 -->
<!-- jQuery 2.0.2 -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="js/ui.datepicker.js" type="text/javascript"></script>
 <!-- DataTables -->
 <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css">
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- font Awesome -->
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Ionicons -->
<link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
<link href="css/datepicker.css" rel="stylesheet" type="text/css" />
<!-- DATA TABLES -->
<!-- Theme style -->
<link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
<script language="javascript">
  $(document).ready(function () {       
    $(".checkUncheckAll").click(function () {
      $(".chkIds").prop("checked", $(this).prop("checked"));      
    });
  });
  var counter = 0;
  
  
  function doDelete()
  {
    if($(".chkIds").is(":checked"))
    {
      if(confirm("Are you sure to delete."))
      {
        $("#action").val("delete");
        $("#frmPages").submit();
      }
    }
    else
      alert("Please select category to delete");
  }
  function doUpdate()
  {
    $("#action").val('update');
    $("#frmPages").submit();
  }
  
</script>
</head>
<body class="skin-blue">
<!-- header logo: style can be found in header.less -->
<?php
  include_once("Header.php");
?>
<div class="wrapper row-offcanvas row-offcanvas-left">
  <!-- Left side column. contains the logo and sidebar -->
  <?php
    include_once("Sidebar.php");
    ?>
  <!-- Right side column. Contains the navbar and content of the page -->
  <aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> Sale Report <small></small> </h1>
      <ol class="breadcrumb">
        <li><a href="Dashboard.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Sale Report</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Sale Report</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
             
                <table id="example2" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                     
                     
                    <th>Brand Name</th>
                    <th>Shop Name</th>
                    <th>Vendor Name</th>
                    <th>15 Days</th>
                    <th>1 Month</th>
                    <th>3 Month</th>
                    <th>6 Month</th>
                    <th>Total Sale</th>

                    </tr>
                  </thead>
                       <?php 

// $sql1 = "SELECT COUNT(ProcedureID) AS count FROM orders WHERE ProcedureID = 0";

$q ="SELECT v.ID as VID,o.ProcedureID AS OrderStatus , o.PaymentMethod, p.ProductName,b.BrandName, sum(od.Price) AS TotalSale,v.ShopName,CONCAT(v.FirstName,' ' ,v.LastName) AS VendorName FROM orders o left JOIN orders_details od ON o.ID = od.OrderID inner JOIN products p ON p.ID= od.ProductID left JOIN brands b ON p.Manufacture = b.ID left JOIN vendor v ON p.vendorID = v.ID GROUP BY v.ID";

$result = mysql_query($q);

?>  
                  <tbody>
 <?php while ($row = mysql_fetch_assoc($result)) {
$ID = $row['VID'];
$qOneFiveDays = mysql_query("SELECT o.ProcedureID AS OrderStatus , 
o.PaymentMethod, p.ProductName,b.BrandName,sum(od.Price) AS TotalSale,v.ShopName,CONCAT(v.FirstName,' ' ,v.LastName) AS VendorName
FROM orders o left JOIN orders_details od ON o.ID = od.OrderID inner JOIN products p ON p.ID= od.ProductID left JOIN brands b ON p.Manufacture = b.ID left JOIN vendor v ON p.vendorID = v.ID  WHERE od.DateAdded >= (DATE_SUB(NOW(),INTERVAL 15 DAY)) AND v.ID = $ID");
$qOneMonth = mysql_query("SELECT o.ProcedureID AS OrderStatus , 
o.PaymentMethod, p.ProductName
,b.BrandName,sum(od.Price) AS TotalSale,v.ShopName,CONCAT(v.FirstName,' ' ,v.LastName) AS VendorName
FROM orders o left JOIN orders_details od ON o.ID = od.OrderID inner JOIN products p ON p.ID= od.ProductID left JOIN brands b ON p.Manufacture = b.ID left JOIN vendor v ON p.vendorID = v.ID  WHERE od.DateAdded >= (DATE_SUB(NOW(),INTERVAL 30 DAY)) AND v.ID = $ID");
$qThreeMonth = mysql_query("SELECT o.ProcedureID AS OrderStatus , 
o.PaymentMethod, p.ProductName
,b.BrandName,sum(od.Price) AS TotalSale,v.ShopName,CONCAT(v.FirstName,' ' ,v.LastName) AS VendorName
FROM orders o left JOIN orders_details od ON o.ID = od.OrderID inner JOIN products p ON p.ID= od.ProductID left JOIN brands b ON p.Manufacture = b.ID left JOIN vendor v ON p.vendorID = v.ID  WHERE od.DateAdded >= (DATE_SUB(NOW(),INTERVAL 90 DAY)) AND v.ID = $ID");
$qSixMonth = mysql_query("SELECT o.ProcedureID AS OrderStatus , 
o.PaymentMethod, p.ProductName
,b.BrandName,sum(od.Price) AS TotalSale,v.ShopName,CONCAT(v.FirstName,' ' ,v.LastName) AS VendorName
FROM orders o left JOIN orders_details od ON o.ID = od.OrderID inner JOIN products p ON p.ID= od.ProductID left JOIN brands b ON p.Manufacture = b.ID left JOIN vendor v ON p.vendorID = v.ID  WHERE od.DateAdded >= (DATE_SUB(NOW(),INTERVAL 180 DAY)) AND v.ID = $ID");
$rowOneFiveDays =  mysql_fetch_assoc($qOneFiveDays);
$rowOneMonth =  mysql_fetch_assoc($qOneMonth);
$rowThreeMonth =  mysql_fetch_assoc($qThreeMonth);
$rowSixMonth =  mysql_fetch_assoc($qSixMonth);

  ?>
    
<tr>
 
  <td><?php echo $row['BrandName']; ?></td>
  <td><?php echo $row['ShopName']; ?></td>
  <td><?php echo $row['VendorName']; ?></td>
   <td><?php echo CURRENCY_SYMBOL. $rowOneFiveDays['TotalSale'];?></td>
   <td><?php echo CURRENCY_SYMBOL. $rowOneMonth['TotalSale'] ; ?></td>
   <td><?php echo CURRENCY_SYMBOL. $rowThreeMonth['TotalSale'] ; ?></td>
   <td><?php echo CURRENCY_SYMBOL. $rowSixMonth['TotalSale'] ; ?></td>
   <td><?php echo CURRENCY_SYMBOL. $row['TotalSale']; ?></td>


</tr>

<?php } ?> 
                  </tbody>
                </table>
              </form>
            </div>
            <br>
           
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
    </section>
    <!-- /.content -->
  </aside>
  <!-- /.right-side -->
</div>
<div class="no-print" style="width:100%;
  height:50px;
  background-color:#ed1c24;
  text-align:center;
  vertical-align:center;
  padding-top:15px;
  color:white;"> Copyright &copy; <?php echo date("Y") ?>Arey Wow</div>
<!-- ./wrapper -->
<!-- Bootstrap -->
<!-- <script src="js/bootstrap.min.js" type="text/javascript"></script> -->
<!-- AdminLTE App -->
<!-- <script src="js/AdminLTE/app.js" type="text/javascript"></script> -->
<!-- page script -->


<!-- jQuery 2.2.3 -->

<!-- Bootstrap 3.3.6 -->
<script src="js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="new/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/tabletools/2.2.2/swf/copy_csv_xls_pdf.swf"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.print.min.js"></script>
  </script>



<!-- SlimScroll -->
<script src="new/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="new/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="new/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="new/dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $("#example2").DataTable({
      dom: 'Bfrtip',
'iDisplayLength': 100,
     buttons: [
             'csv',
             'excel',
             'pdf',
             'print'
        ],

          "lengthChange": false,
    });
  });
</script>
</body>
</html>
