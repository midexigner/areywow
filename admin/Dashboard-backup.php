<?php include("Common.php"); ?>
<?php include("CheckAdminLogin.php");
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Dashboard</title>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<!-- bootstrap 3.0.2 -->
<!-- jQuery 2.0.2 -->
<link rel="stylesheet" href="css/style-2.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="js/ui.datepicker.js" type="text/javascript"></script>
<script language="javascript" src="js/local_clock.js" type="text/javascript"></script>
<!-- <script language="javascript" src="../js/functions.js" type="text/javascript"></script>
 --><link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- font Awesome -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- font Awesome -->
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Ionicons -->
<script language="javascript" src="scripts/innovaeditor.js"></script>
<link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
<!-- Morris chart -->
<link href="css/morris/morris.css" rel="stylesheet" type="text/css" />
<!-- jvectormap -->
<link href="css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
<!-- fullCalendar -->
<link href="css/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
<!-- Daterange picker -->
<link href="css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
<!-- bootstrap wysihtml5 - text editor -->
<link href="css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />

<!-- Theme style -->
<link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
<style>
#labelimp {
	background-color: rgba(60, 141, 188, 0.19);
	padding: 4px;
	font-size: 20px;
	width: 100%;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	padding-left: 5px;
}
</style>


</head>
<body class="skin-blue">
<!-- header logo: style can be found in header.less -->
<?php
	include_once("Header.php");
?>
<div class="wrapper row-offcanvas row-offcanvas-left">
  <!-- Left side column. contains the logo and sidebar -->
  <?php
			include_once("Sidebar.php");
?>
  <!-- Right side column. Contains the navbar and content of the page -->
  <aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> Dashboard <small></small> </h1>
      <ol class="breadcrumb">
        <li><a href="Dashboard.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
                    <!-- <div class="row">
                        <div class="col-lg-4 col-xs-12">
                           
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h3>
                                        <?php //echo total_products(); ?>
                                    </h3>
                                    <p>
                                        Products 
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-bag"></i>
                                </div>
                                <a href="#" class="small-box-footer">
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-xs-12">
                            
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3>
                                         <?php //echo total_categories(); ?>
                                    </h3>
                                    <p>
                                        Categories
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-pie-graph"></i>
                                </div>
                                <a href="#" class="small-box-footer">
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-xs-12">
                            
                            <div class="small-box bg-red">
                                <div class="inner">
                                    <h3>
                                        <?php //echo total_brands(); ?>
                                    </h3>
                                    <p>
                                        Manufactures
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-ios7-pricetag-outline"></i>
                                </div>
                                <a href="#" class="small-box-footer">
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
                    </div> -->

                <div class="row-one">
                    <div class="col-md-4 widget">
                        <div class="stats-left ">
                            <!-- <h5>Today</h5> -->
                            <h4>Products</h4>
                        </div>
                        <div class="stats-right">
                            <label> <?php echo total_products(); ?></label>
                        </div>
                        <div class="clearfix"> </div>   
                    </div>
                    <div class="col-md-4 widget states-mdl">
                        <div class="stats-left">
                            <!-- <h5>Today</h5> -->
                            <h4>Categories</h4>
                        </div>
                        <div class="stats-right">
                            <label> <?php echo total_categories(); ?></label>
                        </div>
                        <div class="clearfix"> </div>   
                    </div>
                    <div class="col-md-4 widget states-last">
                        <div class="stats-left">
                            <!-- <h5>Today</h5> -->
                            <h4>Manufactures</h4>
                        </div>
                        <div class="stats-right">
                            <label><?php echo total_brands(); ?></label>
                        </div>
                        <div class="clearfix"> </div>   
                    </div>
                    <div class="clearfix"> </div>   
                </div>

                        
                    <div class="row">
                        <div class="col-md-4 ">
                            <h4 class="title">Bar Chart Example</h4>
                            <canvas id="bar" height="300" width="400"> </canvas>
                        </div>
                        <div class="col-md-4 ">
                            <h4 class="title">Line Chart Example</h4>
                            <canvas id="line" height="300" width="400"> </canvas>
                        </div>
                        <div class="col-md-4 ">
                            <h4 class="title">Pie Chart Example</h4>
                            <canvas id="pie" height="300" width="400"> </canvas>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 ">
                            <h4 class="title">doughnut Example</h4>
                            <canvas id="doughnut" height="300" width="400"> </canvas>
                        </div>
                        <div class="col-md-4 ">
                            <h4 class="title">Line Chart Example</h4>
                            <canvas id="line1" height="300" width="400"> </canvas>
                        </div>
                        <div class="col-md-4 ">
                            <h4 class="title">Pie Chart Example</h4>
                            <canvas id="pie1" height="300" width="400"> </canvas>
                        </div>
                    </div>

              
   
                   <!-- <div class="row">
                     
                        <section class="col-lg-12 connectedSortable">                            
                            
							
                            <div class="nav-tabs-custom">

                                <ul class="nav nav-tabs pull-right">
                                    <li class="pull-left header"><i class="fa fa-inbox"></i> Sales</li>
                                </ul>
                                <div class="tab-content no-padding">
                                    
                                    <div class="chart tab-pane active" id="revenue-chart" style="position: relative; height: 300px;"></div>
                                </div>
                            </div>
							
							
                            <div class="box box-info">
                                <div class="box-header">
                                    <i class="fa fa-envelope"></i>
                                    <h3 class="box-title">Quick Email</h3>

                                    <div class="pull-right box-tools">
										<button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button> 
                                    </div>
                                </div>
                                <div class="box-body">
                                    <form action="#" method="post">
                                        <div class="form-group">
                                            <input type="email" class="form-control" name="emailto" placeholder="Email to:"/>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="subject" placeholder="Subject"/>
                                        </div>
                                        <div>
                                            <textarea class="textarea" placeholder="Message" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                        </div>
                                    </form>
                                </div>
                                <div class="box-footer clearfix">
                                    <button class="pull-right btn btn-default" id="sendEmail">Send <i class="fa fa-arrow-circle-right"></i></button>
                                </div>
                            </div>
							
							
                       
                            <div class="box box-primary">
                                <div class="box-header">
                                    <i class="ion ion-clipboard"></i>
                                    <h3 class="box-title">To Do List</h3>
                                </div>
                                <div class="box-body">
                                    <ul class="todo-list">
                                        <li>
                                       
                                            <span class="handle">
                                                <i class="fa fa-ellipsis-v"></i>
                                                <i class="fa fa-ellipsis-v"></i>
                                            </span>
                                            
                                            <input type="checkbox" value="" name=""/>

                                            <span class="text">Design a nice theme</span>
                                           
                                            <small class="label label-danger"><i class="fa fa-clock-o"></i> 2 mins</small>

                                            <div class="tools">
                                                <i class="fa fa-edit"></i>
                                                <i class="fa fa-trash-o"></i>
                                            </div>
                                        </li>
                                        <li>
                                            <span class="handle">
                                                <i class="fa fa-ellipsis-v"></i>
                                                <i class="fa fa-ellipsis-v"></i>
                                            </span>
                                            <input type="checkbox" value="" name=""/>
                                            <span class="text">Make the theme responsive</span>
                                            <small class="label label-info"><i class="fa fa-clock-o"></i> 4 hours</small>
                                            <div class="tools">
                                                <i class="fa fa-edit"></i>
                                                <i class="fa fa-trash-o"></i>
                                            </div>
                                        </li>
                                        <li>
                                            <span class="handle">
                                                <i class="fa fa-ellipsis-v"></i>
                                                <i class="fa fa-ellipsis-v"></i>
                                            </span>
                                            <input type="checkbox" value="" name=""/>
                                            <span class="text">Let theme shine like a star</span>
                                            <small class="label label-warning"><i class="fa fa-clock-o"></i> 1 day</small>
                                            <div class="tools">
                                                <i class="fa fa-edit"></i>
                                                <i class="fa fa-trash-o"></i>
                                            </div>
                                        </li>
                                        <li>
                                            <span class="handle">
                                                <i class="fa fa-ellipsis-v"></i>
                                                <i class="fa fa-ellipsis-v"></i>
                                            </span>
                                            <input type="checkbox" value="" name=""/>
                                            <span class="text">Let theme shine like a star</span>
                                            <small class="label label-success"><i class="fa fa-clock-o"></i> 3 days</small>
                                            <div class="tools">
                                                <i class="fa fa-edit"></i>
                                                <i class="fa fa-trash-o"></i>
                                            </div>
                                        </li>
                                        <li>
                                            <span class="handle">
                                                <i class="fa fa-ellipsis-v"></i>
                                                <i class="fa fa-ellipsis-v"></i>
                                            </span>
                                            <input type="checkbox" value="" name=""/>
                                            <span class="text">Check your messages and notifications</span>
                                            <small class="label label-primary"><i class="fa fa-clock-o"></i> 1 week</small>
                                            <div class="tools">
                                                <i class="fa fa-edit"></i>
                                                <i class="fa fa-trash-o"></i>
                                            </div>
                                        </li>
                                        <li>
                                            <span class="handle">
                                                <i class="fa fa-ellipsis-v"></i>
                                                <i class="fa fa-ellipsis-v"></i>
                                            </span>
                                            <input type="checkbox" value="" name=""/>
                                            <span class="text">Let theme shine like a star</span>
                                            <small class="label label-default"><i class="fa fa-clock-o"></i> 1 month</small>
                                            <div class="tools">
                                                <i class="fa fa-edit"></i>
                                                <i class="fa fa-trash-o"></i>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="box-footer clearfix no-border">
                                    <button class="btn btn-default pull-right"><i class="fa fa-plus"></i> Add item</button>
                                </div>
                            </div>

                           
							
							                          

                        </section>
                    </div>-->
    </section>
    <!-- /.content -->
  </aside>
  <!-- /.right-side -->
</div>
<?php include_once("Footer.php"); ?>
        <!-- jQuery 2.0.2 -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- jQuery UI 1.10.3 -->
        <script src="js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <!-- Morris.js charts -->
        <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <!-- <script src="js/plugins/morris/morris.min.js" type="text/javascript"></script> -->
        <!-- Sparkline -->
        <script src="js/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
        <!-- jvectormap -->
        <script src="js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
        <script src="js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
        <!-- fullCalendar -->
        <script src="js/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
        <!-- jQuery Knob Chart -->
        <script src="js/plugins/jqueryKnob/jquery.knob.js" type="text/javascript"></script>
        <!-- daterangepicker -->
        <script src="js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
        <!-- iCheck -->
        <script src="js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>

        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app2.js" type="text/javascript"></script>
        
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <!-- <script src="js/AdminLTE/dashboard.js" type="text/javascript"></script> -->

        <script src="js/Chart.js"></script>      

        <script>
                var randomScalingFactor = function(){ return Math.round(Math.random()*100)};
                var barChartData = {
                                    labels : ["Jan","Feb","March","April","May","June","July"],
                                    datasets : [
                                        {
                                            fillColor : "rgba(233, 78, 2, 0.9)",
                                            strokeColor : "rgba(233, 78, 2, 0.9)",
                                            highlightFill: "#e94e02",
                                            highlightStroke: "#e94e02",
                                            data : [65,59,90,81,56,55,40]
                                        },
                                        {
                                            fillColor : "rgba(79, 82, 186, 0.9)",
                                            strokeColor : "rgba(79, 82, 186, 0.9)",
                                            highlightFill: "#ed1c24",
                                            highlightStroke: "#ed1c24",
                                            data : [40,70,55,20,45,70,60]
                                        }
                                    ]
                                    
                                };
                var pieData = [
                                        {
                                            value: 90,
                                            color:"rgba(233, 78, 2, 1)",
                                            label: "Product 1"
                                        },
                                        {
                                            value : 50,
                                            color : "rgba(242, 179, 63, 1)",
                                            label: "Product 2"
                                        },
                                        {
                                            value : 60,
                                            color : "rgba(88, 88, 88,1)",
                                            label: "Product 3"
                                        },
                                        {
                                            value : 40,
                                            color : "rgba(79, 82, 186, 1)",
                                            label: "Product 4"
                                        }
                                        
                                    ];
            var randomScalingFactor = function(){ return Math.round(Math.random()*100)};
            var lineChartData = {
                                    labels : ["Jan","Feb","March","April","May","June","July"],
                                    datasets : [
                                        {
                                            fillColor : "rgba(242, 179, 63, 1)",
                                            strokeColor : "#F2B33F",
                                            pointColor : "rgba(242, 179, 63, 1)",
                                            pointStrokeColor : "#fff",
                                            data : [70,60,72,61,75,59,80]

                                        },
                                        {
                                            fillColor : "rgba(97, 100, 193, 1)",
                                            strokeColor : "#ed1c24",
                                            pointColor : "rgba(97, 100, 193,1)",
                                            pointStrokeColor : "#9358ac",
                                            data : [50,65,51,67,52,64,50]

                                        }
                                    ]
                                    
                                };
                                var doughnutData = [
                                {
                                    value: 300,
                                    color:"#F7464A",
                                    highlight: "#FF5A5E",
                                    label: "Red"
                                },
                                {
                                    value: 50,
                                    color: "#46BFBD",
                                    highlight: "#5AD3D1",
                                    label: "Green"
                                },
                                {
                                    value: 100,
                                    color: "#FDB45C",
                                    highlight: "#FFC870",
                                    label: "Yellow"
                                },
                                {
                                    value: 40,
                                    color: "#949FB1",
                                    highlight: "#A8B3C5",
                                    label: "Grey"
                                },
                                {
                                    value: 120,
                                    color: "#4D5360",
                                    highlight: "#616774",
                                    label: "Dark Grey"
                                }
                            ];

                    window.onload = function(){
                    var ctx = document.getElementById("bar").getContext("2d");
                    window.myBar = new Chart(ctx).Bar(barChartData, {
                    responsive : true
                    });

                    var ctxx = document.getElementById("pie").getContext("2d");
                    window.myPie = new Chart(ctxx).Pie(pieData,{
                    responsive : true    
                    });

                    var ctx = document.getElementById("line").getContext("2d");
                    window.myLine = new Chart(ctx).Line(lineChartData, {
                        responsive: true
                    });
                
                
                    var ctx = document.getElementById("doughnut").getContext("2d");
                    window.myDoughnut = new Chart(ctx).Doughnut(doughnutData, {responsive : true});
                    }

    </script>


    </body>
</html>                         
                                
                                
