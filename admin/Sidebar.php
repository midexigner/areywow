<aside class="left-side sidebar-offcanvas sidebar-navigation">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->

    <div class="user-panel">
      <div class="pull-left image"> </div>
      <div class="pull-left info"> </div>
    </div>
    <!-- search form -->
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li> <a href="dashboard.php"> <i class="fa fa-dashboard"></i> <span>Dashboard</span> </a> </li>


	  <li class="treeview">
		<a href="#">
			<i class="fa fa-gear"></i>
			<span>Users Management</span>
			<i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="UserGroups.php"><i class="fa fa-angle-double-right"></i> User Groups</a></li>
			<li><a href="Users.php"><i class="fa fa-angle-double-right"></i> Users</a></li>
			<li><a href="WebsiteUsers.php"><i class="fa fa-angle-double-right"></i> Customers</a></li>
			<li><a href="vendorlist.php"><i class="fa fa-angle-double-right"></i> Vendor</a></li>
		</ul>
	  </li>



	  <li class="treeview">
		<a href="#">
			<i class="fa fa-book"></i>
			<span>Catalog</span>
			<i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="Products.php"><i class="fa fa-angle-double-right"></i> Products</a></li>
			<li><a href="ProductsPending.php"><i class="fa fa-angle-double-right"></i> Products Pending Vendor</a></li>
			<li><a href="ProductsApproval.php"><i class="fa fa-angle-double-right"></i> Products Approval Vendor</a></li>
			<li><a href="P_Options.php"><i class="fa fa-angle-double-right"></i> Product Options</a></li>
		<li><a href="P_Stocks.php"><i class="fa fa-angle-double-right"></i> Product Stocks</a></li>
			<li><a href="Categories.php"><i class="fa fa-angle-double-right"></i> Categories</a></li>
			<li><a href="Brands.php"><i class="fa fa-angle-double-right"></i> Brands</a></li>
		</ul>
	 </li>



	 <li class="treeview">
		<a href="#">
			<i class="fa fa-shopping-cart"></i>
			<span>Orders</span>
			<i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="Orders.php"><i class="fa fa-angle-double-right"></i> Orders</a></li>
			<li><a href="Cancelled.php"><i class="fa fa-angle-double-right"></i> Canceled</a></li>
			<li><a href="pending.php"><i class="fa fa-angle-double-right"></i> Pending</a></li>
			<li><a href="Confirmed.php"><i class="fa fa-angle-double-right"></i> Ready To Dispatch</a></li>
			<li><a href="Courier.php"><i class="fa fa-angle-double-right"></i> Delivered to Courier</a></li>
			<li><a href="Delivered.php"><i class="fa fa-angle-double-right"></i> Successfully Delivered</a></li>
			<li><a href="Returns.php"><i class="fa fa-angle-double-right"></i> Returns</a></li>
			<li><a href="Shippingoffers.php"><i class="fa fa-angle-double-right"></i> Shipping Offers</a></li>
			<li><a href="promotions.php"><i class="fa fa-angle-double-right"></i> Promotions</a></li>
		</ul>
	 </li>

	<li class="treeview">
		<a href="#">
			<i class="fa fa-pencil"></i>
			<span>Design</span>
			<i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="Logos.php"><i class="fa fa-angle-double-right"></i> Logo</a></li>
			<li><a href="Sliders.php"><i class="fa fa-angle-double-right"></i> Slider</a></li>
			<li><a href="Banner1.php"><i class="fa fa-angle-double-right"></i> Home Page Banners</a></li>
			<!-- <li><a href="Banner2.php"><i class="fa fa-angle-double-right"></i> Home Page Video</a></li> -->
			<li><a href="Banner3.php"><i class="fa fa-angle-double-right"></i> ADs Banner Home</a></li>
			<!-- <li><a href="Banner4.php"><i class="fa fa-angle-double-right"></i> Banner 4</a></li>
			<li><a href="Banner5.php"><i class="fa fa-angle-double-right"></i> Banner 5</a></li> 
			<li><a href="Thumbnails.php"><i class="fa fa-angle-double-right"></i> Thumbnails</a></li>-->
			<li><a href="Socialmedia.php"><i class="fa fa-angle-double-right"></i> Social Media</a></li>
			<!-- <li><a href="lookbook.php"><i class="fa fa-angle-double-right"></i> Look Book's Images</a></li> -->
			<li><a href="MiddleText.php"><i class="fa fa-angle-double-right"></i> Welcome Note</a></li>
			<li><a href="Telephones.php"><i class="fa fa-angle-double-right"></i> Phone Number</a></li>
			<li><a href="EmailAddress.php"><i class="fa fa-angle-double-right"></i> Email Address</a></li>
			<li><a href="AboutWebsite.php"><i class="fa fa-angle-double-right"></i> About Website</a></li>
			<li><a href="Address.php"><i class="fa fa-angle-double-right"></i>Address</a></li>
			
			<!-- <li><a href="Videos.php"><i class="fa fa-angle-double-right"></i> Videos</a></li> -->
			<li><a href="FooterCategories.php"><i class="fa fa-angle-double-right"></i> Footer Categories</a></li>
			<li><a href="FooterLinks.php"><i class="fa fa-angle-double-right"></i> Footer Links</a></li>
		</ul>
	 </li>
<!--
	 <li class="treeview">
		<a href="#">
			<i class="fa fa-link"></i>
			<span>Plugin Customization</span>
			<i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="CustomTemplates.php"><i class="fa fa-angle-double-right"></i> Custom Templates</a></li>
			<li><a href="DesignCategories.php"><i class="fa fa-angle-double-right"></i> Design Categories</a></li>
			<li><a href="DesignIcons.php"><i class="fa fa-angle-double-right"></i> Design Icons</a></li>
		</ul>
	 </li>
-->
	 <li class="treeview">
		<a href="#">
			<i class="fa fa-bar-chart-o"></i>
			<span>Sales</span>
			<i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="UpSaleProducts.php"><i class="fa fa-angle-double-right"></i> Up Sale Products</a></li>
		</ul>
	 </li>

	 <li class="treeview">
		<a href="#">
			<i class="fa fa-file-text"></i>
			<span>CMS</span>
			<i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
		<li><a href="Pages.php"><i class="fa fa-angle-double-right"></i> Pages</a></li>
		<li><a href="Faq.php"><i class="fa fa-angle-double-right"></i> Faq</a></li>
		</ul>
	 </li>

	 <li class="treeview">
		<a href="#">
			<i class="fa fa-user"></i>
			<span>Customer Care</span>
			<i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<!--<li><a href="#"><i class="fa fa-angle-double-right"></i> Customers</a></li>-->
			<li><a href="LiveChat.php"><i class="fa fa-angle-double-right"></i> Live Chat</a></li>
		</ul>
	 </li>

	 <li class="treeview">
		<a href="#">
			<i class="fa fa-truck"></i>
			<span>Shipping</span>
			<i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="Shippings.php"><i class="fa fa-angle-double-right"></i> Shipping Methods</a></li>
			<li><a href="ShippingRates.php"><i class="fa fa-angle-double-right"></i> Shipping Rates</a></li>
		</ul>
	 </li>

	 <li class="treeview">
		<a href="#">
			<i class="fa fa-list-ol"></i>
			<span>SEO</span>
			<i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="ProductKeywords.php"><i class="fa fa-angle-double-right"></i> Product Keywords</a></li>
			<li><a href="ProductDescription.php"><i class="fa fa-angle-double-right"></i> Product Description</a></li>
			<li><a href="CategoryKeywords.php"><i class="fa fa-angle-double-right"></i> Category Keywords</a></li>
			<li><a href="CategoryDescription.php"><i class="fa fa-angle-double-right"></i> Category Description</a></li>
			<li><a href="BrandKeywords.php"><i class="fa fa-angle-double-right"></i> Brand Keywords</a></li>
			<li><a href="BrandDescription.php"><i class="fa fa-angle-double-right"></i> Brand Description</a></li>
		</ul>
	 </li>

	<li class="treeview">
		<a href="#">
			<i class="fa fa-upload"></i>
			<span>Multimedia</span>
			<i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="MultimediaImages.php"><i class="fa fa-angle-double-right"></i> Images</a></li>
			<!--<li><a href="#"><i class="fa fa-angle-double-right"></i> Pages</a></li>-->
		</ul>
	 </li>

	<li class="treeview">
		<a href="#">
			<i class="fa fa-envelope"></i>
			<span>Newsletter</span>
			<i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<!-- <li><a href="MailChimp.php"><i class="fa fa-angle-double-right"></i> Newsletters</a></li> -->
			<li><a href="Newsletters.php"><i class="fa fa-angle-double-right"></i> Newsletters</a></li>
			<li><a href="NewsletterSubscribers.php"><i class="fa fa-angle-double-right"></i> Subscribers List</a></li>
			<li><a href="SendNewsletters.php"><i class="fa fa-angle-double-right"></i> Send Newsletters</a></li>
		</ul>
	 </li>



	<!-- <li class="treeview">
		<a href="#">
			<i class="fa fa-calendar"></i>
			<span>Reports</span>
			<i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="#"><i class="fa fa-angle-double-right"></i> Orders</a></li>
			<li><a href="#"><i class="fa fa-angle-double-right"></i> Cancelled</a></li>
			<li><a href="#"><i class="fa fa-angle-double-right"></i> Delivered</a></li>
			<li><a href="#"><i class="fa fa-angle-double-right"></i> Returns</a></li>
		</ul>
	 </li>-->

	 <!--<li class="treeview">-->
		<!--<a href="#">-->
		<!--	<i class="fa fa-info"></i>-->
		<!--	<span>Information</span>-->
		<!--	<i class="fa fa-angle-left pull-right"></i>-->
		<!--</a>-->
		<!--<ul class="treeview-menu">-->
		<!--<li><a href="P_Options.php"><i class="fa fa-angle-double-right"></i> Product Options</a></li>-->
		<!--<li><a href="P_Stocks.php"><i class="fa fa-angle-double-right"></i> Product Stocks</a></li>-->
		<!--<li><a href="PaymentMethods.php"><i class="fa fa-angle-double-right"></i> Payment Methods</a></li>-->

		<!--</ul>-->
	 <!--</li>-->

   <li class="treeview">
   <a href="#">
     <i class="fa fa-bar-chart-o"></i>
     <span>Reports</span>
     <i class="fa fa-angle-left pull-right"></i>
   </a>
   <ul class="treeview-menu">
   	<li><a href="logreport.php"><i class="fa fa-angle-double-right"></i> Log Report</a></li>
   	<li><a href="SaleReport.php"><i class="fa fa-angle-double-right"></i> Sale Report</a></li>
     <li><a href="OrderStatus.php"><i class="fa fa-angle-double-right"></i> Order Status</a></li>
     <li><a href="OrderTimes.php"><i class="fa fa-angle-double-right"></i> Order Timings</a></li>
     <li><a href="paymentVia.php"><i class="fa fa-angle-double-right"></i> Payment Reports</a></li>
     <li><a href="ordersCities.php"><i class="fa fa-angle-double-right"></i>Orders by Cities</a></li>
     <li><a href="ordersCountries.php"><i class="fa fa-angle-double-right"></i>Orders by Countries</a></li>
     <li><a href="bestBuyers.php"><i class="fa fa-angle-double-right"></i>Best Buyers</a></li>
     <li><a href="highStock.php"><i class="fa fa-angle-double-right"></i>Products High in Stock</a></li>
     <li><a href="lowStock.php"><i class="fa fa-angle-double-right"></i>Products Low in Stock</a></li>
     <li><a href="reOrders.php"><i class="fa fa-angle-double-right"></i>Re-Orders</a></li>
     <li><a href="topSearches.php"><i class="fa fa-angle-double-right"></i>Most searched Products</a></li>
     <li><a href="topsellerProducts.php"><i class="fa fa-angle-double-right"></i>Most Sold Products</a></li>
     <li><a href="Most-visited-pages.php"><i class="fa fa-angle-double-right"></i>Most Visited Pages</a></li>
   </ul>
  </li>


<li>
		<a href="locator.php">
			<i class="fa fa-gear"></i>
			<span>Store Locator</span>
		</a>
	  </li>


<li> <a href="career.php"> <i class="fa fa-dashboard"></i> <span>Careers</span> </a> </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
