<?php
include_once("Common.php");
include("CheckAdminLogin.php");



	$msg="";
	$ID=0;
	$Status=1;
	$zChangeable=1;
	$removable=1;
	$draggable=1;
	$rotatable=1;
	$resizable=1;
	$boundingBox=1;
	$autoCenter=1;
	$topdistance=0;
	$leftdistance=0;
	$Name="";
	$CategoryID=0;
	$colors="#000000";
		
if(isset($_POST["action"]) && $_POST["action"] == "submit_form")
{
	if(isset($_POST["Status"]) && ((int)$_POST["Status"] == 0 || (int)$_POST["Status"] == 1))
		$Status=trim($_POST["Status"]);
	if(isset($_POST["zChangeable"]) && ((int)$_POST["zChangeable"] == 0 || (int)$_POST["zChangeable"] == 1))
		$zChangeable=trim($_POST["zChangeable"]);
	if(isset($_POST["removable"]) && ((int)$_POST["removable"] == 0 || (int)$_POST["removable"] == 1))
		$removable=trim($_POST["removable"]);
	if(isset($_POST["draggable"]) && ((int)$_POST["draggable"] == 0 || (int)$_POST["draggable"] == 1))
		$draggable=trim($_POST["draggable"]);
	if(isset($_POST["rotatable"]) && ((int)$_POST["rotatable"] == 0 || (int)$_POST["rotatable"] == 1))
		$rotatable=trim($_POST["rotatable"]);
	if(isset($_POST["resizable"]) && ((int)$_POST["resizable"] == 0 || (int)$_POST["resizable"] == 1))
		$resizable=trim($_POST["resizable"]);
	if(isset($_POST["boundingBox"]) && ((int)$_POST["boundingBox"] == 0 || (int)$_POST["boundingBox"] == 1))
		$boundingBox=trim($_POST["boundingBox"]);
	if(isset($_POST["autoCenter"]) && ((int)$_POST["autoCenter"] == 0 || (int)$_POST["autoCenter"] == 1))
		$autoCenter=trim($_POST["autoCenter"]);
	if(isset($_POST["topdistance"]) && ctype_digit($_POST["topdistance"]))
		$topdistance=trim($_POST["topdistance"]);
	if(isset($_POST["leftdistance"]) && ctype_digit($_POST["leftdistance"]))
		$leftdistance=trim($_POST["leftdistance"]);
	if(isset($_POST["Name"]))
		$Name=trim($_POST["Name"]);
	if(isset($_POST["CategoryID"]) && ctype_digit($_POST["CategoryID"]))
		$CategoryID=trim($_POST["CategoryID"]);
	if(isset($_POST["colors"]))
		$colors=trim($_POST["colors"]);
	if(isset($_FILES["flPage"]) && $_FILES["flPage"]['name'] != "")
	{
		$filenamearray=explode(".", $_FILES["flPage"]['name']);
		$ext=strtolower($filenamearray[sizeof($filenamearray)-1]);
	
		if(!in_array($ext, $_IMAGE_ALLOWED_TYPES))
		{
			$msg='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Only '.implode(", ", $_IMAGE_ALLOWED_TYPES) . ' files can be uploaded.
			</div>';
		}			
		else if($_FILES["flPage"]['size'] > (MAX_IMAGE_SIZE*1024))
		{
			$msg='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Image size must be ' . MAX_IMAGE_SIZE . ' KB or less.
			</div>';
		}
	}
		

	
		if($Name == "")
		{
			$msg='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Please enter Design Icon Name.</b>
			</div>';
		}
		else if((!isset($_FILES["flPage"])) || ($_FILES["flPage"]['name'] == ""))
		{
			$msg='<div class="alert alert-danger alert-dismissable">
				<i class="fa fa-ban"></i>
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<b>Please Upload Design Icon.</b>
			</div>';
		}



	if($msg=="")
	{
		$query="INSERT INTO design_icons SET  Status='".(int)$Status . "', DateAdded=NOW(), 
				Name = '" . dbinput($Name) . "',
				CategoryID='".(int)$CategoryID . "',
				zChangeable='".(int)$zChangeable . "',
				removable='".(int)$removable . "',
				rotatable='".(int)$rotatable . "',
				resizable='".(int)$resizable . "',
				draggable='".(int)$draggable . "',
				boundingBox='".(int)$boundingBox . "',
				autoCenter='".(int)$autoCenter . "',
				topdistance='".(int)$topdistance . "',
				leftdistance='".(int)$leftdistance . "',
				colors = '" . dbinput($colors) . "'";
		mysql_query($query) or die (mysql_error());
		// echo $query;
		$ID = mysql_insert_id();
		$_SESSION["msg"]='<div class="alert alert-success alert-dismissable">
		<i class="fa fa-ban"></i>
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<b>DesignIcon has been added.</b>
		</div>';
		
		if(isset($_FILES["flPage"]) && $_FILES["flPage"]['name'] != "")
		{
			if(is_file(DIR_DESIGNICONS . $StoreImage))
				unlink(DIR_DESIGNICONS . $StoreImage);
		
			ini_set('memory_limit', '-1');
			
			$tempName = $_FILES["flPage"]['tmp_name'];
			$realName = $ID . "." . $ext;
			$StoreImage = $realName; 
			$target = DIR_DESIGNICONS . $realName;

			$moved=move_uploaded_file($tempName, $target);
		
			if($moved)
			{			
			
				$query="UPDATE design_icons SET Image='" . dbinput($realName) . "' WHERE  ID=" . (int)$ID;
				mysql_query($query) or die(mysql_error());
			}
			else
			{
				$_SESSION["msg"]='<div class="alert alert-warning alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<b>Design Icon has been saved but Image can not be uploaded.</b>
					</div>';
			}
		}


		redirect("AddNewDesignIcon.php");	
	}
		

}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Add Design Icon</title>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<!-- bootstrap 3.0.2 -->
<script language="javascript" src="scripts/innovaeditor.js"></script>

<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="js/local_clock.js" type="text/javascript"></script>
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Ionicons -->
<link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
<!-- Theme style -->
<link href="//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css" rel="stylesheet" type="text/css" />
<!-- daterange picker -->
<link href="css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
<!-- iCheck for checkboxes and radio inputs -->
<link href="css/iCheck/all.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap Color Picker -->
<link href="css/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet"/>
<!-- Bootstrap time Picker -->
<link href="css/timepicker/bootstrap-timepicker.min.css" rel="stylesheet"/>
<link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
<style>
#footer {
	width:100%;
	height:50px;
	background-color:#3c8dbc;
	text-align:center;
	vertical-align:center;
	padding-top:15px;
}
#labelimp {
	background-color: rgba(60, 141, 188, 0.19);
	padding: 4px;
	font-size: 20px;
}
#labelimp {
	background-color: rgba(60, 141, 188, 0.19);
	padding: 4px;
	font-size: 20px;
	width: 100%;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	padding-left: 5px;
}
</style>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
</head>
<body class="skin-blue">
<!-- header logo: style can be found in header.less -->
<?php
		include_once("Header.php");
		?>
<div class="wrapper row-offcanvas row-offcanvas-left">
  <!-- Left side column. contains the logo and sidebar -->
  <?php
	include_once("Sidebar.php");
?>
  <!-- Right side column. Contains the navbar and content of the page -->
  <aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> Add Design Icon</h1>
      <ol class="breadcrumb">
        <li><a href="DesignIcons.php"><i class="fa fa-dashboard"></i>Design Icons</a></li>
        <li class="active">Add Design Icon</li>
      </ol>
    </section>
    <!-- Main content -->
    <form role="form" action="<?php echo $_SERVER["PHP_SELF"];?>" method="post" enctype="multipart/form-data" name="frmPage">
      <section class="content">
       <div class="box-footer" style="text-align:right;">
                <button type="submit" class="btn btn-success margin">Save</button>
                <button class="btn btn-primary margin" type="button" onClick="location.href='DesignIcons.php'">Cancel</button>
            </div>
              <?php
		  		echo $msg;
				if(isset($_SESSION["msg"]))
				{
					echo $_SESSION["msg"];
					$_SESSION["msg"]="";
				}
				?>
        <div class="col-md-6">
          <div class="box">
          
            <!-- general form elements -->
            <div style="padding:15px;" class="box-primary">
            
              <!-- form start -->
              <div class="box-body">
               
                <div class="form-group">
                  <label id="labelimp" class="labelimp" for="Name">Design Icon Name: </label>
                  <?php 
				echo '<input type="text" id="Name" name="Name" class="form-control"  value="'.$Name.'" />';
				?>
                </div>
				
				<div class="form-group">
				  <label id="labelimp" for="CategoryID" >Design Category: </label>
				  <select name="CategoryID" id="CategoryID" class="form-control">
					<?php
					 $query = "SELECT ID,Name FROM design_categories WHERE Status = 1 ORDER BY Sort ASC";
					$res = mysql_query($query);
					while($row = mysql_fetch_array($res))
					{
					echo '<option '.($CategoryID == $row['ID'] ? 'selected' : '').' value="'.$row['ID'].'">'.$row['Name'].'</option>';
					} 
					?>
					</select>
				</div>
				
				<div class="form-group">
                  <label id="labelimp" for="image" class="labelimp" >Design Icon: </label>
                  <input type="file" name="flPage" />
				  <p class="help-block">Image types allowed: jpg, jpeg, gif, png.</p>
                </div>
				
				<div class="form-group">
					<label id="labelimp" for="colors">Icon Color:</label>
					<input type="text" name="colors" id="colors" <?php echo 'value="'.$colors.'"' ?> class="form-control my-colorpicker1"/>
				</div><!-- /.form group -->
				
				<div class="form-group">
                  <label id="labelimp" >Status: </label>
                  <label>
                  <input type="radio" name="Status" value="1"<?php echo ($Status == 1 ? ' checked="checked"' : ''); ?>>
                  Enable</label>
                  <label>
                  <input type="radio" name="Status" value="0"<?php echo ($Status == 0 ? ' checked="checked"' : ''); ?>>
                  Disable</label>
                </div>			
				
                <input type="hidden" name="action" value="submit_form" />
              </div>
              <!-- /.box-body -->
            
            </div>
            <!-- /.box -->
            <!-- Form Element sizes -->
          </div>
        </div>
        <div class="col-md-6">
          <div class="box">
            
            <!-- general form elements -->
            <div style="padding:15px;" class="box-primary">
             
              <!-- form start -->
              <div class="box-body">
				<div class="form-group">
				
				  <div class="form-group">
                  <label id="labelimp" class="labelimp" for="topdistance">Distance from Top (In Pixels): </label>
					  <?php 
					echo '<input type="text" id="topdistance" name="topdistance" class="form-control"  value="'.$topdistance.'" />';
					?>
				  </div>
				  
				  <div class="form-group">
                  <label id="labelimp" class="labelimp" for="leftdistance">Distance from Left (In Pixels): </label>
					  <?php 
					echo '<input type="text" id="leftdistance" name="leftdistance" class="form-control"  value="'.$leftdistance.'" />';
					?>
				  </div>
				
                  <label id="labelimp" >Bound In Box: </label>
                  <label>
                  <input type="radio" name="boundingBox" value="1"<?php echo ($boundingBox == 1 ? ' checked="checked"' : ''); ?>>
                  Yes</label>
                  <label>
                  <input type="radio" name="boundingBox" value="0"<?php echo ($boundingBox == 0 ? ' checked="checked"' : ''); ?>>
                  No</label>
				  
				  <label id="labelimp" >Auto Center: </label>
                  <label>
                  <input type="radio" name="autoCenter" value="1"<?php echo ($autoCenter == 1 ? ' checked="checked"' : ''); ?>>
                  Yes</label>
                  <label>
                  <input type="radio" name="autoCenter" value="0"<?php echo ($autoCenter == 0 ? ' checked="checked"' : ''); ?>>
                  No</label>
				  
				  <label id="labelimp" >Layer Position Changeable: </label>
                  <label>
                  <input type="radio" name="zChangeable" value="1"<?php echo ($zChangeable == 1 ? ' checked="checked"' : ''); ?>>
                  Yes</label>
                  <label>
                  <input type="radio" name="zChangeable" value="0"<?php echo ($zChangeable == 0 ? ' checked="checked"' : ''); ?>>
                  No</label>
				  
				  <label id="labelimp" >Draggable: </label>
                  <label>
                  <input type="radio" name="draggable" value="1"<?php echo ($draggable == 1 ? ' checked="checked"' : ''); ?>>
                  Yes</label>
                  <label>
                  <input type="radio" name="draggable" value="0"<?php echo ($draggable == 0 ? ' checked="checked"' : ''); ?>>
                  No</label>
				  
				  <label id="labelimp" >Rotatable: </label>
                  <label>
                  <input type="radio" name="rotatable" value="1"<?php echo ($rotatable == 1 ? ' checked="checked"' : ''); ?>>
                  Yes</label>
                  <label>
                  <input type="radio" name="rotatable" value="0"<?php echo ($rotatable == 0 ? ' checked="checked"' : ''); ?>>
                  No</label>
				  
				  <label id="labelimp" >Resizable: </label>
                  <label>
                  <input type="radio" name="resizable" value="1"<?php echo ($resizable == 1 ? ' checked="checked"' : ''); ?>>
                  Yes</label>
                  <label>
                  <input type="radio" name="resizable" value="0"<?php echo ($resizable == 0 ? ' checked="checked"' : ''); ?>>
                  No</label>
				  
				  <label id="labelimp" >Removable: </label>
                  <label>
                  <input type="radio" name="removable" value="1"<?php echo ($removable == 1 ? ' checked="checked"' : ''); ?>>
                  Yes</label>
                  <label>
                  <input type="radio" name="removable" value="0"<?php echo ($removable == 0 ? ' checked="checked"' : ''); ?>>
                  No</label>
				  
                </div>
				
                <input type="hidden" name="action" value="submit_form" />
              </div>
              <!-- /.box-body -->
             
            <!-- /.box -->
            <!-- Form Element sizes -->
          </div>
        </div>
      </section>
    </form>
    <!-- /.content -->
  </aside>
  <!-- /.right-side -->
</div>




<!-- ./wrapper -->
<?php include_once("Footer.php"); ?>
<!-- add new calendar event modal -->
<!-- jQuery 2.0.2 -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" type="text/javascript"></script>
<!-- InputMask -->
<script src="js/plugins/input-mask/jquery.inputmask.js" type="text/javascript"></script>
<script src="js/plugins/input-mask/jquery.inputmask.date.extensions.js" type="text/javascript"></script>
<script src="js/plugins/input-mask/jquery.inputmask.extensions.js" type="text/javascript"></script>
<!-- date-range-picker -->
<script src="js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
<!-- bootstrap color picker -->
<script src="js/plugins/colorpicker/bootstrap-colorpicker.min.js" type="text/javascript"></script>
<!-- bootstrap time picker -->
<script src="js/plugins/timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="js/AdminLTE/app2.js" type="text/javascript"></script>
<!-- AdminLTE for demo purposes -->
<script src="../js/AdminLTE/demo.js" type="text/javascript"></script>
<script type="text/javascript">
            $(function() {
                //Datemask dd/mm/yyyy
                $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
                //Datemask2 mm/dd/yyyy
                $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
                //Money Euro
                $("[data-mask]").inputmask();

                //Date range picker
                $('#reservation').daterangepicker();
                //Date range picker with time picker
                $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
                //Date range as a button
                $('#daterange-btn').daterangepicker(
                        {
                            ranges: {
                                'Today': [moment(), moment()],
                                'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                                'Last 7 Days': [moment().subtract('days', 6), moment()],
                                'Last 30 Days': [moment().subtract('days', 29), moment()],
                                'This Month': [moment().startOf('month'), moment().endOf('month')],
                                'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                            },
                            startDate: moment().subtract('days', 29),
                            endDate: moment()
                        },
                function(start, end) {
                    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                }
                );

                //iCheck for checkbox and radio inputs
                $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                    checkboxClass: 'icheckbox_minimal',
                    radioClass: 'iradio_minimal'
                });
                //Red color scheme for iCheck
                $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                    checkboxClass: 'icheckbox_minimal-red',
                    radioClass: 'iradio_minimal-red'
                });
                //Flat red color scheme for iCheck
                $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                    checkboxClass: 'icheckbox_flat-red',
                    radioClass: 'iradio_flat-red'
                });

                //Colorpicker
                $(".my-colorpicker1").colorpicker();
                //color picker with addon
                $(".my-colorpicker2").colorpicker();

                //Timepicker
                $(".timepicker").timepicker({
                    showInputs: false,
                });
            });
        </script>
</body>
</html>
