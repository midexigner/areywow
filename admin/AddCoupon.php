<?php
include_once("Common.php");
include("CheckAdminLogin.php");



	$msg="";
	$amount=0;
	$code="";
	$name="";
	$type=0;
	
		
if(isset($_POST["action"]) && $_POST["action"] == "submit_form")
{
	
	extract($_POST);
	$Category = implode(',', $Category);
	
		if($name == "")
		{
			$msg='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Please enter Coupon Name.</b>
			</div>';
		} else if($code == "")
		{
			$msg='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Please enter Coupon Code.</b>
			</div>';
		} else if($amount == 0)
		{
			$msg='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Please enter Discount.</b>
			</div>';
		} else if($expire == 0)
		{
			$msg='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Please enter Expiry date.</b>
			</div>';
		} else if($Category == "")
		{
			$msg='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Please Select Category.</b>
			</div>';
		}



	if($msg=="")
	{

	
		$query="INSERT INTO coupons SET DateAdded=NOW(),
		name = '".dbinput($name)."',
		code = '".dbinput($code)."',
		amount = '".$amount."',
		type = '".$type."',
		expire = '".$expire."',
		categories = '".$Category."'";
		mysql_query($query) or die ('Could not add coupon because: ' . mysql_error());
		// echo $query;
		/*$ID = mysql_insert_id();*/
		$_SESSION["msg"]='<div class="alert alert-success alert-dismissable">
		<i class="fa fa-ban"></i>
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<b>Coupon has been added.</b>
		</div>';
		
		

		redirect("promotions.php");	
	}
		

}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Add Coupon</title>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<!-- bootstrap 3.0.2 -->
<script language="javascript" src="scripts/innovaeditor.js"></script>

<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="js/local_clock.js" type="text/javascript"></script>
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Ionicons -->
<link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
<!-- Theme style -->
<link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
<style>
#footer {
	width:100%;
	height:50px;
	background-color:#3c8dbc;
	text-align:center;
	vertical-align:center;
	padding-top:15px;
}
#labelimp {
	background-color: rgba(60, 141, 188, 0.19);
	padding: 4px;
	font-size: 20px;
}
#labelimp {
	background-color: rgba(60, 141, 188, 0.19);
	padding: 4px;
	font-size: 20px;
	width: 100%;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	padding-left: 5px;
}
.selectBox {
        position: relative;
    }
    .selectBox select {
        width: 100%;
    }
    .overSelect {
        position: absolute;
        left: 0; right: 0; top: 0; bottom: 0;
    }
    #checkboxes {
        display: none;
        border: 1px #dadada solid;
    }
    #checkboxes label {
        display: block;
    }
    #checkboxes label:hover {
        background-color: #1e90ff;
    }
</style>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

</head>
<body class="skin-blue">
<!-- header logo: style can be found in header.less -->
<?php
		include_once("Header.php");
		?>
<div class="wrapper row-offcanvas row-offcanvas-left">
  <!-- Left side column. contains the logo and sidebar -->
  <?php
	include_once("Sidebar.php");
?>
  <!-- Right side column. Contains the navbar and content of the page -->
  <aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> Add Coupon</h1>
      <ol class="breadcrumb">
        <li><a href="promotions.php"><i class="fa fa-dashboard"></i>Coupons</a></li>
        <li class="active">Add Coupon</li>
      </ol>
    </section>
    <!-- Main content -->
    <form role="form" action="<?php echo $_SERVER["PHP_SELF"];?>" method="post" enctype="multipart/form-data" name="frmPage">
      <section class="content">
       <div class="box-footer" style="text-align:right;">
                <button type="submit" class="btn btn-success margin">Save</button>
                <button class="btn btn-primary margin" type="button" onClick="location.href='promotions.php'">Cancel</button>
            </div>
              <?php
		  		echo $msg;
				if(isset($_SESSION["msg"]))
				{
					echo $_SESSION["msg"];
					$_SESSION["msg"]="";
				}
				?>
        <div class="col-md-9">
          <div class="box">
          
            <!-- general form elements -->
            <div style="padding:15px;" class="box-primary">
            
              <!-- form start -->
              <div class="box-body">
               
                <div class="form-group">
                  <label id="labelimp" class="labelimp" for="name">Name: </label>
                  <?php 
				echo '<input type="text" id="Name" name="name" class="form-control"  value="'.(isset($_POST["name"]) ? $_POST["name"] : "").'" />';
				?>
                </div>

                <div class="form-group">
                  <label id="labelimp" class="labelimp" for="code">Code: </label>
                  <?php 
				echo '<input type="text" id="code" name="code" class="form-control"  value="'.(isset($_POST["code"]) ? $_POST["code"] : "").'" />';
				?>
                </div>

                <div class="form-group">
                  <label id="labelimp" class="labelimp" for="categories">Categories: </label>
                  <div class="selectBox" onclick="showCheckboxes()">
						<select class="form-control">
							<option>Select Category</option>
						</select>
						<div class="overSelect"></div>
					</div>
					<div id="checkboxes" style="height:250px; overflow:scroll;">						
						<?php
						$query = "SELECT CategoryName,ID FROM categories where Parent = 0 ORDER BY Sort";
						$res = mysql_query($query);
						while($row = mysql_fetch_array($res))
						{
						echo '<label><input '.(in_array($row['ID'], $Cat) ? "checked = checked" : "").' type="checkbox" name="Category[]" value="'.$row['ID'].'"/>'.$row['CategoryName'].'</label>';
						
							$query1 = "SELECT CategoryName,ID FROM categories where Parent = ".$row['ID']." ORDER BY Sort";
							$res1 = mysql_query($query1);
							while($row1 = mysql_fetch_array($res1))
							{
							echo '<label><input '.(in_array($row1['ID'], $Cat) ? "checked = checked" : "").' type="checkbox" name="Category[]" value="'.$row1['ID'].'"/>--------'.$row1['CategoryName'].'</label>';

									$query2 = "SELECT CategoryName,ID FROM categories where Parent = ".$row1['ID']." ORDER BY Sort";
									$res2 = mysql_query($query2);
									while($row2 = mysql_fetch_array($res2))
									{
									echo '<label><input '.(in_array($row2['ID'], $Cat) ? "checked = checked" : "").' type="checkbox" name="Category[]" value="'.$row2['ID'].'"/>----------------'.$row2['CategoryName'].'</label>';						
									}
							}
						}
						?>
				  </div>
                </div>

                <div class="form-group">
                  <label id="labelimp" class="labelimp">Type: </label>
                  <label>
                  <input type="radio" name="type" value="1"<?php echo ($type == 1 ? ' checked="checked"' : ''); ?>>
                  Percentage</label>
                  <label>
                  <input type="radio" name="type" value="0"<?php echo ($type == 0 ? ' checked="checked"' : ''); ?>>
                  Amount</label>
                </div>

                <div class="form-group">
                  <label id="labelimp" class="labelimp" for="amount">Discount Offering: </label>
                  <?php 
				echo '<input type="number" id="amount" name="amount" class="form-control"  value="'.(isset($_POST["amount"]) ? $_POST["amount"] : "").'" />';
				?>
                </div>

                <div class="form-group">
                  <label id="labelimp" class="labelimp" for="expire">Expires After: </label>
                  <?php 
				echo '<input type="date" id="expire" name="expire" class="form-control"  value="'.(isset($_POST["expire"]) ? $_POST["expire"] : "").'" />';
				?>
                </div>
				
				
				
				
                <input type="hidden" name="action" value="submit_form" />
              </div>
              <!-- /.box-body -->
            
            </div>
            <!-- /.box -->
            <!-- Form Element sizes -->
          </div>
        </div>
          
      </section>
    </form>
    <!-- /.content -->
  </aside>
  <!-- /.right-side -->
</div>




<!-- ./wrapper -->
<?php include_once("Footer.php"); ?>
<!-- add new calendar event modal -->
<!-- jQuery 2.0.2 -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<!-- jQuery UI 1.10.3 -->
<script src="js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="js/AdminLTE/app.js" type="text/javascript"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="js/AdminLTE/dashboard.js" type="text/javascript"></script>
<script>
    var expanded = false;
    function showCheckboxes() {
        var checkboxes = document.getElementById("checkboxes");
        if (!expanded) {
            checkboxes.style.display = "block";
            expanded = true;
        } else {
            checkboxes.style.display = "none";
            expanded = false;
        }
    }
	</script>
</body>
</html>
