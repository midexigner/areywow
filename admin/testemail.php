<?PHP

// put your api key here  note the ending text past the - this is your datacenter 
// the datacenter needs to be added into to the url in the curlopt_url (see below)
$apikey = "909c002857c9727bc434317e04ec0019-us14"; // my datacenter was "us11"


// listid goes here - to find this... log into mail chimp go to Lists menu , 
// look to far right of list name for a drop down arrow, select the "Settings" dropdown,
// scroll to bottom and look  for  "Unique id for list"
$list_id = "xxxxxxxxxx"; // web site list


// the data I used to register (there may be others you can use, check API docs)
$email = "<<email_address_to_register>>";
$fname = "<<first_name>>";
$lname = "<<last_name>>";


$auth = base64_encode( 'user:'.$apikey );


// Notice the value of 'status' is 'pending'  
// I found this via a google search indicating a double opt in subscription process 

$data = array(
'apikey'        => $apikey,
'email_address' => $email,
'status'        => 'pending',
'merge_fields'  => array(
'FNAME' => $fname,
'LNAME' => $lname,
)
);
$json_data = json_encode($data);

$ch = curl_init();

// notice datacenter  "us11" comes after the // - make sure you update this to your datacenter (e.g. us2, us7 etc) or you'll get the "wrong datacenter" error.
$curlopt_url = "https://us14.api.mailchimp.com/3.0/lists/$list_id/members/";
curl_setopt($ch, CURLOPT_URL, $curlopt_url);

curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
    'Authorization: Basic '.$auth));
curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/3.0');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_TIMEOUT, 10);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);

$result = curl_exec($ch);
/*
// some debug statements 
var_dump($result);
print_r ($result);
*/


// here is simple way to determine status of a subscription
// $result is in JSON format
// this following loop is a simple JSON decode loop I found via google


 $status = "undefined";
    $msg = "unknown error occurred";
$myArray = json_decode($result, true);

foreach($myArray as $key => $value)
{

    // debug key<<< = >>>$value<<< <br>";

    if( $key == "status" )
    {
        $status=$value;
        //debug                 echo" status found $status<Br>";
    }
    else if ($key == "title")
    {
        $msg=$value;
        //debug                 echo" title found $msg<Br>";
    }


}

// create the output that gets displayed or returned if invoked by AJAX method
if( $status == "pending" )
{
    $msg = "Success! <br>$email has been subscribed <Br>check your inbox for the confirmation email to complete your subscription";
}
else
{
    $msg = "Sorry can not subscribe email $email <br>$msg <Br>";
}


echo "$msg <br>";


die(' '); // frees up mem etc..

?>
