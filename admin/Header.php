<header class="header">
            <a href="dashboard.php" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                <img src="images/logo.png" style="width:100%;" />
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <!-- Messages: style can be found in dropdown.less-->
                          <li class="dropdown tasks-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                
                               <!-- font Awesome -->
                                        <small id="clockdisp"> <?php 
$h = "5";
$hm = $h * 60; 
$ms = $hm * 60;
$gmdate = gmdate("l jS M Y", time()+($ms)); // the "-" can be switched to a plus if that's what your time zone is.
echo $gmdate;
?> </small>
                            </a>
                            
                        </li>
					   <li class="dropdown messages-menu">
							<?php 
							$query="SELECT pr.ID,pr.Name,pr.Review,pr.Ratings,p.Image,p.ProductName,DATE_FORMAT(pr.DateAdded, '%D %b %Y') AS Added FROM product_reviews pr LEFT JOIN products p ON pr.ProductID = p.ID WHERE pr.ID <>0 AND pr.Status=0";
							$result = mysql_query ($query) or die(mysql_error()); 
							$num = mysql_num_rows($result);
							?>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-envelope"></i>
                                <span class="label label-success"><?php echo $num; ?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">You have <?php echo $num; ?> Product Reviews</li>
                                <li>
                                   
                                    <ul class="menu">
                                        
										<?php
										if($num == 0)
										{
											echo '&ensp;Currently not available any review.';
										}
										while($row = mysql_fetch_array($result,MYSQL_ASSOC))
										{
										$Image=explode(',', $row["Image"]);
										$img1 = $Image[0];
										?>
										<li style="overflow:scroll;">  
                                            <a>
                                                <div class="pull-left">
                                                    <img src="<?php echo DIR_PRODUCTS_IMAGES.$img1 ?>" class="img-circle" alt=""/>
                                                </div>
                                                <h4>
													<p>Product: <?php echo $row["ProductName"]; ?></p>
                                                    <?php echo $row["Name"]; ?> (<?php echo $row["Ratings"]; ?> Stars)
                                                    <small><i class="fa fa-clock-o"></i> <?php echo $row["Added"]; ?></small>
                                                </h4>
                                                <p><?php echo $row["Review"]; ?></p>
												<br>
												<button onClick="location.href='ApproveReview.php?ID=<?php echo $row["ID"]; ?>'" class="btn btn-success btn-sm">Approve</button>
												<button onClick="location.href='DeleteReview.php?ID=<?php echo $row["ID"]; ?>'" class="btn btn-danger btn-sm">Denied</button>
                                            </a>
										</li> 
										<?php
										}
										?>
                                    </ul>
                                </li>
                                <li class="footer"><a href="#">Product Reviews</a></li>
                            </ul>
                        </li>
                        
                        <!--<li class="dropdown notifications-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-warning"></i>
                                <span class="label label-warning">10</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">You have 10 notifications</li>
                                <li>
                                   
                                    <ul class="menu">
                                        <li>
                                            <a href="#">
                                                <i class="ion ion-ios7-people info"></i> 5 new members joined today
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-warning danger"></i> Very long description here that may not fit into the page and may cause design problems
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-users warning"></i> 5 new members joined
                                            </a>
                                        </li>

                                        <li>
                                            <a href="#">
                                                <i class="ion ion-ios7-cart success"></i> 25 sales made
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="ion ion-ios7-person danger"></i> You changed your username
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="footer"><a href="#">View all</a></li>
                            </ul>
                        </li>-->
                        
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <!-- <i class="glyphicon glyphicon-user"></i> -->
                                <div class="user-image">
                                    <img src="images/img/avatar2.png" alt="" class="img-responsive img-circle">
                                </div>
                                <span><?php echo $_SESSION["UserFullName"]; ?> <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">
                                    <img src="images/img/avatar2.png" class="img-circle" alt="User Image" />
                                    <p>
                                         <?php echo $_SESSION["UserFullName"]; ?><br>
										 <small>
										 <?php
										 if($_SESSION["Admin"]==true)
										 {
											echo 'Administrator';
										 }
										 else
										 {
											$query = "SELECT Role from roles WHERE ID=".$_SESSION["RoleID"];
											$role = mysql_query($query) or die('Line 6: '.mysql_error());
											$row = mysql_fetch_array($role);
											echo $row['Role'];
										 }
										 ?>
										 </small>
                                        <small>Member since <?php echo $_SESSION["JoinDate"]; ?></small>
                                    </p>
                                </li>
                                <!-- Menu Body 
                                <li class="user-body">
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Followers</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Sales</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Friends</a>
                                    </div>
                                </li>-->
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="LockNow.php" class="btn btn-default btn-flat">Lock Screen</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="Logout.php" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>