<?php
include_once("Common.php");
include("CheckAdminLogin.php");
	
	$msg = "";
	if (isset($_REQUEST['dID'])) {
		$ss = "SELECT File FROM career WHERE ID = '".$_REQUEST['dID']."'";
		$r = mysql_query($ss);
		$t = mysql_fetch_assoc($r);
		unlink('../'.$t['File']);

		$s = "DELETE FROM career WHERE ID = '".$_REQUEST['dID']."'";
		mysql_query($s) or die(mysql_error());
	}

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Careers</title>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<!-- bootstrap 3.0.2 -->
<script language="javascript" src="scripts/innovaeditor.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="js/local_clock.js" type="text/javascript"></script>
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Ionicons -->
<link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
<!-- Theme style -->
<link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
</head>
<body class="skin-blue">
<!-- header logo: style can be found in header.less -->
<?php
	include_once("Header.php");
?>
<div class="wrapper row-offcanvas row-offcanvas-left">
  <!-- Left side column. contains the logo and sidebar -->
  <?php
	include_once("Sidebar.php");
?>
  <!-- Right side column. Contains the navbar and content of the page -->
  <aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Careers</h1>
      <ol class="breadcrumb">
        <li><a href="career.php"><i class="fa fa-dashboard"></i>Careers</a></li>
        <li class="active">Careers</li>
      </ol>
    </section>
    <!-- Main content -->
      <section class="content">
        <div class="box-footer" style="text-align:right;">
        </div>
        <?php
			  	echo $msg;
				if(isset($_SESSION["msg"]))
				{
					echo $_SESSION["msg"];
					$_SESSION["msg"]="";
				}
				?>
        <div class="col-md-12">
          <div class="box">
            <!-- general form elements -->
            <div style="padding:15px;" class="box-primary">
              <!-- form start -->
              <div class="box-body" style="min-width: 80% !important;">
	              <div class="">
					  <table class="table table-bordered table-responsive">
					  <thead>
					      <tr>
					        <th>Name</th>
					        <th>Phone</th>
					        <th>Email</th>
					        <th>City</th>
					        <th>Address</th>
					        <th>CNIC</th>
					        <th>Position</th>
					        <th>CV</th>
					        <th>Actions</th>
					      </tr>
					    </thead>
					    <tbody>
					    <?php $query="SELECT * FROM career ORDER BY ID DESC";
	$result = mysql_query($query) or die(mysql_error()); 
	while ($row = mysql_fetch_assoc($result)) { ?>
					      <tr>
					        <td><?php echo $row['Name']; ?></td>
					        <td> <?php echo $row['phone']; ?> </td>
					        <td><?php echo $row['email']; ?></td>
					        <td><?php echo $row['city']; ?></td>
					        <td><?php echo $row['address']; ?></td>
					        <td><?php echo $row['cnic']; ?></td>
					        <td><?php echo $row['Position']; ?></td>
					        <td><a class="btn btn-success" href="../<?php echo $row['File'] ?>" download>Download</a></td>
					        <td><a class="btn btn-danger" href="<?php echo $_SERVER['PHP_SELF']; ?>?dID=<?php echo $row['ID']; ?>">Delete</a></td>
					      </tr>
					      <?php } ?>
					    </tbody>
					  </table>
					</div>
	              </div>
              
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
            <!-- Form Element sizes -->
          </div>
        </div>
      </section>
    </form>
    <!-- /.content -->
  </aside>
  <!-- /.right-side -->
</div>
<!-- ./wrapper -->
<?php include_once("Footer.php"); ?>
<!-- add new calendar event modal -->
<!-- jQuery 2.0.2 -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<!-- jQuery UI 1.10.3 -->
<script src="js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="js/AdminLTE/app.js" type="text/javascript"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="js/AdminLTE/dashboard.js" type="text/javascript"></script>
</body>
</html>