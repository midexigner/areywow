<?php
include_once("Common.php");
include("CheckAdminLogin.php");
	
	$msg="";
	$ID=0;
	$Name='';
	$Amount = 0;
	$SFree=0;
	$SFreeAfter=0;
	
	$ID=0;
	if(isset($_REQUEST["ID"]) && ctype_digit(trim($_REQUEST["ID"])))
		$ID=trim($_REQUEST["ID"]);
	
		
	
if(isset($_POST["action"]) && $_POST["action"] == "submit_form")
{	
	
	if(isset($_REQUEST["ID"]) && ctype_digit(trim($_REQUEST["ID"])))
		$ID=trim($_REQUEST["ID"]);		
	if(isset($_POST["Name"]))
		$Name=trim($_POST["Name"]);
	if(isset($_POST["Amount"]))
		$Amount=trim($_POST["Amount"]);
	if(isset($_POST["SFree"]))
		$SFree=trim($_POST["SFree"]);
	if(isset($_POST["SFreeAfter"]))
		$SFreeAfter=trim($_POST["SFreeAfter"]);
	
	
	if($msg=="")
	{
		
		{
			$query="UPDATE promotions SET Amount='".(int)$Amount . "',
			Name = '" . dbinput($Name) . "',
			SFree='".$SFree."',
			SFreeAfter='".$SFreeAfter."'";
			mysql_query($query) or die ('Could not update promotion because: ' . mysql_error());
			//echo $query;
			
			$msg='<div class="alert alert-success alert-dismissable">
				<i class="fa fa-ban"></i>
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<b>Updated.</b>
			</div>';
		
		}
			
	}

}
else
{
	$query="SELECT ID,Amount, Name, SFree, SFreeAfter FROM promotions";
	
	$result = mysql_query ($query) or die("Could not select promotion because: ".mysql_error()); 
	$num = mysql_num_rows($result);
	
	if($num==0)
	{
		$_SESSION["msg"]='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Invalid promotion ID.</b>
		</div>';
	}
	else
	{
		$row = mysql_fetch_array($result,MYSQL_ASSOC);
		
		$ID=$row["ID"];
		$Name=$row["Name"];
		$Amount=$row["Amount"];
		$SFree=$row['SFree'];
		$SFreeAfter=$row['SFreeAfter'];
	}
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Edit Shipping information</title>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<!-- bootstrap 3.0.2 -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="js/local_clock.js" type="text/javascript"></script>
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Ionicons -->
<link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
<!-- Theme style -->
<link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
<style>
#footer {
	width:100%;
	height:50px;
	background-color:#3c8dbc;
	text-align:center;
	vertical-align:center;
	padding-top:15px;
}
#labelimp {
	background-color: rgba(60, 141, 188, 0.19);
	padding: 4px;
	font-size: 20px;
	width: 100%;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	padding-left: 5px;
}
</style>
</head>
<body class="skin-blue">
<!-- header logo: style can be found in header.less -->
<?php
	include_once("Header.php");
?>
<div class="wrapper row-offcanvas row-offcanvas-left">
  <!-- Left side column. contains the logo and sidebar -->
  <?php
	include_once("Sidebar.php");
?>
  <!-- Right side column. Contains the navbar and content of the page -->
  <aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Edit Shipping information</h1>
      <ol class="breadcrumb">
        <li><a href="Shipping informations.php"><i class="fa fa-dashboard"></i>Shipping information</a></li>
        <li class="active">Edit Shipping information</li>
      </ol>
    </section>
    <!-- Main content -->
    <form role="form" action="<?php echo $_SERVER["PHP_SELF"];?>?ID=<?php echo $ID; ?>"" method="post" enctype="multipart/form-data" name="frmPage">
      <input type="hidden" name="action" value="submit_form" />
      <input type="hidden" name="ID" value="<?php echo $ID; ?>" />
      <section class="content">
        <div class="box-footer" style="text-align:right;">
          <button type="submit" class="btn btn-success margin">Save</button>
          <button class="btn btn-primary margin" type="button" onClick="location.href='dashboard.php'">Cancel</button>
        </div>
        <?php
			  	echo $msg;
				if(isset($_SESSION["msg"]))
				{
					echo $_SESSION["msg"];
					$_SESSION["msg"]="";
				}
				?>
        
<h3 style="margin-left: 10px; margin-top: 10px;">Shipping Offers: </h3>
        <div class="col-md-6">
          <div class="box" style="padding: 20px;">
            <!-- general form elements -->
            		<label style="margin-right: 5px;">Shipping Free? </label>
                       <input value="0" type="radio" name="SFree" <?php echo ($SFree==0)?'checked':''; ?> > No 
                       <input value="1" type="radio" name="SFree" <?php echo ($SFree==1)?'checked':''; ?> > Yes
                        <span class="input-group-btn">
                        </span>
              </div>
              <!-- /.box-body -->
            </div>
            <div class="col-md-6">
          <div class="box" style="padding: 20px;">
            <!-- general form elements -->
            		<label style="margin-right: 5px;">Shipping Free On order above: </label>
                       <input value="<?php echo $SFreeAfter; ?>" type="number" name="SFreeAfter" >
                        <span class="input-group-btn">
                        </span>
                    </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
            <!-- Form Element sizes -->
          </div>
        </div>
		
      </section>
    </form>
    <!-- /.content -->
  </aside>
  <!-- /.right-side -->
</div>
<!-- ./wrapper -->
<?php include_once("Footer.php"); ?>
<!-- add new calendar event modal -->
<!-- jQuery 2.0.2 -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<!-- jQuery UI 1.10.3 -->
<script src="js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="js/AdminLTE/app.js" type="text/javascript"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="js/AdminLTE/dashboard.js" type="text/javascript"></script>
</body>
</html>
