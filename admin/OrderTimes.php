<?php include("Common.php"); ?>
<?php include("CheckAdminLogin.php");?>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Order Timings</title>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<!-- bootstrap 3.0.2 -->
<!-- jQuery 2.0.2 -->



<!-- <script language="javascript" src="js/local_clock.js" type="text/javascript"></script>
<script language="javascript" src="../js/functions.js" type="text/javascript"></script> -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- font Awesome -->
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Ionicons -->
<link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />

<!-- DATA TABLES -->
<link href="css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- Theme style -->
<link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />


</head>
<body class="skin-blue">
<!-- header logo: style can be found in header.less -->
<?php
	include_once("Header.php");
?>
<div class="wrapper row-offcanvas row-offcanvas-left">
  <!-- Left side column. contains the logo and sidebar -->
  <?php
		include_once("Sidebar.php");
		?>
  <!-- Right side column. Contains the navbar and content of the page -->
  <aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> Order Timings <small></small> </h1>
      <ol class="breadcrumb">
        <li><a href="Dashboard.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Order Timings</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Order Timings</h3>
			 
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
             	
             	<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
              <table class="table table-bordered table-striped" style="margin-bottom:20px;">
              	
              		<tr>
                     <th>
						<div class="form-group">
							<label>Starting Date:</label>
							<div class="input-group date">
							<div class="input-group-addon">
							<i class="fa fa-calendar"></i>
							</div>
							<input type="text" placeholder="mm-dd-yy" name="date-1" class="form-control datepicker" value="<?php echo $_POST['date-1']; ?>" id="datepicker1" required>

							</div>
							<!-- /.input group -->
						</div>
                     </th>
              <th>
              	<div class="form-group">
                <label>Ending Date:</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" placeholder="mm-dd-yy"  name="date-2" class="form-control datepicker" value="<?php echo $_POST['date-2']; ?>" id="datepicker2" required>
                </div>
                <!-- /.input group -->
              </div>
              </th>
              <th style="vertical-align: middle !important; text-align: center;">
              	<input type="submit" name="datesubmit" value="Filter Results" class="btn btn-large btn-success" >
              </th>
                    </tr>
              	
              </table>
              </form>
              <?php 

				if (isset($_POST['datesubmit'])) {
                $date1 = $_POST['date-1'];
                $date2 = $_POST['date-2'];
                $date1 = date("Y-m-d", strtotime($date1));
                $date2 = date("Y-m-d", strtotime($date2));
              	
					if ($date1>$date2) {
						echo "<div><h3>Starting date should be less than ending date!</h3></div>";
					}
					else{	?> 
              
                <table id="dataTable" class="table table-bordered table-striped">
                  <thead>
                    
                  </thead>
				  
                  <tbody>
                  <tr>
                  	<th>Number Of Orders</th>
					  <th>TIME</th>

                  </tr>
                    <?php 
$sql = "SELECT COUNT(ID) FROM orders WHERE HOUR(DateAdded) BETWEEN '00' AND '01' AND DATE(DateAdded) BETWEEN '$date1' AND '$date2'";
$query = mysql_query($sql);
$result = mysql_fetch_array($query);
 
?>

<tr>
	
	<td><?php echo $result[0]; ?></td>
	<td> 12 am and 01 am</td>

	</td>

</tr>

<tr>
	                    <?php 
$sql = "SELECT COUNT(ID) FROM orders WHERE HOUR(DateAdded) BETWEEN '01' AND '02' AND DATE(DateAdded) BETWEEN '$date1' AND '$date2'";
$query = mysql_query($sql);
$result = mysql_fetch_array($query);
 
?>
	<td><?php echo $result[0]; ?></td>
	<td> 01 am and 02 am</td>

	</td>

</tr>

<tr>
	                    <?php 
$sql = "SELECT COUNT(ID) FROM orders WHERE HOUR(DateAdded) BETWEEN '02' AND '03' AND DATE(DateAdded) BETWEEN '$date1' AND '$date2'";
$query = mysql_query($sql);
$result = mysql_fetch_array($query);
 
?>
	<td><?php echo $result[0]; ?></td>
	<td> 02 am and 03 am</td>

	</td>

</tr>

<tr>
	                    <?php 
$sql = "SELECT COUNT(ID) FROM orders WHERE HOUR(DateAdded) BETWEEN '03' AND '04' AND DATE(DateAdded) BETWEEN '$date1' AND '$date2'";
$query = mysql_query($sql);
$result = mysql_fetch_array($query);
 
?>
	<td><?php echo $result[0]; ?></td>
	<td> 03 am and 04 am</td>

	</td>

</tr>

<tr>
<?php 
$sql = "SELECT COUNT(ID) FROM orders WHERE HOUR(DateAdded) BETWEEN '04' AND '05' AND DATE(DateAdded) BETWEEN '$date1' AND '$date2'";
$query = mysql_query($sql);
$result = mysql_fetch_array($query);
 
?>
	<td><?php echo $result[0]; ?></td>
	<td> 04 am and 05 am</td>

	</td>

</tr>
	                    <?php 
$sql = "SELECT COUNT(ID) FROM orders WHERE HOUR(DateAdded) BETWEEN '05' AND '06' AND DATE(DateAdded) BETWEEN '$date1' AND '$date2'";
$query = mysql_query($sql);
$result = mysql_fetch_array($query);
 
?>
<tr>
	
	<td><?php echo $result[0]; ?></td>
	<td> 05 am and 06 am</td>

	</td>

</tr>
	                    <?php 
$sql = "SELECT COUNT(ID) FROM orders WHERE HOUR(DateAdded) BETWEEN '06' AND '07' AND DATE(DateAdded) BETWEEN '$date1' AND '$date2'";
$query = mysql_query($sql);
$result = mysql_fetch_array($query);
 
?>
<tr>
	
	<td><?php echo $result[0]; ?></td>
	<td> 06 am and 07 am</td>

	</td>

</tr>

<tr>
		                    <?php 
$sql = "SELECT COUNT(ID) FROM orders WHERE HOUR(DateAdded) BETWEEN '07' AND '08' AND DATE(DateAdded) BETWEEN '$date1' AND '$date2'";
$query = mysql_query($sql);
$result = mysql_fetch_array($query);
 
?>
	<td><?php echo $result[0]; ?></td>
	<td> 07 am and 08 am</td>

	</td>

</tr>

<tr>
		                    <?php 
$sql = "SELECT COUNT(ID) FROM orders WHERE HOUR(DateAdded) BETWEEN '08' AND '09' AND DATE(DateAdded) BETWEEN '$date1' AND '$date2'";
$query = mysql_query($sql);
$result = mysql_fetch_array($query);
 
?>
	<td><?php echo $result[0]; ?></td>
	<td> 08 am and 09 am</td>

	</td>

</tr>
<tr>
		                    <?php 
$sql = "SELECT COUNT(ID) FROM orders WHERE HOUR(DateAdded) BETWEEN '09' AND '10' AND DATE(DateAdded) BETWEEN '$date1' AND '$date2'";
$query = mysql_query($sql);
$result = mysql_fetch_array($query);
 
?>
	<td><?php echo $result[0]; ?></td>
	<td> 09 am and 10 am</td>

	</td>

</tr>
<tr>
		                    <?php 
$sql = "SELECT COUNT(ID) FROM orders WHERE HOUR(DateAdded) BETWEEN '10' AND '11' AND DATE(DateAdded) BETWEEN '$date1' AND '$date2'";
$query = mysql_query($sql);
$result = mysql_fetch_array($query);
 
?>
	<td><?php echo $result[0]; ?></td>
	<td> 10 am and 11 am</td>

	</td>

</tr>
<tr>
	<?php 
$sql = "SELECT COUNT(ID) FROM orders WHERE HOUR(DateAdded) BETWEEN '11' AND '12' AND DATE(DateAdded) BETWEEN '$date1' AND '$date2'";
$query = mysql_query($sql);
$result = mysql_fetch_array($query);
 
?>
	<td><?php echo $result[0]; ?></td>
	<td> 11 am and 12 pm</td>

	</td>

</tr>
<tr>
	<?php 
$sql = "SELECT COUNT(ID) FROM orders WHERE HOUR(DateAdded) BETWEEN '12' AND '13' AND DATE(DateAdded) BETWEEN '$date1' AND '$date2'";
$query = mysql_query($sql);
$result = mysql_fetch_array($query);
 
?>
	<td><?php echo $result[0]; ?></td>
	<td> 12 pm and 01 pm</td>

	</td>

</tr>
<tr>
	<?php 
$sql = "SELECT COUNT(ID) FROM orders WHERE HOUR(DateAdded) BETWEEN '13' AND '14' AND DATE(DateAdded) BETWEEN '$date1' AND '$date2'";
$query = mysql_query($sql);
$result = mysql_fetch_array($query);
 
?>
	<td><?php echo $result[0]; ?></td>
	<td> 01 pm and 02 pm</td>

	</td>

</tr>
<tr>
	<?php 
$sql = "SELECT COUNT(ID) FROM orders WHERE HOUR(DateAdded) BETWEEN '14' AND '15' AND DATE(DateAdded) BETWEEN '$date1' AND '$date2'";
$query = mysql_query($sql);
$result = mysql_fetch_array($query);
 
?>
	<td><?php echo $result[0]; ?></td>
	<td> 02 pm and 03 pm</td>

	</td>

</tr>
<tr>
	<?php 
$sql = "SELECT COUNT(ID) FROM orders WHERE HOUR(DateAdded) BETWEEN '15' AND '16' AND DATE(DateAdded) BETWEEN '$date1' AND '$date2'";
$query = mysql_query($sql);
$result = mysql_fetch_array($query);
 
?>
	<td><?php echo $result[0]; ?></td>
	<td> 03 pm and 04 pm</td>

	</td>

</tr>
<tr>
	<?php 
$sql = "SELECT COUNT(ID) FROM orders WHERE HOUR(DateAdded) BETWEEN '16' AND '17' AND DATE(DateAdded) BETWEEN '$date1' AND '$date2'";
$query = mysql_query($sql);
$result = mysql_fetch_array($query);
 
?>
	<td><?php echo $result[0]; ?></td>
	<td> 04 pm and 05 pm</td>

	</td>

</tr>
<tr>
	<?php 
$sql = "SELECT COUNT(ID) FROM orders WHERE HOUR(DateAdded) BETWEEN '17' AND '18' AND DATE(DateAdded) BETWEEN '$date1' AND '$date2'";
$query = mysql_query($sql);
$result = mysql_fetch_array($query);
 
?>
	<td><?php echo $result[0]; ?></td>
	<td> 05 pm and 06 pm</td>

	</td>

</tr>
<tr>
	<?php 
$sql = "SELECT COUNT(ID) FROM orders WHERE HOUR(DateAdded) BETWEEN '18' AND '19' AND DATE(DateAdded) BETWEEN '$date1' AND '$date2'";
$query = mysql_query($sql);
$result = mysql_fetch_array($query);
 
?>
	<td><?php echo $result[0]; ?></td>
	<td> 06 pm and 07 pm</td>

	</td>

</tr>
<?php 
$sql = "SELECT COUNT(ID) FROM orders WHERE HOUR(DateAdded) BETWEEN '19' AND '20' AND DATE(DateAdded) BETWEEN '$date1' AND '$date2'";
$query = mysql_query($sql);
$result = mysql_fetch_array($query);
 
?>
<tr>
	
	<td><?php echo $result[0]; ?></td>
	<td> 07 pm and 08 pm</td>

	</td>

</tr>
<?php 
$sql = "SELECT COUNT(ID) FROM orders WHERE HOUR(DateAdded) BETWEEN '20' AND '21' AND DATE(DateAdded) BETWEEN '$date1' AND '$date2'";
$query = mysql_query($sql);
$result = mysql_fetch_array($query);
 
?>
<tr>
	
	<td><?php echo $result[0]; ?></td>
	<td> 08 pm and 09 pm</td>

	</td>

</tr>
<?php 
$sql = "SELECT COUNT(ID) FROM orders WHERE HOUR(DateAdded) BETWEEN '21' AND '22' AND DATE(DateAdded) BETWEEN '$date1' AND '$date2'";
$query = mysql_query($sql);
$result = mysql_fetch_array($query);
 
?>
<tr>
	
	<td><?php echo $result[0]; ?></td>
	<td> 09 pm and 10 pm</td>

	</td>

</tr>
<?php 
$sql = "SELECT COUNT(ID) FROM orders WHERE HOUR(DateAdded) BETWEEN '22' AND '23' AND DATE(DateAdded) BETWEEN '$date1' AND '$date2'";
$query = mysql_query($sql);
$result = mysql_fetch_array($query);
 
?>
<tr>
	
	<td><?php echo $result[0]; ?></td>
	<td> 10 pm and 11 pm</td>

	</td>

</tr>
<?php 
$sql = "SELECT COUNT(ID) FROM orders WHERE HOUR(DateAdded) BETWEEN '23' AND '00' AND DATE(DateAdded) BETWEEN '$date1' AND '$date2'";
$query = mysql_query($sql);
$result = mysql_fetch_array($query);
 
?>
<tr>
	
	<td><?php echo $result[0]; ?></td>
	<td> 11 pm and 12 am</td>

	</td>

</tr>
                  </tbody>
                </table>
              </form>
              <?php  }

                } 

               ?>
            </div>
         
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
    </section>
    <!-- /.content -->
  </aside>
  <!-- /.right-side -->
</div>

<?php include_once("Footer.php"); ?>
<!-- ./wrapper -->
<!-- Bootstrap -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>

<!-- DATA TABES SCRIPT -->
<script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>




	

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
$( function() {
$( "#datepicker1").datepicker();
$( "#datepicker2").datepicker();
} );
</script>
<!-- AdminLTE App -->
<script src="js/AdminLTE/app.js" type="text/javascript"></script>
<!-- page script -->
</body>
</html>
