<?php
include_once("Common.php");
include("CheckAdminLogin.php");



	$msg="";
	$ID=0;
	$Percentage=0;
	$Country='XXX';
		
if(isset($_POST["action"]) && $_POST["action"] == "submit_form")
{	
	if(isset($_POST["Country"]))
		$Country=trim($_POST["Country"]);
	if(isset($_POST["Percentage"]) && ((int)$_POST["Percentage"] >= 0 || (int)$_POST["Percentage"] <= 100) && ctype_digit($_POST['Percentage']))
		$Percentage=trim($_POST["Percentage"]);

	
		

	
		if($Country == "XXX")
		{
			$msg='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Please Select Country.</b>
			</div>';
		}
		else if($Percentage < 0)
		{
			$msg='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Please Enter in Positive.</b>
			</div>';
		}

		
			


	if($msg=="")
	{

	
		$query="INSERT INTO shipping_rates SET Percentage='".(int)$Percentage . "', DateAdded=NOW(),
				CountryCode = '" . dbinput($Country) . "'";
		mysql_query($query) or die (mysql_error());
		// echo $query;
		// $ID = mysql_insert_id();
		$_SESSION["msg"]='<div class="alert alert-success alert-dismissable">
		<i class="fa fa-ban"></i>
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<b>Shipping Rate has been added.</b>
		</div>';		

		redirect("AddNewShippingRate.php");	
	}
		

}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Add Shipping Rate</title>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<!-- bootstrap 3.0.2 -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="js/local_clock.js" type="text/javascript"></script>
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Ionicons -->
<link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
<!-- Theme style -->
<link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
<style>
#footer {
	width:100%;
	height:50px;
	background-color:#3c8dbc;
	text-align:center;
	vertical-align:center;
	padding-top:15px;
}
#labelimp {
	background-color: rgba(60, 141, 188, 0.19);
	padding: 4px;
	font-size: 20px;
}
#labelimp {
	background-color: rgba(60, 141, 188, 0.19);
	padding: 4px;
	font-size: 20px;
	width: 100%;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	padding-left: 5px;
}
</style>
</head>
<body class="skin-blue">
<!-- header logo: style can be found in header.less -->
<?php
		include_once("Header.php");
		?>
<div class="wrapper row-offcanvas row-offcanvas-left">
  <!-- Left side column. contains the logo and sidebar -->
  <?php
	include_once("Sidebar.php");
?>
  <!-- Right side column. Contains the navbar and content of the page -->
  <aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Add Shipping Rate </h1>
      <ol class="breadcrumb">
        <li><a href="ShippingRates.php"><i class="fa fa-dashboard"></i>Shipping Rates</a></li>
        <li class="active">Add Shipping Rate</li>
      </ol>
    </section>
    <!-- Main content -->
    <form role="form" action="<?php echo $_SERVER["PHP_SELF"];?>" method="post" enctype="multipart/form-data" name="frmPage">
      <section class="content">
       <div class="box-footer" style="text-align:right;">
                <button type="submit" class="btn btn-success margin">Save</button>
                <button class="btn btn-primary margin" type="button" onClick="location.href='ShippingRates.php'">Cancel</button>
            </div>
              <?php
		  		echo $msg;
				if(isset($_SESSION["msg"]))
				{
					echo $_SESSION["msg"];
					$_SESSION["msg"]="";
				}
				?>
        <div class="col-md-6">
          <div class="box">
          
            <!-- general form elements -->
            <div style="padding:15px;" class="box-primary">
            
              <!-- form start -->
              <div class="box-body">
               
                <div class="form-group">
                  <label id="labelimp" for="Country" >Country: </label>
                  
				  <select name="Country" id="Country" class="form-control">
					<option <?php echo ($Country == 'XXX' ? 'selected' : ''); ?> value="XXX">Select Country</option>
					<option <?php echo ($Country == 'AFG' ? 'selected' : ''); ?> value="AFG">Afghanistan</option>
					<option <?php echo ($Country == 'ALA' ? 'selected' : ''); ?> value="ALA">Aland Islands</option>
					<option <?php echo ($Country == 'ALB' ? 'selected' : ''); ?> value="ALB">Albania</option>
					<option <?php echo ($Country == 'DZA' ? 'selected' : ''); ?> value="DZA">Algeria</option>
					<option <?php echo ($Country == 'ASM' ? 'selected' : ''); ?> value="ASM">American Samoa</option>
					<option <?php echo ($Country == 'AND' ? 'selected' : ''); ?> value="AND">Andorra</option>
					<option <?php echo ($Country == 'AGO' ? 'selected' : ''); ?> value="AGO">Angola</option>
					<option <?php echo ($Country == 'AIA' ? 'selected' : ''); ?> value="AIA">Anguilla</option>
					<option <?php echo ($Country == 'ATA' ? 'selected' : ''); ?> value="ATA">Antarctica</option>
					<option <?php echo ($Country == 'ATG' ? 'selected' : ''); ?> value="ATG">Antigua and Barbuda</option>
					<option <?php echo ($Country == 'ARG' ? 'selected' : ''); ?> value="ARG">Argentina</option>
					<option <?php echo ($Country == 'ARM' ? 'selected' : ''); ?> value="ARM">Armenia</option>
					<option <?php echo ($Country == 'ABW' ? 'selected' : ''); ?> value="ABW">Aruba</option>
					<option <?php echo ($Country == 'AUS' ? 'selected' : ''); ?> value="AUS">Australia</option>
					<option <?php echo ($Country == 'AUT' ? 'selected' : ''); ?> value="AUT">Austria</option>
					<option <?php echo ($Country == 'AZE' ? 'selected' : ''); ?> value="AZE">Azerbaijan</option>
					<option <?php echo ($Country == 'BHS' ? 'selected' : ''); ?> value="BHS">Bahamas</option>
					<option <?php echo ($Country == 'BHR' ? 'selected' : ''); ?> value="BHR">Bahrain</option>
					<option <?php echo ($Country == 'BGD' ? 'selected' : ''); ?> value="BGD">Bangladesh</option>
					<option <?php echo ($Country == 'BRB' ? 'selected' : ''); ?> value="BRB">Barbados</option>
					<option <?php echo ($Country == 'BLR' ? 'selected' : ''); ?> value="BLR">Belarus</option>
					<option <?php echo ($Country == 'BEL' ? 'selected' : ''); ?> value="BEL">Belgium</option>
					<option <?php echo ($Country == 'BLZ' ? 'selected' : ''); ?> value="BLZ">Belize</option>
					<option <?php echo ($Country == 'BEN' ? 'selected' : ''); ?> value="BEN">Benin</option>
					<option <?php echo ($Country == 'BMU' ? 'selected' : ''); ?> value="BMU">Bermuda</option>
					<option <?php echo ($Country == 'BTN' ? 'selected' : ''); ?> value="BTN">Bhutan</option>
					<option <?php echo ($Country == 'BOL' ? 'selected' : ''); ?> value="BOL">Bolivia, Plurinational State of</option>
					<option <?php echo ($Country == 'BES' ? 'selected' : ''); ?> value="BES">Bonaire, Sint Eustatius and Saba</option>
					<option <?php echo ($Country == 'BIH' ? 'selected' : ''); ?> value="BIH">Bosnia and Herzegovina</option>
					<option <?php echo ($Country == 'BWA' ? 'selected' : ''); ?> value="BWA">Botswana</option>
					<option <?php echo ($Country == 'BVT' ? 'selected' : ''); ?> value="BVT">Bouvet Island</option>
					<option <?php echo ($Country == 'BRA' ? 'selected' : ''); ?> value="BRA">Brazil</option>
					<option <?php echo ($Country == 'IOT' ? 'selected' : ''); ?> value="IOT">British Indian Ocean Territory</option>
					<option <?php echo ($Country == 'BRN' ? 'selected' : ''); ?> value="BRN">Brunei Darussalam</option>
					<option <?php echo ($Country == 'BGR' ? 'selected' : ''); ?> value="BGR">Bulgaria</option>
					<option <?php echo ($Country == 'BFA' ? 'selected' : ''); ?> value="BFA">Burkina Faso</option>
					<option <?php echo ($Country == 'BDI' ? 'selected' : ''); ?> value="BDI">Burundi</option>
					<option <?php echo ($Country == 'KHM' ? 'selected' : ''); ?> value="KHM">Cambodia</option>
					<option <?php echo ($Country == 'CMR' ? 'selected' : ''); ?> value="CMR">Cameroon</option>
					<option <?php echo ($Country == 'CAN' ? 'selected' : ''); ?> value="CAN">Canada</option>
					<option <?php echo ($Country == 'CPV' ? 'selected' : ''); ?> value="CPV">Cape Verde</option>
					<option <?php echo ($Country == 'CYM' ? 'selected' : ''); ?> value="CYM">Cayman Islands</option>
					<option <?php echo ($Country == 'CAF' ? 'selected' : ''); ?> value="CAF">Central African Republic</option>
					<option <?php echo ($Country == 'TCD' ? 'selected' : ''); ?> value="TCD">Chad</option>
					<option <?php echo ($Country == 'CHL' ? 'selected' : ''); ?> value="CHL">Chile</option>
					<option <?php echo ($Country == 'CHN' ? 'selected' : ''); ?> value="CHN">China</option>
					<option <?php echo ($Country == 'CXR' ? 'selected' : ''); ?> value="CXR">Christmas Island</option>
					<option <?php echo ($Country == 'CCK' ? 'selected' : ''); ?> value="CCK">Cocos (Keeling) Islands</option>
					<option <?php echo ($Country == 'COL' ? 'selected' : ''); ?> value="COL">Colombia</option>
					<option <?php echo ($Country == 'COM' ? 'selected' : ''); ?> value="COM">Comoros</option>
					<option <?php echo ($Country == 'COG' ? 'selected' : ''); ?> value="COG">Congo</option>
					<option <?php echo ($Country == 'COD' ? 'selected' : ''); ?> value="COD">Congo, the Democratic Republic of the</option>
					<option <?php echo ($Country == 'COK' ? 'selected' : ''); ?> value="COK">Cook Islands</option>
					<option <?php echo ($Country == 'CRI' ? 'selected' : ''); ?> value="CRI">Costa Rica</option>
					<option <?php echo ($Country == 'CIV' ? 'selected' : ''); ?> value="CIV">Côte d'Ivoire</option>
					<option <?php echo ($Country == 'HRV' ? 'selected' : ''); ?> value="HRV">Croatia</option>
					<option <?php echo ($Country == 'CUB' ? 'selected' : ''); ?> value="CUB">Cuba</option>
					<option <?php echo ($Country == 'CUW' ? 'selected' : ''); ?> value="CUW">Curaçao</option>
					<option <?php echo ($Country == 'CYP' ? 'selected' : ''); ?> value="CYP">Cyprus</option>
					<option <?php echo ($Country == 'CZE' ? 'selected' : ''); ?> value="CZE">Czech Republic</option>
					<option <?php echo ($Country == 'DNK' ? 'selected' : ''); ?> value="DNK">Denmark</option>
					<option <?php echo ($Country == 'DJI' ? 'selected' : ''); ?> value="DJI">Djibouti</option>
					<option <?php echo ($Country == 'DMA' ? 'selected' : ''); ?> value="DMA">Dominica</option>
					<option <?php echo ($Country == 'DOM' ? 'selected' : ''); ?> value="DOM">Dominican Republic</option>
					<option <?php echo ($Country == 'ECU' ? 'selected' : ''); ?> value="ECU">Ecuador</option>
					<option <?php echo ($Country == 'EGY' ? 'selected' : ''); ?> value="EGY">Egypt</option>
					<option <?php echo ($Country == 'SLV' ? 'selected' : ''); ?> value="SLV">El Salvador</option>
					<option <?php echo ($Country == 'GNQ' ? 'selected' : ''); ?> value="GNQ">Equatorial Guinea</option>
					<option <?php echo ($Country == 'ERI' ? 'selected' : ''); ?> value="ERI">Eritrea</option>
					<option <?php echo ($Country == 'EST' ? 'selected' : ''); ?> value="EST">Estonia</option>
					<option <?php echo ($Country == 'ETH' ? 'selected' : ''); ?> value="ETH">Ethiopia</option>
					<option <?php echo ($Country == 'FLK' ? 'selected' : ''); ?> value="FLK">Falkland Islands (Malvinas)</option>
					<option <?php echo ($Country == 'FRO' ? 'selected' : ''); ?> value="FRO">Faroe Islands</option>
					<option <?php echo ($Country == 'FJI' ? 'selected' : ''); ?> value="FJI">Fiji</option>
					<option <?php echo ($Country == 'FIN' ? 'selected' : ''); ?> value="FIN">Finland</option>
					<option <?php echo ($Country == 'FRA' ? 'selected' : ''); ?> value="FRA">France</option>
					<option <?php echo ($Country == 'GUF' ? 'selected' : ''); ?> value="GUF">French Guiana</option>
					<option <?php echo ($Country == 'PYF' ? 'selected' : ''); ?> value="PYF">French Polynesia</option>
					<option <?php echo ($Country == 'ATF' ? 'selected' : ''); ?> value="ATF">French Southern Territories</option>
					<option <?php echo ($Country == 'GAB' ? 'selected' : ''); ?> value="GAB">Gabon</option>
					<option <?php echo ($Country == 'GMB' ? 'selected' : ''); ?> value="GMB">Gambia</option>
					<option <?php echo ($Country == 'GEO' ? 'selected' : ''); ?> value="GEO">Georgia</option>
					<option <?php echo ($Country == 'DEU' ? 'selected' : ''); ?> value="DEU">Germany</option>
					<option <?php echo ($Country == 'GHA' ? 'selected' : ''); ?> value="GHA">Ghana</option>
					<option <?php echo ($Country == 'GIB' ? 'selected' : ''); ?> value="GIB">Gibraltar</option>
					<option <?php echo ($Country == 'GRC' ? 'selected' : ''); ?> value="GRC">Greece</option>
					<option <?php echo ($Country == 'GRL' ? 'selected' : ''); ?> value="GRL">Greenland</option>
					<option <?php echo ($Country == 'GRD' ? 'selected' : ''); ?> value="GRD">Grenada</option>
					<option <?php echo ($Country == 'GLP' ? 'selected' : ''); ?> value="GLP">Guadeloupe</option>
					<option <?php echo ($Country == 'GUM' ? 'selected' : ''); ?> value="GUM">Guam</option>
					<option <?php echo ($Country == 'GTM' ? 'selected' : ''); ?> value="GTM">Guatemala</option>
					<option <?php echo ($Country == 'GGY' ? 'selected' : ''); ?> value="GGY">Guernsey</option>
					<option <?php echo ($Country == 'GIN' ? 'selected' : ''); ?> value="GIN">Guinea</option>
					<option <?php echo ($Country == 'GNB' ? 'selected' : ''); ?> value="GNB">Guinea-Bissau</option>
					<option <?php echo ($Country == 'GUY' ? 'selected' : ''); ?> value="GUY">Guyana</option>
					<option <?php echo ($Country == 'HTI' ? 'selected' : ''); ?> value="HTI">Haiti</option>
					<option <?php echo ($Country == 'HMD' ? 'selected' : ''); ?> value="HMD">Heard Island and McDonald Islands</option>
					<option <?php echo ($Country == 'VAT' ? 'selected' : ''); ?> value="VAT">Holy See (Vatican City State)</option>
					<option <?php echo ($Country == 'HND' ? 'selected' : ''); ?> value="HND">Honduras</option>
					<option <?php echo ($Country == 'HKG' ? 'selected' : ''); ?> value="HKG">Hong Kong</option>
					<option <?php echo ($Country == 'HUN' ? 'selected' : ''); ?> value="HUN">Hungary</option>
					<option <?php echo ($Country == 'ISL' ? 'selected' : ''); ?> value="ISL">Iceland</option>
					<option <?php echo ($Country == 'IND' ? 'selected' : ''); ?> value="IND">India</option>
					<option <?php echo ($Country == 'IDN' ? 'selected' : ''); ?> value="IDN">Indonesia</option>
					<option <?php echo ($Country == 'IRN' ? 'selected' : ''); ?> value="IRN">Iran, Islamic Republic of</option>
					<option <?php echo ($Country == 'IRQ' ? 'selected' : ''); ?> value="IRQ">Iraq</option>
					<option <?php echo ($Country == 'IRL' ? 'selected' : ''); ?> value="IRL">Ireland</option>
					<option <?php echo ($Country == 'IMN' ? 'selected' : ''); ?> value="IMN">Isle of Man</option>
					<option <?php echo ($Country == 'ISR' ? 'selected' : ''); ?> value="ISR">Israel</option>
					<option <?php echo ($Country == 'ITA' ? 'selected' : ''); ?> value="ITA">Italy</option>
					<option <?php echo ($Country == 'JAM' ? 'selected' : ''); ?> value="JAM">Jamaica</option>
					<option <?php echo ($Country == 'JPN' ? 'selected' : ''); ?> value="JPN">Japan</option>
					<option <?php echo ($Country == 'JEY' ? 'selected' : ''); ?> value="JEY">Jersey</option>
					<option <?php echo ($Country == 'JOR' ? 'selected' : ''); ?> value="JOR">Jordan</option>
					<option <?php echo ($Country == 'KAZ' ? 'selected' : ''); ?> value="KAZ">Kazakhstan</option>
					<option <?php echo ($Country == 'KEN' ? 'selected' : ''); ?> value="KEN">Kenya</option>
					<option <?php echo ($Country == 'KIR' ? 'selected' : ''); ?> value="KIR">Kiribati</option>
					<option <?php echo ($Country == 'PRK' ? 'selected' : ''); ?> value="PRK">Korea, Democratic People's Republic of</option>
					<option <?php echo ($Country == 'KOR' ? 'selected' : ''); ?> value="KOR">Korea, Republic of</option>
					<option <?php echo ($Country == 'KWT' ? 'selected' : ''); ?> value="KWT">Kuwait</option>
					<option <?php echo ($Country == 'KGZ' ? 'selected' : ''); ?> value="KGZ">Kyrgyzstan</option>
					<option <?php echo ($Country == 'LAO' ? 'selected' : ''); ?> value="LAO">Lao People's Democratic Republic</option>
					<option <?php echo ($Country == 'LVA' ? 'selected' : ''); ?> value="LVA">Latvia</option>
					<option <?php echo ($Country == 'LBN' ? 'selected' : ''); ?> value="LBN">Lebanon</option>
					<option <?php echo ($Country == 'LSO' ? 'selected' : ''); ?> value="LSO">Lesotho</option>
					<option <?php echo ($Country == 'LBR' ? 'selected' : ''); ?> value="LBR">Liberia</option>
					<option <?php echo ($Country == 'LBY' ? 'selected' : ''); ?> value="LBY">Libya</option>
					<option <?php echo ($Country == 'LIE' ? 'selected' : ''); ?> value="LIE">Liechtenstein</option>
					<option <?php echo ($Country == 'LTU' ? 'selected' : ''); ?> value="LTU">Lithuania</option>
					<option <?php echo ($Country == 'LUX' ? 'selected' : ''); ?> value="LUX">Luxembourg</option>
					<option <?php echo ($Country == 'MAC' ? 'selected' : ''); ?> value="MAC">Macao</option>
					<option <?php echo ($Country == 'MKD' ? 'selected' : ''); ?> value="MKD">Macedonia, the former Yugoslav Republic of</option>
					<option <?php echo ($Country == 'MDG' ? 'selected' : ''); ?> value="MDG">Madagascar</option>
					<option <?php echo ($Country == 'MWI' ? 'selected' : ''); ?> value="MWI">Malawi</option>
					<option <?php echo ($Country == 'MYS' ? 'selected' : ''); ?> value="MYS">Malaysia</option>
					<option <?php echo ($Country == 'MDV' ? 'selected' : ''); ?> value="MDV">Maldives</option>
					<option <?php echo ($Country == 'MLI' ? 'selected' : ''); ?> value="MLI">Mali</option>
					<option <?php echo ($Country == 'MLT' ? 'selected' : ''); ?> value="MLT">Malta</option>
					<option <?php echo ($Country == 'MHL' ? 'selected' : ''); ?> value="MHL">Marshall Islands</option>
					<option <?php echo ($Country == 'MTQ' ? 'selected' : ''); ?> value="MTQ">Martinique</option>
					<option <?php echo ($Country == 'MRT' ? 'selected' : ''); ?> value="MRT">Mauritania</option>
					<option <?php echo ($Country == 'MUS' ? 'selected' : ''); ?> value="MUS">Mauritius</option>
					<option <?php echo ($Country == 'MYT' ? 'selected' : ''); ?> value="MYT">Mayotte</option>
					<option <?php echo ($Country == 'MEX' ? 'selected' : ''); ?> value="MEX">Mexico</option>
					<option <?php echo ($Country == 'FSM' ? 'selected' : ''); ?> value="FSM">Micronesia, Federated States of</option>
					<option <?php echo ($Country == 'MDA' ? 'selected' : ''); ?> value="MDA">Moldova, Republic of</option>
					<option <?php echo ($Country == 'MCO' ? 'selected' : ''); ?> value="MCO">Monaco</option>
					<option <?php echo ($Country == 'MNG' ? 'selected' : ''); ?> value="MNG">Mongolia</option>
					<option <?php echo ($Country == 'MNE' ? 'selected' : ''); ?> value="MNE">Montenegro</option>
					<option <?php echo ($Country == 'MSR' ? 'selected' : ''); ?> value="MSR">Montserrat</option>
					<option <?php echo ($Country == 'MAR' ? 'selected' : ''); ?> value="MAR">Morocco</option>
					<option <?php echo ($Country == 'MOZ' ? 'selected' : ''); ?> value="MOZ">Mozambique</option>
					<option <?php echo ($Country == 'MMR' ? 'selected' : ''); ?> value="MMR">Myanmar</option>
					<option <?php echo ($Country == 'NAM' ? 'selected' : ''); ?> value="NAM">Namibia</option>
					<option <?php echo ($Country == 'NRU' ? 'selected' : ''); ?> value="NRU">Nauru</option>
					<option <?php echo ($Country == 'NPL' ? 'selected' : ''); ?> value="NPL">Nepal</option>
					<option <?php echo ($Country == 'NLD' ? 'selected' : ''); ?> value="NLD">Netherlands</option>
					<option <?php echo ($Country == 'NCL' ? 'selected' : ''); ?> value="NCL">New Caledonia</option>
					<option <?php echo ($Country == 'NZL' ? 'selected' : ''); ?> value="NZL">New Zealand</option>
					<option <?php echo ($Country == 'NIC' ? 'selected' : ''); ?> value="NIC">Nicaragua</option>
					<option <?php echo ($Country == 'NER' ? 'selected' : ''); ?> value="NER">Niger</option>
					<option <?php echo ($Country == 'NGA' ? 'selected' : ''); ?> value="NGA">Nigeria</option>
					<option <?php echo ($Country == 'NIU' ? 'selected' : ''); ?> value="NIU">Niue</option>
					<option <?php echo ($Country == 'NFK' ? 'selected' : ''); ?> value="NFK">Norfolk Island</option>
					<option <?php echo ($Country == 'MNP' ? 'selected' : ''); ?> value="MNP">Northern Mariana Islands</option>
					<option <?php echo ($Country == 'NOR' ? 'selected' : ''); ?> value="NOR">Norway</option>
					<option <?php echo ($Country == 'OMN' ? 'selected' : ''); ?> value="OMN">Oman</option>
					<option <?php echo ($Country == 'PAK' ? 'selected' : ''); ?> value="PAK">Pakistan</option>
					<option <?php echo ($Country == 'PLW' ? 'selected' : ''); ?> value="PLW">Palau</option>
					<option <?php echo ($Country == 'PSE' ? 'selected' : ''); ?> value="PSE">Palestinian Territory, Occupied</option>
					<option <?php echo ($Country == 'PAN' ? 'selected' : ''); ?> value="PAN">Panama</option>
					<option <?php echo ($Country == 'PNG' ? 'selected' : ''); ?> value="PNG">Papua New Guinea</option>
					<option <?php echo ($Country == 'PRY' ? 'selected' : ''); ?> value="PRY">Paraguay</option>
					<option <?php echo ($Country == 'PER' ? 'selected' : ''); ?> value="PER">Peru</option>
					<option <?php echo ($Country == 'PHL' ? 'selected' : ''); ?> value="PHL">Philippines</option>
					<option <?php echo ($Country == 'PCN' ? 'selected' : ''); ?> value="PCN">Pitcairn</option>
					<option <?php echo ($Country == 'POL' ? 'selected' : ''); ?> value="POL">Poland</option>
					<option <?php echo ($Country == 'PRT' ? 'selected' : ''); ?> value="PRT">Portugal</option>
					<option <?php echo ($Country == 'PRI' ? 'selected' : ''); ?> value="PRI">Puerto Rico</option>
					<option <?php echo ($Country == 'QAT' ? 'selected' : ''); ?> value="QAT">Qatar</option>
					<option <?php echo ($Country == 'REU' ? 'selected' : ''); ?> value="REU">Réunion</option>
					<option <?php echo ($Country == 'ROU' ? 'selected' : ''); ?> value="ROU">Romania</option>
					<option <?php echo ($Country == 'RUS' ? 'selected' : ''); ?> value="RUS">Russian Federation</option>
					<option <?php echo ($Country == 'RWA' ? 'selected' : ''); ?> value="RWA">Rwanda</option>
					<option <?php echo ($Country == 'BLM' ? 'selected' : ''); ?> value="BLM">Saint Barthélemy</option>
					<option <?php echo ($Country == 'SHN' ? 'selected' : ''); ?> value="SHN">Saint Helena, Ascension and Tristan da Cunha</option>
					<option <?php echo ($Country == 'KNA' ? 'selected' : ''); ?> value="KNA">Saint Kitts and Nevis</option>
					<option <?php echo ($Country == 'LCA' ? 'selected' : ''); ?> value="LCA">Saint Lucia</option>
					<option <?php echo ($Country == 'MAF' ? 'selected' : ''); ?> value="MAF">Saint Martin (French part)</option>
					<option <?php echo ($Country == 'SPM' ? 'selected' : ''); ?> value="SPM">Saint Pierre and Miquelon</option>
					<option <?php echo ($Country == 'VCT' ? 'selected' : ''); ?> value="VCT">Saint Vincent and the Grenadines</option>
					<option <?php echo ($Country == 'WSM' ? 'selected' : ''); ?> value="WSM">Samoa</option>
					<option <?php echo ($Country == 'SMR' ? 'selected' : ''); ?> value="SMR">San Marino</option>
					<option <?php echo ($Country == 'STP' ? 'selected' : ''); ?> value="STP">Sao Tome and Principe</option>
					<option <?php echo ($Country == 'SAU' ? 'selected' : ''); ?> value="SAU">Saudi Arabia</option>
					<option <?php echo ($Country == 'SEN' ? 'selected' : ''); ?> value="SEN">Senegal</option>
					<option <?php echo ($Country == 'SRB' ? 'selected' : ''); ?> value="SRB">Serbia</option>
					<option <?php echo ($Country == 'SYC' ? 'selected' : ''); ?> value="SYC">Seychelles</option>
					<option <?php echo ($Country == 'SLE' ? 'selected' : ''); ?> value="SLE">Sierra Leone</option>
					<option <?php echo ($Country == 'SGP' ? 'selected' : ''); ?> value="SGP">Singapore</option>
					<option <?php echo ($Country == 'SXM' ? 'selected' : ''); ?> value="SXM">Sint Maarten (Dutch part)</option>
					<option <?php echo ($Country == 'SVK' ? 'selected' : ''); ?> value="SVK">Slovakia</option>
					<option <?php echo ($Country == 'SVN' ? 'selected' : ''); ?> value="SVN">Slovenia</option>
					<option <?php echo ($Country == 'SLB' ? 'selected' : ''); ?> value="SLB">Solomon Islands</option>
					<option <?php echo ($Country == 'SOM' ? 'selected' : ''); ?> value="SOM">Somalia</option>
					<option <?php echo ($Country == 'ZAF' ? 'selected' : ''); ?> value="ZAF">South Africa</option>
					<option <?php echo ($Country == 'SGS' ? 'selected' : ''); ?> value="SGS">South Georgia and the South Sandwich Islands</option>
					<option <?php echo ($Country == 'SSD' ? 'selected' : ''); ?> value="SSD">South Sudan</option>
					<option <?php echo ($Country == 'ESP' ? 'selected' : ''); ?> value="ESP">Spain</option>
					<option <?php echo ($Country == 'LKA' ? 'selected' : ''); ?> value="LKA">Sri Lanka</option>
					<option <?php echo ($Country == 'SDN' ? 'selected' : ''); ?> value="SDN">Sudan</option>
					<option <?php echo ($Country == 'SUR' ? 'selected' : ''); ?> value="SUR">Suriname</option>
					<option <?php echo ($Country == 'SJM' ? 'selected' : ''); ?> value="SJM">Svalbard and Jan Mayen</option>
					<option <?php echo ($Country == 'SWZ' ? 'selected' : ''); ?> value="SWZ">Swaziland</option>
					<option <?php echo ($Country == 'SWE' ? 'selected' : ''); ?> value="SWE">Sweden</option>
					<option <?php echo ($Country == 'CHE' ? 'selected' : ''); ?> value="CHE">Switzerland</option>
					<option <?php echo ($Country == 'SYR' ? 'selected' : ''); ?> value="SYR">Syrian Arab Republic</option>
					<option <?php echo ($Country == 'TWN' ? 'selected' : ''); ?> value="TWN">Taiwan, Province of China</option>
					<option <?php echo ($Country == 'TJK' ? 'selected' : ''); ?> value="TJK">Tajikistan</option>
					<option <?php echo ($Country == 'TZA' ? 'selected' : ''); ?> value="TZA">Tanzania, United Republic of</option>
					<option <?php echo ($Country == 'THA' ? 'selected' : ''); ?> value="THA">Thailand</option>
					<option <?php echo ($Country == 'TLS' ? 'selected' : ''); ?> value="TLS">Timor-Leste</option>
					<option <?php echo ($Country == 'TGO' ? 'selected' : ''); ?> value="TGO">Togo</option>
					<option <?php echo ($Country == 'TKL' ? 'selected' : ''); ?> value="TKL">Tokelau</option>
					<option <?php echo ($Country == 'TON' ? 'selected' : ''); ?> value="TON">Tonga</option>
					<option <?php echo ($Country == 'TTO' ? 'selected' : ''); ?> value="TTO">Trinidad and Tobago</option>
					<option <?php echo ($Country == 'TUN' ? 'selected' : ''); ?> value="TUN">Tunisia</option>
					<option <?php echo ($Country == 'TUR' ? 'selected' : ''); ?> value="TUR">Turkey</option>
					<option <?php echo ($Country == 'TKM' ? 'selected' : ''); ?> value="TKM">Turkmenistan</option>
					<option <?php echo ($Country == 'TCA' ? 'selected' : ''); ?> value="TCA">Turks and Caicos Islands</option>
					<option <?php echo ($Country == 'TUV' ? 'selected' : ''); ?> value="TUV">Tuvalu</option>
					<option <?php echo ($Country == 'UGA' ? 'selected' : ''); ?> value="UGA">Uganda</option>
					<option <?php echo ($Country == 'UKR' ? 'selected' : ''); ?> value="UKR">Ukraine</option>
					<option <?php echo ($Country == 'ARE' ? 'selected' : ''); ?> value="ARE">United Arab Emirates</option>
					<option <?php echo ($Country == 'GBR' ? 'selected' : ''); ?> value="GBR">United Kingdom</option>
					<option <?php echo ($Country == 'USA' ? 'selected' : ''); ?> value="USA">United States</option>
					<option <?php echo ($Country == 'UMI' ? 'selected' : ''); ?> value="UMI">United States Minor Outlying Islands</option>
					<option <?php echo ($Country == 'URY' ? 'selected' : ''); ?> value="URY">Uruguay</option>
					<option <?php echo ($Country == 'UZB' ? 'selected' : ''); ?> value="UZB">Uzbekistan</option>
					<option <?php echo ($Country == 'VUT' ? 'selected' : ''); ?> value="VUT">Vanuatu</option>
					<option <?php echo ($Country == 'VEN' ? 'selected' : ''); ?> value="VEN">Venezuela, Bolivarian Republic of</option>
					<option <?php echo ($Country == 'VNM' ? 'selected' : ''); ?> value="VNM">Viet Nam</option>
					<option <?php echo ($Country == 'VGB' ? 'selected' : ''); ?> value="VGB">Virgin Islands, British</option>
					<option <?php echo ($Country == 'VIR' ? 'selected' : ''); ?> value="VIR">Virgin Islands, U.S.</option>
					<option <?php echo ($Country == 'WLF' ? 'selected' : ''); ?> value="WLF">Wallis and Futuna</option>
					<option <?php echo ($Country == 'ESH' ? 'selected' : ''); ?> value="ESH">Western Sahara</option>
					<option <?php echo ($Country == 'YEM' ? 'selected' : ''); ?> value="YEM">Yemen</option>
					<option <?php echo ($Country == 'ZMB' ? 'selected' : ''); ?> value="ZMB">Zambia</option>
					<option <?php echo ($Country == 'ZWE' ? 'selected' : ''); ?> value="ZWE">Zimbabwe</option>
				  </select>
                </div>			
                
                <input type="hidden" name="action" value="submit_form" />
              </div>
              <!-- /.box-body -->
            
            </div>
            <!-- /.box -->
            <!-- Form Element sizes -->
          </div>
        </div>
		
		<div class="col-md-6">
          <div class="box">
          
            <!-- general form elements -->
            <div style="padding:15px;" class="box-primary">
            
              <!-- form start -->
              <div class="box-body">
               
			   <div class="form-group">
                  <label id="labelimp" for="Percentage">Percentage: </label>
                  <?php 
				echo '<input type="number" min="0" max="100" id="Percentage" name="Percentage" class="form-control"  value="'.$Percentage.'" />';
				?>
                </div>
              <!-- /.box-body -->
            
            </div>
            <!-- /.box -->
            <!-- Form Element sizes -->
          </div>
        </div>
		
      </section>
    </form>
    <!-- /.content -->
  </aside>
  <!-- /.right-side -->
</div>
<!-- ./wrapper -->
<?php include_once("Footer.php"); ?>
<!-- add new calendar event modal -->
<!-- jQuery 2.0.2 -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<!-- jQuery UI 1.10.3 -->
<script src="js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="js/AdminLTE/app.js" type="text/javascript"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="js/AdminLTE/dashboard.js" type="text/javascript"></script>
</body>
</html>
