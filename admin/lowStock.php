<?php include("Common.php"); ?>
<?php include("CheckAdminLogin.php");?>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Products Low in Stock</title>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<!-- bootstrap 3.0.2 -->
<!-- jQuery 2.0.2 -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="js/ui.datepicker.js" type="text/javascript"></script>
 <!-- DataTables -->
 <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css">
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- font Awesome -->
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Ionicons -->
<link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
<link href="css/datepicker.css" rel="stylesheet" type="text/css" />
<!-- DATA TABLES -->
<!-- Theme style -->
<link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
<script language="javascript">
  $(document).ready(function () {       
    $(".checkUncheckAll").click(function () {
      $(".chkIds").prop("checked", $(this).prop("checked"));      
    });
  });
  var counter = 0;
  
  
  function doDelete()
  {
    if($(".chkIds").is(":checked"))
    {
      if(confirm("Are you sure to delete."))
      {
        $("#action").val("delete");
        $("#frmPages").submit();
      }
    }
    else
      alert("Please select category to delete");
  }
  function doUpdate()
  {
    $("#action").val('update');
    $("#frmPages").submit();
  }
  
</script>
</head>
<body class="skin-blue">
<!-- header logo: style can be found in header.less -->
<?php
  include_once("Header.php");
?>
<div class="wrapper row-offcanvas row-offcanvas-left">
  <!-- Left side column. contains the logo and sidebar -->
  <?php
    include_once("Sidebar.php");
    ?>
  <!-- Right side column. Contains the navbar and content of the page -->
  <aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> Products Low in Stock <small></small> </h1>
      <ol class="breadcrumb">
        <li><a href="Dashboard.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Products Low in Stock</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Products Low in Stock</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
             
                <table id="example2" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                     
                      <th>Product Name</th>
                       <th>Quantity</th>
                    </tr>
                  </thead>
          
                  <tbody>
                    <?php 

$sql = "SELECT ID, ProductName, Quantity FROM products WHERE Quantity <= 5 AND Quantity > 0";
$query = mysql_query($sql);
while($row = mysql_fetch_array($query)){ ?>

<tr>
   <td><?php echo $row['ProductName']; ?></td>
  <td><?php echo $row['Quantity']; ?></td>
</tr>


<?php } ?>
                  </tbody>
                </table>
              </form>
            </div>
            <br>
           
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
    </section>
    <!-- /.content -->
  </aside>
  <!-- /.right-side -->
</div>
<div class="no-print" style="width:100%;
  height:50px;
  background-color:#ed1c24;
  text-align:center;
  vertical-align:center;
  padding-top:15px;
  color:white;"> Copyright © <?php echo date("Y") ?> AndyBird</div>
<!-- ./wrapper -->
<!-- Bootstrap -->
<!-- <script src="js/bootstrap.min.js" type="text/javascript"></script> -->
<!-- AdminLTE App -->
<!-- <script src="js/AdminLTE/app.js" type="text/javascript"></script> -->
<!-- page script -->


<!-- jQuery 2.2.3 -->

<!-- Bootstrap 3.3.6 -->
<script src="js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="new/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="new/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="new/plugins/datatables/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="new/plugins/datatables/buttons.html5.min.js">
  </script>



<!-- SlimScroll -->
<script src="new/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="new/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="new/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="new/dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $("#example2").DataTable({
      dom: 'Bfrtip',
'iDisplayLength': 100,
     buttons: [
             'csv',
        ],
          "lengthChange": false,
    });
  });
</script>
</body>
</html>
