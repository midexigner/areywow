<?php 
require_once 'admin/Common.php'; 
require_once 'functions.php'; 
require_once 'include/head.php'; ?>

	<?php require_once 'include/headerpartial.php'; ?>
	<!-- // being pages section -->
 <div id="content" class="site-content" tabindex="-1">
	<div class="container">

    	<nav class="woocommerce-breadcrumb"><a href="<?php echo BASE_URL; ?>">Home</a><span class="delimiter"><i class="fa fa-angle-right"></i></span>Error 404</nav>
        <div id="primary" class="content-area">
    		<main id="main" class="site-main">
                <div class="center-block">
                    <div class="info-404">
                        <div class="text-xs-center inner-bottom-xs">
                            <h2 class="display-3">404!</h2>
                            <p class="lead">Nothing was found at this location. Try searching, or check out the links below.</p>
                            <hr class="m-y-2">
                            <div class="sub-form-row inner-bottom-xs">
                                <div class="widget woocommerce widget_product_search">
                                    <form class="woocommerce-product-search"  method="get" action="<?php echo BASE_URL; ?>/searchproducts.php?search=yes" role="search">
                                        <label for="woocommerce-product-search-field" class="screen-reader-text">Search for:</label>
                                        <input type="search" title="Search for:" name="keyword" value="" placeholder="Search Products…" class="search-field" id="woocommerce-product-search-field">
                                        <input type="submit" value="Search">
                                        <input type="hidden" value="product" name="post_type">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    </div>
</div>


 <?php require_once 'include/footer.php'; ?>
 <?php require_once 'include/foot.php'; ?>
