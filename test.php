function HumanSize($Bytes)
{
  $Type=array("", "K", "M", "G", "T", "P", "E", "Z", "Y");
  $Index=0;
  while($Bytes>=1024)
  {
    $Bytes/=1024;
    $Index++;
  }
  return("".round($Bytes)." ".$Type[$Index]);
}
// Or, an option that also works before PHP 5.3
//echo php_uname('n'); // may output e.g,: sandie



// $df contains the number of bytes available on "/"
$df = disk_free_space("/");

// On Windows:
$df_c = disk_free_space("C:");
$df_d = disk_free_space("D:");
$df_d = disk_free_space("F:");
$df_h = disk_free_space("H:");
 
 echo HumanSize($df_h);
 echo '<br/>';
echo 'Operating system :'.php_uname('s');
echo '<br/>';
echo 'Host name :'.php_uname('n');
echo '<br/>';
echo 'Release name :'.php_uname('r');
echo '<br/>';
echo 'Version information :'.php_uname('v');
echo '<br/>';
echo 'Machine type :'.php_uname('m');
echo '<br/>';